/* runner.c

   run a process from a fork

   Copyright (C) 2017, Darryl West

   Licensed under Apache 2.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[], char *envp[])
{
	pid_t ppid = getpid();
	pid_t pid = fork();
    const char *command = "/usr/local/bin/node";

	if (pid == -1) {
        printf("ERROR forking...\n");
	} else if (pid > 0) {
        printf("ppid: %d, pid: %d\n", ppid, pid);
        int status;
        waitpid(pid, &status, 0);
    } else {
        int err = execve(command, argv, envp);
        printf("error %d\n", err);
    }

    return 0;
}

