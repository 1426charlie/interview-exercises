#!/usr/bin/env node

// dpw@alameda.local
// 2015.03.04
'use strict';

const fs = require('fs'),
    spawn = require('child_process').spawn,
    clearScreen = '[H[2J';

let files = new Set();
let tid = null;

const run = function() {
    process.stdout.write( clearScreen ); 
    console.log('Changed files: ', files);

    let list = Array.from(files); // tests.map(t => t);

    const loop = function() {
        let file = list.shift();
        if (file) {
		    file = file.replace(/.cpp$/, '');
            console.log(file);
            let runner = spawn( 'make', [ file ] );

            runner.stdout.on('data', function( data ) {
                process.stdout.write( data );
            });

            runner.stderr.on('data', function( data ) {
                process.stdout.write( data );
            });

            runner.on('close', function(code) {
                tid = null;
                files.clear();
                loop();
            });
        } else {
            console.log('tests complete...')
        }
    }

    loop();
};

const changeHandler = function(event, filename) {
    // console.log( 'raw file change: ', filename);
    if ( filename.endsWith('.cpp') ) {
        files.add( filename );

        if (!tid) {
            tid = setTimeout(function() {
                run();
            }, 250);
        }
    }
};

// run();
fs.watch( './', { recursive:false }, changeHandler );

