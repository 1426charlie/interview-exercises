#include <iostream>
#include <thread>

void threadFunc()
{
    std::cout << "hello from thread func!\n";
}

void threadTwo()
{
    std::cout << "hello from thread two!\n";
}

int main()
{
    std::thread tf(&threadFunc);
    std::thread tt(&threadTwo);

    std::cout << "hi from main!\n";

    tf.join();
    tt.join();

    std::cout << "i'm out...\n";

    return 0;
}

