#include <cstdio>
#include <cstdlib>

double factorial(unsigned int n) {
    double fact = 1.0;

    if (n > 1) {
        for (unsigned int i = 2; i <= n; i++) {
            fact = fact * i;
        }
    }

    return fact;
}

int main() {
    unsigned int n = 20;
    double fact = factorial(n);
    printf("factorial of %d is %0.f\n", n, fact);
}

