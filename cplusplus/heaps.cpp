#include <functional>
#include <queue>
#include <vector>
#include <iostream>

priority_queue<int, vector<int>, less<int> > lower = priority_queue<int, vector<int>, less<int> >();
priority_queue<int, vector<int>, greater<int> > higher = priority_queue<int, vector<int>, greater<int> >();

void add(int i){ 
    if(lower.empty())
        lower.push(i);
    else{
        if(lower.size() > higher.size()){
            if(lower.top() > i){
                higher.push(lower.top());
                lower.pop();
                lower.push(i);
            }
            else
                higher.push(i);
        }
        else{
            if(higher.top() >= i)
                lower.push(i);
            else{
                lower.push(higher.top());
                higher.pop();
                higher.push(i);
            }
        }
    }
}

double find(){ 
    int n = lower.size() + higher.size();
    return (n % 2 == 0) ? (higher.top() + lower.top())/2.0 : (double)(lower.top());
    
}
