#!/usr/bin/env swift

// dpw@oakland.local
// 2016.02.25

// Problem Definition: Write a program that does the following for all the numbers between 1..100 (inclusive)
// 1) print <number> followed by the string "Fizz" if the number is a multiple of 3; 
// 2) prints <number> followed by the string "Buzz" if the number is a multiple of 5; 
// 3) prints <number> followed by the the string "FizzBuzz" if the number is a multiple of 3 and a multiple of 5; 
// 4) prints <number> if none of the other rules apply.

// how do you know that it works?

// tests

let expects = ["1", "2", "3Fizz", "4", "5Buzz", "6Fizz", "7", "8", "9Fizz", "10Buzz", "11", "12Fizz", "13", "14", "15FizzBuzz", "16", "17", "18Fizz", "19", "20Buzz", "21Fizz", "22", "23", "24Fizz", "25Buzz", "26", "27Fizz", "28", "29", "30FizzBuzz", "31", "32", "33Fizz", "34", "35Buzz", "36Fizz", "37", "38", "39Fizz", "40Buzz", "41", "42Fizz", "43", "44", "45FizzBuzz", "46", "47", "48Fizz", "49", "50Buzz", "51Fizz", "52", "53", "54Fizz", "55Buzz", "56", "57Fizz", "58", "59", "60FizzBuzz", "61", "62", "63Fizz", "64", "65Buzz", "66Fizz", "67", "68", "69Fizz", "70Buzz", "71", "72Fizz", "73", "74", "75FizzBuzz", "76", "77", "78Fizz", "79", "80Buzz", "81Fizz", "82", "83", "84Fizz", "85Buzz", "86", "87Fizz", "88", "89", "90FizzBuzz", "91", "92", "93Fizz", "94", "95Buzz", "96Fizz", "97", "98", "99Fizz", "100Buzz"]

func testLines(lines:[String]) -> Bool {
    if lines.count != expects.count {
        print("FAIL: lines length is \( lines.count ),  should be \( expects.count )")
        return false
    }

    for idx in 0..<expects.count {
        if lines[idx] != expects[idx] {
            print("FAIL: line \( idx + 1 ) is \( lines[idx] ), but should should be \( expects[ idx ] )")
            return false
        }

        print("line \( idx + 1 ) is \( lines[idx] ), ok")
    }

    print("PASS")

    return true
}

// implement here...

// solution

protocol FizzBuzzer {
    func showLines(lines:[String])
    func calc(from: Int, to: Int) -> [String]
}

struct FizzBuzz : FizzBuzzer {

    func testFizz(value:Int) -> String? {
        if value % 3 == 0 {
            return "Fizz"
        }

        return nil
    }

    func testBuzz(value:Int) -> String? {
        if value % 5 == 0 {
            return "Buzz"
        }

        return nil
    }

    func testInput(value:Int, fnList:[(Int) -> String?]) -> String {
        var result = [ "\( value )" ]

        for fn in fnList {
            if let s = fn( value ) {
                result.append( s )
            }
        }

        return result.joined(separator:"")
        // return result.joined(separator:"")
    }

    func calc(from:Int, to:Int) -> [String] {
        let tests = [ testFizz, testBuzz ]

        let lines:[String] = (from...to).map { testInput( value:$0, fnList:tests ) }

        return lines
    }

    func showLine(line:String) {
        print( line );
    }

    func showLines(lines:[String]) {
        lines.forEach( showLine )
    }
}

let fizzBuzz = FizzBuzz()
let lines = fizzBuzz.calc(from:1, to:100)

if testLines( lines:lines ) {
    fizzBuzz.showLines( lines:lines  )
}

