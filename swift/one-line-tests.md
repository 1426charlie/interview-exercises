# One Line Swift Questions

_Playground is [here](https://swiftlang.ng.bluemix.net)..._

## Generics & Tuples

### Swap

Write a function to swap two variables of the same type.  The test script:

```
let swapped = swapit( x:5, y:6 )
print( swapped )
print( swapped.0 == 6 && swapped.1 == 5 )

// or

let (x, y) = swapit( x:5, y:6 )
print( "x=\( x ), y=\( y ), pass=\( x == 6 && y == 5 )" )
```

The solution

```
func swapit<T>(x: T, y: T) -> (T, T) {
	return (y, x)
}
```

### Words Search

Write a function to return determine if a string of text contains a word from a list. E.g.,

```
import Foundation

let words = ["Swift", "iOS", "cocoa", "OSX", "tvOS" ]
let str = NSString(string: "This is an example sentence talking about Swift and other things.")

let valid = !words.filter({ str.containsString( $0 )}).isEmpty
print( valid )

// - or -
print( words.contains( str.containsString ) )
```

## General Calculations

### factorial

This easiest possible way to calculate factorial for n:

```
let n = 8
let result = (1...n).reduce( 1, combine: * )

print("factorial( \( n ) ) = \( result )")
```

### mx+b

Given an input of y coordinates (CGFloat) between 0 and 680 (actually 35 to 630) create a method that produces output of 5 to 1 (int).  The test script:

```
func test(x: Double, _ y: Int) -> (Double, Int, Int, Bool) {
    let v = calc(Double( x ))

    print( "x=\( x ) expect \( y ) got \( v ) ok \( y == v )" )
    return (x, y, v, y == v)
}

let list = [ // x,expects
    ( 35.0, 5 ), ( 130.0, 5 ), ( 140.0, 4 ), ( 150.0, 4 ), ( 238.0, 4 ), 
    ( 270.0, 4 ), ( 362.0, 3 ), ( 395.0, 3 ), ( 490.0, 2 ), ( 500.0, 2 ), 
    ( 522.0, 2 ), ( 606.0, 1 ), ( 630.0, 1 )
]

for (x, y) in list {
    test( x, y )
}
```

The solution:

```
// x = 0 -> y = 5
// x = 680 -> y = 1

// b = y intercept (m0 + b = 5) -> (b = 5)
// m = rise / run, dy / dx; dy = 0 - 5, dx = 680 - 0 ; -5 / 680
let slope:Double = -5 / 680
let yintercept:Double = 5.0

func calc(x: Double) -> Int {
	return Int( ceil( slope * x + yintercept ))
}

```

### Find Min / Max

Write a function to find and return the minimum and maximum amounts from a list of integers:

```
let list = [ 10, -22, 753, 55, 137, -1, -279, 1024, 77 ]
let min = list.sort().first
let max = list.sort().last

print( min, max )

// - or -

print( list.minElement(), list.maxElement() )
```

## Closures

Write a closure that returns a function to increment an integer by a set amount.  The Test script:

```
let inc10 = makeIncrementer(forIncrement: 10)

for idx in 0..<10 {
	let value = inc10()
	print("idx=\( idx ), value=\( value ), pass=\( value == idx * 10 + 10 )")
}
```

Solution:

```
func makeIncrementer(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementer() -> Int {
        runningTotal += amount
        return runningTotal
    }
    
    return incrementer
}

```

## Mutability

### Simple Immutatability

After running this code, what is the of the print statement?

```
struct Person {
    var name = "Foo"
}

var p1 = Person()
var p2 = p1
p2.name = "Bar"

print(p1.name == p2.name)
```

What happens when "struct" is changed to "class"?


## Optionals

### Guard

Improve this code:

```
func divide(dividend: Double?, by divisor: Double?) -> Double? {
  if dividend == .None {
    return .None
  }
 
  if divisor == .None {
    return .None
  }
 
  if divisor == 0 {
    return .None
  }
 
  return dividend! / divisor!
}
```

Solution:

```
func divide(dividend: Double?, by divisor: Double?) -> Double? {
	guard let dd = dividend, dv = divisor where divisor != 0 else {
		return nil
	}
	return dd / dv
}
```


- - -
###### Version 2016.09.29 | darryl.west@bluelasso.com
