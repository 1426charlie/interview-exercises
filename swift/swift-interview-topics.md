# Swift 2.0 Interview Topic/Questions

## Basic Language Knowledge

### Variables & Constants

* how to define a read only variable
* how to define a constant
* optionals & optional chaining
* property observers: willSet & didSet

### Collections

* instantiate an array of strings
* instantiate a dictionary/map of String:Int
* iterate over an array
* iterate over an array with an index (using enumerate)
* sort an array with sort or sorted
* demonstrate map, filter, reduce methods

### Strings

#### The basics

* a string is an ordered collection of unicode characters
* not indexable like objc / c strings
* init with format (old c style)
* characters.count for length
* uppercaseString, lowercaseString, capitalizedString, etc
* join
* concat with + or append
* splice
* isEmpty

#### String Iterators: Advance

```
let s = "this is my string"
for char in s {
	print( char )
}
```

#### Initializers

* let chars = Array("abcde") // == [ "a","b","c","d","e" ]
* let str = String( [ "a", "b", "c" ] )  // == "abc"
* let dbl:Double = 2.408
* let value = "\( dbl )" or String(format: "%d", dbl)

### Functions & Closures

#### Functions

* difference between function and method
* are functions closures?
* inner functions
* ARC object
* returning tuples

#### Closures

* simple functional syntax (params) -> (output)
* chaining
* ARC objects
* how to avoid memory cycle/leaks? (weak, unowned)

### Pattern Matching & Switches

* what types can be used
* when to use default
* where clauses
* patterns

### Classes, Structures and Enums

* difference between class and struct
* use of enum
* what are protocols?
* Any, AnyObject, AnyClass

#### Classes & Structs

* initializers
* properties including lazy properties
* support Subscripts
* implement protocol
* can be extended
* re-assigning functions or closures

_Classes can also..._

* inherit from other classes
* be type cast
* define deinit
* allow reference counting

Structs are alway value objects (copied); Classes are reference objects on the heap.

### Error Handling

* guard statements
* do / try / catch
* throws
* try!

### Extensions

* How do you get NSDate to do this `let dt = NSDate().tomorrow`

* Printable
* Subscript

### Introspection / Reflection

* mirror = reflect( obj ) // structs, classes, enums
* returns a mirror count of properties
* properties include propertyName, childMirror.valueType, childMirror.value
* could be used to create a key/value map of any object

### Protocols

* protocol based design
* work with classes, structs, enums
* better to use than extending a base class
* protocol extensions
* enable testing

### Generics

_Protocols + Generics enable the coder to safely extend the language capabilities..._

```
func swap<T>(inout x: T, inout y: T) {
	let tmp = x
	x = y
	y = tmp
}

var dx = 34
var dy = 22

swap(&dx, &dy)

println("dx: \( dx ), dy: \( dy )") // shows dx: 22, dy: 34
```
* protocols
* memoization

### Operators

#### Miscellaneous

* print
* \_\_FILE\_\_, \_\_FUNCTION\_\_, \_\_LINE\_\_
* assert
* try / catch

## XCode/iOS Framework Knowledge

### MVC

### Test Framework

_[find a primer here](http://nshipster.com/xctestcase/)_

* how to implement a test?
* how to run a specific test
* how to run a full suite of tests
* how to run from command line
* create a test for an asynchronous operation (XCTestExpectation, fulfill)

### Inter-application Communication Options

* Protocol/Delegates for direct calls
* KVO / Property observers
* NSNotificationCenter
* Callbacks (like NSOperation)

### Persistance

* SQLite
* Filesystem
* Core Data
* User Defaults

### UIView, CALayer, Etc

* relationship between CALayer and UIView

### I18N / Bundles

## Exam Project

basic login dialog: https://bitbucket.org/roundpeg/swift-interview/overview

- - -
darryl.west@roundpeg.com | Version 2015-07-05.11




