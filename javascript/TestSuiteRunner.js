#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.22

const RED = "\033[31m";
const GREEN = "\033[32m";
const BLACK = "\033[39m"; // default...

// create with stats including name, list of test functions, error & run count 
// and there you go.  just do new TestSuiteRunner(stats) and all is good.
const TestSuiteRunner = function(stats) {
    'use strict';
    const suite = this;
    
    let tests = stats.tests;

    const loop = function(err) {
        if (err) {
            stats.errorCount++;
            console.log(RED, err.message, BLACK);
        }

        const test = tests.shift();
        if (!test) {
            let color = stats.errorCount === 0 ? GREEN : RED;
            let message = `${color}Total of ${stats.testCount} tests run for ${stats.name}, ${stats.errorCount} errors...`;
            if (stats.errorCount > 0) {
                message += `\n\n${stats.name}: ERROR COUNT: ${stats.errorCount}\n`;
            }
            console.log(message, BLACK);
            return;
        }

        stats.testCount++;
        console.log(`${BLACK}${stats.testCount}) ${test.name}()...`);
        test(loop);
    };

    console.log(`Running ${stats.name} tests...`);
    loop();
};

module.exports = TestSuiteRunner;

