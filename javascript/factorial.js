#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.28
'use strict';

// implemented with reduce and Array.from

const factorial = function(n) {
    return Array.from({length:n}, (v, k) => k + 1).reduce((a,b) => a*b, 1);
};

for (let i = 1; i <= 20; i++) {
    console.log(i, factorial(i));
}
// console.log(factorial(20));
