#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.23
'use strict';

const assert = require('assert');
const TestSuiteRunner = require('./TestSuiteRunner');
const TrianglePath = require('./TrianglePath');

const createSimpleTriangle = function() {
    // best path: 4 + 9 + 6 + 8 = 27
    return [ [ 4 ], [ 6, 9 ], [ 2, 7, 6 ], [ 4, 2, 1, 8 ] ];
};

const createRosettaSimpleReference = function() {
    // path is: 55 + 94 + 95 + 77 = 321
    return [ [ 55 ], [ 94, 48 ], [ 95, 30, 96 ], [ 77, 71, 26, 67 ] ];
}

const createRosettaReference = function() {
    const data = `55
        94 48
        95 30 96
        77 71 26 67
        97 13 76 38 45
        07 36 79 16 37 68
        48 07 09 18 70 26 06
        18 72 79 46 59 79 29 90
        20 76 87 11 32 07 07 49 18
        27 83 58 35 71 11 25 57 29 85
        14 64 36 96 27 11 58 56 92 18 55
        02 90 03 60 48 49 41 46 33 36 47 23
        92 50 48 02 36 59 42 79 72 20 82 77 42
        56 78 38 80 39 75 02 71 66 66 01 03 55 72
        44 25 67 84 71 67 11 61 40 57 58 89 40 56 36
        85 32 25 85 57 48 84 35 47 62 17 01 01 99 89 52
        06 71 28 75 94 48 37 10 23 51 06 48 53 18 74 98 15
        27 02 92 23 08 71 76 84 15 52 92 63 81 10 44 10 69 93`.split('\n');
    
    const rows = data.map(v => {
        const line = v.trim().split(' ');
        return line.map(v => parseInt( v ));
    });

    return rows;  
};

const testInstance = function(callback) {    
    const tp = new TrianglePath();
    assert.ok( tp, 'should exist' );

    callback();
};

const testCombineRows = function(callback) {
    try {
        const tp = new TrianglePath();
        const triangle = createSimpleTriangle();

        let bottom = triangle.pop()
        let top = triangle.pop();

        const row = tp.combineRows(top, bottom);
        assert.equal(row.length, top.length, `combined row length should be ${top.length} but was ${row.length}`);
        let expect = [ 6, 9, 14 ];
        row.forEach((v, i) => {
            assert.equal(v, expect[i], `row ${i} should be ${expect[i]} but was ${v}`);
        })

        callback();
    } catch(e) {
        callback(e);
    }
};

const testLargestSum = function(callback) {
    try {
        const tp = new TrianglePath();
        let sum = tp.calcLargestSum( createSimpleTriangle() );
        assert.equal(sum, 27, `should equal 27 but got ${sum}`);

        sum = tp.calcLargestSum( createRosettaSimpleReference() );
        assert.equal(sum, 321, `should equal 27 but got ${sum}`);

        sum = tp.calcLargestSum( createRosettaReference() );
        assert.equal(sum, 1320, `should equal 27 but got ${sum}`);

        callback();
    } catch(e) {
        callback(e);
    }
};

const stats = {
    name: 'TrianglePathTests',
    tests: [ testInstance, testCombineRows, testLargestSum ],
    testCount: 0,
    errorCount: 0
};

new TestSuiteRunner( stats );


