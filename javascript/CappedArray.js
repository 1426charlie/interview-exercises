#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.22
'use strict';

/**
 * The CappedArray may serve as a simple capped array of values, or a container for a rolling 
 * list of operations (functions) similar to mongo capped arrays for ops.
 */

function* Counter(maxsize, loop='infinite') {
    let count = 0;
    let keepAlive = true;
    while (keepAlive) {
        yield count++;
        if (count >= maxsize) {
            if (loop === 'infinite') {
                count = 0;
            } else {
                keepAlive = false;
            }
        }
    }
}

const CappedArray = function(size = 20, value = 0) {
    const cap = this;

    let index = Counter(size);

    let list = Array.from({length:size}, () => value);

    this.getSize = function() {
        return list.length;
    };

    // put the object and return the index
    this.put = function(obj) {
        const nxt = index.next();
        const idx = nxt.value
        list[ idx ] = obj;

        return idx;
    };

    this.peek = function(idx) {
        return list[ idx ];
    };

    // return a copy of index
    this.getAll = function() {
        return list.map(v => v);
    };

    // return a new iterator
    this.iterator = function() {
        return Counter(size, 'once');
    }
};

module.exports = CappedArray;

