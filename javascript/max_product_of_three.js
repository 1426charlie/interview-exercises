#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.07
'use strict';

/*
A non-empty zero-indexed array A consisting of N integers is given. The product of triplet (P, Q, R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).

For example, array A such that:

  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6
contains the following example triplets:

(0, 1, 2), product is −3 * 1 * 2 = −6
(1, 2, 4), product is 1 * 2 * 5 = 10
(2, 4, 5), product is 2 * 5 * 6 = 60
Your goal is to find the maximal product of any triplet.

Write a function:

function solution(A);
that, given a non-empty zero-indexed array A, returns the value of the maximal product of any triplet.

For example, given array A such that:

  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6
the function should return 60, as the product of triplet (2, 4, 5) is maximal.

Assume that:

N is an integer within the range [3..100,000];
each element of array A is an integer within the range [−1,000..1,000].
Complexity:

expected worst-case time complexity is O(N*log(N));
expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

/*
NOTES:
    pass 1: 45%
        first pass did not consider absolute values of a single positive and two negative numbers...
    pass 3: 75%
        check for the bottom two; if they are negative then use them for the calc
    pass 4: 100%
        check for zero at the top; if true, then just return zero
    pass 5:
        re-implemented without sorting to find the top 3 and bottom 2; sorting is better...
*/

// I think this is the best solution
function solution(A) {
    let len = A.length;
    A.sort((a,b) => a - b);

    return Math.max(A[0]*A[1]*A[len-1], A[len-1]*A[len-2]*A[len-3]);
}

const testdata = [
    { A:[ -3, 1, 2, -2, 5, 6 ], expect:60 },
    { A:[ -5, 5, -5, 4 ], expect: 125 },
    { A:[ -50, -45, -5, 4 ], expect: 9000 },
    { A:[ -2, -2, 5 ], expect:20 },
    { A:[ -200, 2, 1, 2, 2, 2, 5, 3, 6, 40 ], expect: 40 * 6 * 5 },
    { A:[ -20, -21, -22, 2, 1, 2, 2, 2, 5, 3, 6, 18 ], expect: -21 * -22 * 18 },
    { A:[ 4, 7, 3, 2, 1, -3, -5 ], expect: 105 },
    { A:[ 4, 7, 4, 2, 1, -3, -5 ], expect: 112 },
    { A:[ -2, -3, -5, -6, 0 ], expect: 0 },
    { A:[ 10, 3, 5, 6, 20 ], expect: 1200 },
    { A:[ -10, -3, -5, -6, -20 ], expect: -90 },
    { A:[ 1, -4, 3, -6, 7, 0 ], expect: 168 },
    { A:[ -3, 7, 2, 1, 5, 7 ], expect: 245 },
];

testdata.forEach(data => {
    const { A, expect } = data;
    const result = solution(A.slice(0));
    const ok = result === expect;
    console.log("result =>", A, expect, result, ok);
});
