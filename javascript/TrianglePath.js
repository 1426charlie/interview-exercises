#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.23
'use strict';

/**
 * calcuate the route that has the largest sum...
 * e.g
 *           4
 *          6 9
 *         2 7 6
 *        4 2 1 8
 *
 * so, the largest sum for any given path = 27
 *
 * the implementation below uses a bottom-up approach to combine the best route going
 * up rather than down.
 *
 * @see http://rosettacode.org/wiki/Maximum_triangle_path_sum 
 */
const TrianglePath = function() {
    const tp = this;

    this.combineRows = function(top, bottom) {
        let row = Array.from({ length:top.length }, (k,v) => 0);
        
        top.forEach((v, i) => {
            row[i] = v + Math.max(bottom[i], bottom[i + 1]);
        });

        return row;
    };

    this.calcLargestSum = function(triangle) {
        // create a copy
        let rows = triangle.map(v => v);

        while (rows.length > 1) {
            const bottom = rows.pop();
            const top = rows.pop();

            rows.push( tp.combineRows( top, bottom ));
        }

        // rows now equals [ [ sum ] ]
        return rows[0][0];
    };

};

module.exports = TrianglePath;
