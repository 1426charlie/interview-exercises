#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.17
'use strict';

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

createTestData().forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        if (A instanceof Array) {
            return solution(A.slice(), debug);
        } else {
            return solution(A, debug);
        }
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
});

/** problem description & examples...
A non-empty zero-indexed array A consisting of N integers is given.

A peak is an array element which is larger than its neighbours. More precisely, it is an index P such that 
0 < P < N − 1 and A[P − 1] < A[P] > A[P + 1].

For example, the following array A:

    A[0] = 1
    A[1] = 5
    A[2] = 3
    A[3] = 4
    A[4] = 3
    A[5] = 4
    A[6] = 1
    A[7] = 2
    A[8] = 3
    A[9] = 4
    A[10] = 6
    A[11] = 2
has exactly four peaks: elements 1, 3, 5 and 10.

You are going on a trip to a range of mountains whose relative heights are represented by array A, as shown in 
a figure below. You have to choose how many flags you should take with you. The goal is to set the maximum number 
of flags on the peaks, according to certain rules.

 

Flags can only be set on peaks. What's more, if you take K flags, then the distance between any two flags should 
be greater than or equal to K. The distance between indices P and Q is the absolute value |P − Q|.

For example, given the mountain range represented by array A, above, with N = 12, if you take:

two flags, you can set them on peaks 1 and 5;
three flags, you can set them on peaks 1, 5 and 10;
four flags, you can set only three flags, on peaks 1, 5 and 10.
You can therefore set a maximum of three flags in this case.

Write a function:

function solution(A);
that, given a non-empty zero-indexed array A of N integers, returns the maximum number of flags that can be set on the peaks of the array.

For example, the following array A:

    A[0] = 1
    A[1] = 5
    A[2] = 3
    A[3] = 4
    A[4] = 3
    A[5] = 4
    A[6] = 1
    A[7] = 2
    A[8] = 3
    A[9] = 4
    A[10] = 6
    A[11] = 2
the function should return 3, as explained above.

Assume that:

N is an integer within the range [1..400,000];
each element of array A is an integer within the range [0..1,000,000,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
 */

/*
    NOTES: the first solution failed with low numbers of flags set, e.g:
    100 peaks, got 19 expected 28
    10000 random, got 19, expected 99
    100000 chaotic got 25 expected 316
    25000 peaks got 56 expected 313
    First try score: 40%

    Second Try:
    Internet search produced O(n log n) and O(n) solutions;

*/
// test data
function createTestData() {
    return [
        { A:[ 1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2 ], expect:3, debug:true },
    ];
}

function findPeaks(input, debug) {
    const len = input.length;
    let peaks = Array.from({length:len});

    peaks[0] = false;
    peaks[len - 1] = false;

    for (let i = 1; i < len - 1; i++) {
        const [ left, center, right ] = [ input[i - 1], input[i], input[i + 1] ];
        peaks[i] = (center > Math.max( left, right ));
    }

    return peaks;

}

function nextPeak(input, debug) {
    const len = input.length;
    const peaks = findPeaks(input, debug);
    let next = Array({length:len}, x => 0);
    next[len - 1] = -1;
    for (let i = len - 2; i > -1; i--) {
        if (peaks[i]) {
            next[i] = i;
        } else {
            next[i] = next[i + 1];
        }
    }

    if (debug) console.log('next', next);

    return next;
}

function solution(input, debug) {
    if (debug) console.log( 'input', input );
    const len = input.length;
    const next = nextPeak(input, debug);

    let i = 1;
    let flags = 0;

    while ((i - 1) * i <= len) {
        let pos = 0;
        let num = 0;
        while (pos < len && num < i) {
            pos = next[pos];
            if (pos === -1) {
                break;
            }
            num++;
            pos += i;
        }
        flags = Math.max(flags, num);
        i++;
    }
    
    return flags;
}

console.log('\n', tests, 'tests completed', errors, 'errors');
console.log('\n\t', errors > 0 ? 'FAIL!' : 'PASS', '\n');

