#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.01
'use strict';

// return the target's index or -1 if not found
// items are found in a max of Math.log2(n) + 1 operations
function binarySearch(list, target) {
    let [ low, high ] = [ 0, list.length - 1 ];

    while (low <= high) { 
        const mid = (high - low) >> 1;

        if (list[mid] < target) {
            low = mid + 1;
        } else if (list[mid] > target) {
            high = mid - 1;
        } else {
            return mid;
        }
    }

    return -1;
}

const sortedList = [ 1, 3, 4, 6, 7, 9, 10, 11, 13 ];
console.log(sortedList);

let count = 0;
for (let i = -2; i < 15; i++) {
    const idx = binarySearch(sortedList, i);
    if (idx >= 0) {
        count++;
        console.log(`found item: ${i} in list`);
    }
}

if (count !== sortedList.length) {
    console.log(`ERROR! found ${count} items, expected ${sortedList.length}`);
} else {
    console.log(`found all ${count} items in list...`);
}
