#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.20
'use strict';

const gcd = function(x, y) {
    while (y != 0) {
        [ x, y ] = [ y, x % y ];
        console.log(x, y);
    }

    return x;
};

gcd(97, 101)
gcd(101, 97)
