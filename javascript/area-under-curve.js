#!/usr/bin/env node

// dpw@oakland.local
// 2017.01.03
'use strict';

/*\
    You know the simple y = sin(x) equation.

    Given a range of x [start, end], your mission is to calculate the total signed area of the region in the 
    xy-plane that is bounded by the sin(x) plot, the x-axis and the vertical lines x = start and x = end. The 
    area above the x-asix should be added to the result, and the area below it should be subtracted.
\*/

const cos = Math.cos;

function sinArea(start, end) {
    return cos(start) - cos(end);
}

const data = [
    { start: 0, end: 10, expect: 1.8390715290764525 },
    { start: 4, end: 6, expect: -1.6138139075139781 },
    { start: 1, end: 2, expect: 0.9564491424152821 },
    { start: 2, end: 3, expect: 0.5738456600533031 },
    { start: 0, end: Math.PI, expect: 2.0 }
];

let tests = 0;
const errors = data.reduce((list, obj) => {
    tests++;
    const result = sinArea(obj.start, obj.end);
    if (result != obj.expect) {
        console.log(`start: ${obj.start} end: ${obj.end} expect: ${obj.expect} got ${result} ERROR...`);
    } else {
        console.log(`start: ${obj.start} end: ${obj.end} expect: ${obj.expect} got ${result} ok...`);
    }
    return list;
}, []);


if (errors.length > 0) {
    console.log(`ERRORS: ${errors}`);
} else {
    console.log(`${tests} tests, zero errors...`);
}
