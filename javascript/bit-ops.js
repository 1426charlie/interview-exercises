#!/usr/bin/env node

// dpw@alameda.local
// 2017.01.02
'use strict';


/*\
    bit ops: set, clear, 

    construct with max size?
\*/

const BitArray = function(size = 256) {
    const ba = this;
    let bits = Uint32Array.from({length:(size / 32)});

    function calcIndexAndPosition(n) {
        return [ bits.length - 1 - (n >> 5), n % 32 ];
    }

    this.set = function(n) {
        const [ idx, pos ] = calcIndexAndPosition(n);

        bits[idx] |= 1 << pos;
    };

    this.clear = function(n) {
        const [ idx, pos ] = calcIndexAndPosition(n);

        let mask = ~(1 << pos);
        bits[idx] &= mask;
    };

    this.test = function(n) {
        const [ idx, pos ] = calcIndexAndPosition(n);
        let mask = (1 << pos);
        return (bits[idx] & mask) !== 0;
    };

    this.clearAll = function() {
        for (let i = 0; i < bits.length; i++) {
            bits[i] = 0;
        }
    };

    this.toString = function() {
        let show = [];
        for (let i = 0; i < bits.length; i++) {
            show.push(bits[i].toString(2));
        }

        return show.join(' | ');
    };
};

module.exports = BitArray;

if (process.argv[1].endsWith('/bit-ops.js')) {
    // defaults to 256 bit positions...
    const ba = new BitArray();

    for (let i = 1; i < 237; i += 11) {
        ba.set(i);
    }

    for (let i = 2; i < 25; i += 3) {
        ba.set(i);
    }

    console.log(ba.toString());
}
