#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.03
'use strict';

// use sieve of Eratosthenes to calculate all the primes up to n and return in an array
const sieve = function(n) {
    let primes = [];
    let table = Array.from({length:n + 1}, (x) => true);

    table[0] = table[1] = false;

    for (let i = 2; i * i <= n; i++) {
        if (table[i]) {
            primes.push(i);
            for (let k = i * i; k <= n; k += i) {
                table[k] = false;
            }
        }
    }

    const sqrtn = Math.floor(Math.sqrt(n));
    for (let i = sqrtn + 1; i <= n; i++) {
        if (table[i]) {
            primes.push(i)
        }
    }

    return primes
}

const primes = sieve(110);
for (let i = 0; i < primes.length; i++) {
    const s = primes[i] ? 'prime' : '';
    console.log(primes[i], s);
}
console.log(JSON.stringify(primes));
console.log(primes.length);

