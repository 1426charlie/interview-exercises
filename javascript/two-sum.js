#!/usr/bin/env node

// dpw@seattle.local
// 2018.02.01
'use strict';

/*
  The two sum problem is a common interview question, and it is a variation of the subset sum problem. There is a popular dynamic 
  programming solution for the subset sum problem, but for the two sum problem we can actually write an algorithm that runs in O(n) 
  time. The challenge is to find all the pairs of two integers in an unsorted array that sum up to a given S. 
*/

const debug = true;
let logid = 100;
const refList = [3, 5, 2, -4, 8, 11];
const refTarget= 7;

function log(msg) {
    if (debug) {
        console.log(logid++, msg);
    }
}

// O(n^2) exp time
function twoSumBrute(list, target) {
    const sums = [];

    for (let i = 0; i < list.length; i++) {
        for (let j = i + 1; j < list.length; j++) {
            if (list[i] + list[j] == target) {
                sums.push([list[i], list[j]]);
            }
        }
    }

    return sums;
}

let results = twoSumBrute(refList, refTarget);
console.log("Brute force results: ", results);

// O(n) linear time implemented with a set...
function twoSum(list, target) {
    const sums = [];
    const set = new Set();

    list.forEach(value => {
        const sumMinusElement = target - value;
        log("s-e: " + sumMinusElement.toString());

        if (set.has(sumMinusElement)) {
            sums.push([value, sumMinusElement]);
            log("push sums: " + sums);
        }

        set.add(value);
        log(set);
    });

    return sums;
}

results = twoSum(refList, refTarget);
console.log("O(n) results: ", results);

