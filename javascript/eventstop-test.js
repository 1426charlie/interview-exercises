#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.05
'use strict';

const EventStop = require('eventstop');

const event = new EventStop();

const unsub = event.subscribe('myevent', msg => {
    console.log(`message from event: ${msg}`);
});

event.emit('myevent', 'hello event stop!');

unsub();

