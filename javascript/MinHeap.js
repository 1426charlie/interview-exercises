#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.19
'use strict';

// Add
// create a new node at the end of the heap (push)
// compare with parent (p = (idx - 1) / 2)
// if parent GT child, then swap [ heap[pidx], heap[idx] ] = [ heap[idx], heap[pidx] ]
// Remove top (pop)
// switch bottom of heap to top
// filter new top down to correct position
// child calculations: left = root * 2 + 1, right = root * 2 + 2

const MinHeap = function(options = {}) {
    const container = this;
    const debug = options.debug;

    let heap = [];
    
    // add an item to the heap;
    this.add = function(data) {
        let idx = heap.length;
        data = parseInt(data);

        heap.push(data);

        if (idx == 0) {
            return;
        }

        let pidx = (idx - 1) >> 1;
        while (data < heap[pidx]) {
            // swap
            [ heap[pidx], heap[idx] ] = [ heap[idx], heap[pidx] ];

            // next parent
            idx = pidx;
            pidx = (idx - 1) >> 1;
            if (pidx < 0) {
                break;
            }
        }
    };

    function findSmallestChild(root) {
        const len = heap.length;
        let [ left, right ] = [ (root * 2) + 1, (root * 2 + 2) ];
        let minidx = null;

        if (left < len && heap[left] < heap[root]) {
            minidx = left;
        }

        if (right < len && heap[right] < heap[root]) {
            if (minidx && heap[right] < heap[minidx] || heap[right] < heap[left]) {
                minidx = right;
            }
        }

        return minidx;
    }

    this.pop = function() {
        switch (heap.length) {
        case 0:
            throw new Error("ERROR! attempt to pop an empty heap");
        case 1:
            return heap.pop();
        }

        let root = 0;
        const returnValue = heap[root];
        heap[root] = heap.pop();

        let idx = findSmallestChild(root);
        while (idx !== null) {
            [ heap[root], heap[idx] ] = [ heap[idx], heap[root] ];
            root = idx;
            idx = findSmallestChild(root);
        }

        return returnValue;
    };

    this.peek = function() {
        return heap[0];
    };

    this.size = function() {
        return heap.length;
    };

    this.clear = function() {
        heap.splice(0);
    };

    this.checkTop = function() {
        const top = heap[0];
        const min = heap.reduce((a, b) => Math.min(a, b), Number.MAX_SAFE_INTEGER);
        return top === min;
    };

    this.toString = function() {
        return heap.join(', ');
    }
};

module.exports = MinHeap;

if (process.argv[1].endsWith('MinHeap.js')) {
    let input  = [ 35, 33, 42, 10, 14, 19, 27, 44, 26, 31 ];
    let expect = [ 10, 14, 19, 26, 31, 42, 27, 44, 35, 33 ];
    const heap = new MinHeap({ debug:true });

    input.forEach(v => {
        heap.add(v);
        console.log(heap.toString());
    });

    if (heap.size() !== expect.length) {
        throw new Error(`heap size ${heap.size()} should be ${expect.length}`);
    }

    let top = heap.pop();
    if (top !== expect[0]) {
        throw new Error(`heap top = ${top}, expected ${expect[0]}`);
    }
    console.log(`old top: ${top}, new top: ${heap.peek()}`);
    console.log(heap.toString());

    // edge case tests
    heap.clear();
    if (heap.size() !== 0) {
        throw new Error(`heap size ${heap.size()} should be ${expect.length}`);
    }
    
    try {
        heap.pop();
        throw new Error(`should throw an error on attempt to pop zero heap...`);
    } catch (ex) {
        ; // console.log(ex.message);
    }

    heap.add(34);
    top = heap.peek();
    if (top !== 34 || heap.size() !== 1) {
        throw new Error("heap of troubles...");
    }
    top = heap.pop();
    if (top !== 34 || heap.size() !== 0) {
        throw new Error("heap of troubles...");
    }

    heap.clear();
    input = [ 94455, 20555, 53125, 62995, 73432 ];
    expect = '53125, 62995, 73432, 94455';
    input.forEach(v => {
        heap.add(v);
        console.log(heap.toString());
    });

    top = heap.pop();
    console.log(top, ':', heap.toString());
    if (heap.toString() !== expect) {
        throw new Error("not what I wanted...");
    }
}

