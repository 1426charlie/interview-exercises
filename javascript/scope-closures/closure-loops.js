'use strict';

// var gets bound once
for (var i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log( i ); // prints 6 6 6 6 6 
    }, 0);
}


// let gets bound to each iteration of the for loop
for (let i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log( i ); // prints 1 2 3 4 5
    }, 0);
}


