// 'use strict';

function foo() {
    console.log(this.bar);
}

// gets assigned to global space
bar = "bar1";

var o2 = { bar: "bar2", foo: foo };
var o3 = { bar: "bar3", foo: foo };

// default binding
foo();
o2.foo();
o3.foo();

// implicit binding
o2.foo.call( o3 );

// hard binding
var orig = foo;
foo = function() {
    orig.call(o2);
}

foo();        // bar2
foo.call(o3); // bar2

// or, use bind(fn, o) {

function bind(fn, o) {
    return function() {
        fn.call(o);
    };
}

foo();
foo.call(o2);

