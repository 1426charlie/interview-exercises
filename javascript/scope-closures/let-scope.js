'use strict';


function foo() {
    var bar = 'bar',
        i = "other scope";

    for (let i = 0; i < 3; i++) {
        console.log( i );
    }

    // this will die at run time
    console.log("value of i: ", i);

    {
        let i = 55;

        console.log('curly scoped let:',  i );
    }
}

foo();

