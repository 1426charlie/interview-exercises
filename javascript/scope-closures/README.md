# Javascript Scap and Closures

## Advanced Javascript : pluralsight.com / Kyle Simpson

As a compiler, you ask _"what is the LHS or RHS for..."_

~~~

// global scope
var foo = 'bar';

function bar() {
    var foo = 'baz';
}

function baz(foo) {
    foo = 'bam';
    bam = 'yay';
}

~~~

* JS is compiled
* initial pass, other passes, then executes
* re-evaluates based on monitoring and instrumentation; may re-compile
* jit compilation
* left-hand-side / right-hand-side (lhs/rhs) 


_always 'use strict' to avoid accidental global scope vars..._

### Slide 2

line 1: Hey global scope, I have a declaration for a varible foo; 
line 3: Hey global scope, I have a reference to a global scope

line 13: Hey global scope, I have an RHS reference for a function reference bar

line 4: Hey bar() scope, I have a 
line 10: Hey scope of bar() I have a RHS reference for the function baz;
line 7: Hey baz, I have a LHS reference for the reference foo

line 14: Hey global scope I have an RHS variable reference for foo, what's the value (foo)
line 15: Hey global scope I have an RHS variable reference for bam, what's the value (yay)
line 16: Hey global scope I have an RHS function reference fro baz, what's the value (crash)

~~~
var foo = "bar";

function bar() {
	var foo = "baz";
	
	function baz(foo) {
		foo = "bam";
		bam = "yay";
	}
	
	baz();
}

bar();
foo;
bam;
baz();
~~~

### Slide 3: Function Expressions

Function is first word in statement, then it's a function declaration;
If not, it's a function expression.  Named functions are easier for debuggers.

~~~
// create a named function expression with self reference (bar)
var foo = function bar() {
	var foo = "baz";
	
	function baz(foo) {
		foo = bar; 
		foo;
	}
	baz();
};

foo();
bar(); // error
~~~

Shadowing?

### Slide 4: 

~~~
var foo;

try {
	foo.length;
} catch (err) {
	// err is local to the catch clause
	console.log(err);
}

console.log(err); // reference error
~~~

### Lexical and Dynamic Scopes

Most languages use lexical scoping (compile time scope).  Bash uses dynamic scope (defined at runtime).

Scope bubbles...

_eval creates dynamic scoping but slows down coding by defeating optimizations._

### Block Scope with let

ES6+

~~~
var a = "foo";
{
	let a = "bar";
	console.log(a); // bar
}

console.log(a); // foo
~~~

### Hoisting

variables are handled at compile time.  functions are hoisted first, then variable declarations.

~~~
foo();

var foo = 2;

function foo() {
	console.log("first declaration");
}

function foo() {
	console.log('second declaration');
}
~~~

### The this Keyword

~~~
function foo() {
	console.log(this.bar);
}

var bar = "bar1";
var o2 = { bar: "bar2", foo: foo };
var o3 = { bar: "bar3", for: foo };

foo(); // bar1
o2.foo(); // bar2
o3.foo(); // bar3

// explicit binding
o2.foo.call( o3 ); // bar3

// hard binding
var orig = foo;
foo = function() {
	orig.call(o2);
}

foo();  // bar2
foo.call(o3); // bar2
~~~

#### Rules

1. implicit and default binding rules
3. explicit binding: foo.call(o2)
4. hard binding

### New keyword & binding

_The 'new' keyword modifies the function to be a constructor call..._

1. brand new empty object is created
2. new object gets linked to a new object*
3. new object gets bound to this
4. returns 'this' (i.e., the new object)

questions to ask for order of precidence...

1. was the function called with new keyword
2. explicit binding: was the function called with call or apply
3. implicity binding: was the function called via a containing object
4. default global object


### Closure

