'use strict';

var MyThing = function() {
    var nm = this;

    this.name = "my thing name";

    this.show = function() {
        console.log( nm.name );
    }
};

var MyOtherThing = function other() {
    this.name = "the other thing name";

    this.show = function() {
        console.log( other.name );
    }
};

var mt = new MyThing();
var ot = new MyOtherThing();

mt.show();
ot.show();

