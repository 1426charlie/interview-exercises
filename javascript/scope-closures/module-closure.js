// module pattern -> principal of least exposure

var opts = {
    thing:"thang",
    foo:"bar"
};

var Foo = function fooModule(params) {
    var o = { bar: "bar" };

    return { 
        bar: function() {
            console.log(o.bar);
        },
        p: function() {
            console.log( params );
        }
    };
};

var foo = Foo(opts);

foo.bar();
foo.p();

