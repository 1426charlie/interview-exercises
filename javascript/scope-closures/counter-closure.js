function Counter() {
    var count = 0;

    this.increment = function() {
        count++;
        console.log( count );
    };

}

var counter = new Counter()
counter.increment()
counter.increment()
