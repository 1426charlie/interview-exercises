function Foo(who) {
    this.me = who;
}

Foo.prototype.identify = function() {
    return 'I am ' + this.me;
};

var a1 = new Foo('a1');
var a2 = new Foo('a2');

a2.speak = function() {
    console.log('hi, ' + this.identify() + '.');
};

console.log( a1.identify() );
a2.speak()

// add a shadow
a1.identify = function() {
    return 'I have always been A1';
};

console.log( a1.identify() );

console.log( a1.constructor === Foo );
console.log( a1.constructor === a2.constructor );
console.log( a1.__proto__ === Foo.prototype ); // dunder-proto.  dunder == __
console.log( a1.__proto__ === a2.__proto__ );

// this is probably bad...
function Bar(who) {
    Foo.call(this, who);
}

Bar.prototype = Object.create(Foo.prototype);
Bar.prototype.speak = function() {
    console.log('howdy, ' + this.identify() + '.');
};

var b1 = new Bar('b1');
var b2 = new Bar('b2');

b1.speak();
b2.speak();

