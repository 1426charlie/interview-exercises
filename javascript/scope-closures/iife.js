'use strict';

var foo = "foo";

(function(bar) {
    var foo = bar;
    console.log(foo); // foo
})(foo);

+function(baz) {
    var foo = baz;

    console.log( baz );
}("yahoo");

console.log(foo); // foo


