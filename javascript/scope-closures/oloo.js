// zero constructors for behaviour delegation...
var Foo = {
    init: function(who) {
        this.me = who;
    },
    identify: function() {
        return 'I am ' + this.me;
    }
};

var Bar = Object.create( Foo );

Bar.speak = function() {
    console.log( 'howdy, ' + this.identify() + '.' );
};

Bar.create = function(who) {
    var obj = Object.create(Bar);
    obj.init( who );

    return obj;
};

// if this was implemented without Bar.create, you need two steps; this may be better
// especially if you want to create objects at one point (like in a pool) then init
// at a later date...
var b1 = Bar.create('b1');
// b1.init('b1');

b1.speak();

