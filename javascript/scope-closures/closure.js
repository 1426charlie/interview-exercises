function foo() {
    var bar = "bar";

    function baz() {
        console.log( bar );
    }

    bam(baz);
}

function bam(fn) {
    // even though this is called by bam, it remembers bar because of lexical scope

    fn();
}

foo();

