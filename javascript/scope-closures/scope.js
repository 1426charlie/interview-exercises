
// don't use strict for this demo...
// 'use strict';

// global scope
var foo = 'bar';

function bar() {
    var foo = 'baz';
}

function baz(foo) {
    foo = 'bam';
    bam = 'yay';
}

