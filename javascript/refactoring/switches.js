#!/usr/bin/env node

// dpw@seattle.local
// 2016.03.29
'use strict';

const STATES = {
    VIP:'vip',
    Preferred:'preferred',
    Regular:'regular'
};

// return a new value based on switch
const switchMachine = function(state, input) {
    let output;

    switch (state) {
    case STATES.VIP:
        output = input * 0.50;
        break;
    case STATES.Preferred:
        output = input * 0.75;
        break;
    case STATES.Regular:
        output = input * 0.95;
        break;
    }

    return output
};

const StateMachine = function() {
    const machine = this;

    this.calcVIP = function(input) {
        return input * 0.50;
    };

    this.calcPreferred = function(input) {
        return input * 0.75;
    };

    this.calcRegular = function(input) {
        return input * 0.95;
    };

    this[ STATES.VIP ] = this.calcVIP;
    this[ STATES.Preferred ] = this.calcPreferred;
    this[ STATES.Regular ] = this.calcRegular;
};

const tests = [
    { state: STATES.VIP, input: 20.0, expects: 10.0 },
    { state: STATES.Preferred, input: 12.0, expects: 9.0 },
    { state: STATES.Regular, input: 100.0, expects: 95.0 }
];

const results1 = tests.map(obj => {
    const output = switchMachine(obj.state, obj.input);
    return output === obj.expects;
});

console.log('switch machine results: ', results1 );

const stateMachine = new StateMachine();

const results2 = tests.map(obj => {
    const output = stateMachine[ obj.state ]( obj.input );

    return output === obj.expects
});

console.log('state machine results: ', results2 );

