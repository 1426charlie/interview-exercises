#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.12
'use strict';

const arr = [];

arr.push('one', 'two', 'three');

let b = arr.slice();
console.log(arr, b);

b.splice(2, 0, 'five', 'six');
console.log('insert 5 6', b);

b.splice(0);
console.log('empty?', b);

b = arr.slice();
b.reverse(); // reverse in-place
console.log(arr, 'reversed', b); 
let c = b.map((x, i, A) => A[A.length - i - 1]); // reverse not-in-place
console.log(arr, 'reversed', b, '2nd reverse', c);

let rest = b.slice(1);
console.log('rest', rest);

// copyWithin - high speed copy; great for IntArray
b = [ 1, 2, 3, 4, 5 ];

// new trick to fill an array on construction
c = Array(80).join('*');

console.log(c);

// from length, string, set, map : implements fizz/buzz
b = Array.from({length:17}, (x,i) => {
    const n = i + 1;
    let buf = [ n ];
    if (n % 3 == 0) {
        buf.push('Fizz');
    }
    if (n % 5 == 0) {
        buf.push('Buzz');
    }

    return buf.join('');

});
console.log(b);

b = Array.from('doggod');
console.log(b);
const unique = new Set();
b.forEach(ch => unique.add(ch));
console.log(unique);
console.log(Array.from(unique));

const hash = new Map();
hash.set('foo', 'bar');
hash.set('bizz', 'baz');
b = Array.from(hash);
console.log(b);

b = [ 0, 1, 2, 3, 4, 5, 6, 7 ].map((x,i) => 2 << i);
console.log(b);
let sum = b.reduce((a, b) => {
    return a + b;
}, 0);
console.log(b, 'sum =', sum);

const fours = b.reduce((a, b) => {
    if (b % 4 == 0) {
        a.push(b);
    }
    return a;
}, []);
console.log(fours);

const flattened = [[0,1],[2,3],[4,5]].reduce((a,b) => {
    return a.concat(b);
}, []);
console.log('flattened', flattened);

// map reduce
const maxCallback = (pre,cur) => Math.max( pre, cur );
const aobj = [ { x: 22 }, { x: 34 } ];
const maximum = aobj.map(item => item.x).reduce(maxCallback);
console.log(aobj, 'max', maximum);

// find first element
b = [ 0, 1, 6, 8, 4, 5, 6, 7 ];
let found = b.find(x => x > 4);
console.log(b, 'found x > 4', found, 'idx', b.findIndex(x => x > 4));
function isPrime(n) {
    if (n < 2) {
        return false;
    }
    const sqrt = Math.floor(Math.sqrt(n)) + 1;
    for (let i = 2; i < sqrt; i++) {
        if (n % i === 0) {
            return false;
        }
    }
    return true;
}
// find first prime
console.log(b, 'first prime', b.find(isPrime));
console.log(b, 'find nothing:', b.find(x => x == 25));

console.log([ 1, 2, 3, 5 ].includes(4)); // -> false

b = [ 1, 2, 3, 4, 5 ];
console.log(b, 'reversed with reduceRight', b.reduceRight((a, b) => {
    return a.concat(b);
}, []));

// use 'every' to determine if this is in sorted order
console.log(b, 'sorted?', b.every((x, i, A) => {
    if (i === 0) {
        return true;
    }
    return (A[i - 1] < x);
}));
// use 'every' to determine if this is in sorted order
b.push(2);
console.log(b, 'sorted?', b.every((x, i, A) => {
    if (i === 0) {
        return true;
    }
    return (A[i - 1] < x);
}));

const rnd = Array.from({length:6}, (x) => Math.floor(Math.random()*1000) << 8);
console.log('random', rnd);

console.log('sorted', rnd.slice().sort((a, b) => a - b));

// use reduce to find the occurrences of '01'
let hex = (2015).toString(2).split('');
let zeros = hex.reduce((a, b, idx, A) => {
    if (idx > 0 && A[idx-1] === '0' && b === '1') {
        a++;
    }
    return a;
}, 0);

console.log(hex, zeros);
