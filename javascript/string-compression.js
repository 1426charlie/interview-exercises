#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.28
'use strict';

// 1.6: String compression implemented with reduce

function findShortest(s) {
    const comp = compress(s);
    return comp.length < s.length ? comp : s;
}

function compress(s) {
    let count = 0;
    let buf = [];
    buf.push(s[0]);

    s.split('').reduce((prev, curr) => {
        if (prev === curr) {
            count++;
        } else {
            buf.push(count);
            count = 1;
            buf.push(curr);
        }
        return curr;
    }, s[0]);

    buf.push(count);
    return buf.join('');
}

const s = 'aabcccccaaa';
const expect = 'a2b1c5a3';

const r = findShortest(s);
console.log(expect, r, expect === r);

