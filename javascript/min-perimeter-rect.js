#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.17
'use strict';

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in', (elapsed).toFixed(6), 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

createTestData().forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        return solution(A, debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running xx test...');
    test_runner(() => solution(A));
}

/** problem description & examples...
An integer N is given, representing the area of some rectangle.

The area of a rectangle whose sides are of length A and B is A * B, and the perimeter is 2 * (A + B).

The goal is to find the minimal perimeter of any rectangle whose area equals N. The sides of this 
rectangle should be only integers.

For example, given integer N = 30, rectangles of area 30 are:

(1, 30), with a perimeter of 62,
(2, 15), with a perimeter of 34,
(3, 10), with a perimeter of 26,
(5, 6), with a perimeter of 22.
Write a function:

function solution(N);
that, given an integer N, returns the minimal perimeter of any rectangle whose area is exactly equal to N.

For example, given an integer N = 30, the function should return 22, as explained above.

Assume that:

N is an integer within the range [1..1,000,000,000].
Complexity:

expected worst-case time complexity is O(sqrt(N));
expected worst-case space complexity is O(1).
************************************************************/

// test data
function createTestData() {
    return [
        { A:30, expect:22, debug:false },
        { A:1, expect:4, debug:false },
        { A:2, expect:6, debug:false },
        { A:36, expect:24, debug:false },
        { A:1e6, expect:4000, debug:false },
        { A:1e9, expect:126500, debug:false },
    ];
}

function solution(n, debug) {
    let i = 1;
    let min_perimeter = Number.MAX_SAFE_INTEGER;
    let w = n / i;

    while (i * i < n) {
        w = n / i;

        if (n % i === 0 && w % 1 === 0) {
            min_perimeter = Math.min( min_perimeter, 2 * (w+i) );
            if (debug) console.log('n', n, 'i', i, 'w', w, 'n/i', n / i, 'mp', min_perimeter);
        }

        i++;
    }

    w = n / i;
    if (n % i === 0 && w % 1 === 0) {
        min_perimeter = Math.min( min_perimeter, 2 * (i + w) );
        if (debug) console.log('n', n, 'i', i, 'w', w, 'n/i', n / i, 'mp', min_perimeter);
    }

    return min_perimeter;
}

console.log('\n', tests, 'tests completed', errors, 'errors');
console.log('');

