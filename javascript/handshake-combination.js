#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.28
'use strict';

// Four people meet and shake hands in a traditional way.  How many individual handshakes will
// occur? 4C2 (n-Choose-k) is the formula, how would you implement an algorithm?  What's the time O?

const A = [ 'A', 'B', 'C', 'D' ];

// implemented with the standard formula n-Choose-k
function proof() {
    function factorial(n) {
        let r = 1
        while (n > 1) {
            r = r * n;
            n--;
        }
        return r;
    }

    // formula, n = total people count, c = two people to shake hands (where A<=>B cancels B<=>A)
    // n! / (c!)*(n-c)! = 6!  (
    const n = A.length;
    const c = 2;
    const fn = factorial(n);
    const fc = factorial(c);
    const fnc = factorial(n-c);

    console.log('fn', fn, 'fc', fc, 'fn-c', fnc);
    const shakes = fn / (fc * fnc);
    console.log('shakes', shakes);
}

// algo 1 - iterate and count the unique shakes using pointer math
function uniqueShakes(people) {
    let len = people.length;
    if (len < 2) {
        return 0;
    } else if (len < 3) {
        return 1;
    }

    let count = 0;
    for (let i = 0; i < len - 1; i++) {
        for (let j = i + 1; j < len; j++) {
            console.log(people[i], people[j]);
            count++
        }
    }

    return count;
}

console.log('shakes', uniqueShakes(A))

