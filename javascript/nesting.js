#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.09
'use strict';

/*
A string S consisting of N characters is called properly nested if:

S is empty;
S has the form "(U)" where U is a properly nested string;
S has the form "VW" where V and W are properly nested strings.
For example, string "(()(())())" is properly nested but string "())" isn't.

Write a function:

function solution(S);
that, given a string S consisting of N characters, returns 1 if string S is properly nested and 0 otherwise.

For example, given S = "(()(())())", the function should return 1 and given S = "())", the function should return 0, as explained above.

Assume that:

N is an integer within the range [0..1,000,000];
string S consists only of the characters "(" and/or ")".
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(1) (not counting the storage required for input arguments).
*/

const debug = true;

function solution(S) {
    const len = S.length;
    // do the easy shortcuts first
    if (len === 0) {
        return 1
    }

    // balanced only if even numbers
    if (len % 2 === 1) {
        return 0
    }

    let list = S.split('');
    let nest = 0;

    for (let i = 0; i < len; i++) { 
        if (list[i] === '(') {
            nest++;
        } else {
            nest--;
            if (nest < 0) {
                return 0;
            }
        }
    }

    return nest === 0 ? 1 : 0;
}

const testdata = [
    { S:'((()()))', expect: 1 },
    { S:'()', expect: 1 },
    { S:'(()((())', expect: 0 },
];

testdata.forEach(data => {
    const { S, expect } = data;
    const result = solution(S);
    const ok = result === expect;
    console.log("result =>", S, expect, result, ok);
});
