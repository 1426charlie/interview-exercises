#!/usr/bin/env node

// dpw@seattle.local
// 2016.03.10
'use strict';

const crypto = require('crypto');

const db = {
    users:new Map(),
    keys:new Map(),
    routes:new Map()
};

const xor = function(n1, n2) {
    // mask the two strings
    const b1 = new Buffer( n1 );
    const b2 = new Buffer( n2 );

    let result = [];

    for (let i = 0; i < b1.length; i++) {
        result.push( b1[i] ^ b2[i] );
    }

    let value = new Buffer( result ).toString('hex');

    return value;
};

const createKey = function(user) {
    let hash = crypto.createHash('sha512')
    hash.update( [ user.username, user.password ].join('!') );

    return hash.digest('hex');
};

const createUser = function() {
    let user = {};

    user.id = 'e70405c5d5044ba19a69d253b0576a27';

    user.username = 'dpw@bluelasso.com';
    user.password = 'secret!'

    db.users.set( `User:${user.id}`, JSON.stringify( user ));

    return user;
};

const createRoutes = function(id) {
    let routes = [];

    // generate a uuid
    let mask1 = '206d634ea2b44cadb93f5c148d23bae4';

    // calculate the xor mask
    let mask2 = xor( id, mask1);

    let r1 = [ 342, 44305, '4c4ab22f5e7f4b0cbb0efeb8bd022b52' ].join('/');
    let r2 = [ 427, 82305, '7cbed6d7ce0b4fb58a3ceed905ef67a6' ].join('/'); 

    routes.push( { route: r1, mask:mask1 } );
    routes.push( { route: r2, mask:mask2 } );

    routes.value = new Buffer( `${r1}:${r2}` ).toString('hex');

    db.routes.set( r1, new Buffer( mask1 ).toString('hex') );
    db.routes.set( r2, mask2 );

    return routes;
};

const decodeRoutes = function(value) {
    let str = new Buffer(value, 'hex').toString();

    let routes = str.split(':');

    return routes;
};

const findUserId = function(key) {
    console.log('find the id from key: ', key );

    // find the routes
    let rval = db.keys.get(`Auth:${key}`);

    // decode the rval
    let routes = decodeRoutes( rval );
    console.log( 'routes: ', routes );

    let masks = routes.map(route => new Buffer( db.routes.get( route ), 'hex' ));
    let idval = xor(masks[0], masks[1]);
    let id = new Buffer( idval, 'hex' ).toString();

    return id;
};

const saveUserKey = function(user) {
    let key = createKey( user );

    console.log( `user: ${user.id}, key: ${key}` );

    let routes = createRoutes( user.id );

    db.keys.set( `Auth:${key}`, routes.value );
};

(function() {
    console.log('create a new user and save the login data...');

    const ref = createUser();
    saveUserKey( ref );

    console.log( 'Database state:' );
    console.log( db );

    // decode, given the value for key in db.keys, find the routes to determine the user id
    let id = findUserId( createKey( ref ) );

    console.log('id: ', id, '=', ref.id, id === ref.id );
    const user = JSON.parse( db.users.get( `User:${id}` ) );

    console.log('record: ', user);
}());


