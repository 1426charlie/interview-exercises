# Generators & Yield

## Overview

Yield  in/out messaging

## Generator

~~~
function* gen() {
    console.log('howdy');
    yield null;
    console.log('world');
}

var it = gen();

it.next();
it.next();
~~~

## Promises + Generators

Fixes the inversion of control by exposing resume/reject 

## Async Lib

