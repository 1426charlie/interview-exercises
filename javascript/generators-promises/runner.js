function coroutine(gen) {
    var it = gen();
    return function() {
        return it.next.apply(it, arguments);
    };
}

function getData(d) {
    setTimeout(function() {
        run(d);
    }, 1000);
}

const run = coroutine(function* () {
    var x = 1 + (yield null);
    var y = 1 + (yield null);
    var answer = yield (x + y);

    console.log( answer );
});

run();
run(10);
console.log("meaning of life: " + run(30).value);

