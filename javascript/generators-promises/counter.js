#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.21
'use strict';

function* Counter() {
    let count = 1;
    while (true) {
        yield count++;
    }
};

const counter = Counter();

for (let i = 0; i < 10; i++) {
    let c = counter.next();
    console.log(c);
}

for (let i = 0; i < 10; i++) {
    let c = counter.next();
    console.log(c);
}

