
function* gen(words = ['hello', 'world']) {
    while (words.length > 0) {
        yield words.shift();
    }
}

let iter = gen();
let it = iter.next();
while (!it.done) {
    console.log( it.value );
    it = iter.next();
}

iter = gen(['my', 'bonny', 'lies', 'under', 'the', 'ocean']);
it = iter.next();
while (!it.done) {
    console.log( it.value );
    it = iter.next();
}
