#!/usr/bin/env node

// dpw@oakland.local
// 2016.12.24
'use strict';

const calc = function(priceEach, minPrices) {
    const len = minPrices.length;
    minPrices.sort((a,b) => a-b);
    let min = 0;

    let cluster = Array.from({length:len}, (x, i) => {
        let n = Math.max(Math.ceil(minPrices[i] / priceEach), min);
        min = n + 1;
        return n;
    });
    
    return cluster.reduce((a, b) => (a+b), 0);
};

const list = [ 6, 4, 6 ];
console.log(calc(2, list));

