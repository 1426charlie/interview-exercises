#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.09
'use strict';


/*
You are going to build a stone wall. The wall should be straight and N meters long, and its thickness should be constant; however, it should have different heights in different places. The height of the wall is specified by a zero-indexed array H of N positive integers. H[I] is the height of the wall from I to I+1 meters to the right of its left end. In particular, H[0] is the height of the wall's left end and H[N−1] is the height of the wall's right end.

The wall should be built of cuboid stone blocks (that is, all sides of such blocks are rectangular). Your task is to compute the minimum number of blocks needed to build the wall.

Write a function:

function solution(H);
that, given a zero-indexed array H of N positive integers specifying the height of the wall, returns the minimum number of blocks needed to build it.

For example, given array H containing N = 9 integers:

  H[0] = 8    H[1] = 8    H[2] = 5
  H[3] = 7    H[4] = 9    H[5] = 8
  H[6] = 7    H[7] = 4    H[8] = 8
the function should return 7. The figure shows one possible arrangement of seven blocks.
         |
 | |     | |     |
 | |   | | | |   |
 | |   | | | |   |
 | | | | | | |   |
 | | | | | | | | |
 | | | | | | | | |
 | | | | | | | | |
 | | | | | | | | |
 0 1 2 3 4 5 6 7 8
[8,8,5,7,9,8,7,4,8] 

Assume that:

N is an integer within the range [1..100,000];
each element of array H is an integer within the range [1..1,000,000,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/


function solution(specs, debug) {
    const len = specs.length;
    let blocks = [];
    let count = 0;

    for (let i = 0; i < len; i++) {
        let top = blocks.length - 1;
        if (top < 0) {
            blocks.push(specs[i]);
            count++;
        } else if (specs[i] !== blocks[top]) {
            while (top >= 0 && specs[i] < blocks[top]) {
                blocks.pop();
                top--;
            }

            if (top < 0) {
                blocks.push(specs[i]);
                count++;
            } else if (specs[i] > blocks[top]) {
                count++;
                blocks.push(specs[i]);
            } 
        }

        if (debug) console.log('blocks:', blocks, 'count:', count );
    }

    return count;
}

const MAX_LENGTH = 100000;
const run_stress = true;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

const testdata = [
    { H:[ 8, 8, 5, 7, 9, 8, 7, 4, 8 ], expect: 7, debug:true },
    { H:[ 2, 3, 2, 1 ], expect: 3, debug: true },
];

testdata.forEach(data => {
    const { H, expect, debug } = data;
    const test = function() {
        return solution(H.slice(), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', H, expect, result, ok);
    if (!ok) {
        errors++;
    }
});

if (run_stress) {
    let H = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running big zeros test...');
    test_runner(() => solution(H));

    H = Array.from({length:MAX_LENGTH}, (x) => Math.floor(Math.random() * 1e9));
    console.log('running big random test...');
    test_runner(() => solution(H));
}

