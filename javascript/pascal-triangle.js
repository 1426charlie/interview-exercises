#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.21
'use strict';

// TODO: cache the results to simulate python's functools.lru_cache
// TODO: wrap in a class that contains memoized caching

const calcRow = function(n) {
    // make a row vector of 1+n of columns populated with ones
    let row = Array.from({length: n + 1}, (v,k) => 1);

    for (let i = 0; i < Math.round(n / 2); i++) {
        let x = Math.round(row[i] * (n - i) / (i + 1));

        row[i + 1] = row[n - 1 - i] = x;
    }

    return row;
};

let n = 0;
while (n < 26) {
    let row = calcRow(n);
    let pow = Math.pow(2, n);
    let sum = row.reduce((a, b) => a + b, 0 );
    console.log(`n:${n}:${pow}:${sum}:${pow === sum} = ${row}`);
    n++;
}

