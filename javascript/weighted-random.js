#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.16
'use strict';

/**
 * Challenge: for the given list of city/populations, implement a weighted random city selector where a 
 * city is selected based on its population ratio.
 *
 * This can be implemented in many ways, but the solution below creates a sorted list of ranges then applies
 * random number (0..1) to the range list length and iterates the list to select the city.
 *
 * This algorithm reduces memory footprint but has a slower speed due to the iterations.  Each selection must
 * iterate the list to find the correct segment and return it's index, or O(n). It's linear and when the list is
 * sorted by population, most selections only iterate part of the list.  But what if the list had 1M cities?
 *
 * What would be a faster implementation where the selection time would be O(1)?
 */

const populations = [
    { name:'oakland', population:400000 },
    { name:'seattle', population:550000 },
    { name:'san francisco', population:940000 },
    { name:'palo alto', population:246000 },
    { name:'berkeley', population:130000 }
];

// for the given list[0..N], return weighted series series [0..N] of weighted intervals
function prepareWeightedSeries(list) {
    const len = list.length;

    let series = Array.from({length:len});
    let total = 0;
    let start = 0;

    for (let i = 0; i < len; i++) {
        start = total;
        total += list[i];
        series[i] = total ;
    }

    return [total, series];
}

// return the index of the selected city
function findFromSeries(series , x) {
    for (let i = 0; i < series.length; i++) {
        if (x < series[i]) {
            return i;
        }
    }

    throw new Error('out of range');
}

function calcDistribution() {
    const random = Math.random;

    // sort descending by population
    const cities = populations.slice(0).sort((a,b) => b.population - a.population);
    const totalPopulation = cities.map(city => city.population).reduce((prev, curr) => prev += curr);
    cities.forEach(city => {
        city.selected = 0;
        city.selectedRatio = 0;
        city.popRatio = Number((100 * (city.population / totalPopulation)).toFixed(3));
    });

    // normalize to lowest common denominator
    const normalFactor = 100000; // normalize();
    const [total, series] = prepareWeightedSeries(cities.map(item => item.population / normalFactor));

    const sampleSize = 1e5;
    for (let n = 0; n < sampleSize ; n++) {
        let x = random() * total;

        const city = cities[ findFromSeries(series, x) ];
        city.selected++;
    };

    cities.forEach(city => {
        city.selectedRatio = Number((100 * (city.selected / sampleSize)).toFixed(3));
        city.deviation = Number((city.selectedRatio - city.popRatio).toFixed(4));
    });

    return cities;
}


// now test the solution by showing the sums for each city 
const aggregate = calcDistribution();
console.log( aggregate );

