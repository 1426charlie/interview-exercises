#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.04
'use strict';

const debug = true;
function log(msg) {
    if (debug) console.log(msg);
}

function solution(A) {
    // log(`array: ${A}`);
    const len = A.length;
    let set = new Set(A);
    for (let i = 0; i < len; i++) {
        set.delete(A[i]);
        if (set.size === 0) {
            return i;
        }
    }        
    
    return ret;
}

const data = [
    { list:[2, 2, 1, 0, 1], expect:3 },
    { list:[ 99, 98, 97, 96, 95, 94, 93, 92, 91, 94, 96, 99 ], expect:8 },
]

data.forEach(obj => {
    let arr = obj.list;
    
    console.log(solution(data));
}
