#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.04
'use strict';

// for a non-empty zero indexed array of integers, find the element without a pair value

function findOddOccurrence(list) {
    if (list.length === 1) {
        return list[0];
    }
    
    list.sort((a,b) => a - b)
    let i = list.length - 1;
    if (list[i] != list[i-1]) {
        return list[i];
    }
    
    let max = list.length - 1;
    i = 1;
    while (i < max) {
        if (list[i-1] != list[i]) {
            return list[i-1];
        }
        i += 2;
    }
}

const data = [
    [ 9, 3, 9, 3, 9, 7, 9 ],
    [ 42 ],
    [ 4, 42, 4 ],
];

data.forEach(d => {
    console.log(d);
    const result = findOddOccurrence(d);
    console.log( 'result:', result );
});
