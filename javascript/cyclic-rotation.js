#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.05
'use strict';

/*
A zero-indexed array A consisting of N integers is given. Rotation of the array means that each 
element is shifted right by one index, and the last element of the array is also moved to the first place.

For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7]. The goal is to rotate array A K 
times; that is, each element of A will be shifted to the right by K indexes.

Write a function:

function solution(A, K);
that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.

For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].

Assume that:

N and K are integers within the range [0..100];
each element of array A is an integer within the range [−1,000..1,000].
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
*/


function solution(list, times) {
    if (list.length === 0 || times === 0) {
        return list;
    }

    // return sright(list, times)
    let rotated = list.slice(0);
    while (times > 0) {
        rotated.unshift( rotated.pop() );
        times--;
    }

    return rotated;
}

// shift right
function sright(list, times) {
    let rotated = list.map((x, i) => {
        let idx = (i + times) % list.length;
        let y = list[idx];
        console.log(x, i, times, idx, y);
        return y;
    });
    return rotated;
};

// shift left
function sleft(list, times) {
    let rotated = Array.from({length:list});
    list.map((x, i) => {
        return list[(i + times) % list.length];
    });
};

const testdata = [
    { list:[ 3, 8, 9, 7, 6 ], times: 1, expect:[ 6, 3, 8, 9, 7 ] },
    { list:[ 1, 4, 7, 10, 13, 16, 19, 22, 25, 28 ], times:3, expect: [ 22, 25, 28, 1, 4, 7, 10, 13, 16, 19 ] },
    { list:[ 3, 8, 9, 7, 6 ], times: 3, expect:[9, 7, 6, 3, 8] },
    { list:[], times:1000, expect:[] },
];

function check(rotated, expected) {
    if (rotated.length != expected.length) {
        return false;
    }
    for (let i = 0; i < rotated.length; i++) {
        if (rotated[i] !== expected[i]) {
            return false;
        }
    }
    return true;
}

testdata.forEach(obj => {
    const { list, times, expect } = obj;
    const result = solution(list, times);
    const ok = check(result, expect);
    console.log(list, times, result, ok);
    if (!ok) {
        console.log(result);
        console.log(expect);
    }
});
