#!/usr/bin/env node

// https://repl.it/DRuG
// dpw@oakland.local
// 2016.02.25

// Problem Definition: Write a program that does the following for all the numbers between 1..100 (inclusive)
// 1) console.log <number> followed by the string "Fizz" if the number is a multiple of 3; 
// 2) console.logs <number> followed by the string "Buzz" if the number is a multiple of 5; 
// 3) console.logs <number> followed by the the string "FizzBuzz" if the number is a multiple of 3 and a multiple of 5; 
// 4) console.logs <number> if none of the other rules apply.

// how do you know that it works?

// tests

const Tester = function() {
    'use strict';
    const tester = this;
    const expects = ["1", "2", "3Fizz", "4", "5Buzz", "6Fizz", "7", "8", "9Fizz", "10Buzz", "11", "12Fizz", "13", "14", "15FizzBuzz", "16", "17", "18Fizz", "19", "20Buzz", "21Fizz", "22", "23", "24Fizz", "25Buzz", "26", "27Fizz", "28", "29", "30FizzBuzz", "31", "32", "33Fizz", "34", "35Buzz", "36Fizz", "37", "38", "39Fizz", "40Buzz", "41", "42Fizz", "43", "44", "45FizzBuzz", "46", "47", "48Fizz", "49", "50Buzz", "51Fizz", "52", "53", "54Fizz", "55Buzz", "56", "57Fizz", "58", "59", "60FizzBuzz", "61", "62", "63Fizz", "64", "65Buzz", "66Fizz", "67", "68", "69Fizz", "70Buzz", "71", "72Fizz", "73", "74", "75FizzBuzz", "76", "77", "78Fizz", "79", "80Buzz", "81Fizz", "82", "83", "84Fizz", "85Buzz", "86", "87Fizz", "88", "89", "90FizzBuzz", "91", "92", "93Fizz", "94", "95Buzz", "96Fizz", "97", "98", "99Fizz", "100Buzz"]

    let errors = [];
    let showPassLines = false;

    function showMessage(msg) {
        console.log( msg );
    }

    function showErrors(errors) {
        errors.forEach((err, idx) => {
            showMessage( `err: ${ idx } : ${ err }` );
        });
    }

    this.testLines = function(lines) {

        // first clear out any existing errors
        if (errors.length > 0) {
            errors.splice(0);
        }

        console.log( 'test lines: ', lines.length, ' to expected ', expects.length);

        // if line counts don't match then there is an error
        if (lines.length !== expects.length) {
            showMessage(`FAIL: lines length is ${ lines.length },  should be ${ expects.length }`);
            return false;
        }

        // console.log(JSON.stringify( lines ));

        expects.forEach((expected, idx) => {
            const line = lines[ idx ];
            if (line !== expected) {
                errors.push(`FAIL: line ${idx+1} equals ${ line }, but should should be ${ expected }`);
            } else if (showPassLines) {
                showMessage(`PASS: line ${idx+1} ${ line } equals ${ expected }`);
            }
        });

        showErrors( errors );

        if (errors.length > 0) {
            showMessage(`FAIL with ${ errors.length } errors...`);
            return false;
        } else {
            showMessage('PASS: zero errors...');
            return true;
        }
    }
};

// implement here...

// solution

const FizzBuzz = function() {
    'use strict';
    const fb = this;

    this.testFizz = function(value) {
        return (value % 3 === 0) ? "Fizz" : '';
    }

    this.testBuzz = function(value) {
        return (value % 5 === 0) ? "Buzz" : '';
    };

    this.testInput = function(value, fnList) {
        var result = [ value ];

        fnList.forEach(fn => {
            result.push( fn( value ) );
        });

        return result.join('');
    };

    this.calc = function(from, to) {
        // test the number
        let tests = [ fb.testFizz, fb.testBuzz ];

        let lines = new Array( Math.abs( to - from + 1 ) )
        let idx = 0;
        while (idx < to) {
            lines[ idx++ ] = fb.testInput( from++, tests );
        }

        return lines;
    };

    this.showLine = function(line) {
        console.log( line );
    };

    this.showLines = function(lines) {
        lines.forEach(line => showLine( line ) );
    };
}

const fizzBuzz = new FizzBuzz();
const tester = new Tester();

const lines = fizzBuzz.calc(1, 100);

tester.testLines( lines );

