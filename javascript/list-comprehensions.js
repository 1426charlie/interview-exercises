#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.08
'use strict';

const random_array = Array.from({length:20}, x => Math.floor(Math.random() * 10000000));
console.log(random_array);

const string_array = Array.from({length:35}, (x,i) => `${i+1}` ).map(i => { 
    const n = parseInt(i);
    if (n % 3 == 0 && n % 5 == 0) {
        return `${i}FizzBuzz`;
    } 
    if (n % 3 == 0) {
        return `${i}Fizz`;
    } 
    if (n % 5 == 0) {
        return `${i}Buzz`;
    }
    return i;
});
console.log(string_array);

