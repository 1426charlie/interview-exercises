#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.05
'use strict';

/*
A non-empty zero-indexed array A consisting of N integers is given.

A permutation is a sequence containing each element from 1 to N once, and only once.

For example, array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
is a permutation, but array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
is not a permutation, because value 2 is missing.

The goal is to check whether array A is a permutation.

Write a function:

function solution(A);
that, given a zero-indexed array A, returns 1 if array A is a permutation and 0 if it is not.

For example, given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
the function should return 1.

Given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
the function should return 0.

Assume that:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [1..1,000,000,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.

NOTE: check for dups
*/

function solution(list) {
    if (list.length === 0) {
        return 0;
    }

    const len = list.length;

    // check for dups
    let set = new Set(list);
    if (set.size < len) {
        return 0;
    }

    const expected = (len * (len + 1) / 2);
    const sum = list.reduce((a,b) => a+b);

    return expected === sum ? 1 : 0;
}

const testdata = [
    { list: [4, 3, 1], expect: 0 },
    { list: [4, 3, 2, 1], expect: 1 },
    { list: [1, 4, 1], expect: 0 }
];

testdata.forEach(obj => {
    const { list, expect } = obj;
    const result = solution(list);
    console.log(list, expect, result, expect === result);
});
