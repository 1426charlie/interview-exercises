#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.01
'use strict';

/*
    10.2 Sorting and Searching: write a function to sort a single array of strings so that all
    anagrams are grouped in their own list. 
    example: [ abed, races, cares, bead ] => [ [ 'abed', 'cares' ], [ 'bead', 'races' ] ]
*/

function groupByAnagram(list) {
    const hash = list.reduce((map, word) => {
        const key = word.split('').sort().join('');
        if (map.has(key)) {
            map.get(key).push(word);
        } else {
            map.set(key, [ word ]);
        }

        return map;
    }, new Map());


    return hash;
}

console.log(groupByAnagram(createAnagramList()));
// TODO : now sort the grouped list of words words

function createAnagramList() {
    const anagrams = shuffle([
        'acres', 'races', 'cares', 'scare',
        'abed', 'bade', 'bead',
        'airmen', 'marine', 'remain',
        'apt', 'pat', 'tap',
        'alerted', 'related', 'treadle', 'altered',
        'amen', 'name', 'mane', 'mean',
        'ales', 'leas', 'sale', 'seal',
        'are', 'ear', 'era',
        'bluest', 'bluets', 'bustle', 'sublet', 'subtle',
        'does', 'dose', 'odes',
        'enters', 'nester', 'resent', 'tenser',
        'sway', 'ways', 'yaws',
        'whiter', 'wither', 'writhe'
    ]);

    function shuffle(a) {
        let n = a.length;
        let r = Array.from({length:n}, () => {
            const i = Math.floor(Math.random() * n--);
            return a.splice(i, 1)[0];
        });

        return r;
    }

    return shuffle(anagrams);
}


