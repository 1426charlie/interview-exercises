#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.29
'use strict';

/*
    1.8: Zero Matrix: Given a matrix MxN, if an element has a zero value set its row and
    column values to zero.
*/

// probably a better solution, maybe...
function zeroMatrix(matrix, rowsize = 8, colsize = 5) {
    let zerorows = [];
    let zerocols = [];

    for (let row = 0; row < rowsize; row++) {
        for (let col = 0; col < colsize; col++) {
            if (matrix[row][col] === 0) {
                if (zerorows.indexOf(row) < 0) {
                    zerorows.push(row);
                }
                if (zerocols.indexOf(col) < 0) {
                    zerocols.push(col);
                }
            }
        }
    }

    console.log(zerorows, zerocols);
    while (zerorows.length > 0) {
        let row = zerorows.pop();
        for (let col = 0; col < colsize; col++) {
            matrix[row][col] = 0;
        }
    }
    while (zerocols.length > 0) {
        let col = zerocols.pop();
        for (let row = 0; row < rowsize; row++) {
            matrix[row][col] = 0;
        }
    }


    return matrix;
}

const matrix = [
    [ 1, 2, 3, 4, 5 ],
    [ 6, 7, 8, 9, 10 ],
    [ 1, 0, 3, 0, 5 ],
    [ 1, 2, 3, 4, 5 ],
    [ 1, 2, 3, 4, 5 ],
    [ 6, 7, 8, 9, 10 ],
    [ 6, 7, 8, 9, 10 ],
    [ 6, 7, 8, 9, 10 ],
];

const expect = [
    [ 1, 0, 3, 0, 5 ],
    [ 6, 0, 8, 0, 10 ],
    [ 0, 0, 0, 0, 0 ],
    [ 1, 0, 3, 0, 5 ],
    [ 1, 0, 3, 0, 5 ],
    [ 6, 0, 8, 0, 10 ],
    [ 6, 0, 8, 0, 10 ],
    [ 6, 0, 8, 0, 10 ],
];

const results = zeroMatrix(matrix, 8, 5);
console.log(results);

