#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.04
'use strict';

function* range(start=0, stop=10, step=1) {
    let n = start;
    while (n < stop) {
        yield n;

        n += step;
    }

    // should add methods to this like map, filter, reduce, forEach, etc
}

module.exports = range;

// run a simple test
if (process.mainModule && process.mainModule.filename.endsWith('/range.js')) {
    const pow = Math.pow;
    for (let i of range(0, 10)) {
        console.log(i, pow(i, 2));
    }
}
