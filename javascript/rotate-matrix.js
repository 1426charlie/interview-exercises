#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.28
'use strict';

/*
    1.7: Rotate Matrix: Given an image represented by an NxN matrix of 4 color bytes for each cell, rotate
    the entire matrix by 90 degrees.
*/

function rotatePlane(plane) {
    console.log('rotate plane', plane);
    let rotated = Uint8Array.from(plane);

    return rotated;
}

// will rotate each plane one at a time then overwrite matrix
function rotateMatrix(matrix)  {
    for (let i = 0; i < matrix.length; i++) {
        rotatePlane(matrix[i]);
    }
}

// create a matrix of 4x4 with 4 planes
function createMatrix(width = 4, depth = 4) {
    const matrix = Array.from({length:depth});
    const size = { length:width << 2 };

    for (let i = 0; i < depth; i++) {
        const base = 10 * (i + 1);
        matrix[i] = Uint8Array.from(size, (x, i) => (base + 2*(i+1)) );
    }

    return matrix;
}

const matrix = createMatrix();
console.log(matrix);
rotateMatrix(matrix);
console.log(matrix);
