#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.22
'use strict';

const LRUCache = function(maxsize = 128) {
    const lru = this;

    // using a map means we can have any key type, object, number, string, etc
    let cache = new Map();

    // remove the oldest items until cache list < maxsize
    const prune = function() {
        // find the oldest
        // remove until cache size <= maxsize
        let list = [];
        cache.forEach((value, key) => {
            list.push({key:key, ts:value.ts});
        });

        list = list.sort((a, b) => { return a.ts < b.ts });

        while (cache.size > maxsize) {
            let item = list.pop();
            
            cache.delete( item.key );
        }

    };

    // nano-second timestamp
    const createTimestamp = function() {
        // strip off the millis and add the nanos
        return `${Math.round(Date.now() / 1000)}.${process.hrtime()[1]}`;
    };

    const createItem = function(key, value) {
        return {value:value, ts:createTimestamp()};
    };

    // will get the item if it exists and return value, else returns undefined
    this.get = function(key) {
        const item = cache.get(key);
        if (item) {
            // update cache
            return lru.set(key, item.value)
        }
    };

    // set the item and return the value; prune cache if maxsize is exceeded
    this.set = function(key, value) {
        const item = createItem(key, value);

        cache.set(key, item)
        if (cache.size > maxsize) {
            // prune in the background
            setImmediate( prune )
        }

        return value;
    };

    // simply return the cache
    this.getCache = function() {
        return cache;
    };

    // return the max size
    this.getMaxSize = function() {
        return maxsize;
    };

    // return the number of currently cached elements
    this.getCacheSize = function() {
        return cache.size;
    };

    // clear the entire cache
    this.clear = function() {
        cache.clear();
    }
};

module.exports = LRUCache;

