#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.02
'use strict';

/*\
    implement an insertion sort of integers...

    size: 1 <= size <= 1000
    array: -1e5 <= e <= 1e5, e
\*/

function processData(input) {
    const parts = input.split('\n');
    const size = parseInt(parts[0]);
    let a = Int16Array.from(parts[1].split(' ').map(n => parseInt(n)));

    if (size < 2) {
        console.log(a.join(' '));
    }

    for (let i = 1; i < size; i++) {
        let idx = i;

        while (a[idx] < a[idx - 1]) {
            [ a[idx], a[idx - 1] ] = [ a[idx - 1], a[idx] ];

            idx--;
        }

        console.log(a.join(' '));
    }

}

processData('6\n1 4 3 5 6 2');
processData('7\n4 3 5 6 2 7 1');
