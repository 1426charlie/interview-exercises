#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.16
'use strict';

/*
A non-empty zero-indexed array A consisting of N integers is given.

A triplet (X, Y, Z), such that 0 ≤ X < Y < Z < N, is called a double slice.

The sum of double slice (X, Y, Z) is the total of:
A[X + 1] + A[X + 2] + ... + A[Y − 1] + A[Y + 1] + A[Y + 2] + ... + A[Z − 1].

For example, array A such that:

    A[0] = 3
    A[1] = 2
    A[2] = 6
    A[3] = -1
    A[4] = 4
    A[5] = 5
    A[6] = -1
    A[7] = 2
contains the following example double slices:

double slice (0, 3, 6), sum is 2 + 6 + 4 + 5 = 17,
double slice (0, 3, 7), sum is 2 + 6 + 4 + 5 − 1 = 16,
double slice (3, 4, 5), sum is 0.
The goal is to find the maximal sum of any double slice.

Write a function:

function solution(A);
that, given a non-empty zero-indexed array A consisting of N integers, returns the maximal sum of any double slice.

For example, given:

    A[0] = 3
    A[1] = 2
    A[2] = 6
    A[3] = -1
    A[4] = 4
    A[5] = 5
    A[6] = -1
    A[7] = 2
the function should return 17, because no double slice of array A has a sum of greater than 17.

Assume that:

N is an integer within the range [3..100,000];
each element of array A is an integer within the range [−10,000..10,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
 */
/*
[ ] Ignore A[0] and A[n-1] because they cannot be part of the double-slice sum
[ ] define two sequence/slices, s1, s2; first iterate to the the maximum for any single element between [1..N-1] storing in s1[i]
[ ] now iterate backwards over the array [N-2..1] and store s2[i] with the single maximum
[ ] finally iterate [1..N-1] and find the max for each segment s1[i-1]+s2[i+1]
 */

function solution(input, debug) {
    if (debug) console.log( input );
    const max = Math.max;
    const len = input.length;

    let s1 = Array.from({length:len}, x => 0);
    let s2 = Array.from({length:len}, x => 0);

    for (let i = 1; i < len - 1; i++) {
        s1[i] = max(s1[i - 1] + input[i], 0);
    }

    for (let i = len - 2; i > 0; i--) {
        s2[i] = max(s2[i+1] + input[i], 0);
    }

    let max_slice = 0;

    for (let i = 1; i < len - 1; i++) {
        max_slice = max(max_slice, s1[i-1] + s2[i+1]);
    }

    return max_slice;
}

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

const testdata = [
    { A:[ 3, 2, 6, -1, 4, 5, -1, 2 ], expect:17, debug:true },
];

testdata.forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        return solution(A.slice(), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
    if (!ok) {
        errors++;
    }
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running xx test...');
    test_runner(() => solution(A));
}

console.log('\n', tests, 'tests completed', tests, errors, 'errors', '\n');

