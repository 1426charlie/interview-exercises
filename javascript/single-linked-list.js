#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.29
'use strict';

const Node = function(value, next) {
    this.value = value;
    this.next = next;
}

const LinkedList = function() {
    const list = this;

    let root = null;
    let tail = null;

    this.add = function(value) {
        let node = new Node(value);
        if (!root) {
            root = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
    }

    this.walk = function(visit) {
        let node = root;
        while (node) {
            if (typeof visit === 'function') {
                visit(node);
            }
            node = node.next;
        }
    }

    this.trimDups = function() {
        let dups = new Set();
        let curr = root;
        let prev = null;

        function removeDup(dup, prev) {
            prev.next = dup.next;
            dup.next = null;
        }

        while (curr) {
            if (dups.has(curr.value)) {
                removeDup(curr, prev);
                curr = prev.next;
            } else {
                dups.add(curr.value);
                prev = curr;
                curr = curr.next;
            }

        }

        tail = prev;
    }
}

function show(node) {
    if (node.next) {
        console.log(node.value);
    } else {
        console.log(node.value, 'end...');
    }
}

let list = new LinkedList();
for (let i = 0; i < 10; i++) {
    list.add(10 + i);
}

// add a few dups
list.add(12);
list.add(14);

list.walk(show);
list.trimDups();
list.walk(show);

// add more dups and some uniques to the end
list.add(10);
list.add(26);
list.add(13);
list.add(25);
list.walk(show);
list.trimDups();
list.walk(show);
