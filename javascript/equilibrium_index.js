#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.04
'use strict';

function solution(list, n) {
    let results = [];
    if (n < 1 || list.length < 1) {
        return results ;
    }

    const sum = list.reduce((a,b) => a+b)
    
    let leftsum = 0;
    for (let i = 0; i < n; i++) {
        let rightsum = sum - leftsum - list[i];

        if (leftsum === rightsum) {
            results.push(i);
        }

        leftsum += list[i];
    }

    return results;
}

const ref = [ -1, 3, -4, 5, 1, -6, 2, 1 ];
// create a known output with really small numbers
const big = ref.map(x => Math.round(x * Number.MAX_SAFE_INTEGER / 10));

let datasets = [
    // [ ],
    // [ 0, 0, 0, 0 ],
    ref,
    // big,
    [ -7, 1, 5, 2, -4, 3, 0],
];


datasets.forEach(data => {
    console.log( 'data input:', data );
    const results = solution(data, data.length);
    if (results.length === 0) {
        console.log('no solution...');
    } else {
        console.log('solutions are:', results);
    }
});
