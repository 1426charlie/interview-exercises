#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.08
'use strict';

// inputs from 1..n
function fizzBuzz(n) {
    let s = `${n}`

    if (n % 3 == 0) {
        s += 'Fizz';
    }
    if (n % 5 == 0) {
        s += 'Buzz';
    }

    return s;
}

const r = Array.from({length:100}, (v, i) => { return fizzBuzz(i + 1); });
console.log(r);
