#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.08
'use strict';

/*
You are given two non-empty zero-indexed arrays A and B consisting of N integers. Arrays A and B represent N voracious fish in a river, ordered downstream along the flow of the river.

The fish are numbered from 0 to N − 1. If P and Q are two fish and P < Q, then fish P is initially upstream of fish Q. Initially, each fish has a unique position.

Fish number P is represented by A[P] and B[P]. Array A contains the sizes of the fish. All its elements are unique. Array B contains the directions of the fish. It contains only 0s and/or 1s, where:

0 represents a fish flowing upstream,
1 represents a fish flowing downstream.
If two fish move in opposite directions and there are no other (living) fish between them, they will eventually meet each other. Then only one fish can stay alive − the larger fish eats the 
smaller one. More precisely, we say that two fish P and Q meet each other when P < Q, B[P] = 1 and B[Q] = 0, and there are no living fish between them. After they meet:

If A[P] > A[Q] then P eats Q, and P will still be flowing downstream,
If A[Q] > A[P] then Q eats P, and Q will still be flowing upstream.
We assume that all the fish are flowing at the same speed. That is, fish moving in the same direction never meet. The goal is to calculate the number of fish that will stay alive.

For example, consider arrays A and B such that:

  A[0] = 4    B[0] = 0
  A[1] = 3    B[1] = 1
  A[2] = 2    B[2] = 0
  A[3] = 1    B[3] = 0
  A[4] = 5    B[4] = 0
Initially all the fish are alive and all except fish number 1 are moving upstream. Fish number 1 meets fish number 2 and eats it, then it meets fish number 3 and eats it too. Finally, it 
meets fish number 4 and is eaten by it. The remaining two fish, number 0 and 4, never meet and therefore stay alive.

Write a function:

function solution(A, B);
that, given two non-empty zero-indexed arrays A and B consisting of N integers, returns the number of fish that will stay alive.

For example, given the arrays shown above, the function should return 2, as explained above.

Assume that:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [0..1,000,000,000];
each element of array B is an integer that can have one of the following values: 0, 1;
the elements of A are all distinct.
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

/*
NOTE: stack based implemention to preseve the stream of downstream fishes (75%, two timeouts)
    fish swimming upstream can be eaten or can eat downstream fish that have been stacked.  for each iteration, either stack
    a downstream fish, or if it's upstream and there are stacked fish, then let them fight it out.  A big upstream fish can
    eat and entire stack of downstreams, or some combination until a bigger downstream eats him.
NOTE: improve the stack implementation to pass these tests:
    1) all fish in same direction to finish in < 1.0 seconds
    2) one fish upstream and the rest down < 0.96 seconds
*/

function solution(fish_size, direction) {
    const UP_STREAM = 0;
    const DOWN_STREAM = 1;
    let len = fish_size.length;
    let downstream = [];
    let alive = fish_size.length;

    // if (debug) console.log('initial:', fish_size, direction, alive);
    let up_count = 0, down_count = 0;
    const test_extreem = function() {
        for (let i = 0; i < len; i++) {
            if (direction[i] === DOWN_STREAM) {
                down_count++;
            } else {
                up_count++;
            }
        }
    };

    test_extreem();
    if (up_count === 0 || down_count === 0) {
        // nobody dies today
        return len;
    }

    // do some swimming and eating...
    for (let i = 0; i < len; i++) {
        if (direction[i] === DOWN_STREAM) {
            // simply stack the downstream fish (don't use shift/unshift--they are way too slow)
            downstream.push( fish_size[i] );
        } else if (downstream.length > 0) { // upstream
            // let them fight it out
            while (downstream.length > 0) {
                // one fish always dies...
                alive--;
                if (downstream[downstream.length - 1] > fish_size[i]) {
                    // eat the upstream and bail
                    break;
                } else {
                    // downstream is eaten, see if more downstream will die
                    downstream.pop();
                }
            }

            // if (debug) console.log('stack:', downstream);
        }

    }

    return alive;
}

const run_stress = true;
let tests = 0;
let errors = 0;
const testdata = [
    { A:[ 4, 3, 2, 1, 5 ], B:[ 0, 1, 0, 0, 0 ], expect: 2, debug:false },
    { A:[ 4, 3, 2, 1, 5 ], B:[ 0, 0, 0, 0, 0 ], expect: 5, debug:false },
    { A:[ 4, 3, 2, 1, 5 ], B:[ 1, 1, 1, 1, 1 ], expect: 5, debug:false },
    { A:[ 5, 4, 3, 2, 1 ], B:[ 1, 0, 0, 0, 0 ], expect: 1, debug:false },
    { A:[ 4, 5, 3, 2, 1 ], B:[ 1, 0, 0, 0, 0 ], expect: 4, debug:false },
    { A:[ 8, 7, 5, 7, 3, 4, 1, 2, 6 ], B:[0, 0, 1, 1, 1, 0, 0, 1, 0 ], expect: 4, debug:true },
    { A:[ 6, 7, 5, 7, 3, 4, 1, 2, 8 ], B:[0, 1, 1, 1, 1, 0, 0, 1, 0 ], expect: 2, debug:true },
];

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0;
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', results);
    }

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }

    return result;
}

testdata.forEach(data => {
    const { A, B, expect, debug } = data;
    const test = function() {
        return solution(A.slice(0), B.slice(0), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log("result =>", A, B, expect, result, ok);
});

console.log('simple tests done...');
if (run_stress) {
    let A = Array.from({length:100000}, (x) => 0);
    let B = Array.from({length:100000}, (x) => 1);
    console.log('running all same direction tests...');
    test_runner(() => solution(A, B));
}

if (run_stress) {
    let A = Array.from({length:100000}, (x) => Math.floor(Math.random() * 9000000));
    let B = Array.from({length:100000}, (x) => 0);
    B[2] = 1; // now have one swim down stream
    console.log('running all same direction tests...');
    test_runner(() => solution(A, B));
}

if (run_stress) {
    let A = Array.from({length:100000}, (x) => Math.floor(Math.random() * 9000000));
    let B = Array.from({length:100000}, (x) => 1);
    B[2] = 0; // now have one swim up stream
    console.log('running all same direction tests...');
    test_runner(() => solution(A, B));
}

console.log('\n', tests, 'Tests completed with', errors, 'errors...\n');
