#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.08
'use strict';

/*
w N discs on a plane. The discs are numbered from 0 to N − 1. A zero-indexed array A of N non-negative integers, specifying the radiuses of the discs, is given. The J-th disc is drawn with its center at (J, 0) and radius A[J].

We say that the J-th disc and K-th disc intersect if J ≠ K and the J-th and K-th discs have at least one common point (assuming that the discs contain their borders).

The figure below shows discs drawn for N = 6 and A as follows:

  A[0] = 1
  A[1] = 5
  A[2] = 2
  A[3] = 1
  A[4] = 4
  A[5] = 0

see: https://codility-frontend-prod.s3.amazonaws.com/media/task_img/number_of_disc_intersections/media/auto/mp094977d203c943f7007b35c2123536d2.png

There are eleven (unordered) pairs of discs that intersect, namely:

discs 1 and 4 intersect, and both intersect with all the other discs;
disc 2 also intersects with discs 0 and 3.
Write a function:

function solution(A);
that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs. The function should return −1 if the number of intersecting pairs exceeds 10,000,000.

Given array A shown above, the function should return 11, as explained above.

Assume that:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [0..2,147,483,647].
Complexity:

expected worst-case time complexity is O(N*log(N));
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/
/*
NOTES:
    pairs from above...
    0,1 - 1
    0,2 - 2
    0,4 - 3
    1,2 - 4
    1,3 - 5
    1,4 - 6
    1,5 - 7
    2,3 - 8
    2,4 - 9
    3,4 - 10
    4,5 - 11

    If you use sets (maps) for start/stop endpoints, you can ignore the dups and scan from left to right
*/

const debug = false;

function solution(A) {
    const MAX = 10000000;
    let len = A.length;

    // empty set edge case (0..100,000)
    if (len < 2) {
        return 0;
    }

    let start = Array.from({length:len});
    let stop = Array.from({length:len});

    for (let i = 0; i < len; i++) {
        // left side of segment
        start[i] = i - A[i]
        stop[i] = i + A[i];
    }

    if (debug) {
        console.log('    A:', A);
        console.log('start:', start);
        console.log(' stop:', stop);
    }

    start.sort((a,b) => a - b);
    stop.sort((a,b) => a - b);

    let intersections = 0;
    let startidx = 0

    for (let stopidx = 0; stopidx < len; stopidx++) {
        while (startidx < len && stop[stopidx] >= start[startidx]) {
            startidx++;
        }

        intersections += startidx - stopidx - 1;
        if (intersections > MAX) {
            return -1;
        }
    }

    return intersections;
}

const testdata = [
    { A:[ 1, 5, 2, 1, 4, 0 ], expect: 11 },
    { A:[ ], expect: 0 },
    { A:[ 0, 1 ], expect: 1 },
    { A:[ 0, 0 ], expect: 0 },
    { A:[ 1, 0, 0, 3 ], expect: 4 },
    { A:[ 3, 0, 1, 6 ], expect: 6 },
    { A:[ 1, 2147483647, 0 ], expect: 2 },
    { A:[1, 10, 100, 1], expect: 5 },
];

testdata.forEach(data => {
    const { A, expect } = data;
    const result = solution(A.slice(0));
    const ok = result === expect;
    console.log("result =>", A, expect, result, ok);
});
