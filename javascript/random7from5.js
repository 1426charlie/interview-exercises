#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.02
'use strict';

/*\
    implement random7 that returns a uniformly distributed random integer from 1 to 7
\*/

function checkDistribution(list) {
    const dist = list.reduce((map, value) => {
        let count = 1;
        if (map.has(value)) {
            count = map.get(value) + 1;
        }

        map.set(value, count);

        return map;
    }, new Map());

    // extract and sort the keys 1..7
    let keys = [];
    let sum = 0;
    for (let key of dist.keys()) {
        keys.push( key );
        sum += dist.get(key);
    }

    keys.sort((a, b) => a-b).forEach(key => {
        const count = dist.get(key);
        const perc = (count * 100) / sum;
        console.log(`n=${key} count=${count} percentage:${perc.toFixed(3)}`);
    });
}

function rand5() {
    return Math.floor(Math.random() * 5) + 1;
}

// this is incorrect...
function rand7bad() {
    return (rand5() + rand5() + rand5() + rand5() + rand5() + rand5() + rand5() + rand5()) % 7 + 1;
}

// correct solution
function rand7(r5) {
    const v = (r5() - 1) * 5 + (r5() - 1);
    return v >= 21 ? rand7(r5) : v % 7 + 1;
}

const a = Array.from({length:10000}, () => rand7(rand5));
// console.log(a);
checkDistribution(a);

