#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.07
'use strict';

/*
A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:

A[P] + A[Q] > A[R],
A[Q] + A[R] > A[P],
A[R] + A[P] > A[Q].
For example, consider array A such that:

  A[0] = 10    A[1] = 2    A[2] = 5
  A[3] = 1     A[4] = 8    A[5] = 20
Triplet (0, 2, 4) is triangular.

Write a function:

function solution(A);
that, given a zero-indexed array A consisting of N integers, returns 1 if there exists a triangular triplet for this array and returns 0 otherwise.

For example, given array A such that:

  A[0] = 10    A[1] = 2    A[2] = 5
  A[3] = 1     A[4] = 8    A[5] = 20
the function should return 1, as explained above. Given array A such that:

  A[0] = 10    A[1] = 50    A[2] = 5
  A[3] = 1
the function should return 0.

Assume that:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
Complexity:

expected worst-case time complexity is O(N*log(N));
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

function solution(A) {
    // sort descending
    if (A.length < 3) {
        return 0;
    }

    A.sort((a,b) => b - a);

    // console.log(A);
    const len = A.length;
    for (let i = 0, j = 1, k = 2; k < len; i++, j++, k++) {
        if (A[k] < 1) {
            // short circuit if at zero
            return 0;
        }

        if (A[k] > A[i] - A[j]) {
            // console.log(A[i], A[j], A[k]);
            return 1;
        }
    }

    return 0;
}

const testdata = [
    { A:[ 10, 2, 5, 1, 8, 20 ], expect:1 },
    { A:[ 10, 10, 8, 20 ], expect:1 },
    { A:[ 10, 10, -8, 19 ], expect:1 },
    { A:[ 10, 50, 5, 1 ], expect:0 },
    { A:[], expect: 0 },
    { A:[5], expect: 0 },
    { A:[5, 3], expect: 0 },
    { A:[5, 3, 4], expect: 1 },
    { A:[1, 2, 2], expect: 1 },
    { A:[1, 1, 2], expect: 0 },
    { A:[0, 2, 2], expect: 0 },
];

testdata.forEach(data => {
    const { A, expect } = data;
    const result = solution(A.slice(0));
    const ok = result === expect;
    console.log("result =>", A, expect, result, ok);
});
