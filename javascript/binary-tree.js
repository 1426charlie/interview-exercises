#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.12
'use strict';

const Node = function(value, left, right) {
    this.value = value;
    this.left = left || null;
    this.right = right || null;
};

const Tree = function() {
    const tree = this;

    this.root = null;

    // insert the value and return the new node
    this.insert = function(value, node) {
        if (!tree.root) {
            tree.root = new Node(value);
            return tree.root;
        }

        if (!node) {
            node = tree.root;
        }

        // walk the tree
        if (value < node.value) {
            if (node.left) {
                tree.insert(value, node.left);
            } else {
                node.left = new Node(value);
                return node.left;
            }
        } else {
            if (node.right) {
                tree.insert(value, node.right);
            } else {
                node.right = new Node(value);
                return node.right;
            }
        }
    };

    // return all values in preorder
    this.preorder = function() {
        let values = [];

        const order = function(node) {
            if (!node) {
                return;
            }

            values.push(node.value);
            order(node.left);
            order(node.right);
        };

        order(tree.root);

        return values;
    };

    // sorted
    this.inorder = function() {
        let values = [];

        const order = function(node) {
            if (!node) {
                return;
            }

            order(node.left);
            values.push(node.value);
            order(node.right);
        };

        order(tree.root);
        return values;
    };

    // return a list of the numbers in post order
    this.postorder = function() {
        let values = [];

        const order = function(node) {
            if (!node) {
                return;
            }

            order(node.left);
            order(node.right);
            values.push(node.value);
        };

        order(tree.root);
        return values;
    };
    
    // return an array of the numbers in level order
    this.levelorder = function() {
        // create a queue useing push and shift
        let queue = [];
        let values = [];
        let level = 0;

        if (tree.root) {
            tree.root.level = level;
            queue.push( tree.root );
        }

        while (queue.length != 0) {
            const node = queue.shift();
            let tq = [];
            if (node) {
                level++;
                // TODO replace with visit...
                values.push(node.value);
                if (node.left) {
                    node.left.level = level;
                    queue.push(node.left);
                }
                if (node.right) {
                    node.right.level = level;
                    queue.push(node.right);
                }
            }
        }

        return values;
    };
};

const data = [ 15, 7, 9, 19, 17, 3, 1, 4, 16, 22, 18 ];

const tree = new Tree();
data.forEach(n => tree.insert(n));

// console.dir(tree.root);
console.log(data);
console.log(tree.preorder());
console.log(tree.inorder());
console.log(tree.postorder());
console.log(tree.levelorder());
