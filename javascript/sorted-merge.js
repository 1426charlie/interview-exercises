#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.01
'use strict';

// merges from the back of the list to minimize copy elements
const debug = false;

const A = Int16Array.from([ 5, 8, 22, 29, 30, -1, -1, -1 ])
const B = Int16Array.from([ 6, 12, 50 ]);

function log(msg) {
    if (debug) {
        console.log( msg );
    }
}

const merge = function(A, B) {
    
    // find the start by skipping < 0
    let aptr = A.length - 1;
    while (A[aptr] < 0) {
        aptr--;
    }
    log(`A ptr ${aptr}, value: ${A[aptr]}`);

    for (let bptr = B.length - 1; bptr >= 0; bptr--) {
        while (B[bptr] < A[aptr]) {
            aptr--;
        }

        log(`insert ${B[bptr]} after ${A[aptr]}`);
        A.copyWithin(aptr + 1, aptr);
        A[aptr + 1] = B[bptr];

    }

    return A;
};

console.log(`merge sorted lists ${A.filter(x => x > 0)} with ${B}`);
const C = merge(A, B);

const expect = Int16Array.from([ 5, 6, 8, 12, 22, 29, 30, 50 ])
if (expect.every((x, i) => x === C[i])) {
    console.log('merged =>', A, "ok...");
} else {
    console.log('merged =>', A, "FAILED! expected:", expect);
}

