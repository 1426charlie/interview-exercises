#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.05
'use strict';

// use xor to swap two integers without a tmp
const puts = console.log;

// swaps and returns a tuple [ a, b ]
const swap = (a, b) => {
    a = a ^ b;
    b = a ^ b;
    a = a ^ b;

    return [ a, b ];
}

let a = 123, b = 456;
puts(a, b, swap(a, b));
