#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.04
'use strict';

function findLargestGap(n) {
    let gaps = []
    let gap = 0;
    (n).toString(2).split('').forEach(num => {
        const x = parseInt(num);
        // console.log(`gap ${gap} x ${x}`)
        if (x === 0) {
            gap++;
        } else {
            if (gap > 0) {
                gaps.push( gap );
            }
            gap = 0;
        }
    });

    if (gaps.length > 0) {
        return gaps.sort((a,b) => a - b).pop();
    } else {
        return 0;
    }
}

const data = [
    9,
    529,
    20,
    1041
];

data.forEach(n => {
    const largest = findLargestGap(n);
    console.log(`n ${n} ${(n).toString(2)} -> ${largest}`);
});
