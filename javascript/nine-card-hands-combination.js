#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.28
'use strict';

/*
    - 36 unique cards with 4 suites, numbered 1-9
    - a single hand is 9 cards delt from the deck of 36
    - how many combinations could exist with a single hand?

    n! / ((n-k)! * k!)

    n! = 36! = 371993326789901217467999448150835200000000
    n-k! = (36-9)! = 27! = 10888869450418352160768000000
    k! = 9! = 362880

    - solve this without exceeding 2^30 (1G) integer limit
*/

/*
    1) create three arrays na = [ 36..2 ], nk [ 27..2 ], k [ 9..2 ]
    2) remove elements from na that exist in nk
    3) process the remaining elements of na and k to get GCD
*/

// not working yet...
function solution() {
    const n = 36;
    const k = 9;
    const na = Array.from({length:(n - (n - k))}, (x, i) => n - i);
    const ka = Array.from({length:k - 1}, (x, i) => k - i);
    console.log(na);
    console.log(ka);

    let rna = [ 36, 32, 35, 30, 28, 33, 34, 31 ];
    let kna = [  9,  8,  7,  6,  4,  3,  2, 5 ];

    let result = rna.map((x, i) => x / kna[i]);
    console.log(rna, kna, result);

    result = result.reduce((prev, curr) => prev*curr, 1);

    return result;
}

const r = solution();
console.log('combinations: ', r)
