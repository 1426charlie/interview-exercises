#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.02
'use strict';

/*
    Will check for palindrome of a single word or phrase where punctuation and spaces are ignored.

    time complexity = O(n/2) = O(n)
 */

function isTextPalindrome(text, debug) {
    if (debug) console.log(text);

    if (!text) {
        return false
    }

    let [ left, right ] = [ 0, text.length - 1 ];
    while (left < right) {
        if (text[left] !== text[right]) {
            return false;
        }

        left++;
        right--;
    }

    return true;
}

// quick one-liner, but not the most efficent in time or space...
function isTextPalindromeOneLiner(text) {
    return text === text.split('').reverse().join('');
}

function isPhrasePalindrome(text, debug) {
    if (!text) {
        return false
    }

    // strip out any spaces or punctuation and get the lower case
    const chars = text.replace(/[^a-zA-Z]/g, '').toLowerCase();
    return isTextPalindrome(chars, debug);
}

const tests = [
    { phrase:'madam', expect:true },
    { phrase:'madan', expect:false },
    { phrase:'Taco cat', expect:true },
    { phrase:'this test is bogux', expect:false },
    { phrase:'Ah, Satan sees Natasha', expect:true, debug:false },
    { phrase:'Do Geese see God', expect:true },
    { phrase:'Never odd or even', expect:true },
];

const errors = tests.reduce((elist, obj) => {
    const [ phrase, expect, debug ] =  [ obj.phrase, obj.expect, obj.debug ];

    const r = isPhrasePalindrome(phrase, debug);
    if (r !== expect) {
        elist.push(`Expected ${phrase} to be ${expect}, got ${r}`);
    }

    return elist;
}, []);

if (errors.length > 0) {
    console.log('ERRORS', errors);
} else {
    console.log(`${tests.length} tests, zero errors...`);
}
