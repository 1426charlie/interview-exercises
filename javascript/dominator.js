#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.09
'use strict';

/*
A zero-indexed array A consisting of N integers is given. The dominator of array A is the value that occurs in more than half of the elements of A.

For example, consider array A such that

 A[0] = 3    A[1] = 4    A[2] =  3
 A[3] = 2    A[4] = 3    A[5] = -1
 A[6] = 3    A[7] = 3
The dominator of A is 3 because it occurs in 5 out of 8 elements of A (namely in those with indices 0, 2, 4, 6 and 7) and 5 is more than a half of 8.

Write a function

function solution(A);
that, given a zero-indexed array A consisting of N integers, returns index of any element of array A in which the dominator of A occurs. The function should return −1 if array A does not have a dominator.

Assume that:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
For example, given array A such that

 A[0] = 3    A[1] = 4    A[2] =  3
 A[3] = 2    A[4] = 3    A[5] = -1
 A[6] = 3    A[7] = 3
the function may return 0, 2, 4, 6 or 7, as explained above.

Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

/*
NOTES: (66%)
[x] My first thought is that this solution will include a hash to hold a count of integers similar to a word count, but with integers.  An O(n) complexity
    suggests only a single pass, so the hash with a 'max count' may be a good solution. O(n) time, but requires > O(1) size because it has a map
[*] 100% The array is sorted then len / 2 points to the middle of the array, or a likely candidate.  Iterate out from there to get the candidate's total
    count and see if its > len / 2; if so return it, else return -1 ; but this solution is O(n log n) because it requires a sort; and this won't work
    because we need to return the value's index which excludes the sort-in-place.
[x] 100% The stack approach is also linear O(n) and doesn't require a real stack but just a stack pointer (int). It outperforms the other solutions
    by a factor of 2.
NOTES:
    Read the specs: they say "The dominator of arry A is the value that occurs in more than half fo the elements of A".  So, if you have multiple occurrances
    of an array that are not > 1/2, then return either -1 or len (an out-of-bounds index) for the array

*/

function find_candidate_with_map(input, debug) {
    const len = input.length;
    let map = new Map();
    let idx = -1;
    let max = 0;

    // loop backwards to get the minimum idx
    for (let i = len - 1; i >= 0; i--) {
        const key = input[i];
        let count = map.get(key);
        if (count) {
            count++;
        } else {
            count = 1;
        }

        map.set(key, count);
        if (count > max) {
            max = count;
            idx = i;
        }
    }

    const candidate = input[idx];

    return [ candidate, max, idx ];
}

// returns the candidate, count, and original index
function find_candidate_with_sort(input) {
    const len = input.length;
    const midpoint = Math.floor(len / 2);
    const sorted = input.slice(0).sort((a,b) => a - b);
    const candidate = sorted[midpoint];
    let count = 0;
    for (let i = midpoint, j = midpoint - 1; i < len; i++, j--) {
        let match = 0;
        if (sorted[i] === candidate) {
            count++;
            match++;
        }

        if (sorted[j] === candidate) {
            count++;
            match++;
        }
        if (match === 0) {
            break;
        }
    }
    const idx = input.indexOf(candidate);
    return [ candidate, count, idx ];
}

// 100%: runs much faster than the other solutions...
function find_candidate_with_stack(input, debug) {
    const len = input.length;
    let size = 0;
    let value;

    for (let i = 0; i < len; i++) {
        if (size === 0) {
            size++;
            value = input[i];
        } else {
            if (value === input[i]) {
                size++;
            } else {
                size--;
            }
        }
    }

    let candidate = -1;
    if (size > 0) {
        candidate = value;
    }

    let count = 0;
    for (let i = 0; i < len; i++) {
        if (input[i] === candidate) {
            count++;
        }
    }

    let idx = -1;
    if (count > Math.floor(len / 2)) {
        idx = input.indexOf(candidate);
    } else {
        candidate = -1;
    }

    return [ candidate, count, idx ];
}

// sort seemed to run consistently fast, and it received 100%
const find_candidate = find_candidate_with_stack;

function solution(input, debug) {
    if (debug) console.log( input );
    let len = input.length;

    // first check array size for less than 3?  
    if (len < 3) {
        switch (len) {
        case 0: 
            return -1;
        case 1: 
            return 0;
        case 2: 
            return (input[0] == input[1]) ? 0 : -1
        }
    }

    let [ candidate, count, dominator_index ] = find_candidate(input, debug);
    if (debug) console.log(candidate, count, dominator_index);

    // now determine if the value is > 1/2 of the len; if not, then return len
    return (count > len / 2) ?  dominator_index : -1;
}

const MAX_LENGTH = 100000;
const run_stress = true;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

const testdata = [
    { A:[ 3, 4, 3, 2, 3, -1, 3, 3 ], expect:0, debug:true },
    { A:[ 3, 4, 4, 2, 3, -1, 3, 4, 6, 4, 16, 44 ], expect: -1, debug:true },
    { A:[ 0 ], expect:0, debug:true },
    { A:[ 2, 1, 1, 3 ], expect: -1, debug: true },
    { A:[ 2, 1, 1, 1, 3 ], expect: 1, debug: true },
];

testdata.forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        return solution(A.slice(), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
    if (!ok) {
        errors++;
    }
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running max length test with values = 0');
    test_runner(() => solution(A));
}

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => Math.floor(Math.random() * 2e9));
    console.log('running max length test with values = random');
    test_runner(() => solution(A));
}

console.log('\n', tests, 'tests completed', errors, 'errors', '\n');

