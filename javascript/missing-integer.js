#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.05
'use strict';

/*
Write a function:

function solution(A);
that, given a non-empty zero-indexed array A of N integers, returns the minimal positive integer (greater than 0) that does not occur in A.

For example, given:

  A[0] = 1
  A[1] = 3
  A[2] = 6
  A[3] = 4
  A[4] = 1
  A[5] = 2
the function should return 5.

Assume that:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

const debug = true;
function log() {
    if (debug) console.log(arguments);
}

function solution(A) {
    let list = Array.from(new Set(A.filter(x => x > 0)));
    // log( list );

    const len = list.length;
    if (len < 1) {
        return 1;
    }

    const expected = ((len+1)*(len+2)) / 2;
    const sum = list.reduce((a,b) => a+b);

    const result = expected - sum;
    if (result > 0) {
        return result
    } else {
        // this occurs if there are multiple gaps, so use brute force to find the minimum
        list.sort((a,b) => a - b);
        // log('brute force:',  list);
        for (let i = 0, n = 1; i < list.length; i++, n++) {
            if (list[i] !== n) {
                return n;
            }
        }
    }
}

const testdata = [
    { list: [ 1, 3, 6, 4, 1, 2 ], expect: 5 },
    { list: [ -1, -3, -6, 4, 1, 2 ], expect: 3 },
    { list: [ 2 ], expect: 1 },
    { list: [ 1, -4, -266, 0, 0, 0, 3, 4, 6, 7 ], expect: 2 },
    { list: [ 1, -4, 5, 2, -266, 0, 0, 0, 3, 4, 6, 7 ], expect: 8 },
    { list: [ 1, -4, 5, 2, -266, 0, 0, 0, 10, 3, 4, 6, 7, 8 ], expect: 9 },
    { list: [ -23, -444, -95333, -37 ], expect: 1 },
    { list: [ 95333 ], expect: 1 },
    { list: [ 0 ], expect: 1 },
    { list: [ 2, 3, 4, 5, 6, 7, 8, 8, 89, 4, 4, 4, 4430, 9393, 2033, 9442, 1 ], expect: 9 },
    { list: [ 2147483647, 1, 2, 3, 11, 12, 13, 15, 4, 5, 6, 7, 8, 9, -2147483648], expect: 10 },
];

let errors = [];
testdata.forEach(obj => {
    const { list, expect } = obj;
    const result = solution(list);
    console.log(list, expect, result, expect === result);
    if (expect !== result) {
        let msg = `ERROR! expected ${expect} got ${result}`;
        errors.push(msg);
        console.log( msg );
    }
});

if (errors.length > 0) {
    console.log(`\nTESTS FAILED: ${errors.length} errors!\n`);
    errors.forEach(m => console.log(m));
}
