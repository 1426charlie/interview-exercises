#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.29
'use strict';

const Node = function(value, next) {
    this.value = value;
    this.next = next;
}

/* 
    2.2: Get the kth from the last node
*/
const LinkedList = function() {
    const list = this;

    let root = null;
    let tail = null;

    this.add = function(value) {
        let node = new Node(value);
        if (!root) {
            root = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }

        return node;
    }

    this.findKthFromEnd = function(k) {
        let count = 0;
        let node = root;
        let found = null;

        while (node) {
            count++;
            if (count >= k) {
                if (found) {
                    found = found.next;
                } else {
                    found = new Node(root.value, root.next);
                }
                // console.log(count, found)
            }

            node = node.next;
        }

        return found;
    };

    this.walk = function(visit) {
        let node = root;
        let idx = 0;
        while (node) {
            if (typeof visit === 'function') {
                visit(node, idx);
            }
            node = node.next;
            idx++;
        }
    }
}

function show(node, idx) {
    if (node.next) {
        console.log(`${idx}) ${node.value}`);
    } else {
        console.log(`${idx}) ${node.value} end...`);
    }
}

let list = new LinkedList();
const len = 25;
let kth = 0;
for (let i = 0; i < len; i++) {
    const value = Math.floor(Math.random() * 10000);
    let node = list.add(value);
    if (i == len - 5) {
        kth = node
    }
}

list.walk(show);
let node = list.findKthFromEnd(5);
console.log( 'found', node.value  );
if (node.value !== kth.value) {
    console.log(`ERROR! ${node.value} is not = ${kth.value}`);
}

