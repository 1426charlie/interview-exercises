#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.09
'use strict';

const mori = require('mori');

const inc = function(n) {
    return n + 1;
};

let list = mori.intoArray(mori.map(inc, mori.vector(1,2,3,4,5)));
console.log(list);
list = list.map(x => x+1);
console.log(list);

const sum = function(a, b) {
    return a + b;
};

let v = mori.vector(1, 2,3,4);
let x = mori.reduce(sum, v);
console.log(v, x)

let r = mori.range(0, 50, 3);

console.log(r);

