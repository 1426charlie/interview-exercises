#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.16
'use strict';

/*
A zero-indexed array A consisting of N integers is given. It contains daily prices of a stock share for a period of N consecutive days. 
If a single share was bought on day P and sold on day Q, where 0 ≤ P ≤ Q < N, then the profit of such transaction is equal to A[Q] − A[P], 
provided that A[Q] ≥ A[P]. Otherwise, the transaction brings loss of A[P] − A[Q].

For example, consider the following array A consisting of six elements such that:

  A[0] = 23171
  A[1] = 21011
  A[2] = 21123
  A[3] = 21366
  A[4] = 21013
  A[5] = 21367
If a share was bought on day 0 and sold on day 2, a loss of 2048 would occur because A[2] − A[0] = 21123 − 23171 = −2048. If a share was 
bought on day 4 and sold on day 5, a profit of 354 would occur because A[5] − A[4] = 21367 − 21013 = 354. Maximum possible profit was 356. 
It would occur if a share was bought on day 1 and sold on day 5.

Write a function,

function solution(A);
that, given a zero-indexed array A consisting of N integers containing daily prices of a stock share for a period of N consecutive days, 
returns the maximum possible profit from one transaction during this period. The function should return 0 if it was impossible to gain 
any profit.

For example, given array A consisting of six elements such that:

  A[0] = 23171
  A[1] = 21011
  A[2] = 21123
  A[3] = 21366
  A[4] = 21013
  A[5] = 21367
the function should return 356, as explained above.

Assume that:

N is an integer within the range [0..400,000];
each element of array A is an integer within the range [0..200,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

const testdata = [
    { A:[ 23171, 21011, 21123, 21366, 21013, 21367 ], expect:356, debug:true },
];

testdata.forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        return solution(A.slice(), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
    if (!ok) {
        errors++;
    }
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running xx test...');
    test_runner(() => solution(A));
}

console.log('\n', tests, 'tests completed', errors, 'errors', '\n');

function quadratic(input, debug) {
    const max = Math.max;
    const len = input.length;

    let profit = 0;
    for (let i = 0; i < len - 1; i++) {
        for (let j = i + 1; j < len; j++) {
            let p = input[j] - input[i];
            profit = max(p, profit);
        }
    }

    return profit;
}

function kadane(input, debug) {
    const max = Math.max;
    const min = Math.min;
    const len = input.length;

    let min_buy = input[0];
    let profit = 0;

    for (let i = 1; i < len; i++) {
        profit = max(profit, input[i] - min_buy);
        min_buy = min(min_buy, input[i]);
    }

    return profit;
}

function solution(input, debug) {
    if (debug) console.log( input );
    return kadane(input, debug);
}

