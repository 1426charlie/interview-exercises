#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.17
'use strict';

// from hackerrank Utopian tree exercise...

/*\
// so if you examine the bit, for even years it's always 
yr 1 (2 cycles)   1*2+1 =  3 =     11 2**2-1 1+yr
yr 2 (4 cycles)   3*2+1 =  7 =    111 2**3-1 1+yr
yr 3 (6 cycles)   7*2+1 = 15 =   1111 2**4-1 1+yr
yr 4 (8 cycles)  15*2+1 = 31 =  11111 2**5-1 1+yr
yr 5 (10 cycles) 31*2+1 = 63 = 111111 2**6-1 1+yr

year = cycle // 2
so for even years, 1 << (yr // 2) - 1
or 1 << (( cycle >> 1 ) - 1

and to accomodate the odd cycles, double the result by left shifting by 1

or (1 << (( cycle >> 1 ) - 1) << cycle % 2 // will be 0 or 1

\*/
function calcHeight(cycles) {
    return ((1 << (( cycles >> 1) + 1 )) - 1) << cycles % 2
}

function createMap() {
    let acc = 1;
    let map = new Map();
    map.set(0, acc);
    for (let i = 1; i <= 60; i++) {
        if (i % 2 === 0) {
            acc++;
        } else {
            acc *= 2;
        }

        map.set(i, acc);

        let height = calcHeight(i)

        console.log(i, acc, height, (acc).toString(2))
    }

    return map;
}

const map = createMap();
// console.log( map );
