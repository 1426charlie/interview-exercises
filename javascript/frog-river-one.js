#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.05
'use strict';


/*
A small frog wants to get to the other side of a river. The frog is initially located on one bank of the 
river (position 0) and wants to get to the opposite bank (position X+1). Leaves fall from a tree onto the 
surface of the river.

You are given a zero-indexed array A consisting of N integers representing the falling leaves. A[K] represents 
the position where one leaf falls at time K, measured in seconds.

The goal is to find the earliest time when the frog can jump to the other side of the river. The frog can 
cross only when leaves appear at every position across the river from 1 to X (that is, we want to find the 
earliest moment when all the positions from 1 to X are covered by leaves). You may assume that the speed of 
the current in the river is negligibly small, i.e. the leaves do not change their positions once they fall in 
the river.

For example, you are given integer X = 5 and array A such that:

  A[0] = 1
  A[1] = 3
  A[2] = 1
  A[3] = 4
  A[4] = 2
  A[5] = 3
  A[6] = 5
  A[7] = 4
In second 6, a leaf falls into position 5. This is the earliest time when leaves appear in every position across the river.

Write a function:

function solution(X, A);
that, given a non-empty zero-indexed array A consisting of N integers and integer X, returns the earliest time when the frog can jump to the other side of the river.

If the frog is never able to jump to the other side of the river, the function should return −1.

For example, given X = 5 and array A such that:

  A[0] = 1
  A[1] = 3
  A[2] = 1
  A[3] = 4
  A[4] = 2
  A[5] = 3
  A[6] = 5
  A[7] = 4
the function should return 6, as explained above.

Assume that:

N and X are integers within the range [1..100,000];
each element of array A is an integer within the range [1..X].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(X), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.

NOTE:
    perm = (n * (n+1)) / 2
    so, if the sum of the set = perm, then return the index
    and, don't forget about the possibility that the frog can't make it to the other side, i.e., return -1
    make it speedy...
*/

function solution(x, list) {
    let len = list.length;
    if (len == 0) {
        return -1;
    }

    let set = new Set();
    let perm = (x * (x + 1)) / 2;
    let sum = 0

    for (let i = 0 ; i < len; i++) {
        const value = list[i];
        if (!set.has(value)) {
            set.add(value);
            sum += value;
        }

        if (sum == perm) {
            return i;
        }
    }

    return -1;
}

const testdata = [
    { x: 5, list: [ 1, 3, 1, 4, 2, 3, 5, 4 ], expect: 6  },
    { x: 1, list: [ 1 ], expect: 0 },
    { x: 10, list: [ 2, 1, 1, 1, 3, 2, 2, 1, 4, 1, 1, 4, 5, 1, 1, 1, 7, 2, 6, 2, 9, 10, 1, 1, 1, 8, 10, 4, 11, 9 ], expect: 25 },
    { x: 0, list: [], expect: -1 },
    { x: 5, list: [3], expect: -1 },
];

testdata.forEach(obj => {
    const { x, list, expect } = obj;
    const result = solution(x, list);
    console.log(x, JSON.stringify(list), expect, result, expect === result);
});
