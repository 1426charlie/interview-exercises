#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.08
'use strict';

/*
A string S consisting of N characters is considered to be properly nested if any of the following conditions is true:

S is empty;
S has the form "(U)" or "[U]" or "{U}" where U is a properly nested string;
S has the form "VW" where V and W are properly nested strings.
For example, the string "{[()()]}" is properly nested but "([)()]" is not.

Write a function:

function solution(S);
that, given a string S consisting of N characters, returns 1 if S is properly nested and 0 otherwise.

For example, given S = "{[()()]}", the function should return 1 and given S = "([)()]", the function should return 0, as explained above.

Assume that:

N is an integer within the range [0..200,000];
string S consists only of the following characters: "(", "{", "[", "]", "}" and/or ")".
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N) (not counting the storage required for input arguments).
*/

const debug = true;

// a map of open/closer characters
const opposites = {
    '(':')',
    '{':'}',
    '[':']'
};

// the primary solution
function solution(S) {
    const len = S.length;
    // do the easy shortcuts first
    if (len === 0) {
        return 1
    }

    // balanced only if even numbers
    if (len % 2 === 1) {
        return 0
    }

    let list = S.split('');
    let stack = [];

    for (let i = 0; i < len; i++) { 
        if (!evaluateChar(list[i], stack)) {
            return 0;
        }
    }

    return stack.length === 0 ? 1 : 0;
}

// separated for testability
function evaluateChar(ch, stack) {
    if ([ '[', '(', '{' ].indexOf(ch) < 0) {
        const key = stack.pop();
        if (!key || opposites[ key ] !== ch) {
            return false
        }
    } else {
        stack.push(ch);
    }

    return true
}

// testing ---------------------------------------------------------------------------------------------------------------------------------
const testdata = [
    { S:'{[()()]}', expect: 1 },
    { S:'([)()]', expect: 0 },
    { S:'', expect: 1 },
    { S:'{[[[[]]]])', expect:0 },
    { S:'{[[[[]]]]}', expect:1 },
];

testdata.forEach(data => {
    const { S, expect } = data;
    const result = solution(S);
    const ok = result === expect;
    console.log("result =>", S, expect, result, ok);
});
