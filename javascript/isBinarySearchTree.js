#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.12
'use strict';

const Node = function(value, left, right) {
    this.value = value;
    this.left = left;
    this.right = right;

    this.isBinarySearchTree = function() {
        if (left && left.value >= value) {
            // console.log('left bad', left.value, value);
            return false;
        }
        if (right && right.value <= value) {
            // console.log('right bad: ', right.value, value);
            return false;
        }

        return true;
    };
};

// traverse from top down...
function topDown(node, flags) {
    if (!node.isBinarySearchTree()) {
        flags.push(false);
        return;
    }

    // console.log(node.value);

    if (node.left) {
        topDown(node.left, flags);
    }

    if (node.right) {
        topDown(node.right, flags);
    }
}

function determineBinSearchTree(root) {
    let flags = [];
    topDown(root, flags);
    // console.log(flags);
    return flags.every(flag => flag === true);
}

let bad = new Node(3,
    new Node(5, new Node(1), new Node(4)),
    new Node(2, new Node(6), null)
);

let good = new Node(3,
    new Node(2, null, new Node(6)),
    new Node(5, new Node(4), new Node(7))
);

let partgood = new Node(3,
    new Node(2, new Node(1), new Node(6)),
    new Node(5, new Node(4), new Node(5))
);

const list = [ 
    { node:bad, expect:false },
    { node:good, expect:true },
    { node:partgood, expect:false }
];

let tests = 0;
let passes = 0;

list.forEach(data => {
    // console.log(data.node);
    tests++;
    const ok = determineBinSearchTree(data.node);
    if (ok === data.expect) {
        console.log('ok');
        passes++;
    } else {
        console.log('FAILED');
    }
});

console.log(`${tests} tests, ${passes} passed, ${tests-passes} failed...`);

