#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.22
'use strict';

const assert = require('assert');
const TestSuiteRunner = require('./TestSuiteRunner');
const CappedArray = require('./CappedArray');

const testInstance = function(callback) {
    let cap = new CappedArray();

    assert.ok(cap, 'should have an instance');
    assert(cap instanceof CappedArray);
    assert(cap.getSize() === 20, 'default size should be 20');

    cap = new CappedArray(10, 1);
    assert.ok(cap, 'should have an instance');
    assert(cap.getSize() === 10, 'new size should be 10');

    for (let idx = 0; idx < 10; idx++) {
        let val = cap.peek(idx);
        assert(val === 1, `value at index ${idx} should be a one`);
    }

    callback();
};

const testPut = function(callback) {
    let cap = new CappedArray(3);

    for (let n = 0; n < 8; n++) {
        let value = Math.random() * 100;
        let idx = cap.put(value);
        // console.log(n, idx);
        assert(idx === (n % 3), 'first item addeed');

        assert(cap.peek(idx) === value, 'retrieved value should match');
    }

    callback();
};

const testPeek = function(callback) {
    let cap = new CappedArray();

    let value = Math.random() * 20;
    let idx = cap.put(value);
    assert(idx === 0, 'first item addeed');
    assert(cap.peek(idx) === value, 'value should match');

    // TODO: test with negative index...

    callback();
};

const testGetAll = function(callback) {
    try {
        const sz = 22;
        let cap = new CappedArray(sz);

        for (let n = 0; n < sz; n++) {
            cap.put(Math.random() * 45);
        }

        let list = cap.getAll();
        // console.log( list );
        assert(list.length === sz, 'sizes should match');

        list.pop();
        assert(list.length === sz - 1, 'capped list size should remain the same');
        assert(cap.getSize() === sz, 'capped list size should remain the same');

        callback();
    } catch (e) {
        callback( e );
    }
};

const testIterator = function(callback) {
    const sz = 12;
    let cap = new CappedArray(sz, 1);

    let iter = cap.iterator();
    let count = 0;
    let it = iter.next()
    do {
        // console.log( it.value );
        assert(count < sz, 'count should always be less than the size');
        assert( cap.peek( it.value ) === 1, 'all values should be 1');
        count++;
        it = iter.next();
    } while (it.done !== true);

    assert(it.done, 'should be false');

    callback();
};

const stats = {
    name: 'CappedArrayTests',
    tests:[ testInstance, testPut, testPeek, testGetAll, testIterator ],
    testCount:0,
    errorCount:0
};

new TestSuiteRunner( stats );

