#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.30
'use strict';

// each cell of the maze is defined by row,col where 0,0 is the upper left down to 7,7 as lower right
// mark the blocked cells with a 1 bit

const RouterGame = function() {
    const game = this;
    const ColSize = 8;
    const RowSize = 8;

    const maze = Uint8Array.from({length:8})

    maze[0] = 0b11111011;
    maze[1] = 0b10000001;
    maze[2] = 0b10111101;
    maze[3] = 0b10101001;
    maze[4] = 0b10101101;
    maze[5] = 0b10101001;
    maze[6] = 0b10000001;
    maze[7] = 0b11111111;

    this.getCell = function(row, col) {
        const cell = maze[row] >> (7-col) & 0b01;

        return cell;
    };

    this.getExitCell = function() {
        return [ 0, 5 ];
    };

    // return [ row, col ] if an up move is possible, else false
    this.moveUp = function(row, col) {
        row--;
        if (row > 0) {
            const cell = game.getCell(row, col);
            if (cell == 0) {
                return [ row, col ];
            }
        }
        return false;
    }

    // return an array of row, col coordinates of the adjacent cells
    this.getAdjacents = function(row, col) {
        let adjacents = [];
        // up?
        // down?
        // left?
        // right

        return adjacents;
    };

    this.getBoardSize = function() {
        return [ RowSize, ColSize ];
    };
};

const Tests = function() {
    const suite = this;
    const game = new RouterGame();
    let tests = 0;
    let errors = 0;

    const [ rowSize, colSize ] = game.getBoardSize();

    this.run = function() {
        suite.testGameBoard();

        console.log(`\nTests completed: ${tests}, errors: ${errors}`);
    };

    function assert(s, f) {
        tests++;
        if (!f) {
            errors++;
            console.log(`ERROR: ${s}`);
        }
    }

    this.testGameBoard = function() {
        console.log('test the game board...');

        let cell = game.getExitCell();
        assert('exit cell = [0,5]', (cell[0] == 0 && cell[1] == 5));

        // verify perimeter is blocked with the exception of exit cell
        for (let row = 0; row < rowSize; row++) {
            cell = game.getCell(row, 0);
            assert('left side should be closed', cell === 1);
            cell = game.getCell(row, colSize - 1);
            assert('right side should be closed', cell === 1);
            if (row === 0 || row === rowSize - 1) {
                // check top and bottom
                for (let col = 1; col < colSize - 2; col++) {
                    cell = game.getCell(row, col);
                    if (row === 0 && col === 5 ) {
                        assert(`exit should be open but is ${cell}`, cell === 0);
                    } else {
                        assert(`top/bottom side should be closed but is ${cell}`, cell === 1);
                    }
                }
            }
        }

        // verify perimeter - 1 is open
    }
};

const tests = new Tests();
tests.run();
