#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.21
'use strict';

const arr = [ 'this thing', 'that thing', 'the other thing' ];

let item = arr.find((v, idx) => { return v === 'that thing' });
console.log( 'item: ', item);

let notfound = arr.find(val => { return val === 'other thing' });
console.log( 'notfound: ', notfound);
