#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.22
'use strict';

// print numbers between 1 and 100

const show = (n) => console.log( n );

console.log( 'with recursion:' );
const pn = (from, to) => {
    if (from > to) {
        return;
    }

    show( from );
    pn(from + 1, to);
};

pn(1, 10);

// or, like this

console.log( 'by creating an array (from):' );
const fromto = Array.from({length:10}, (n, i) => i + 1);

console.log( fromto.join('\n') );

