#!/usr/bin/env node --harmony

// dpw@seattle.local
// 2016.02.01
'use strict';

function sum(...values) {
    // simple with fat-arrow and reduce
    return values.reduce(( prev, curr ) => prev + curr );
}

console.log( sum( 2, 3, 6, 4, 22 )); // 37

