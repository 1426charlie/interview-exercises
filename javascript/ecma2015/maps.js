#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
'use strict';

let map = new Map();

// add a method to map but not to the prototype
map.show = function() {
    let s = [];

    for (var key of this.keys()) {
        s.push( `${key}=${this.get( key )}` );
    }

    return s.join(', ');
};

map.set('a', 'my a thing');
map.set(5, 'my five number');
map.set(new Date().toJSON(), 'my date...');

console.log(`map: ${map.show()}, size: ${map.size}`);
console.log('get 5:', map.get(5));

map.forEach((value, key) => console.log(`key:${key}, value:${value}`));

let iter = map.entries();

let kv = iter.next().value;
while (kv) {
    console.log( kv, ' done=', iter.done );
    kv = iter.next().value;
}
console.log( 'done=', iter.done );

