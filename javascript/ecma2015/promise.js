#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.01

/**
 * Objective: using promises, read a list of files asynchronously and report the file
 * name/sizes in the order that they were submitted.
 *
 * Note: I'm not sure that using promises is an improvement over simple callbacks for this...
 */

'use strict';

const fs = require('fs');

// returns a promise
const readFile = function(filename) {
    console.log('create promise read file: ', filename);
    return new Promise((resolve, reject) => {
        fs.readFile(filename, function(err, data) {
            if (err) {
                return reject( err );
            }

            return resolve( { filename:filename, text:data.toString(), length:data.length } );
        });
    });
};

let filelist = [ 'promise.js', 'generator.js', 'iterator.js' ];
let promises = filelist.map(file => readFile(file) );

let p = Promise.all( promises );

p.then(values => {
    if (values.length !== filelist.length) {
        throw new Error('not all files were processed...');
    }

    // now store the data in a map
    let map = new Map();
    values.forEach(v => {
        map.set( v.filename, { 
            text: v.text,
            length: v.length
        });
    });

    // show in the correct order
    filelist.forEach(key => {
        let v = map.get( key );
        console.log(`file ${key} length: ${v.length}...`)
    });
}).catch(err => {
    console.log(err);
});

