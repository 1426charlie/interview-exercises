#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
'use strict';

function* anotherCount(i) {
    yield i + 1;
    yield i + 2;
    yield i + 3;
}

function *count(value) {
    if (!value) {
        value = 1;
    }

    while(value < 10) {
        yield value;
        value++;

        if (value === 4) {
            // delegating generator
            yield* anotherCount(500);
        }
    }
}

// this iteration works with the full iterator object
let iterator = count();
let obj = iterator.next();
while (obj.done === false) {
    console.log( obj );
    obj = iterator.next();
}

// this just returns the values
for (let v of count()) {
    console.log( v );
}

