#!/usr/bin/env node --harmony

// dpw@seattle.local
// 2016.02.01
'use strict';

// this exammple was take from the V8 Release 4.9 page but is broken even on node 5.5

let obj = { a: [1, 2, 3], b: { p: 4 }, c: { q: 5 }};
let { a: [ x, y ], b: {p}, c, d } = obj;

console.log( obj );
console.log( x, y, p, c );

obj = { version:2, order:[ 2, 3, 4] };
let { version, order } = obj;

console.log(version, order);

