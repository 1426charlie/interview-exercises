#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.02
'use strict';

// implement closure to memoize the count
function CreateCounter() {
    var count = 0;

    return {
        value: function() {
            return count;
        },
        increment: function() {
            return count++;
        }
    };
};

// this is lame...
class CounterClass {
    constructor(initialCount) {
        this.values = {
            count: Number.isInteger( initialCount ) ? initialCount : 0 
        };
    }

    increment() {
        return this.values.count++
    }

    value() {
        return this.values.count;
    }
}

let counter = new CounterClass();
while (counter.value() < 6) {
    console.log( counter.increment() );
};

counter = CreateCounter();
while (counter.value() < 5) {
    console.log( counter.increment() );
};
