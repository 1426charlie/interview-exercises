#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
'use strict';

let foo = new Set();

// add a helper function to the set object
foo.show = function() {
    var s = [];
    for (let item of this) s.push( item );
    return s.join(', ');
};

foo.add(1);
foo.add(2);
foo.add(3);

console.log(`has: ${foo.has(1)} should be true...`);

console.log( `foo: ${foo.show()}, size: ${foo.size}` );
foo.add(2);
console.log( `foo: ${foo.show()}, size: ${foo.size}` );

const iter = foo.values();

for(let value of foo.values()) {
    console.log('iter value =', value);
}

foo.clear();
console.log( `foo: ${foo.show()}, size: ${foo.size}` );

