#!/usr/bin/env node

// dpw@oakland.local
// 2016.02.02
'use strict';

class Prime {
    constructor() { }

    isPrime(n, start) {
        start = Number.isInteger( start ) ? start : 2;

        let nn = Math.sqrt( n );

        while (start <= nn) {
            let m = n % start++;
            if (m < 1) {
                return false;
            }
        }

        return n > 1;
    }

    findFirst(start) {
        while (this.isPrime( start, start ) === false) {
            start++;
        }

        return start;
    };
}

let p = new Prime();

console.log( `is 29 a prime number? ${p.isPrime( 29 ) ? 'yes' : 'no way'}`);

let numbers = [ 4, 6, 8, 21, 24, 26, 8999993 ];

let prime = numbers.find( p.isPrime );
console.log(`find first prime from: ${numbers}: prime: ${prime}`);

// let bigprime = 94906271 + 1;
let bigprime = 100000000;
for (let i = 0; i < 10; i++) {
    prime = p.findFirst( bigprime );

    console.log( `first prime >= ${bigprime} is ${prime}`);

    bigprime = prime + 2;
}
