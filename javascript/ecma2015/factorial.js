#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.22
'use strict';

const fac = (n) => n === 1 ? 1 : n * fac( n - 1);

const list = [ 5, 2, 4, 20 ];

console.log( list.map(n => [ n, fac(n) ] ));

