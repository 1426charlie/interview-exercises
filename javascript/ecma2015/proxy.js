#!/usr/bin/env node --harmony

// dpw@seattle.local
// 2016.02.21
'use strict';

const Target = function() {
    const target = this;

    this.sayHi = function() {
        console.log('hello');
    };

    this.props = {
        a:'a thing',
        b:'b thing'
    };
};

let target = new Target();
let handler = {
    get: function(target, name) {
        return name in target ? target[name] : null;
    }
};

let p = new Proxy(target, handler);

console.log(p.props);
console.log(p.sayHi());

console.log(p.mything);

