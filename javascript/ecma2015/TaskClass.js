#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
'use strict';

class Task {

    constructor(name) {
        this.name = name;

        var completed = false;

        this.getCompleted = function() {
            return completed;
        };
        this.setCompleted = function(value) {
            completed = value === true;
        }
    };


    complete() {
        this.setCompleted(true);
        console.log(`complete task: ${this.name} completed: ${this.getCompleted()}`);
    };

    save() {
        console.log(`save task: ${this.name}`);
    };

    toString() {
        return `Task: ${this.name}, completed: ${this.getCompleted()}`;
    };
}

let task1 = new Task('create task A');
let task2 = new Task('create task B');

console.log( task1.toString());
console.log( task2.toString());

task1.complete();
task2.complete();
task1.save();
task2.save();
