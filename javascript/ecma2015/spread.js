#!/usr/bin/env node --harmony

// dpw@seattle.local
// 2016.01.31
'use strict';

// requires harmony or node 5...

let parts = ['shoulders', 'knees'];
let lyrics = ['head', ...parts, 'and', 'toes' ];

console.log( lyrics.join(' ') );
