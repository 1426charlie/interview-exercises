#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
// Fn = Fn-1 + Fn-2
'use strict';

function* fibonacci() {
    let a = 0,
        b = 1; 

    while (true) {
        let c = a;

        a = b;
        b = c + a;

        yield c;
    }
}

let iter = fibonacci();

let i = 0;
while(i++ < 40) {
    console.log( iter.next().value );
}

