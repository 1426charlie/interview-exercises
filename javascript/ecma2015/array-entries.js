#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.21
'use strict';

const arr = [ 5, 23, 993, 2, -45, 22 ];

let entries = arr.entries();

for (let indexValue of entries) {
    console.log( 'entry: ', indexValue[0], indexValue[1] );
}

for (let idx of arr.keys()) {
    console.log('idx: ', idx, ', value: ', arr[ idx ]);
}
