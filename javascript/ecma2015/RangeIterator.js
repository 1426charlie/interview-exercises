#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.31
'use strict';

function RangeIterator(min, max) {
    this[Symbol.iterator] = function() {
        let current = min;

        return {
            next: function() {
                let obj = {
                    done: current >= max,
                    value: current
                };

                current++;
                return obj;
            }
        };
    };
}

const start = 1, end = 10;

console.log(`a range iterator class implementation between ${start} and ${end}...`);
for (let i of new RangeIterator(start, end)) {
    console.log( i );
}

// or this way...
console.log(`as a generator function between ${start} and ${end}...`);
function *rangeIter(min, max) {
    for (let i = min; i < max; i++) {
        yield i;
    }
}

for (let i of rangeIter(start, end)) {
    console.log(i);
}

