#!/usr/bin/env node

// dpw@seattle.local
// 2016.02.02
'use strict';

// better to use Array.of()
function arrayFromArgs() {
    const args = Array.from( arguments );

    return args;
}

let args = arrayFromArgs(1, 3, 55, "bar") ;
console.log( args );

let n = -1;

console.log( args.slice(n));
console.log( args.slice(--n, n + 1) );
console.log( args.slice().reverse() );

args = Array.of('foo', 'bar', 'baz');
console.log(args.sort());

args.filter((v, idx) => console.log(`idx=${idx}, v=${v}`));

function isPrime(n) {
    let start = 2,
        nn = Math.sqrt( n );

    while (start <= nn) {
        let m = n % start++;
        if (m < 1) {
            return false;
        }
    }

    return n > 1;
}

let numbers = [ 4, 6, 8, 21, 24, 26, 8999993 ];
let prime = numbers.find( isPrime );
console.log(`find prime from: ${numbers}: prime: ${prime}`);

