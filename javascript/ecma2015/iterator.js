#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
'use strict';

for (let value of ['a','b','c']) {
    console.log( value );
}

function* gen() {
    // use the spread operator on yield
    yield* ['a','b','c']
}

const it = gen();
while (it.done === false) {
    let v = it.next();
    console.log('it: ', v);

};

