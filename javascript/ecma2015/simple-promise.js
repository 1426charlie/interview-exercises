#!/usr/bin/env node

// dpw@oakland.local
// 2016.03.14
'use strict';

const db = [ 31, 25, 32, 52, 94 ];

// simulate accessing data from an asynchronous source
db.query = function(min) {
    return new Promise((resolve, reject) => {
        setImmediate(() => {
            let values = db.filter(n => n > min);

            if (values.length === 0) {
                return reject( new Error(`zero numbers found with min: ${min}`));
            }

            return resolve( values );
        });

    });
}


db.query( 100 )
    .then(values => console.log( values ))
    .catch(err => console.log('q1 err: ', err));

db.query( 35 )
    .then(values => console.log('q2: ', values ))
    .catch(err => console.log('err: ', err));

db.query( 10 )
    .then(values => console.log('q3: ', values ))
    .catch(err => console.log('err: ', err));


// these run in series but parallel to the queries above
let list = [ 10, 45, 104, 31, 80 ];
(function next() {
    let n = list.shift();
    if (n) {
        db.query( n )
        .then(v => {
            console.log(`next: ${n}=${v}`);
            next();
        })
        .catch(err => {
            console.log('next err: ', err)
            next();
        });
    }
}());

