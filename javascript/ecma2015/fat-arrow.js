#!/usr/bin/env node

// dpw@seattle.local
// 2016.01.30
'use strict';

let foo = [ 'hello', 'es6', 'world' ];
let bar = foo.map(v => v.length);
console.log(foo, bar);

// define an object literal
const myfn = () => ({ propA:'my thing', propB:'my other thing' });
console.log( myfn() );


// use fat arrow to replace function() in callbacks
class Message {
    constructor(text) {
        this.text = text
    };

    callback(value) {
        console.log('callback: ', value);
    };

    talk() {
        // if this was done the traditional way, 'this' would be lost/undefined...
        process.nextTick(() => this.callback(this.text));
    };
};

let message = new Message('this is a longer message...');
message.talk();

let square = x => x * x ;
let add = (a, b) => a + b ;

console.log( `should = 25, ${square( 5 )}`);
console.log( `should also = 25, ${add( 10, 15 )}`);

let Car = function(name) {
    this.name = name;
    this.startDriving = function() {
        console.log(`hey ${this.name}, here we go!!!`);
    };
}

console.log('implementation of setTimeout with fat-arrow...');
Car.prototype.start = function() {
    setTimeout(() => this.startDriving(), 100);
};

let car = new Car('ford');
car.start();

console.log('implementation of array.map with fat-arrow...');
let list = [ 1, 2, 3 ];
console.log( list.map(n => n + 2) );
console.log( list.reduce( (prev, curr) => prev + curr ));
console.log( list.filter(n => n !== 2));
