#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.05
'use strict';

const ramda = require('ramda');

const puts = console.log;

const list = [ 1, 2, 3, 4, 5 ];
const plus5 = ramda.add(5);
const list5 = ramda.map(plus5, list)
const isEven = x => x % 2 === 0;
const fullList = ramda.concat(list, list5)

puts('even list from', fullList);
ramda.forEach(puts, ramda.filter(isEven, fullList));

puts('odd list from', fullList);
ramda.forEach(puts, ramda.reject(isEven, fullList));

const add = (accum, value) => accum + value;
puts('list add', list);
puts(ramda.reduce(add, 0, list));

const square = x => x * x;
const multiply = (a, b) => a * b;

const operate = ramda.pipe(multiply, ramda.inc, square);

puts('pipe mult ((5x3)+1)^2 =', operate(5, 3));

const books = Object.freeze([ { title:"Gone with the Wind", year:1934 }, { title:"Grok This", year:1984 }, { title:"Postcards from the Edge", year:1984 } ]);
const yr = 1984;

// ramda curry
const publishedInYear = ramda.curry((year, book) => book.year === year);
const titlesForYear = ramda.curry((year, books) => {
    return ramda.pipe(
        ramda.filter(publishedInYear(year)),
        ramda.map(book => book.title)
    )(books);
});

puts('curry published 1984:', titlesForYear(yr, books));

// or with es6 filter/map (I think this is better...)
let pubs = books.filter(book => publishedInYear(yr, book)).map(book => book.title);
puts('es6 published 1984  :', pubs );

// or with es6 reduce (not pure)
pubs = books.reduce((list, book) => {
    if (book.year === yr) {
        list.push(book.title);
    }
    return list;
}, []);

puts('es6 published 1984  :', pubs );

puts('line width from deftaultTo:', ramda.defaultTo(80, books.lineWidth));

const water = ramda.cond([
    [ramda.equals(0), ramda.always('water freezes at 0C')],
    [ramda.equals(100), ramda.always('water boils at 100C')],
    [ramda.T,   temp => `nothing special at ${temp}C`]
]);

puts(water(5));
puts(water(0));
puts(water(100));
puts(water(101));

const isOver18 = person => ramda.gte(ramda.prop('age', person), 18);

const numbers = Array.from({length:8}, (x,i) => ramda.inc(i) * 10);
puts(numbers);
puts(`first element is ${ramda.head(numbers)}`);
puts(`second element is ${ramda.nth(1, numbers)}`);
puts(`last element is ${ramda.nth(-1, numbers)}`); // or last...
puts(`take 3 ${ramda.take(3, numbers)}`);

const zrange = ramda.range(0);
puts('zrange:', zrange(10));
