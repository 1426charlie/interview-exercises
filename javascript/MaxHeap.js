#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.19
'use strict';


// create a new node at the end of the heap (push)
// compare with parent (p = (idx - 1) / 2)
// if parent LT child, then swap [ heap[pidx], heap[idx] ] = [ heap[idx], heap[pidx] ]

const MaxHeap = function(options = {}) {
    const debug = options.debug;

    let heap = [];
    
    // add an item to the heap;
    this.add = function(data) {
        data = parseInt(data);
        heap.push(data);

        let len = heap.length;
        if (len < 2) {
            return;
        }
        let idx = len - 1;

        let parent = (idx - 1) >> 1;
        while (heap[parent] < heap[idx]) {
            [ heap[parent], heap[idx] ] = [ heap[idx], heap[parent] ];

            // next parent
            idx = parent;
            parent = (idx - 1) >> 1;
            if (parent < 0) {
                break;
            }
        }
    };

    function findLargestChild(root) {
        const len = heap.length;
        let [ left, right ] = [ (root * 2) + 1, (root * 2) + 2 ];
        let maxidx = null;

        if (left < len && heap[root] < heap[left]) {
            maxidx = left;
        } 

        if (right < len && heap[root] < heap[right]) {
            if (maxidx && heap[maxidx] < heap[right] || heap[left] < heap[right]) {
                maxidx = right;
            }
        }

        return maxidx;
    }

    // remove and return the top of the heap and re-adjust the heap
    this.pop = function() {
        switch (heap.length) {
        case 0:
            throw new Error('ERROR! attempt to pop an empty heap');
        case 1:
            return heap.pop();
        }

        let root = 0;

        const returnValue = heap[root];
        heap[root] = heap.pop();

        let idx = findLargestChild(root);
        while (idx !== null) {
            [ heap[root], heap[idx] ] = [ heap[idx], heap[root] ];
            root = idx;
            idx = findLargestChild(root);
        }

        return returnValue;
    };

    this.peek = function() {
        return heap[0];
    };

    this.clear = function() {
        heap.splice(0);
    };

    this.checkTop = function() {
        const top = heap[0];
        const max = heap.reduce((a, b) => Math.max(a, b), Number.MIN_SAFE_INTEGER);
        return top === max;
    };

    this.size = function() {
        return heap.length;
    };

    this.toString = function() {
        return heap.slice(0,5).join(', ');
    }
};

module.exports = MaxHeap;

if (process.argv[1].endsWith('MaxHeap.js')) {
    let input  = [ 35, 33, 42, 10, 14, 19, 27, 44, 26, 31 ];
    let expect = [ 44, 42, 35, 33, 31, 19, 27, 10, 26, 14 ];
    const heap = new MaxHeap({ debug:true });

    input.forEach(v => {
        heap.add(v);
        console.log(heap.toString());
    });

    if (heap.size() !== expect.length) {
        throw new Error("heap is the wrong size...");
    }

    let top = heap.pop();
    console.log(`old top: ${top}, new top: ${heap.peek()}`);
    console.log(heap.toString());

    heap.clear();
    if (heap.size() !== 0) {
        throw new Error("heap should be clear...");
    }

    input = [ 20555, 20535, 148, 17738, 13449, 62995 ];
    expect = '20555, 20535, 148, 17738, 13449';
    input.forEach(v => {
        heap.add(v);
        console.log(v, ':', heap.toString());
    });

    top = heap.pop();
    if (heap.toString() != expect) {
        throw new Error(`heap is out of wack: ${heap.toString()}`);
    }
    console.log('popped:', heap.toString());

}

