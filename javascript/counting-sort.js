#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.05
'use strict';

/*\
    1) count the histogrom of key frequencies
    2) calculate the starting index for each key
    3) copy to output preserving order of inputs with equal keys

\*/

const data = [
    [ 11, 30, 19, 9, 29, 28, 25, 23, 26, 13, 16, 20, 19, 16, 14, 20, 12, 28, 25, 1, 2, 16, 4, 29, 2, 3, 30, 5, 21, 10 ],
    "63 25 73 1 98 73 56 84 86 57 16 83 8 25 81 56 9 53 98 67 99 12 83 89 80 91 39 86 76 85 74 39 25 90 59 10 94 32 44 3 89 30 27 79 46 96 27 32 18 21 92 69 81 40 40 34 68 78 24 87 42 69 23 41 78 22 6 90 99 89 50 30 20 1 43 3 70 95 33 46 44 9 69 48 33 60 65 16 82 67 61 32 21 79 75 75 13 87 70 33".split(' ')
];

console.log(data[1], data[1].length);


