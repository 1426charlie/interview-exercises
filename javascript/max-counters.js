#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.06
'use strict';

/*
You are given N counters, initially set to 0, and you have two possible operations on them:

increase(X) − counter X is increased by 1,
max counter − all counters are set to the maximum value of any counter.
A non-empty zero-indexed array A of M integers is given. This array represents consecutive operations:

if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
if A[K] = N + 1 then operation K is max counter.
For example, given integer N = 5 and array A such that:

    A[0] = 3
    A[1] = 4
    A[2] = 4
    A[3] = 6
    A[4] = 1
    A[5] = 4
    A[6] = 4
the values of the counters after each consecutive operation will be:

    (0, 0, 1, 0, 0)
    (0, 0, 1, 1, 0)
    (0, 0, 1, 2, 0)
    (2, 2, 2, 2, 2)
    (3, 2, 2, 2, 2)
    (3, 2, 2, 3, 2)
    (3, 2, 2, 4, 2)
The goal is to calculate the value of every counter after all operations.

Write a function:

function solution(N, A);
that, given an integer N and a non-empty zero-indexed array A consisting of M integers, returns a sequence of integers representing the values of the counters.

The sequence should be returned as:

a structure Results (in C), or
a vector of integers (in C++), or
a record Results (in Pascal), or
an array of integers (in any other programming language).
For example, given:

    A[0] = 3
    A[1] = 4
    A[2] = 4
    A[3] = 6
    A[4] = 1
    A[5] = 4
    A[6] = 4
the function should return [3, 2, 2, 4, 2], as explained above.

Assume that:

N and M are integers within the range [1..100,000];
each element of array A is an integer within the range [1..N + 1].
Complexity:

expected worst-case time complexity is O(N+M);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

function log(msg) {
    console.log(msg);
}

function equals(a, b) {
    if (a.length !== b.length) {
        return false;
    }
    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
            return false;
        }
    }
    return true;
}

function solution(N, A) {
    // A is the control array
    const len = A.length;

    // create N counters set to zero
    let counters = Array.from({length:N}, (x) => 0);

    // log(`initial counters: ${counters}`);

    let nplus1 = N + 1;
    let lastMax = 0;
    let max = 0;

    for (let i = 0; i < len; i++) {
        if (A[i] < nplus1) {
            let idx = A[i] - 1;
            if (counters[idx] < lastMax) {
                counters[idx] = lastMax;
            }

            counters[idx]++;
            if (max < counters[idx]) {
                max = counters[idx];
            }
        } else if (lastMax != max) {
            // if the max has changed, then update, else skip
            lastMax = max;
        }

        // log(`counters: ${counters}`);
    }

    // this could be a map function but it increases the memory footprint...
    for (let i = 0; i < counters.length; i++) {
        if (counters[i] < lastMax) {
            counters[i] = lastMax;
        }
    }

    return counters;
}

const testdata = [
    { n: 5, list: [3,4,4,6,1,4,4], expect: [3,2,2,4,2] },
];

let errors = [];
testdata.forEach(obj => {
    const { n, list, expect } = obj;
    const result = solution(n, list);
    const ok = equals(result, expect)
    console.log(n, list, expect, result, ok );
    if (!ok) {
        let msg = `ERROR! expected ${expect} got ${result}`;
        errors.push(msg);
        console.log( msg );
    }
});

if (errors.length > 0) {
    console.log(`\nTESTS FAILED: ${errors.length} errors!\n`);
    errors.forEach(m => console.log(m));
}
