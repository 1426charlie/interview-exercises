#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.15
'use strict';

/*
A non-empty zero-indexed array A consisting of N integers is given.

The leader of this array is the value that occurs in more than half of the elements of A.

An equi leader is an index S such that 0 ≤ S < N − 1 and two sequences A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N − 1] have leaders of the same value.

For example, given array A such that:

    A[0] = 4
    A[1] = 3
    A[2] = 4
    A[3] = 4
    A[4] = 4
    A[5] = 2
we can find two equi leaders:

0, because sequences: (4) and (3, 4, 4, 4, 2) have the same leader, whose value is 4.
2, because sequences: (4, 3, 4) and (4, 4, 2) have the same leader, whose value is 4.
The goal is to count the number of equi leaders.

Write a function:

function solution(A);
that, given a non-empty zero-indexed array A consisting of N integers, returns the number of equi leaders.

For example, given:

    A[0] = 4
    A[1] = 3
    A[2] = 4
    A[3] = 4
    A[4] = 4
    A[5] = 2
the function should return 2, as explained above.

Assume that:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/


function findLeader(input, debug) {
    const len = input.length;
    let size = 0;
    let value;

    for (let i = 0; i < len; i++) {
        if (size === 0) {
            size++;
            value = input[i];
        } else {
            if (value === input[i]) {
                size++;
            } else {
                size--;
            }
        }
    }

    let candidate = -1;
    if (size > 0) {
        candidate = value;
    }

    let count = 0;
    for (let i = 0; i < len; i++) {
        if (input[i] === candidate) {
            count++;
        }
    }

    let hasLeader = true;
    if (count <= (len / 2)) {
        hasLeader = false;
    }

    return [ hasLeader, candidate, count ];
}

function solution(input, debug) {
    const len = input.length;

    // first, find a leader and it's total occurrences...
    const [ hasLeader, candidate, total ] = findLeader(input, debug);
    if (!hasLeader) {
        return 0;
    }

    // count the groups
    let equiLeaders = 0;
    let count = 0;
    for (let i = 0; i < len; i++) {
        if (input[i] === candidate) {
            count++;
        }

        // calc the total possible leaders to the right of the index
        let leadersInRightPart = (total - count);

        // now determine if this sub-group has a leader
        if (count > (i + 1) / 2 && leadersInRightPart > (len - i - 1) / 2) {
            equiLeaders++;
        }
    }

    return equiLeaders;
}

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

const testdata = [
    // input data, count of equi-leaders
    { A:[ 4, 3, 4, 4, 4, 2 ], expect:2, debug:true },
    { A:[ -1000000000, -1000000000] , expect:1, debug:true },
    { A:[ 10, 10 ], expect:1, debug:true },
    { A:[ -10, -11 ], expect:0, debug:true },
    { A:[ 10, 11, 11, 11 ], expect:1, debug:true },
    { A:[ 10, 11, 11, 11, 10, 11 ], expect:2, debug:true },
];

testdata.forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        return solution(A.slice(), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
    if (!ok) {
        errors++;
    }
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running xx test...');
    test_runner(() => solution(A));
}

console.log('\n', tests, 'tests completed', errors, 'errors', '\n');
