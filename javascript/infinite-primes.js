#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.05
'use strict';

const isPrime = function(n) {
    if (n == 2) {
        return true;
    } 
    if (n < 2 || (n % 2) == 0) {
        return false;
    }

    let top = Math.sqrt(n)
    for (let i = 3; i <= top; i += 2) {
        if (n % i == 0) {
            return false;
        }
    }

    return true
};

// combine generator and sieve of Eratosthenes to calculate all the primes into infinity...
function* primeGenerator(n) {
    if (!n) {
        n = 2;
    }

    // return the easy ones...
    let prime = n;

    while (true) {
        // find the next prime 
        while (isPrime(prime) != true) {
            prime++;
        }

        yield prime;
        prime++;
    }
}

// const start = 4503599627378581;
// const start = Math.floor(Number.MAX_SAFE_INTEGER / 3);
// const start = 1000000000000000;
const start = 1e14; // a 14 digit number...
let gen = primeGenerator(start)
for (let i = 0; i < 100; i++) {
    const p = gen.next();
    console.log(p.value);
}

