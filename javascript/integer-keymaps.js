#!/usr/bin/env node

// dpw@alameda.local
// 2016.12.18
'use strict';

const blockSize = 8
function createKey(n) {
    return n >> blockSize;
}

function calcMedian(size, map) {
    center = size >> 1;
    let count = 0;
    for (list in map.values)
}

function testit() {
    let medians = [];
    let map = new Map();
    const t0 = Date.now();
    let count = 0;
    for (let i = 0; i < 1e5; i++) {
        let n = Math.round(Math.random() * 1e5);
        const key = createKey(n);
        if (map.has(key)) {
            let list = map.get(key);
            list.push(n);
            list.sort((a,b) => a-b);
        } else {
            // console.log('new key: ', key);
            map.set(key, [ n ]);
        }

        count++;
        medians.push(calcMedian(count, map));
    }

    const t1 = Date.now()
    map.forEach((k, v) => {
        console.log(k, v);
    });

    console.log('map size:', map.size, 'count:', count, 'elapsed:', (t1-t0)/1000);

}

testit();
