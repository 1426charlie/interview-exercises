#!/bin/sh
# dpw@seattle.local
# 2016.11.09
#

if [ $# != 1 ]
then 
	echo "Error in command line..."
	echo "USE: mkjs command"
else
	echo "#!/usr/bin/env node

// dpw@`uname -n`
// `date +%Y.%m.%d`
'use strict';

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

createTestData().forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        if (A instanceof Array) {
            return solution(A.slice(), debug);
        } else {
            return solution(A, debug);
        }
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running xx test...');
    test_runner(() => solution(A));
}

/** problem description & examples...

 */

// test data
function createTestData() {
    return [
        { A:[], expect:0, debug:true },
    ];
}

function solution(input, debug) {
    if (debug) console.log( input );
}

console.log(tests, 'tests completed', errors, 'errors');
console.log('...');

" > $1
	chmod a+x $1
	vi + $1
fi
