#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.15
'use strict';

/*
A non-empty zero-indexed array A consisting of N integers is given. A pair of integers (P, Q), such that 0 ≤ P ≤ Q < N, 
is called a slice of array A. The sum of a slice (P, Q) is the total of A[P] + A[P+1] + ... + A[Q].

Write a function:

function solution(A);
that, given an array A consisting of N integers, returns the maximum sum of any slice of A.

For example, given array A such that:

A[0] = 3  A[1] = 2  A[2] = -6
A[3] = 4  A[4] = 0
the function should return 5 because:

(3, 4) is a slice of A that has sum 4,
(2, 2) is a slice of A that has sum −6,
(0, 1) is a slice of A that has sum 5,
no other slice of A has sum greater than (0, 1).
Assume that:

N is an integer within the range [1..1,000,000];
each element of array A is an integer within the range [−1,000,000..1,000,000];
the result will be an integer within the range [−2,147,483,648..2,147,483,647].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

function solution(input, debug) {
    const max = Math.max;
    const len = input.length;
    if (debug) console.log( input );

    if (len === 0) {
        return 0;
    }

    let max_ending = null;
    let max_slice = null;

    input.forEach(x => {
        if (max_ending === null) {
            // initialize ending and slice maxes
            max_ending = x;
            max_slice = x;
        } else {
            max_ending = max(x, max_ending + x);
            max_slice = max(max_ending, max_slice);
            if (debug) console.log('x', x, 'mend', max_ending, 'mslice', max_slice);
        }
    });

    return max_slice;
}

const MAX_LENGTH = 100000;
const run_stress = false;
let errors = 0;
let tests = 0;

function test_runner(fn, expect, timeout=0.80) {
    tests++;
    const t0 = Number(process.hrtime().join('.'));
    const result = fn();
    const t1 = Number(process.hrtime().join('.'));
    const elapsed = t1 - t0; 
    console.log('completed in ', elapsed, 'seconds, result:', result);

    if (expect && expect !== result) {
        errors++;
        console.log('ERROR! expected', expect, 'got', result);
    }   

    if (elapsed > timeout) {
        errors++
        console.log('ERROR! elapsed:', elapsed);
    }   

    return result;
}

const testdata = [
    { A:[ 5, -7, 3, 5, -2, 4, -1 ], expect:10, debug:false },
    { A:[ 3, 2, -6, 4, 0 ], expect:5, debug:false },
    { A:[], expect:0, debug:false },
    { A:[17], expect:17, debug:false },
    { A:[-10], expect:-10, debug:false },
    { A:[ -2, 2, -6, 0, -50 ], expect:2, debug:false },
    { A:[ 1, -3, 2, 1, -1 ], expect:3, debug:false },
    { A:[ -1, -2, 3, 4, -5, 6 ], expect:8, debug:false },
    { A:[ -2, 1, -3, 4, -1, 2, 1, -5, 4 ], expect:6, debug:false },
    { A:[2, 3, -1, -20, 5, 10], expect:15, debug:false },
];

testdata.forEach(data => {
    const { A, expect, debug } = data;
    const test = function() {
        return solution(A.slice(), debug);
    }

    const result = test_runner(test, expect);

    const ok = result === expect;
    console.log('result =>', A, expect, result, ok);
    if (!ok) {
        errors++;
    }
});

if (run_stress) {
    const A = Array.from({length:MAX_LENGTH}, (x) => 0);
    console.log('running xx test...');
    test_runner(() => solution(A));
}

console.log('\n', tests, 'tests completed', errors, 'errors', '\n');
