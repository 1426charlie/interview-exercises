#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.07
'use strict';

/*
A mushroom picker is at a spot 'k' on the road and should perform 'm' moves.  In one
move she moves to an adjacent spot.  She collects all the mushrooms growing on spots
she visits.  The goal is to calculate the maximum number of mushrooms that the mushroom
picker can collect in 'm' moves.

1) only allow a single switch of direction
*/

// calc the prefix sums and return P
function prefix_sums(A) {
    const len = A.length + 1;
    let P = Array.from({length:len}, (x) => 0)

    // calculate the totals
    for (let i = 1, j = 0; i < len; i++, j++) {
        P[i] = P[j] + A[j];
    }

    return P;
}

function count_total(P, x, y) {
    return P[y + 1] - P[x];
}

function pick_mushrooms(A, k, m) {
    const len = A.length;
    const prefix = prefix_sums(A);
    let mushrooms = 0;

    console.log(`prefix sums: ${prefix}`);
    console.log(`current position: ${k} with ${m} alocated moves`);

    // first move to the right to search for the max mushrooms
    const right_stride = Math.min(m, k) + 1;
    console.log(`right stride: ${right_stride}`);
    for (let p = 0; p < right_stride; p++) {
        const left_p = k - p;
        const right_p = Math.min(len - 1, Math.max(k, k + m - 2 * p))

        const picked = count_total(prefix, left_p, right_p);
        mushrooms = Math.max(mushrooms, picked);
        console.log(left_p, right_p, picked, mushrooms);
    }

    // now move to the left to search for the max mushrooms
    const left_stride = Math.min(m + 1, len - k);
    console.log(`left stride: ${left_stride}`);
    for (let p = 0; p < left_stride; p++) {
        const left_p = Math.max(0, Math.min(k, k - (m - 2 * p)));
        const right_p = k + p;

        const picked = count_total(prefix, left_p, right_p);
        mushrooms = Math.max(mushrooms, picked);
        console.log(left_p, right_p, picked, mushrooms);
    }

    return mushrooms;
}

const dataset = [
    { A:[ 2, 3, 7, 5, 1, 3, 9 ], k:4, m:6, expect:25 }, // 7+5+1+3+9 
];

dataset.forEach(data => {
    const { A, k, m, expect } = data;
    const result = pick_mushrooms(A, k, m);
    const ok = result === expect;
    console.log(A, k, m, expect, result, ok);
    if (!ok) {
        console.log(`ERROR: expected: ${expect} got ${result}`);
    }
});
