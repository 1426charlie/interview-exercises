#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.05
'use strict';

/*
A zero-indexed array A consisting of N different integers is given. The array contains integers in the 
range [1..(N + 1)], which means that exactly one element is missing.

Your goal is to find that missing element.

Write a function:

function solution(A);
that, given a zero-indexed array A, returns the value of the missing element.

For example, given array A such that:

  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5
the function should return 4, as it is the missing element.

Assume that:

N is an integer within the range [0..100,000];
the elements of A are all distinct;
each element of array A is an integer within the range [1..(N + 1)].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.

HINT: know your factorials!
    1..n = n(n+1)/2
    since the array is missing a single element...
    missing = ((n+1)*(n+2))/2 - actual(sum()) is the answer
NOTES: 
    don't for get the empty and single lists
    missing first or last
BUGS:
    returning '2' when the results should be much higher

*/

function solution(list) {
    if (list.length === 0) {
        return 1;
    }

    const len = list.length;
    const expected = ((len + 1) * (len + 2)) / 2
    const sum = list.reduce((a,b) => a+b);
    return (expected - sum);

}

const testdata = [
    { list:[ 2, 3, 1, 5 ], expect: 4 },
    { list:[ 1, 3 ], expect: 2 },
    { list:[], expect: 1 },
    { list:[ 3,1,4,5,2 ], expect: 6 },
    { list:[ 1,2,3,4 ], expect: 5 },
    { list:[ 1,5,6,3,4,2 ], expect: 7 },
    { list:[ 3,2 ], expect: 1 },
];

testdata.forEach(obj => {
    const { list, expect } = obj;
    const result = solution(list.slice(0));   
    console.log(list, expect, result, result === expect);
});
