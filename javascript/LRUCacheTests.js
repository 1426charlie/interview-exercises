#!/usr/bin/env node

// dpw@seattle.local
// 2016.10.22
'use strict';

const assert = require('assert');
const TestSuiteRunner = require('./TestSuiteRunner');
const LRUCache = require('./LRUCache');

const createKey = function() {
    return (Math.random()).toString(24).replace(/0./, Math.floor(Math.random()*10));
};

const createValue = function(n = 1) {
    return `value ${n} created ${process.hrtime().join('.')}`;
};

const createKeyValueList = function(size = 25) {

    const list = [];
    while (list.length < size) {
        const value = createValue(list.length + 1);
        list.push({ key:createKey(), value:value});
    }

    return list;
};

function testInstance(callback) {
    let cache = new LRUCache();

    assert.ok(cache, 'cache exists');
    assert(cache.getCacheSize() === 0, 'should be zero elements');
    assert(cache.getMaxSize() === 128, 'max size')

    cache = new LRUCache(20);
    assert.ok(cache, 'cache exists');
    assert(cache.getCacheSize() === 0, 'should be zero elements');
    assert(cache.getMaxSize() === 20, 'set max size to 20')

    callback();
};

function testSetItem(callback) {
    const now = Math.round(Date.now() / 1000);
    const cache = new LRUCache(5);
    let [ key, value ] = [ createKey(), createValue() ];

    cache.set(key, value);
    assert(cache.getCacheSize() === 1, 'should now have one item in cache');
    assert(cache.get(key) === value, 'check the value for the key');

    let item = cache.getCache().get(key);
    assert.ok(item.ts, 'check the item timestamp');
    assert(item.ts > now, 'check ts > now');

    callback();
};

function testPrune(callback) {
    try {
        const max = 20;
        const cache = new LRUCache(max);
        const list = createKeyValueList(max);

        // console.log( list );
        assert(list.length === max, `list length should equal ${max}`);
        list.forEach(item => cache.set( item.key, item.value ));

        let [ key, value ] = [ createKey(), createValue() ];
        cache.set(key, value);

        [ key, value ] = [ createKey(), createValue() ];
        cache.set(key, value);

        setTimeout(() => {
            try {
                assert(cache.getCacheSize() === max, `list length should equal ${max}`);
                callback();
            } catch(e) {
                callback(e);
            }
        }, 100);
    } catch(ex) {
        callback(ex);
    }
}

function testClear(callback) {
    const max = 20;
    const cache = new LRUCache( max );
    const list = createKeyValueList( max - 2 );

    assert(cache.getMaxSize() === max, 'max size of list');
    assert(cache.getCacheSize() === 0, 'start out with zero items');
    list.forEach(item => cache.set( item.key, item.value ));

    assert(cache.getCacheSize() === list.length);

    cache.clear();

    assert(cache.getCacheSize() === 0, 'cache should now be empty');

    callback();
}

const stats = {
    name: 'LRUCacheTests',
    tests: [ testInstance, testSetItem, testPrune, testClear ],
    testCount: 0,
    errorCount: 0
}

new TestSuiteRunner( stats );
