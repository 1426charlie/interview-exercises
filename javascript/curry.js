#!/usr/bin/env node

// dpw@seattle.local
// 2017.01.03
'use strict';

const add = function(x) {
    return function(y) {
        return x + y;
    };
};

const add3 = add(3);

console.log('should be 8 =>', add3(5));

const modulo = function(divisor) {
    return function(dividend) {
        return dividend % divisor;
    };
};

const isOdd = modulo(2);
const isEven = function(x) { return isOdd(x) === 0; }

const numbs = Array.from({length:10}, (x, i) => i+1);
const odds = numbs.filter(x => isOdd(x));
const evens = numbs.filter(x => isEven(x));
console.log('odds', odds);
console.log('evens', evens);

