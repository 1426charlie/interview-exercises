#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.06
'use strict';

/*
Write a function:

function solution(A, B, K);
that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:

{ i : A ≤ i ≤ B, i mod K = 0 }
For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.

Assume that:

A and B are integers within the range [0..2,000,000,000];
K is an integer within the range [1..2,000,000,000];
A ≤ B.
Complexity:

expected worst-case time complexity is O(1);
expected worst-case space complexity is O(1).
*/

// a solution: const result = floor(B / K) - floor(A / K) + (0 == A % K ? 1 : 0);

function brute1(A, B, K) {
    let result = 0;
    for (let i = A; i <= B; i++) {
        if (i % K === 0) {
            result++;
        }
    }

    return result;
}

// better but still not O(1)
function brute2(A, B, K) {
    let result = 0;
    while (A <= B) {
        if (A % K === 0) {
            result++;
            A += K;
        } else {
            A++;
        }
    }
    return result;
}

function best(A, B, K) {
    // use floor to compensate for js lack of integer handling
    const floor = Math.floor

    // better solution that satisfies O(1)
    const result = floor(B / K) - floor((A - 1)/K)

    return result;
}

function solution(A, B, K) {
    return brute2(A, B, K);
}

const testdata = [
    { a:6, b:11, k:2, expect:3 },
    { a:5, b:11, k:2, expect:3 },
    { a:5, b:10, k:2, expect:3 },
    { a:59, b:71, k:5, expect:3 },
    { a:1, b:1, k:1, expect:1 },
    { a:0, b:99, k:2, expect:50 },
    { a:0, b:100, k:3, expect:34 },
    { a:11, b:345, k:17, expect:20 },
    { a:10, b:10, k:5, expect:1 },
];

testdata.forEach(obj => {
    const { a, b, k, expect } = obj;
    const result = solution(a, b, k);
    const ok = result === expect;
    console.log(a, b, k, expect, result, ok);
    if (!ok) {
        console.log(`ERROR: `);
    }
});
