#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.12
'use strict';

const map = new Map();

map.set(344, 'three hundred forty four');
map.set('mything', 'my thingy')

console.log("size/map", map.size, map);

console.log("\nFor Each...");
map.forEach((k, v) => {
    console.log('key =', k, 'value =', v);
});

console.log("\nfor of...");
for (let [ k, v ] of map.entries()) {
    console.log('key =', k, 'value =', v);
}

console.log('\nspread the map', ...map);

