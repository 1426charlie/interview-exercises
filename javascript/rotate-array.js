#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.12
'use strict';

// process.stdin.resume();
// process.stdin.setEncoding('ascii');

var input_stdin = "5 4";
var input_stdin_array = "1 2 3 4 5";
var input_currentline = 0;

/*
process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}
*/

/////////////// ignore above this line ////////////////////

function main() {
    var n_temp = input_stdin.split(' ');
    var n = parseInt(n_temp[0]);
    var k = parseInt(n_temp[1]);
    var a = input_stdin_array.split(' ');

    process.stdout.write( a.map((v, i) => {
        return a[(i + k) % a.length];
    }).join(' ') + '\n');

}

main();
