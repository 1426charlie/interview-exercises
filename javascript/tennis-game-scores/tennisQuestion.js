#!/usr/bin/env node

// darryl.west@raincitysoftware.com
// Wed Jan 27 16:06:06 PST 2016
// version 0.1.2

'use strict';

const win = 'WIN';
const advantage = 'AD';
const love = '0';
const validScores = [ love, '15', '30', '40', advantage, win ];
const failMessage = '\nFAIL TEST: ';
const passMessage = 'PASS TEST: ';

var tests = 0;
var failures = 0;
var testMessages = [];

// buffer the log messages and show only on failures
function log() {
    testMessages.push( Array.apply(null, arguments ).join('') );
};

/**
 * validate the inputs; return true if ok, false if not valid
 */
function validateTennisInputs(score, playerIndex) {
    var isValid = false;

    // check the basics
    if (Array.isArray( score ) && score.length === 2 && (playerIndex === 0 || playerIndex === 1)) {

        // validate the array inputs
        if (validScores.indexOf( score[0] ) >= 0 && validScores.indexOf( score[1] ) >= 0) {
            isValid = true;
        }

        // check for an attempted advantage/advantage
        if (score[0] === advantage && score[1] === advantage) {
            isValid = false;
        }

        // check for an attempted double win
        if (score[0] === win || score[1] === win) {
            isValid = false;
        }

        // TODO: probably other extreem cases to test for...
    }

    if (!isValid) {
        log('input is not valid, score: ', score, ', player: ', playerIndex);
    }

    return isValid;
}

function calcNextScore(current) {
    var idx = validScores.indexOf( current );

    if (idx < 0) {
        throw new Error('invalid input score: ' + current);
    }

    idx++;

    return validScores[ idx ];
}

/**
 * Returns the next score in a tennis game given the player that scored.
 *
 * @see {@link http://tennis.about.com/cs/beginners/a/beginnerscore.htm}
 * @param {number[]} score - The current score, player 0 in position 0, player 1 in position 1
 * @param {number} playerIndex - The player that scored
 * @returns {number[]|false} - The new score, player 0 in position 0, player 1 in position 1, or false on any error
 */
function tennis(score, playerIndex) {
    var nextScore,
        opponentIndex = (playerIndex === 0 ? 1 : 0),
        currentPlayerScore,
        opponentScore,
        nextPlayerScore;

    log('score: ', score, ', player: ', playerIndex);

    if (!validateTennisInputs(score, playerIndex)) {
        log('input error...');

        return false;
    }

    // do a simple copy of the scores to make sure the original stays intact
    nextScore = [ score[0], score[1] ];

    // pick off the player's current score using index
    currentPlayerScore = score[ playerIndex ];
    opponentScore = score[ opponentIndex ];

    // modify the opponents score if at advantage
    if (opponentScore === advantage) {
        // now both are at duece
        nextPlayerScore = nextScore[ opponentIndex ] = opponentScore = '40';
        log('new score: ', nextScore, ', player: ', playerIndex);
    } else {
        nextPlayerScore = calcNextScore( currentPlayerScore );
    }
    log('payer idx: ', playerIndex, ' previous score: ', currentPlayerScore, ', next score: ', nextPlayerScore);

    // handle the AD to win case
    if (nextPlayerScore === advantage && opponentScore !== currentPlayerScore) {
        nextPlayerScore = win;
    }

    nextScore[ playerIndex ] = nextPlayerScore;
    log('next score: ', nextScore);

    return nextScore;
}

function handleFailedTest(msg) {
    console.log(failMessage, msg);
    console.log(testMessages);

    console.log('');
    testMessages.splice(0);
    failures++;

    return false;
}

function assert(score, expectedScore, description) {
    tests++;
    if (Array.isArray(score) && Array.isArray(expectedScore)) {
        if (score.length !== expectedScore.length) {
            return handleFailedTest(description);
        }

        if (score[0] !== expectedScore[0] || score[1] !== expectedScore[1]) {
            return handleFailedTest(description);
        }
    } else if (score !== expectedScore) {
        return handleFailedTest(description);
    }

    console.log(passMessage + description);

    // clear the test messages
    testMessages.splice(0);

    return true;
}

function test() {
    // test the basic errors
    assert(tennis([ '0', '0', '0' ], 0), false, 'should return false with bad input score');
    assert(tennis([ '0', '0'  ], 2), false, 'should return false with bad input score');
    assert(tennis({}, 2), false, 'should return false with bad input score');
    assert(tennis([ '0', '0' ]), false, 'should return false with bad input score');
    assert(tennis(), false, 'should return false with bad input score');
    assert(tennis([ win, '15'], 0), false, 'no points possible after a win');
    assert(tennis([ love, win ], 0), false, 'no points possible after a win');
    assert(tennis([ advantage, advantage ], 0), false, 'both players cannot have an advantage');

    // test the typical inputs
    assert(tennis(['0', '0'], 0), ['15', '0'], 'adds a point to the correct player');
    assert(tennis(['0', '40'], 1), ['0', 'WIN'], 'player can win from 40');
    assert(tennis(['40', '40'], 0), [ advantage, '40'], 'takes player to advantage');

    // test the basic end-points
    assert(tennis(['0', '0'], 0), [ '15', love ], 'adds a point to the correct player');
    assert(tennis(['0', '0'], 1), [ love, '15'], 'adds a point to the correct player');
    assert(tennis([advantage, '40'], 0), [ win, '40'], 'player 0 with advantage wins');
    assert(tennis(['40', advantage], 1), [ '40', win ], 'player 1 with advantage wins');
    assert(tennis(['40', '40'], 0), [ advantage, '40' ], 'player 0 gains advantage');
    assert(tennis(['40', '40'], 1), [ '40', advantage ], 'player 1 gains advantage');

    assert(tennis(['40', advantage], 0), [ '40', '40' ], 'player 1 looses advantage, now deuce');
    assert(tennis([advantage, '40'], 1), [ '40', '40' ], 'player 0 looses advantage, now deuce');

    // dump the test stats
    console.log('\nTotal tests: ', tests, ', failures: ', failures);
}

module.exports = tennis;

if (require.main === module) {
    test();
}
