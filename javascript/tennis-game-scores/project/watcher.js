#!/usr/bin/env node

// dpw@alameda.local
// 2015.11.11
'use strict';

var fs = require('fs'),
    spawn = require('child_process').spawn,
    clearScreen = '[H[2J',
    files = [],
    tid,
    lastRun;

var run = function() {
    process.stdout.write( clearScreen ); 

    try {
        var cmd = 'node';
        var runner = spawn( cmd, [ 'ScoreTests.js' ] );

        runner.stdout.on('data', function( data ) {
            process.stdout.write( data );
        });

        runner.stderr.on('data', function( data ) {
            process.stdout.write( data );
        });

        runner.on('close', function(code) {
            if (code !== 0) {
                console.log( cmd, ' did not exit correctly, code: ', code);
            }

            console.log( '------------------------------------ last run: ', new Date().toISOString() );

            tid = null;
            files.splice( 0 );
        });
    } catch (err) {
        console.log( err );
    }
};

var changeHandler = function(event, filename) {
    // console.log( 'file change: ', filename);

    if (!tid) {
        tid = setTimeout(function() {
            run();
        }, 250);
    }
};

fs.watch( './', { recursive:false}, changeHandler );

process.stdout.write( clearScreen ); 
console.log('watching js files...');

