/**
 * @class Logger - a very simple logger with either file or console destinations
 *
 * @author darryl.west@raincitysoftware.com
 * @created 1/27/16.
 */
const fs = require('fs');

const Logger = function(options) {
    var dest;

    if (options) {
        dest = options.dest;
    } else {
        dest = console;
    }

    function dateString() {
        return new Date().toJSON().split('T').pop().replace('Z', '');
    }

    this.info = function() {
        dest.log( dateString() + ' INFO:  ' + Array.apply(null, arguments).join(''));
    };

    this.error = function() {
        dest.log( dateString() + ' ERROR: ' + Array.apply(null, arguments).join(''));
    };
};

const FileLogger = function(options) {
    var filename = options.filename;

    this.log = function(msg) {
        fs.appendFileSync( filename, msg + "\n" );
    };

    // validate the constructor
    if (!filename) {
        throw new Error('file logger must be constructed with a valid file name');
    }
};

Logger.createFileLogger = function() {
    const dest = new FileLogger({ filename:'score-process.log' });

    return new Logger({ dest:dest });
};

module.exports = Logger;
