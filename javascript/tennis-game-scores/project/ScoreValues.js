/**
 * Definition of valid score values shared between validators, score logic, and tests
 *
 * @author darryl.west@raincitysoftware.com
 * @created 1/27/16.
 */

const ScoreValues = {};

ScoreValues.win = 'WIN';
ScoreValues.tie = 'TIE';
ScoreValues.error = 'ERROR';
ScoreValues.advantage = 'AD';
ScoreValues.love = '0';
ScoreValues.scoreSequence = [
    ScoreValues.love,
    '15',
    '30',
    '40',
    ScoreValues.advantage,
    ScoreValues.win
];

module.exports = ScoreValues;