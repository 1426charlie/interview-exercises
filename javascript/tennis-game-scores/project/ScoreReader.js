/**
 * Reads game scores from a text file and returns a set of 
 *
 * @author darryl.west@raincitysoftware.com
 * @created 2016-01-27
 */

const fs = require('fs'),
    ScoreModel = require('./ScoreModel');

const ScoreReader = function(options) {
    'use strict';

    const reader = this,
        log = options.logger,
        validator = options.validator;

    /**
     * read data from the filename and return results in the callback (node standard err,data)
     *
     * @returns an array of ScoreModels
     */
    this.readFileData = function(filename, completeCallback) {
        var fn = filename || 'tennis-data.txt',
            callback;

        log.info('read data from: ', fn);

        callback = function(err, data) {
            var scores = null;
            if (!err) {
                scores = reader.parseData( data );
            }

            completeCallback( err, scores );
        };

        // use validator to accept or reject lines as they are read
        fs.readFile( fn, callback );
    };

    this.parseData = function(data) {
        var scores,
            id = 1,
            lines = data.toString().split('\r\n');

        scores = lines.map(function(line) {
            var model = new ScoreModel({
                id:id++,
                data:line
            });

            return model;
        });

        return scores.filter(function(model) {
            return model.data.length > 10;
        });
    };

    if (!log) {
        throw new Error('validator must be constructed with a log object');
    }

    if (!validator) {
        throw new Error('validator must be constructed with a validator');
    }
};

module.exports = ScoreReader;
