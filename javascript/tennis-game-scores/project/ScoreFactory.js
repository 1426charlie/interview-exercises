/**
 * Construction factory
 *
 * @author darryl.west@raincitysoftware.com
 * @created 2016-01-27
 */

const Logger = require('./Logger'),
    ScoreReader = require('./ScoreReader'),
    ScoreValidator = require('./ScoreValidator'),
    ScoreProcessor = require('./ScoreProcessor');

const ScoreFactory = function(options) {
    const factory = this;
    var log;

    if (options) {
        log = options.logger;
    }

    this.createScoreReader = function() {
        var opts = {};
        opts.logger = log;
        opts.validator = factory.createScoreValidator();

        return new ScoreReader( opts );
    };

    this.createScoreValidator = function() {
        var opts = {};
        opts.logger = log;

        return new ScoreValidator( opts );
    };

    this.createScoreProcessor = function() {
        var opts = {};

        opts.logger = log;
        opts.reader = factory.createScoreReader();
        opts.validator = factory.createScoreValidator();

        return new ScoreProcessor( opts );
    };

    this.createLogger = function() {
        return log;
    };

    if (!log) {
        log = new Logger();
    }
};

module.exports = ScoreFactory;
