/**
 * @class ScoreModel - data model container for a single game with raw data, and calculated scores
 *
 * @author darryl.west@raincitysoftware.com
 * @created 2016-01-27
 */

const ScoreModel = function(params) {
    'use strict';

    if (!params) {
        params = {};
    }

    this.id = params.id;
    this.data = params.data || [];
    this.scores = params.scores || [];

    this.result = params.result;
    this.winner = params.winner;
    this.winindex = params.winindex; // the last data point where the winner won

};

module.exports = ScoreModel;
