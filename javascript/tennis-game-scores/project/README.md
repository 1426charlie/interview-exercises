# Tennis Score Project


## Use

_Note: requires a version of node (or other runner) with harmony (ES6) enabled..._

### Running the application

Application logging is written to a file (score-process.log).  The output is written to stdout.

~~~
node ScoreProcessor [dataFileName]
~~~

Where the optional dataFileName points to an input file.  If dataFileName is omitted the default file (tennis-data.txt) is read.  Errors are logged to the log file (score-process.log).

### Run the Tests

All test logging is done to stdout.

~~~
node ScoreTests

- or -

runtests.sh
~~~

## Specification

1. read input.txt

2. treat each line as a game starting with score [0, 0]

3. progress until a winner is determined, an error is encountered, or the end of line is read. Treat each character read as the player that scored the point

4. if there is a winner, on a new line, print "WIN " + the player number + " " + the point that the person won in (starting with 0)

5. if there is an error, on a new line, print "ERROR"

6. if there is no winner by the end of the line, on a new line, print "TIE"

7. on the final line, print the player number with the most wins, followed by the number of wins more than their opponent, followed by the number of errors. For example, if player 0 won 5 games and player 1 won 7 games, and there were no errors, print "120". If the match is a tie print "X" for the player number, for example, "X01".

## Assumptions

1. a line of data that ends with a 40,AD final score is this considered a tie (players exhausted).
2. a game that ends with a tie, even 0,0 is recorded as a tie
3. blank data lines are skipped
4. for item 4 in the spec, "the point that the person won on" is technically the zero-based index where the winner won the game.

## Project Implementation

### Third Party Modules : Zero Dependencies

This application does not use any modules not available directly from the node module library.  In hindsight it would have saved time to use lodash, logger, mocha/chai, async, and commander modules but it was a good exercise to create a multi-module application using only node modules.

The application also intentionally skipped using npm/package, make, gulp/grunt/webpack.  There is a 'watcher' script written in node to run tests while developing.

### Objects

The application was implemented with a series of objects designed to separate concerns and aid in testing.  The primary object is ScoreProcessor.  It uses the other modules to read and validate data, contain game/score objects and define constants.  These modules are shared by the test framework (ScoreTests).

* ScoreFactory - typical factory pattern used to create objects
* ScoreProcessor - the main processor
* ScoreReader - reads from the specified (or default) file, parses and creates models
* ScoreModel - container for a single set of game data
* ScoreValidator - validates score and player index
* ScoreValues - reference constants including the tennis score sequence.

Other objects

* ScoreTests - the test suite
* Logger - a console and file logger
* watcher - more of a script to provide a continuous code/test environment



- - -
<small>darryl.west@raincitysoftware.com | 28-Jan-2016 | version 0.2.1</small>