/**
 * @class ScoreProcessor - the main application that reads game scores from an external source and processes scores
 *  line by line as a single game to determine the winner.  Each game either concludes with 1) a winner, or 2) a tie,
 *  or 3) an error.
 *
 * @author darryl.west@raincitysoftware.com
 * @created 2016-01-27
 */
const VERSION = '0.2.1',
    ScoreValues = require('./ScoreValues'),
    ScoreModel = require('./ScoreModel');

const ScoreProcessor = function(options) {
    'use strict';

    const processor = this,
        log = options.logger,
        reader = options.reader,
        validator = options.validator,
        writer = options.writer || process.stdout;

    this.run = function(datafile) {
        log.info('run the tennis score processor...');

        // read the tennis score / game file
        function readerCallback(err, games) {
            if (err) {
                return log.error( err );
            }

            // loop through the score models and process
            games.forEach(function(game) {
                processor.scoreGame( game );
                processor.showScores( game );
            });

            processor.showStats( games );
        }

        reader.readFileData(datafile, readerCallback);
    };

    this.scoreGame = function(game) {
        var wins = game.data.split(''),
            score = [ '0', '0'],
            index = 0;

        log.info( 'score game: ', JSON.stringify( game ));

        // the initial score
        game.scores = [ score ];

        function loop() {
            if (wins.length === 0) {
                const len = game.scores.length;

                // pick off the last/final recorded score from a copy of the scores
                score = game.scores.slice().pop();
                if (score[0] === score[1] || score[0] === ScoreValues.advantage || score[1] === ScoreValues.advantage) {
                    game.result = ScoreValues.tie;
                    return;
                } else {
                    log.error('game data is not complete: ', game.data);
                    game.result = ScoreValues.error;
                }

                return;
            }

            var player = parseInt( wins.shift() );

            if (validator.validate(score, player)) {
                score = processor.scorePlay( score, player );

                if (score === false) {
                    game.result = ScoreValues.error;
                    return;
                }

                game.scores.push( score );

                if (score.indexOf( ScoreValues.win ) >= 0) {
                    game.result = ScoreValues.win;
                    game.winner = score[ 0 ] === ScoreValues.win ? 0 : 1;
                    game.winindex = index;
                    return;
                }
            } else {
                game.result = ScoreValues.error;
                return;
            }

            index++;

            loop();
        }

        loop();
    };

    this.calcNextScore = function(current) {
        var idx = ScoreValues.scoreSequence.indexOf( current );

        if (idx < 0) {
            log.error( 'current sequence value: ', current );
            // throw new Error('invalid input: ' + current);
        }

        idx++;

        return ScoreValues.scoreSequence[ idx ];
    };

    this.scorePlay = function(score, playerIndex) {
        var nextScore,
            opponentIndex = (playerIndex === 0 ? 1 : 0),
            currentPlayerScore,
            opponentScore,
            nextPlayerScore;

        log.info('score: ', score, ', player: ', playerIndex);

        if (!validator.validate( score, playerIndex )) {
            return false;
        }

        // do a simple copy of the scores to make sure the original stays intact
        nextScore = [ score[0], score[1] ];

        // pick off the player's current score using index
        currentPlayerScore = score[ playerIndex ];
        opponentScore = score[ opponentIndex ];

        // modify the opponents score if at advantage
        if (opponentScore === ScoreValues.advantage) {
            // now both are tied
            nextPlayerScore = nextScore[ opponentIndex ] = opponentScore = '40';
            log.info('new score: ', nextScore, ', player: ', playerIndex);
        } else {
            nextPlayerScore = processor.calcNextScore( currentPlayerScore );
        }

        log.info('payer idx: ', playerIndex, ' previous score: ', currentPlayerScore, ', next score: ', nextPlayerScore);

        // handle the AD to win case
        if (nextPlayerScore === ScoreValues.advantage && opponentScore !== currentPlayerScore) {
            nextPlayerScore = ScoreValues.win;
        }

        nextScore[ playerIndex ] = nextPlayerScore;
        log.info('next score: ', nextScore);

        return nextScore;
    };

    this.showScores = function(game) {
        var buf = [ game.result],
            finalScore = game.scores.slice().pop();

        // console.log( game );
        if (game.result === ScoreValues.win) {
            buf.push( game.winner );
            buf.push( game.winindex );
        }

        buf.push( '\n' );
        writer.write( buf.join(' ') );
    };

    this.showStats = function(games) {
        var wins = [0, 0],
            errors = 0,
            buf = [];

        log.info('calculate the final stats, games: ', games.length);

        // calc the wins for each player
        games.forEach(function(game) {
            if (game.result === ScoreValues.win) {
                // bump the win count for the winner
                wins[ game.winner ]++;
            } else if (game.result === ScoreValues.error) {
                errors++;
            }
        });

        const diff = wins[ 0 ] - wins[ 1 ];
        log.info("wins: ", wins, ', errors: ', errors, ', diff: ', diff);

        if (diff === 0) {
            buf.push('X');
        } else if (diff < 0) {
            buf.push(1);
        } else {
            buf.push(0);
        }

        buf.push( Math.abs( diff ));

        buf.push( errors );
        buf.push( '\n' );

        writer.write(buf.join(''));
    };

    if (!reader) {
        throw new Error('score processor must be constructed with a score reader');
    }

    if (!validator) {
        throw new Error('score processor must be constructed with a validator')
    }
};

module.exports = ScoreProcessor;

if (require.main == module) {

    const ScoreFactory = require('./ScoreFactory'),
        Logger = require('./Logger');

    var factory,
        processor,
        datafile,
        opts = {};

    // parse command line args for options
    opts.logger = Logger.createFileLogger();

    opts.logger.info('process version ', VERSION);

    factory = new ScoreFactory( opts );
    processor = factory.createScoreProcessor();

    if (process.argv.length > 2) {
        datafile = process.argv[2];
        opts.logger.info("datafile name: ", datafile);
    }


    processor.run( datafile );
}
