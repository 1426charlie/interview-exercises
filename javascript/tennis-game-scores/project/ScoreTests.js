/**
 * Test the score modules...
 *
 * @author darryl.west@raincitysoftware.com
 * @created 2016-01-27
 */
const assert = require('assert'),
    Logger = require('./Logger'),
    ScoreValues = require('./ScoreValues'),
    ScoreFactory = require('./ScoreFactory'),
    ScoreProcessor = require('./ScoreProcessor'),
    ScoreReader = require('./ScoreReader'),
    ScoreModel = require('./ScoreModel'),
    ScoreValidator = require('./ScoreValidator');

const TestSuite = function() {
    const suite = this;
    var log = new Logger(),
        testCount = 0,
        factory = new ScoreFactory({ logger: log });

    function assertScore(score, expectedScore, description) {
        log.info( description );
    }

    function test(value, message) {
        testCount++;
        assert( value, message );
    }

    this.testModels = function(done) {
        log.info('test the models');

        // test basic instantiate without params
        var model = new ScoreModel();

        test(model instanceof ScoreModel, "should be an instance of ScoreModel");

        var params = {
            id:1,
            data:'010100010110110'
        };

        model = new ScoreModel( params );
        test(model.id === params.id, "should have the assigned id");
        test(model.data === params.data, "should have game play data");
        test(model.scores.length === 0, "should have zero scores");

        done();
    };

    this.testReader = function(done) {
        log.info('test the reader');

        var reader = factory.createScoreReader();

        test(reader instanceof ScoreReader, "should bin an instance of ScoreReader");

        var failCallback = function(err, games) {
            test(err !== null, 'should have an error');
            test(games === null, 'should have null data');

            done();
        };

        var successCallback = function(err, games) {
            test(err === null, 'errors should be null');
            test(Array.isArray( games ), 'data should be an array of game scores');
            test(games.length === 25, 'data array should have 26 games');

            games.forEach(function(game) {
                test(game instanceof ScoreModel, 'each element should be a score model');
                test(game.id > 0, 'each game should have a non-zero numeric id');
                test(typeof game.data === 'string', 'game data should be a string');
                test(game.data.length === 15, 'game data should have a specific length');
            });

            reader.readFileData('bad-filename.bad', failCallback);
        };

        reader.readFileData(null, successCallback);
    };

    this.testValidator = function(done) {
        log.info('test the score validator');

        const validator = factory.createScoreValidator();

        test(validator instanceof ScoreValidator, 'should be an instance of score validator');

        // define good score/players
        const good = [
            { score:['0', '0'], player:0 },
            { score:['0', '0' ], player:1 },
            { score:['15', '15'], player:0 },
            { score:['30', '30'], player:0 },
            { score:['40','40'], player:0 }
        ];

        good.forEach(function(value) {
            test(validator.validate( value.score, value.player ), 'should be a valid score/player');
        });

        const bad = [
            { score:['0', '40x'], player:1 },
            { score:['33', '0' ], player:0 },
            { score:['15', '15'], player:2 },
            { score:[30, '0'], player:0 },
            { score:['foo','40'], player:0 }
        ];

        bad.forEach(function(value) {
            console.log(value);
            test(validator.validate( value.score , value.player ) === false, 'should not be a valid score/player');
        });

        done();
    };

    this.testCalcNextScore = function(done) {
        log.info('test the scoring calculator');
        const processor = factory.createScoreProcessor(),
            next = processor.calcNextScore;

        test(next('0') == '15', 'next score in sequece');
        test(next('40') == ScoreValues.advantage, 'next score in sequece is advantage');

        done();
    };

    this.testCalculator = function(done) {
        log.info('test the scoring calculator');
        const processor = factory.createScoreProcessor(),
            calc = processor.scorePlay,
            advantage = ScoreValues.advantage,
            win = ScoreValues.win,
            love = ScoreValues.love;

        function equal(score, expected) {
            if (score.length === 2 && expected.length === 2) {
                return (score[0] == expected[0] && score[1] === expected[1]);
            }

            return false;
        }
        
        test(calc([ '0', '0', '0' ], 0) === false, 'should return false with bad input score');
        test(calc([ '0', '0'  ], 2) === false, 'should return false with bad input score');
        test(calc({}, 2) === false, 'should return false with bad input score');
        test(calc([ '0', '0' ]) === false, 'should return false with bad input score');
        test(calc() === false, 'should return false with bad input score');
        test(calc([ win, '15'], 0) === false, 'no points possible after a win');
        test(calc([ love, win ], 0) === false, 'no points possible after a win');
        test(calc([ advantage, advantage ], 0) === false, 'both players cannot have an advantage');

        // test the typical inputs
        test(equal(calc(['0', '0'], 0), ['15', '0']), 'adds a point to the correct player');
        test(equal(calc(['0', '40'], 1), ['0', win ]), 'player can win from 40');
        test(equal(calc(['40', '40'], 0), [ advantage, '40']), 'takes player to advantage');

        // test the basic end-points
        test(equal(calc(['0', '0'], 0), [ '15', love ]), 'adds a point to the correct player');
        test(equal(calc(['0', '0'], 1), [ love, '15']), 'adds a point to the correct player');
        test(equal(calc([advantage, '40'], 0), [ win, '40']), 'player 0 with advantage wins');
        test(equal(calc(['40', advantage], 1), [ '40', win ]), 'player 1 with advantage wins');
        test(equal(calc(['40', '40'], 0), [ advantage, '40' ]), 'player 0 gains advantage');
        test(equal(calc(['40', '40'], 1), [ '40', advantage ]), 'player 1 gains advantage');

        test(equal(calc(['40', advantage], 0), [ '40', '40' ]), 'player 1 looses advantage, now deuce');
        test(equal(calc([advantage, '40'], 1), [ '40', '40' ]), 'player 0 looses advantage, now deuce');

        done();
    };

    this.testProcessor = function(done) {
        log.info('test the main tennis score processor');

        const processor = factory.createScoreProcessor();
        test(processor instanceof ScoreProcessor, 'should be an instance of ScoreProcessor');

        const games = [
            { model:new ScoreModel({id:1, data:'010100010110110'}), expect:ScoreValues.win, winner:0, idx:5 },
            { model:new ScoreModel({id:2, data:'011110010010010'}), expect:ScoreValues.win, winner:1, idx:4 },
            { model:new ScoreModel({id:3, data:'001111010110000'}), expect:ScoreValues.win, winner:1, idx:5 },
            { model:new ScoreModel({id:4, data:'000001010111011'}), expect:ScoreValues.win, winner:0, idx:3 },
            { model:new ScoreModel({id:5, data:'011101100000111'}), expect:ScoreValues.win, winner:1, idx:5 },
            { model:new ScoreModel({id:6, data:'011101100002111'}), expect:ScoreValues.win, winner:1, idx:5 },
            { model:new ScoreModel({id:7, data:'111010010101100'}), expect:ScoreValues.win, winner:1, idx:4 },
            { model:new ScoreModel({id:8, data:'001101011001110'}), expect:ScoreValues.win, winner:1, idx:13 },
            { model:new ScoreModel({id:9, data:'0101010101010101'}), expect:ScoreValues.tie },
            { model:new ScoreModel({id:10, data:'101010101010101'}), expect:ScoreValues.tie },
            { model:new ScoreModel({id:11, data:'0201'}), expect:ScoreValues.error },
            { model:new ScoreModel({id:12, data:'0x01'}), expect:ScoreValues.error }
        ];

        games.forEach(function(game) {
            const expect = game.expect;
            var model = game.model;

            processor.scoreGame( model );

            if (model.result !== game.expect) {
                console.log( model );
                console.log( model.result );
            }

            test(model.result === game.expect, 'result for game ' + model.id + ' should be ' + expect + ' but was ' + model.result);
            // test(game.model.scores.length > 0, 'scores should have a specific length');

            if (model.result === ScoreValues.win) {
                test(model.winner === game.winner, 'game ' + model.id + ' winner should be ' + game.winner);
                test(model.winindex === game.idx, 'game ' + model.id + ' win idx should be ' + game.idx + ' but was ' + model.winindex);
            }

        });

        done();
    };

    this.runTests = function() {
        var tests = [
            suite.testModels,
            suite.testCalcNextScore,
            suite.testCalculator,
            suite.testReader,
            suite.testValidator,
            suite.testProcessor
        ];

        // TODO : refactor to use generator/yield...
        // run all tests async...
        var loop = function() {
            var fn = tests.pop();
            if (fn) {
                fn.call(null, loop);
            } else {
                console.log('\ntotal tests: ', testCount);
            }
        };

        loop();
    };
};

var suite = new TestSuite();
suite.runTests();
