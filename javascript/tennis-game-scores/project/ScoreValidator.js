/**
 * Validates score data inputs
 *
 * @author darryl.west@raincitysoftware.com
 * @created 2016-01-27
 */

const ScoreValues = require('./ScoreValues');

const ScoreValidator = function(options) {
    const validator = this,
        log = options.logger,
        win = ScoreValues.win,
        advantage = ScoreValues.advantage,
        love = ScoreValues.love,
        scores = ScoreValues.scoreSequence;

    /**
     * validate the score and player index
     *
     * @param score - an array with two elements for player 0 and 1
     * @param playerIndex - either 0 or 1
     *
     * @returns true if ok, false if not valid
     */
    this.validate = function(score, playerIndex) {
        var valid = false;

        if (Array.isArray( score ) && score.length === 2 && (playerIndex === 0 || playerIndex === 1)) {

            if (scores.indexOf( score[0] ) >= 0 && scores.indexOf( score[1] ) >= 0) {
                valid = true;
            }

            if (score[0] === advantage && score[1] === advantage) {
                log.error('both players cannot both have advantage');
                valid = false;
            }

            if (score[0] === win || score[1] === win) {
                log.error('neither players can have a win score');
                valid = false;
            }

            if (scores.indexOf(score[0]) < 0 || scores.indexOf(score[1]) < 0) {
                log.error('invalid score');
                valid = false;
            }

            // TODO : probably more extreme cases...
        }

        if (!valid) {
            log.error('score or player was not valid: ', score, ', player: ', playerIndex);
        }

        return valid;
    };

    if (!log) {
        throw new Error('validator must be constructed with a log object')
    }
};

module.exports = ScoreValidator;
