#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.18
'use strict';

const SortedDoubleLinkedList = function(options = {}) {
    const list = this,
        debug = options.debug;

    const Node = function(data, next, prev) {
        this.data = data;
        this.next = next;
        this.prev = prev;
    };

    const maxSize = 1000;

    let front = null;
    let center = null;
    let back = null;
    let length = 0;

    function createNewNode(data, next, prev) {
        const node = new Node(data, next, prev);
        return node;
    }

    function initialNode(data) {
        if (debug) console.log('create initial node, data:', data);

        front = center = back = createNewNode(data);
        length++;
    }

    function insertBackward(curr, data) {
        if (debug) console.log(`insert ${data} backward from ${curr.data}`);

        let node;
        let count = 0;
        while (data < curr.data) {
            if (!curr.prev) {
                if (debug) console.log(`insert new data ${data} at front...`);
                node = createNewNode(data, curr);
                curr.prev = node;
                front = node;
                return
            }

            curr = curr.prev;
            if (count++ > maxSize) {
                curr.prev = null;   
            }
        }

        if (!node) {
            let next = curr.next;
            if (debug) console.log(`insert after ${curr.data} data ${data} before ${next.data}`);
            node = createNewNode(data, next, curr);
            next.prev = node;
            curr.next = node;
        }
    }

    function insertForward(curr, data) {
        if (debug) console.log(`insert ${data} forward from ${curr.data}`);

        let node;
        let count = 0;
        while (data > curr.data) {
            if (!curr.next) {
                if (debug) console.log(`insert new data ${data} at back...`);
                node = createNewNode(data, null, curr);
                curr.next = node;
                back = node;
                return
            }

            curr = curr.next;
            if (count++ > maxSize) {
                curr.next = null;
            }
        }

        if (!node) {
            let prev = curr.prev;
            if (debug) console.log(`insert after ${prev.data} data ${data} before ${curr.data}`);
            node = createNewNode(data, curr, prev);
            prev.next = node;
            curr.prev = node;
        }
    }

    function evenOdd() {
        return [ (length % 2 === 0), (length % 2 !== 0) ];
    }

    function adjustCenter(data) {
        const [ even, odd ] = evenOdd();
        if (data < center.data) {
            if (even) {
                center = center.prev;
            }
        } else {
            if (odd) {
                center = center.next;
            }
        }
    }

    this.insert = function(data) {
        if (length === 0) {
            return initialNode(data);
        }

        if (data < center.data) {
            insertBackward(center, data);
        } else {
            insertForward(center, data);
        }

        adjustCenter(data);
        length++;
    };

    this.toString = function() {
        if (length > 0) {
            return [ 'front/center/back: ', front.data, center.data, back.data, `sz:${length}`, `median ${list.calcMedian().toFixed(1)}` ].join(' ');
        }
        return 'new empty list...';
    };

    this.calcMedian = function() {
        if (length % 2 === 0) {
            return ((center.data + center.prev.data) / 2);
        }

        return center.data;
    };

    this.walk = function() {
        let r = [];
        let p = front;
        r.push(p.data);

        while (p.next) {
            r.push(p.next.data);
            p = p.next;

        }

        if (length !== r.length) {
            r.push(`ERROR: lenght ${length} != list size: ${r.length}`);
        }

        return r;
    };
};

module.exports = SortedDoubleLinkedList;

// test scripts
if (process.argv[1].endsWith('SortedDoubleLinkedList.js')) {
    const list = new SortedDoubleLinkedList({debug:false});
    const input = [ 12, 4, 5, 3, 8, 7 ];
    const expect = [ '12.0', '8.0', '5.0', '4.5', '5.0', '6.0' ];

    input.forEach((v,i) => {
        list.insert(v);
        console.log(list.toString());
        console.log(list.walk());

        const median = list.calcMedian().toFixed(1);
        if (median !== expect[i]) {
            console.log(`Error! ${median} !== ${expect[i]}`);
        }
    });
}

// test scripts
if (process.argv[1].endsWith('SortedDoubleLinkedList.js')) {
    let list = new SortedDoubleLinkedList();
    const len = 1e4;
    const t0 = Date.now();

    for (let i = 0; i < len; i++) {
        const n = Math.round(Math.random() * 1e5);
        list.insert(n);
    }

    const t1 = Date.now();
    console.log(`elapsed: ${(t1 - t0) / 1000}`);
}

