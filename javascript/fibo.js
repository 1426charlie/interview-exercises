#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.04
'use strict';

/**
 * implement fibonacci with various algorithms
 */

function* analytic(start = 1, stop = 35) {
    const [pow, floor, sqrt] = [Math.pow, Math.floor, Math.sqrt];
    const sqrt5 = sqrt(5);
    const p = (1 + sqrt5) / 2;
    const q = 1 / p;

    let n = start;

    while (n <= stop) {
        yield floor((pow(p,  n) + pow(q, n)) / sqrt5 + 0.5)
        n++;
    }
}

function* generated(start = 1, stop = 35) {
    let [n0, n1] = [ 0, 1 ];
    yield n1;

    let n = start + 1;
    while (n <= stop) {
        [n0, n1] = [n1, n0 + n1];
        yield n1;

        n++;
    }
};

function recurse(start = 1, stop = 35) {
    const fib = function(n) {
        if (n === 0 || n === 1) {
            return n;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    };
    
    let results = []
    for (let i = start; i <= stop; i++) {
        results.push(fib(i));
    }

    return results;
}

function cached(start = 1, stop = 35) {
    const cache = new Map();
    cache.set(1, 1);

    let results = [];

    const fib = function(n) {
        let x = cache.get(n);
        if (x) {
            return x;
        }
        if (n < 2) {
            return n
        } else {
            const num = fib(n - 1) + fib(n - 2);
            cache.set(n, num);
            return num;
        }
    }

    for (let i = start; i <= stop; i++) {
        results.push(fib(i));
    }

    return results;
}

function check(list) {
    const reference = [1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465]
    if (list.length !== reference.length) {
        console.log( list );
        throw new Error('lists are not the same size!');
        return false;
    }

    for (let i = 0; i < list.length; i++) {
        if (list[i] !== reference[i]) {
            console.log('error at n: ', i);
            return false;
        }
    }

    return true;
};

function report(name, results, startTime) {
    check(results);
    console.log(JSON.stringify( results ), name, 'elapsed:', Number(process.hrtime().join('.')) - Number(startTime));
}

let results = [];
let started = process.hrtime().join('.')
for (let n of analytic()) {
    results.push( n );
}
report('analytic', results, started)

results = [];
started = process.hrtime().join('.')
for (let n of generated()) {
    results.push( n );
}
report('generated', results, started)

started = process.hrtime().join('.')
results = recurse();
report('recursive', results, started)

started = process.hrtime().join('.')
results = cached();
report('cached', results, started)
