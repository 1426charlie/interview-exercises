
const version = '1.0.1';
const GitHubApi = require('github');
const github = new GitHubApi();

function wrap(data, tm) {
    const obj = {
        status:'ok',
        ts: new Date().toJSON(),
        tm: tm,
        version:version,
        data:data
    };

    return obj;
}

exports.handler = (event, context, callback) => {
    const t0 = Date.now();
    github.search.repos({
        q: 'sitepoint',
        sort: 'stars'
    }, (err, res) => {
        if (err) {
            callback(err);
        }

        const results = res.items.map(repo => {
            return {
                url: repo.html_url,
                stars: repo.stargazers_count
            };
        });

        const elapsed = (Date.now() - t0) / 1000;
        const wrapped = wrap(results, elapsed);
        const r = {
            statusCode: 200,
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(wrapped)
        };

        callback(null, r);
    });
};

