#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.09
'use strict';

const handler = require('./index').handler;

handler(null, null, (err, resp) => {
    const body = JSON.parse(resp.body);

    console.log(body.status, body.ts, body.tm, body.version);
    body.data.forEach(line => {
        console.log(line.url, line.stars);
    });
});

