# Close5 Code Challenge

In this coding challenge, you will create an API server preferably using express
4, but you can also use other http server frameworks (or none at all). If you do
please comment in the code explaining why you did this.

Below you will find a large JSON array of objects. These objects are small
representations of items that people have listed for sale on Close5. With your
API server, use this static array as your database and create 1 or more http
routes that return this data as JSON in various different ways. For example:

* The entire list sorted by creation date (Ascending and Descending)
* The entire listed sorted by the item’s price (Ascending and Descending)
* Any single item by its id
* An array of items based on any userId
* A route to request for items within 50 miles of their location coordinates

The file(s) should be bundled up into one node project so that anyone can run
`node app.js`. Then, it should be possible to hit localhost:8080 giving a guide
on to how to use your API.

Feel free to ask any questions or for clarity on the challenge. You can email me
the zip file or send me a github link to dylan@close5.com

Good luck!

Dylan

P.S. You can find the data in the file, data.json. Below are the first few
lines.



```javascript
[{
    "id" : "53fb8f26456e74467b000001",
    "loc" : [
        36.1665407118837763,
        -115.1408087193642729
    ],
    "userId" : "53f6c9c96d1944af0b00000b",
    "description" : null,
    "price" : -1,
    "status" : "removed",
    "createdAt" : ISODate("2014-08-25T19:31:50.180Z")
},
{
    "id" : "53fb8f81456e74467b000002",
    "loc" : [
        36.1632776369483580,
        -115.1409809579232757
    ],
    "userId" : "53f6c9c96d1944af0b00000b",
    "description" : "Cup",
    "price" : 20,
    "status" : "removed",
    "createdAt" : ISODate("2014-08-25T19:33:21.153Z")
},
[...]
```

P.P.S. I've taken the liberty of saving you some time and formatting the JSON in a MongoDB digestible format. If you `chmod +x ./mongo-dump.sh` from this directory, you can execute `./mongo-dump.sh` and populate a Mongo collection with all the items. The DB name will be `close5-test`, collection `items`.

\- Cheers, Chris
