#!/bin/sh
# dpw@seattle.local
# 2016.12.06
#

# set silent...
flags='-s'

echo "should return a list of items..."

response=`curl ${flags} http://localhost:8080/shop/v0/items/user/53fe114b56ef467e68000007`

check=`echo $response | fgrep '"userId":"53fe114b56ef467e68000007"'`
if [ "$check" == "" ]
then
    echo "$response"
    echo "FAILED!"
else
    echo "$response"
    echo "ok"
fi

response=`curl ${flags} http://localhost:8080/shop/v0/items/user/my-bad-id`

check=`echo $response | fgrep '"id":"53fbe21c456e74467b000006"'`
if [ "$check" != "" ]
then
    echo "$response"
    echo "FAILED!"
else
    echo "$response"
    echo "ok"
fi
