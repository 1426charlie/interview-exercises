#!/bin/sh
# dpw@seattle.local
# 2016.12.06
#

# set silent...
flags='-s'

echo "should return an item..."

response=`curl ${flags} http://localhost:8080/shop/v0/item/53fbe21c456e74467b000006`

check=`echo $response | fgrep '"id":"53fbe21c456e74467b000006"'`
if [ "$check" == "" ]
then
    echo "$response"
    echo "FAILED!"
else
    echo "$response"
    echo "ok"
fi

response=`curl ${flags} http://localhost:8080/shop/v0/item/my-bad-id`

check=`echo $response | fgrep '"id":"53fbe21c456e74467b000006"'`
if [ "$check" != "" ]
then
    echo "$response"
    echo "FAILED!"
else
    echo "$response"
    echo "ok"
fi
