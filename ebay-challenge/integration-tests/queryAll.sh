#!/bin/sh
# dpw@oakland.local
# 2016.12.06
#

# set silent...
flags='-s'

echo "should return a list of items..."

curl ${flags} "http://localhost:8080/shop/v0/items/all/created/asc"
curl ${flags} "http://localhost:8080/shop/v0/items/all/created/desc"
curl ${flags} "http://localhost:8080/shop/v0/items/all/price/asc"
curl ${flags} "http://localhost:8080/shop/v0/items/all/price/desc"

