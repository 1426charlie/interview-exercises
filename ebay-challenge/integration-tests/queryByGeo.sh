#!/bin/sh
# dpw@oakland.local
# 2016.12.06
#

# set silent...
flags='-s'

echo "should return a list of items by geo..."

# berkeley (should return 44)
lat=37.8716
long=-122.2727
curl ${flags} "http://localhost:8080/shop/v0/items/geo/${lat}/${long}"

# new york (should return zero)
lat=40.7128
long=-74.0059
curl ${flags} "http://localhost:8080/shop/v0/items/geo/${lat}/${long}"

# cancun (7 items)
lat=21.1213285
long=-86.9194803
curl ${flags} "http://localhost:8080/shop/v0/items/geo/${lat}/${long}"
