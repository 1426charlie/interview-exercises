# Close5 / EBAY Challenge | Geo Query

## Overview

The challenge was implemented in Node.js version 6.9.1 using express 4.14 combined with a few dependencies to create a small, light-weight REST container.

Per challenge specification the JSON data was used to simulate a real database. Queries where implemented with standard javascript filtering to substitute for SQL or non-SQL queries.  The data access layer is implemented with promises to emulate asynchronous database operations.

The project is fully unit tested, jshint processed and includes integration test exercises.  There is a make file with a few time-saving targets like `make run`, `make test`, `make integration-test`, `make watch`, etc.

The primary modules are in the `lib` folder with unit tests in the `test` folder.  Example API use can be found in `integration-test` folder in multiple curl scripts (bash shell scripts).  Logs are stored in the `logs` folder.

## Installation Instructions

Unzip the distribution `ebay-challenge.tar.gz`.  Then `cd ebay-challenge` and run `npm install` to pull the dependencies.  From there you should be able to run `make test` or simply run the application with `node app.js`.

## Running Instructions

Follow these steps:

```
npm install
make run
```

There should be a few output lines to explain where the log file is and what the app version is.  

Then on a separate terminal, do this:

```
tail -f logs/shop-api-<pid>.log
```

Where <pid> is the running instance process id.  Then, then integration-tests folder contains a way to run each API.  The list includes:

```
./integration-tests/findItemById.sh
./integration-tests/queryAll.sh
./integration-tests/queryByUserId.sh
./integration-tests/queryByGeo.sh
```

The scripts have multiple curl calls to the API, looking for "good" and "bad" news (expected rows or zero rows).

## API Spec

### Basic Routing Spec

Access to shop items is provided through a public-ish API that generally follows this form:

`http(s)://<host>/shop/<api-version>/<resource>/<criteria>`

Where `<host>` is any host, `<api-version>` identifies the specific API version, `<resource>` identifies the resource noun to operate on, and `<criteria>` is one or more set of search/sort criteria to locate specific records.  A typical example would be to find an item by id.  It's pattern would look like this:

`http://localhost:8080/shop/v0/item/:id`

API versioning provides a way to grow and migrate the API as new features are defined and implemented.  For example, future implementations may want to provide a wrapper around all responses that include timestamp, row count, status ok/failed, etc.

### Query by Create Date

The curl example of fetching all items sorted by creation date in ascending order:

`curl http://localhost:8080/shop/v0/items/all/created/asc`

The same query with items sorted in descending order:

`curl http://localhost:8080/shop/v0/items/all/created/desc`

### Query by Item Price

```
curl "http://localhost:8080/shop/v0/items/all/price/asc"

curl "http://localhost:8080/shop/v0/items/all/price/desc"
```

### Find by Item Id

This endpoint returns a single item model if found, or error if not found.

```
id=542204653f37951d5c000036
curl "http://localhost:8080/shop/v0/item/${id}"
```

If this is a known id, the return object would look something like this:

```
{
	"id":"53fbe21c456e74467b000006",
	"createdAt":"2014-08-26T01:25:48.754Z",
	"loc":[36.15517247887697,-115.14484161837342],
	"description":"",
	"price":20,
	"status":"removed"
}
```

### Query by User Id

```
uid=53fe114b56ef467e68000007
curl "http://localhost:8080/shop/v0/items/user/${uid}"
```

Should return 16 items.

### Query by Geo Location

```
# cancun
lat=21.1213285
long=-86.9194803
curl "http://localhost:8080/shop/v0/items/geo/${lat}/${long}"
```

Should produce seven rows.

## Implementation Details

The package file includes standard node packages plus some configuration data used by the application e.g., port, db location, etc.

### Code Layout

* app.js -- at the top package level
* lib/ -- where the worker code is
* test/ -- where all the tests are
* integration-tests/ -- the curl scripts that exercise all endpoints
* logs/ -- the log files separated by pid
* Makefile -- run, test, jshint, version targets
* package.json -- the standard node package file with additional configuration attributes

### Programming Style

I usually code to a specific style guide but absent one I default to Douglas Crockford's 'Javascript - The Good Parts' and follow the K&R style.  You will also see objects defined as 'classes', but they are Crockford's way of using functions to define what may appear to be a class structure.

In a team environment, the team would adopt and enforce their own standard.

### app.js

This file contains minimal logic--just enough to access resources and start the listener.

### BootStrap

The application's bootstrap is inside `lib/BootStrap.js`.  It creates the shop dao, handlers, middleware and routes.  It's kept small and to the point so doesn't include any rigorous tests.  

### Data Access Object

The DAO component (ShopDao) is a thin wrapper around the static data with an API interface that could be used against a real data store. It reads the database on startup  (initData) and creates formal data models for each item.  Simple mapping and hashing are used to simulate a real database.

The DAO uses promises to return results.  For this implementation direct value returns would have been possible, but a realistic API would work against a real database and always be asynchronous.  So this implementation mimics a real database implementing asynchronous promises.

### Handlers

The Handlers module connects routed requests to actual data access.  It has an associated HandlersTest file for unit tests.

### Routes

The Routes module is where all the routes are defined and assigned to handlers.  There is an associated RoutesTest file to run minimal unit tests.

### Models

There is a single data model called `ItemModel` that formally encapsulates each item.  It mimics the items from the sample JSON database but overrides the `id` attributes to be a bit more javascript friendly.  It is a value object with very little logic, only enough to marshal mongo's id and date/time values to more manageable attributes.  Unit tests for this are in the ShopDaoTests.


### Logging

There is a single log file for the project.  Entries for each log statement include a timestamp, the `category` or class that the statement was generated from and a message.  Log levels extend from debug to fatal but realistically info, warn and error were used for this application. 

## Testing

I used mocha/chai as the test framework.  You can run the unit tests with this:

`make test`

Or by running mocha directly against any of the unit tests.

There is also a watcher that monitors all the source files, including test files for any changes and runs the full suite of unit tests when file changes are detected.  You may run this with:

`make watch`

### Unit Tests

Each module under the `lib` folder has an associated test file.  There is also a specific file `MockExpress` used to mock http request and responses.

### Integration Tests

There is a curl test for each of the endpoints.  It simply hits the service to exercise the API, so not much testing going on.

## References

* [Vincenty's Formulae](https://en.wikipedia.org/wiki/Vincenty%27s_formulae)
* [Stack Overflow' Lat/Long Calcs](https://stackoverflow.com/questions/5031268/algorithm-to-find-all-latitude-longitude-locations-within-a-certain-distance-fro)
* [Aviation Distance Between Points](http://williams.best.vwh.net/avform.htm#Dist)
* [Distance Between Two Points](http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates#Distance)
* [Node Geo Lib](https://github.com/manuelbieh/geolib)

###### darryl.west@raincitysoftware.com | Version 0.0.15 | 2016-12-07

