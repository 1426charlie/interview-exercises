#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

import os

ignores = [ '__pycache__', '.git' ]

def walk(path):
    for child in os.listdir(path):
        if child not in ignores:
            childpath = os.path.join(path, child)
            if os.path.isdir(childpath):
                walk(childpath)
            else:
                print(childpath)
            

if __name__ == '__main__':
    print('simulates os.walk')
    walk('.')

