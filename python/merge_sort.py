#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.24

from heapq import merge
from random import randint

def merge_sort(m):
    if len(m) <= 1:
        return m

    middle = len(m) // 2
    left = m[:middle]
    right = m[middle:]

    left = merge_sort(left)
    right = merge_sort(right)

    return [ x for x in merge(left, right) ]

if __name__ == "__main__":
    list = [ randint(0, 10000) for x in range(10000) ]
    sorted = merge_sort( list )

    print(sorted)

