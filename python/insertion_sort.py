#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.24


#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.24

from random import randint
import bisect

# with code
def insort(seq):
    for i in range(1, len(seq)):
        j = i - 1
        key = seq[i]
        while (seq[j] > key) and (j >= 0):
            seq[j + 1] = seq[j]
            j -= 1

        seq[j + 1] = key

# from the lib
def insertion_sort(seq):
    for i in range(1, len(seq)):
        bisect.insort(seq, seq.pop(i), 0, i)
        
if __name__ == "__main__":
    # this is a O(n**2) so choose a small list
    list = [ randint(0, 1000) for x in range(20) ]
    insort( list )

    # insertion_sort( list )

    print(list)

