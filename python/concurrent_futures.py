#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

from concurrent import futures
import time

def callback(future):
    print('done->', future.result(10))

def do_one(x):
    print('start->', x)
    time.sleep(x)
    print('done->', x)
    return x

def do_many():
    values = [ 4, 2, 7, 2, 1, 6, 10 ]
    workers = min(len(values), 20)
    with futures.ThreadPoolExecutor(workers) as executor:
        res = executor.map(do_one, values)

    print(list(res))
    return len(list(res))

def submit():
    with futures.ThreadPoolExecutor(max_workers=1) as executor:
        future = executor.submit(pow, 323, 1235)
        future.add_done_callback(callback)

if __name__ == '__main__':
    submit()
    do_many()

