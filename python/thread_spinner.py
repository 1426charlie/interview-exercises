#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

import threading
import itertools
import time
import sys

# the sentinal
class Signal():
    go = True

def slow_fn():
    time.sleep(3.5)
    return 42

def spin(msg, signal):
    write, flush = sys.stdout.write, sys.stdout.flush
    for char in itertools.cycle('|/-\\'):
        # status = char + ' ' + msg
        status = '{} {}'.format(char, msg)
        write(status)
        flush()
        write('\x08' * len(status))
        time.sleep(0.1)
        if not signal.go:
            break

    write(' ' * len(status) + '\x08' * len(status))

def supervisor():
    signal = Signal()
    spinner = threading.Thread(target=spin, args=('thinking!', signal))
    print('spinner object:', spinner)
    spinner.start()
    result = slow_fn()
    signal.go = False
    spinner.join()
    return result

def main():
    result = supervisor()
    print('Answer:', result)

if __name__ == '__main__':
    print('python threads')
    main()

