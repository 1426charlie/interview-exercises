#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.18

from math import *
from functools import lru_cache
from clock_decorator import clock

def analytic(n):
    sqrt_5 = sqrt(5)
    p = (1 + sqrt_5) / 2
    q = 1 / p
    return int((p**n + q**n) / sqrt_5 + 0.5)

def iterative(n):
    if n < 2:
        return n

    prev = 1
    fib = 1
    for n in range(2, n):
        prev, fib = fib, fib + prev

    return fib

# really slow!!!
def recursive(n):
    if n < 2:
        return n
    else:
        return recursive(n-1) + recursive(n-2)

# does it's own iteration
def generative(n):
    a, b = 1, 1
    while n > 0:
        yield a
        a, b, n = b, a+b, n-1

@lru_cache(maxsize=128)
def cached(n):
    if n < 2:
        return n

    return cached(n-1) + cached(n-2)

max = 36

@clock('{name}({args}): {elapsed}s')
def run_anafib():
    anafib = [ analytic(x) for x in range(1, max) ]
    return anafib

@clock('{name}({args}): {elapsed}s')
def run_iterfib():
    iterfib = [ iterative(x) for x in range(1, max) ]
    return iterfib

@clock('{name}({args}): {elapsed}s')
def run_recfib():
    recfib = [ recursive(x) for x in range(1, max) ]
    return recfib

@clock('{name}({args}): {elapsed}s')
def run_genfib():
    genfib = [ x for x in generative(max - 1) ]
    return genfib

@clock('{name}({args}): {elapsed}s')
def run_cachedfib():
    cachedfib = [ cached(x) for x in range(1, max) ]
    return cachedfib

def run():
    print(run_anafib())
    print(run_iterfib())
    # print(recfib)
    print(run_genfib())
    print(run_cachedfib())

    print(cached.cache_info())

run()
