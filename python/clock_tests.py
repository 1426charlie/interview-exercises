#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

from functools import lru_cache
from clock_decorator import clock

@clock()
def factorial(n):
    return 1 if n < 2 else n * factorial(n-1)

@lru_cache()
@clock()
def fibonacci(n):
    return n if n < 2 else fibonacci(n-2) + fibonacci(n-1)

if __name__ == '__main__':
    print('*' * 40, 'calling factorial(20)')
    print('20! = ', factorial(20))
    print('*' * 40, 'calling fibonacci(20) with lru_cache')
    print('fib 20 = ', fibonacci(20))
    print('*' * 40, 'calling fibonacci(21) with lru_cache, second call')
    print('fib 21 = ', fibonacci(21))


