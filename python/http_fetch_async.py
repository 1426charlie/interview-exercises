#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.31

import sys
import asyncio
import urllib.parse

def parse_url(url):
    parts = urllib.parse.urlparse(url)

    assert parts.scheme in ('http', 'https'), repr(url)
    ssl = parts.scheme == 'https'

    port = parts.port
    if port is None:
        port = 443 if ssl else 80

    path = parts.path or '/'
    if parts.query:
        path += '?' + parts.query

    return (parts.hostname, ssl, port, path)

@asyncio.coroutine
def fetch(url):

    host, ssl, port, path = parse_url(url)

    r, w = yield from asyncio.open_connection(host, port, ssl=ssl)
    request = 'GET %s HTTP/1.0\r\n\r\n' % path
    print('>', request, file=sys.stderr)
    w.write(request.encode('utf-8'))
    while True:
        line = yield from r.readline()
        line = line.decode('utf-8').rstrip()
        if not line:
            break

        print('<', line, file=sys.stderr)

    print(file=sys.stderr)
    body = yield from r.read()
    return body

def main():
    url = 'http://raincitysoftware.com'
    loop = asyncio.get_event_loop()
    try:
        body = loop.run_until_complete(fetch(url))
        print(body.decode('latin-1'), end='')
    finally:
        loop.close()

if __name__ == '__main__':
    main()

