#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.03

class Bag:
    def __init__(self):
        self.data = []

    def add(self, obj):
        self.data.append(obj)

    def add_twice(self, obj):
        self.add(obj)
        self.add(obj)

if __name__ == '__main__':
    print('property bag')
    b = Bag()
    b.add('a thing')
    b.add_twice('double')

    print('data:', b.data)

