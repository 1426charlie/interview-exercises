#!/usr/bin/env python
"""
A server that uses threads to handle multiple clients.  Entering any line of 
input at the terminal will exit the server.
"""

import select
import socket
import sys
import threading

# create a generic listener to this machine
# HOST = socket.gethostname()

# create a fast localhost connection
HOST = '127.0.0.1'
PORT = 5001

class Server:
    def __init__(self):
        self.host = HOST
        self.port = PORT
        self.backlog = 5
        self.size = 1024
        self.server = None
        self.threads = []

    def open_socket(self):
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.bind((self.host,self.port))
            self.server.listen(5)
        except socket.error:
            if self.server:
                self.server.close()
            print("Could not open socket: ")
            sys.exit(1)

    def run(self):
        self.open_socket()
        input = [self.server,sys.stdin]
        running = True
        print('listening on host/port', self.host, self.port)
        while running:
            inputready,outputready,exceptready = select.select(input,[],[])

            for s in inputready:
                if s == self.server:
                    # handle the server socket
                    (clientsocket, address) = self.server.accept()
                    print('new client: ', address)
                    c = Client(clientsocket, address)
                    c.start()
                    self.threads.append(c)

                elif s == sys.stdin:
                    # handle standard input
                    junk = sys.stdin.readline()
                    running = False

        # close all threads

        self.server.close()
        for c in self.threads:
            c.join()

class Client(threading.Thread):
    def __init__(self, client, address):
        threading.Thread.__init__(self)
        self.client = client
        self.address = address
        self.size = 1024

    def run(self):
        running = True
        while running:
            data = self.client.recv(self.size)
            print('recv <-', data)
            if data:
                self.client.send(data)
            else:
                self.client.close()
                running = False

if __name__ == "__main__":
    s = Server()
    s.run()

