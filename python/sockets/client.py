#!/usr/bin/env python

# NOTE: see websocket implementation for a much better use of ayncio/json/datetime

# dpw@seattle.local
# 2016.10.30

import os
# import asyncio
import socket
import json
from uuid import uuid4
from datetime import datetime
from collections import namedtuple
import time

# HOST = 'seattle.local'
HOST = '127.0.0.1'
PORT = 5001

Wrapper = namedtuple('Wrapper', ['msgid', 'ts', 'status', 'data'])
Customer = namedtuple('Customer', ['id','dateCreated','lastUpdated','version','name','email','status'])

isonow = datetime.utcnow().isoformat()

customers = [
    Customer(uuid4().hex, isonow, isonow, 0, 'Koka Kola','kk@kk.com', 'active'),
    Customer(uuid4().hex, isonow, isonow, 0, 'Renolds','renolds@renren.com', 'active')
]

wrapper = Wrapper(uuid4().hex, datetime.utcnow().timestamp(), 'ok', [c._asdict() for c in customers])

def exchange():
    count = 0

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        
        running = True
        while running:
            msg = json.dumps(wrapper._asdict())
            s.sendall(bytearray(msg, 'utf-8'))
            print('sent ->', msg)
            count += 1

            data = s.recv(1024)
            print('received <-', repr(data))

            time.sleep(0.2)

            if count > 5:
                running = False;

    print('bye...')

if __name__ == '__main__':
    print('start the exchange loop...')
    exchange()
