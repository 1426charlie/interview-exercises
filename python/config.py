#!/usr/bin/env python

"""Configure based on external env, json file, etc"""

import collections

# define the attributes
ApplicationConfig = collections.namedtuple('ApplicatinConfig', [ 
    'version', 'copyright', 'data'
])

cfg = {
    "canDelete": False,
    "isOwner": True,
    "otherThings":[ 1, 4, 22 ]
}

# create the instance
env = ApplicationConfig('90.01.101', 'Copyright 2016, Rain City Software', cfg)

