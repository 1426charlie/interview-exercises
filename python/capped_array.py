#!/usr/bin/env python

"""implement a capped array using itertools"""

# dpw@seattle.local
# 2016.10.31

from itertools import cycle

# NOTE: this is not a very good solution...

class CappedArray:
    def __init__(self, size=25, value=None):
        self.idx = cycle(range(size))
        self.list = [value for x in range(size)]

    def __len__(self):
        return len(self.list)

    def put(self, item):
        i = next(self.idx)
        self.list[i] = item
        return i

    def take(self):
        i = next(self.idx)
        item = self.list[i]
        self.list[i] = None
        return item
        
    def peek(self, index):
        return self.list[index]

if __name__ == '__main__':
    print( 'new python file' )
    size = 20
    capped = CappedArray(size, 1)
    for i in range(size):
        capped.put((i+10))
    
    print(capped.list)
    print(capped.take())
    print(capped.take())
    print(capped.take())
    print(capped.take())
    print(capped.list)
