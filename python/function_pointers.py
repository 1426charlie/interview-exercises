#!/usr/bin/env python
"""
This utility implements the strategy pattern to select the best discount for an order/customer based on
a set of promo/discount rules.  Rather than using class inheritance it uses function pointers to provide
a simpler solution.

@see Fluent Python chapter 6: Design Patterns for First-Class Functions
"""

# dpw@oakland.local
# 2016.10.28

from collections import namedtuple
Customer = namedtuple('Customer', ['name', 'points'])
Order = namedtuple('Order', ['name','items','itemCount','coupon'])

customers = [ Customer('Noo Bee', 10), Customer('ABC Corp', 1900) ]
orders = [
    Order('small', [(3, 'sox'),(4, 'pants')], 2, None),
    Order('bulk', [(44, 'blankets'), (57, 'pillows')], 25, None),
    Order('coop', [(3, 'ears')], 2, 'my coupon')
]

def loyalty_promo(order, customer):
    return 0.05 if customer.points > 1000 else 0.0

def bulk_promo(order, customer):
    return 0.08 if order.itemCount >= 25 else 0.0

def coupon_promo(order, customer):
    return 0.03 if order.coupon is not None else 0.0

promos = [ loyalty_promo, bulk_promo, coupon_promo ]
def best_discount(order, customer):
    return max(promo(order, customer) for promo in promos)

if __name__ == '__main__':
    for customer in customers:
        print(customer)
        n = 1
        for order in orders:
            disc = best_discount(order, customer)
            print(n, order, disc)
            n += 1


