#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.24

import numpy as np
import pandas as pd

df = pd.DataFrame(np.random.random((200,3)))
df['datetime'] = pd.date_range('date', periods=200, freq='D')
mask = (df['datetime'] > '1T23:35:00') & (df['datetime'] <= '2T00:05:00')

print(df.loc[mask])


