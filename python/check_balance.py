#!/usr/bin/env python

"""Determine if the string of nested brackets is balanced where the brackets include {}[]()"""

# darryl.west@raincitysoftware.com
# 2016.11.03

# a map of expected balanced brackets
opposites = { '{':'}', '[':']', '(':')' }

def parse(s):
    """helper to pars a string of brackets into list"""
    return [c for c in s if c != ' ']

def chars_balanced(left, right):
    """use mapping to pick of the expected opposing bracket"""
    bal = opposites[ left ] == right
    # print(left, right, bal)
    return bal

def check_balanced(values):
    """create a one-sided stack and push the left brackets.  
       when a right bracked is encountered, pop the expected left bracket and verify
       return false on first failure.
    """

    # must be even set to be balanced
    if (len(values) % 2 != 0):
        return False;

    stack = []
    for v in values:
        if v in ['{','(','[']:
            stack.append(v)
        elif v in ['}', ')', ']']:
            if len(stack) == 0:
                return False

            c = stack.pop()
            if not chars_balanced(c, v):
                return False
        else:
            print('invalid character...')
            return False

    return len(stack) == 0

if __name__ == '__main__':
    print('check the balance between nested brackets')
    balanced = [
        '{ { [ ] } } {}',
        '{ [ { { ( ( ( ( [ [ ] ] ) ) ) ) } } ] } { { } } ()'
    ]
    unbalanced = [
        '{ { [ } } }',
        '{{{',
        '{ { { { ( ( ( ( [ ] ] ) ) ) ) } } } }'
    ]

    for s in balanced:
        v = parse(s)
        b = check_balanced(v)
        assert(b == True)
        print(v, 'is balanced')

    for s in unbalanced:
        v = parse(s)
        b = check_balanced(v)
        assert(b == False)
        print(v, 'is NOT balanced')

