#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.25

import bisect
import random

SIZE = 7
random.seed(1729)

my_list = []

for i in range(SIZE):
    item = random.randrange(SIZE * 2)
    bisect.insort(my_list, item)
    print('%2d ->' % item, my_list)


