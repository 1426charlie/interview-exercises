#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.01

import time
from coroutil import coroutine
from collections import namedtuple
import inspect
getstate = inspect.getgeneratorstate

Average = namedtuple('Average', [ 'timestamp', 'average', 'total', 'count' ])

@coroutine
def averager():
    total = 0.0
    count = 0
    average = None
    while True:
        term = yield Average(time.time(), average, total, count)
        total += term
        count += 1
        average = total / count

if __name__ == '__main__':
    print('averager using coroutine and primer from coroutil')
    coro = averager()
    # next(coro)
    print(getstate(coro))
    for x in (10, 30, 5):
        print(coro.send(x))
        print(getstate(coro))

