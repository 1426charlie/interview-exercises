#!/usr/bin/env python

from collections import defaultdict

word = 'mississippi'
d = defaultdict(int)

for k in word:
    d[k] += 1

print('word: {}, char counts: {}'.format(word, list(d.items())))

