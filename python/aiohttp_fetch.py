#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

import asyncio
import aiohttp

@asyncio.coroutine
def fetch(url):
    resp = yield from aiohttp.request('GET', url)
    text = yield from resp.read()
    return text

def main():
    print('fetch the page...')
    obj = fetch('http://raincity.s3-website-us-east-1.amazonaws.com/config/links.json')
    print(obj)

if __name__ == '__main__':
    print('aiohttp fetch page')
    main()

