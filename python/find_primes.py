#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.31

import sys
from math import sqrt

def is_prime(a):
    if a == 2: return True
    if a < 2 or a % 2 == 0: return False
    return not any(a % x == 0 for x in range(3, int(a**0.5) + 1, 2))
        
if __name__ == '__main__':
    start = 2000
    count = 0
    for n in range(start, start + 1000):
        if is_prime(n):
            count += 1
            print(n)

    print("count: ", count)

