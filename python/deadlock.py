#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.22

from concurrent.futures import ThreadPoolExecutor
import time

def wait_on_b():
    print('b is sleeping')
    time.sleep(5)
    print(b.result())
    return 5

def wait_on_a():
    print('a is sleeping...')
    time.sleep(5)
    print(a.result())
    return 6

executor = ThreadPoolExecutor(max_workers=4)
a = executor.submit(wait_on_b)
b = executor.submit(wait_on_a)

print('started, press ctrl-c to halt...')
