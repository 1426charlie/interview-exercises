#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.18

# from tornado.concurrent import Future
from tornado.httpclient import HTTPClient

def fetch(url):
    print('synchronous fetch from url: ', url)

    client = HTTPClient()
    response = client.fetch(url)
    print('time: ', response.time_info)

    return response.body

body = fetch('http://dev.nowce.com')
for line in body.split(b'\n'):
    print(line.decode('utf-8'))

