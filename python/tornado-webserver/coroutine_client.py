#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.18

from tornado.httpclient import AsyncHTTPClient
# from tornado.concurrent import Future

async def fetch_coroutine(url):
    print('fetch from url: ', url)
    client = AsyncHTTPClient()
    response = await client.fetch(url)
    return response.body

if __name__ == '__main__':
    body = fetch_coroutine('https://www.toptal.com')
    print('body: ', body)
