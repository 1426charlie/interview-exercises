#!/usr/bin/env python

import tornado.ioloop
import tornado.web

port = 9000

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('hello python world!')

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    print('starting web server on port: ', port)
    app = make_app()
    app.listen(port)
    tornado.ioloop.IOLoop.current().start()

