#!/usr/bin/env python

# dpw@oakland.local
# 2016.10.28

from collections import namedtuple

Average = namedtuple('Average', ['count', 'total', 'avg'])

# not good because series will grow forever
def xmake_averager():
    series = []

    def averager(v):
        series.append(v)
        return sum(series) / len(series)

    return averager

# a better impementation that can be set at definition time...
def make_averager(count = 0, total = 0):
    def averager(v):
        nonlocal count, total
        count += 1
        total += v
        return Average(count, total, total / count)

    return averager

if __name__ == '__main__':
    avg = make_averager()
    values = [ 10, 11, 12, 9, 8, 10, 10, 13, 6 ]
    for n in values:
        print(n, avg(n))

