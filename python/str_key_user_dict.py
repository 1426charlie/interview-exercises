#!/usr/bin/env python
"""
StrKeyDict - subcass of collections.UserDict
"""

# dpw@seattle.local
# 2016.10.26

import collections

class StrKeyDict(collections.UserDict):
    """StrKeyDict"""

    def __missing__(self, key):
        if isinstance(key, str):
            raise KeyError(key)
        return self[str(key)]

    def __contains__(self, key):
        return str(key) in self.data

    def __setitem__(self, key, item):
        self.data[str(key)] = item

if __name__ == '__main__':
    hash = StrKeyDict()
    kv = [(303, 'sliders'), (402,'my thing'), (404, 'no page found')]

    for k,v in kv:
        print(k, v)
        hash[ k ] = v

    print(hash)
