#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.01

from functools import wraps

def coroutine(func):
    """decorator: primest `func` by advancing to first `yield`"""
    @wraps(func)
    def primer(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen
    return primer

