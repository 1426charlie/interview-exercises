#!/usr/bin/env python

# see https://docs.python.org/3.3/library/string.html#formatexamples
import datetime
import string
from string import Template

# print('printable ascii chars: {}'.format(string.printable))

print('# simple padding {}'.format('#' * 60))
print('zero padded: {:0=20.2f}'.format( 35.4 ))
print('star padded: {:*=20.2f}'.format( 53.0 ))
print('comma + pad: {:0=20,.2f}'.format( 65.9 ))
print('binary num : {:0=20b}'.format(223))
print('hex num    : {:0=20x}'.format(33223))


print('\n# alignments {}'.format('#' * 60))
slist = [
    '...{:<30}...'.format('left align'),
    '...{:>30}...'.format('right align'),
    '...{:^30}...'.format('centered'),
    '...{:*^30}...'.format('star-centered')
]

for v in slist:
    print(v)



print('\n# datetime {}'.format('#' * 60))
dt = datetime.datetime(2010, 7, 4, 12, 15, 58)
print('py iso: {}'.format( dt.isoformat() ))
print('isoish: {:%Y-%m-%dT%H:%M:%S}.{:3}Z'.format( dt, dt.microsecond ))

print('\n# tables {}'.format('#' * 60))
width = 5
for num in range(5,12):
    for base in 'dXob':
        print('{0:{width}{base}}'.format(num, base=base, width=width), end=' ')
    print()


print('\n# templates {}'.format('#' * 60))
t = Template('$who likes $what')
s = t.substitute(who='Tommy', what='King Kong (the movie, not the beast)')
print( t, s )

d = dict(who='Timmy')
print(Template('Give $who $100.00').safe_substitute(d))
print(t.safe_substitute(d))
d[ 'what' ] = 'his toes, that\'s what.'
print(t.safe_substitute(d))

print('\n# cap words {}'.format('#' * 60))
s = 'my name: herburt j hooverville'
print(string.capwords(s))
