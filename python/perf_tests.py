#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.25

from time import perf_counter as perf

import numpy as np

floats = np.arange(0.0, 10.0**8)

# do some big calc
t0 = perf(); floats /= 3.0; t1 = perf() - t0

print('perf: ', t1, 'to divide 3.0 into an array of', int(len(floats) / 1e6), 'M floats...');

