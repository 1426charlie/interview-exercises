# AIO HTTP

## Reference

An HTTP client/server for asyncio (PEP 3156). Requires python 3.5+

* http://aiohttp.readthedocs.io/en/stable
