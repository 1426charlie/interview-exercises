#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

from aiohttp import web

async def handle(request):
    name = request.match_info.get('name', 'anybody')
    text = 'howdy, ' + name
    return web.Response(text=text)

def run():
    app = web.Application()
    app.router.add_get('/', handle)
    app.router.add_get('/{name}', handle)

    print('start the web server...')
    web.run_app(app)

if __name__ == '__main__':
    print('aiohttp web')
    run();

