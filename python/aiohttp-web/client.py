#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

import aiohttp
import asyncio
import async_timeout

link = 'http://raincity.s3-website-us-east-1.amazonaws.com/config/links.json'
# link = 'http://raincitysoftware.com'

async def fetch(session, url):
    timeout = 10
    print('fetch:', url, 'with timeout:', timeout)
    with async_timeout.timeout(timeout):
        async with session.get(url) as response:
            print('status:', response.status)
            return await response.json()

async def main(loop):
    async with aiohttp.ClientSession(loop=loop) as session:
        out = await fetch(session, link)
        print(out)

def run():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))

if __name__ == '__main__':
    print('run the http client')
    run()

