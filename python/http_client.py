#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.18

# @see http://docs.python-requests.org/en/master/

import requests
import http.client
import json

# the old way
def fetch_url(host, page):
    conn = http.client.HTTPConnection(host)
    conn.request('GET', page)
    resp = conn.getresponse()
    print(resp.status, resp.reason)
    data = resp.read()
    conn.close()

    return data

def fetch(url):
    r = requests.get(url)
    return r.json()

if __name__ == '__main__':
    obj = fetch('http://raincity.s3-website-us-east-1.amazonaws.com/config/links.json')
    print(json.dumps(obj, indent=2))

