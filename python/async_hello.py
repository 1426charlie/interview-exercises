#!/usr/bin/env python

import asyncio

def hello_world(loop):
    print('howdy asyncr!')
    loop.stop()

loop = asyncio.get_event_loop()

loop.call_soon(hello_world, loop)

loop.run_forever()
print('after run...')
loop.close()
print('closed...')
