#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.02

import asyncio
import datetime

def show_date(s):
    print(s, datetime.datetime.now())

async def display_async_date(loop):
    end_time = loop.time() + 5.0
    while True:
        show_date('async')
        if (loop.time() + 1.0) >= end_time:
            break;

        await asyncio.sleep(1.0)

@asyncio.coroutine
def display_coro_date(loop):
    end_time = loop.time() + 5.0
    while True:
        show_date('coro ')
        if (loop.time() + 1.0) >= end_time:
            break

        yield from asyncio.sleep(1.0)

def main():
    # run the two in series
    loop = asyncio.get_event_loop()
    loop.run_until_complete(display_async_date(loop))
    loop.run_until_complete(display_coro_date(loop))
    loop.close()

if __name__ == '__main__':
    print('five second timer')
    main()

