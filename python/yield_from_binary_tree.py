#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.01

class BinaryTree:
    def __init__(self, left=None, us=None, right=None):
        self.left = left
        self.us = us
        self.right = right

    def __iter__(self):
        if self.left:
            yield from self.left
        if self.us:
            yield from self.us
        if self.right:
            yield from self.right

if __name__ == '__main__':
    print( 'new python file' )
    t = [13, 44, 2, 33, 98]

    # node = BinaryTree()


