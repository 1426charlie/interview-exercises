#!/usr/bin/env python

import asyncio
import websockets
import json
from uuid import uuid4
from datetime import datetime
from collections import namedtuple

host = 'localhost'
port = 8765

# define the message wrapper as a named tuple
Wrapper = namedtuple('Wrapper', ['msgid', 'ts', 'status', 'data'])
Customer = namedtuple('Customer', ['id','dateCreated','lastUpdated','version','name','email','status'])

isonow = datetime.utcnow().isoformat()

customers = [
    Customer(uuid4().hex, isonow, isonow, 0, 'Koka Kola','kk@kk.com', 'active'),
    Customer(uuid4().hex, isonow, isonow, 0, 'Renolds','renolds@renren.com', 'active')
]

wrapper = Wrapper(uuid4().hex, datetime.utcnow().timestamp(), 'ok', [c._asdict() for c in customers])

print(wrapper._asdict())

async def hello():
    count = 0
    async with websockets.connect('ws://{}:{}'.format(host, port)) as websocket:
        message = json.dumps(wrapper._asdict())
        await websocket.send(message)
        print("> {}".format(message))

        while True:
            msg = await websocket.recv()
            print("< {}".format(msg))
            count += 1

            if count % 10 == 0:
                await websocket.send(message)
                print("> {}".format(message))


asyncio.get_event_loop().run_until_complete(hello())

