#!/usr/bin/env python

import asyncio
import websockets
import json
from datetime import datetime

# TODO: refactor to read new socket messages
async def hello(websocket, path):
    message = await websocket.recv()
    print("< {}".format(message))

    wrapper = json.loads(message)
    mid = wrapper['msgid']

    greeting = "recid: {}".format(mid)
    await websocket.send(greeting)
    print("> id={}".format(greeting))

    while True:
        msg = { 'ts':datetime.now().timestamp() }
        await websocket.send(json.dumps(msg))
        print("> msg={}".format(msg))
        await asyncio.sleep(1)

host = 'localhost'
port = 8765

print("starting web socket service on {}, port {}".format(host, port))

start_server = websockets.serve(hello, host, port)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

