#!/usr/bin/env python

import asyncio

def factorial(name, number):
    f = 1
    for i in range(2, number + 1):
        print('task {}: compute factorial({})'.format(name, i))
        yield from asyncio.sleep(1)
        f *= i

    print('task {}: factorial({}) = {}'.format(name, number, f))

loop = asyncio.get_event_loop()
tasks = [
    asyncio.ensure_future(factorial("A", 4)),
    asyncio.ensure_future(factorial("B", 6)),
    asyncio.ensure_future(factorial("C", 3))
]

loop.run_until_complete(asyncio.gather(*tasks))
loop.close()

