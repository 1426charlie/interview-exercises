#!/usr/bin/env python

from functools import partial
from operator import mul

# we want to create a list of functions that square an in put over a series of numbers
# so, for m(2) the output should be [ 0, 2, 4, 6 ]

expected = [ 0, 2, 4, 6, 8 ]
count = len(expected)

# using a generator
def generator_multipliers():
    for i in range(count):
        yield lambda x : i * x

# bind the iterator index to j
def bound_multipliers():
    return [lambda x, j=i : j * x for i in range(count)]

# using func tools
def func_multipliers():
    return [partial(mul, i) for i in range(count)]

result = [m(2) for m in generator_multipliers()]
print( 'generator result: ', result )

result = [m(2) for m in bound_multipliers()]
print( 'bound result: ', result )

result = [m(2) for m in func_multipliers()]
print( 'func result: ', result )

