#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.01

import random

queue = []

class Node:
    def __init__(self, val):
        self.value = val
        self.left = None
        self.right = None

    def insert(self, data):
        if self.value == data:
            return False
        elif self.value > data:
            if self.left:
                return self.left.insert(data)
            else:
                self.left = Node(data)
                return True
        else:
            if self.right:
                return self.right.insert(data)
            else:
                self.right = Node(data)
                return True

    def find(self, data):
        if self.value == data:
            return True
        elif self.value > data:
            if self.left:
                return self.left.find(data)
            else:
                return False
        else:
            if self.right:
                return self.right.find(data)
            else:
                return False

    def preorder(self):
        if self:
            return self.value
            if self.left:
                self.left.preorder()
            if self.right:
                self.right.preorder()

    def postorder(self):
        if self:
            if self.left:
                self.left.postorder()
            if self.right:
                self.right.postorder()
            return self.value

    def inorder(self):
        if self:
            if self.left:
                self.left.inorder()
            return self.value
            if self.right:
                self.right.inorder()

class Tree:
    def __init__(self):
        self.root = None

    def insert(self, data):
        if self.root:
            return self.root.insert(data)
        else:
            self.root = Node(data)
            return True

    def find(self, data):
        if self.root:
            return self.root.find(data)
        else:
            return False

    def preorder(self):
        print("preorder")
        return self.root.preorder()

    def postorder(self):
        print("postorder")
        return eelf.root.postorder()

    def inorder(self):
        print("inorder")
        return self.root.inorder()

if __name__ == '__main__':
    print('binary tree')
    data = [ 7, 1, 0, 3, 2, 5, 4, 6, 9, 8, 10 ]
    bst = Tree()
    for x in data:
        bst.insert(x)

    print(bst.inorder())
    print(bst.preorder())
    print(bst.postorder())

