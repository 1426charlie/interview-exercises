#!/usr/bin/env python
"""an html generator using single dispach for generic processing"""

# dpw@seattle.local
# 2016.10.29

from functools import singledispatch
from collections import abc
import numbers
import html

@singledispatch
def htmlize(obj):
    content = html.escape(repr(obj))
    return '<pre>{}</pre>'.format(content)

@htmlize.register(str)
def _(text):
    content = html.escape(text).replace('\n', '<br/>\n')
    return '<p>{0}</p>'.format(content)

@htmlize.register(numbers.Integral)
def _(n):
    return '<pre>decimal:{0} hex:(0x{0:x}) bin:({0:b})</pre>'.format(n)

@htmlize.register(tuple)
@htmlize.register(abc.MutableSequence)
def _(seq):
    inner = '</li>\n<li>'.join(htmlize(item) for item in seq)
    return '<ul>\n<li>' + inner + '</li>\n</ul>'

if __name__ == '__main__':
    for v in [ {1, 2, 3}, abs, 'This is a string test...', 2015, [ 'alpha', 66, { 3, 2, 1 } ], [ 'this choice', 'or this one', 'or another choice' ]]:
        print(htmlize(v)) 

