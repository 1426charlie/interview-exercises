#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.23

from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "howdy from flask!"

if __name__ == "__main__":
    app.run()

