#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.18

class Singleton:
    __instance = None

    def __new__(cls, val):
        if Singleton.__instance is None:
            Singleton.__instance = object.__new__(cls)

        Singleton.__instance.val = val
        return Singleton.__instance

    def __str__(self):
        return repr(self) + self.val

if __name__ == '__main__':
    x = Singleton('sausage')
    print(x)
    y = Singleton('eggs')
    print(y)
    z = Singleton('spam')
    print(z)
    for v in [ x, y, z ]: print(v)

