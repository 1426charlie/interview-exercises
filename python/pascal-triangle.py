#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.22

# pascal triangle with caching...

from functools import reduce
from functools import lru_cache

@lru_cache(maxsize=128)
def calc_row(n):
    # create an n+1 row vector based on the zero based index
    row = [1 for x in range(n + 1)]

    for i in range(int(n / 2)):
        x = round(row[i] * (n - i) / (i + 1))

        # assign the value to symetric left/right hand sides
        row[i + 1] = row[n - 1 - i] = x

    return row

if __name__ == "__main__":
    # first cache some the results
    for n in range(32):
        calc_row(n)

    # now retreive a run...
    for n in range(26):
        row = calc_row(n)
        pow = 2**n
        sum = reduce(lambda x, y: x+y, row)
        print('row:{}:{}:{}:{} = {}'.format(n, pow, sum, pow==sum, row))

    print('\ncache info: {}'.format(calc_row.cache_info()))

