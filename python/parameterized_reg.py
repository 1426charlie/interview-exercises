#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

registry = set()

def register(active=True):
    def decorate(func):
        print('running register(active=%s->decorate(%s)' % (active, func))
        if active:
            registry.add(func)
        else:
            registry.discard(func)

        return func
    return decorate

@register(active=False)
def f1():
    print('>> running f1()')

@register()
def f2():
    print('>> running f2()')

@register()
def f3():
    print('>> running f3()')

def f4():
    print('>> running f4()')

if __name__ == '__main__':
    for fn in registry:
        print('registered function:', fn.__name__)
        fn()

