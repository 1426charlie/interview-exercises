#!/usr/bin/env python

import json
from datetime import datetime
import uuid

wrapper = {
    'status':'ok',
    'ts' : datetime.utcnow().timestamp(),
    'version' : '1.0',
    'data' : {
        'customers':[
            {
                'id':uuid.uuid4().hex
            },
            {
                'id':uuid.uuid4().hex
            }
        ]
    }
}

msg = json.dumps(wrapper)
print('serialized output: ', msg)

obj = json.loads(msg)
print('parsed: ', obj)
