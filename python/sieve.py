#!/usr/bin/env python

# dpw@seattle.local
# 2016.12.03

from math import sqrt

def sieve(n):
 
    # Find out all the primes with Sieve of Eratosthenes
    prime_table = [False]*2+[True]*(n-1)
    primes = []
    for element in range(2, int(sqrt(n))+1):
        if prime_table[element] == True:
            primes.append(element)
            multiple = element * element
            while multiple <= n:
                prime_table[multiple] = False
                multiple += element

    for element in range(int(sqrt(n))+1, n+1):
        if prime_table[element] == True:
            primes.append(element)

    return (prime_table, primes)

if __name__ == '__main__':
    n = 110
    pt, primes = sieve( n )
    for i in range(0, n+1):
        s = ''
        if pt[i]:
            s = 'true'

        print(i, '->', s)

    print(primes)
    print('count:', len(primes))
