#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.28

from operator import itemgetter

metro_data = [
    ('Tokyo', 'JP', 36.933, (35.689722, 139.691667)),
    ('Delei NCR', 'IN', 21.935, (28.613889, 77.208889)),
    ('Mexico City', 'MX', 20.142, (19.433333, -99.133333)),
    ('New York', 'US', 20.104, (40.808611, -74.020386)),
    ('Sao Paulo', 'BR', 19.549, (-23.547778, -46.635833))
]

def show_columns(cols):
    return [ cols(city) for city in metro_data ]

def show_sortedby(n):
    return [ x for x in sorted(metro_data, key=itemgetter(n)) ]

if __name__ == '__main__':
    print( show_sortedby(0) )
    print( show_columns(itemgetter(1, 0)) )

