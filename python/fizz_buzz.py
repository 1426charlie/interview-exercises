#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.18

def tester(n):
    s = str(n)
    if n % 3 == 0: s += 'Fizz'
    if n % 5 == 0: s += 'Buzz'
    return s

result = [ tester(x) for x in range(1, 101) ]

for x in result: print(x)

