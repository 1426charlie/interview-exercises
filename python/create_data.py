#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

from faker import Factory

faker = Factory.create()

def generate_data(n):
    for x in range(n):
        print(faker.uuid4())

if __name__ == '__main__':
    generate_data(10000)

