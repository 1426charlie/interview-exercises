#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

import weakref

class Cheese:
    def __init__(self, kind):
        self.kind = kind

    def __repr__(self):
        return 'Cheese(%r)' % self.kind


if __name__ == '__main__':
    stock = weakref.WeakValueDictionary()
    kinds = ['Red Leicester', 'Tilsit', 'Brie', 'Parmesan']
    catalog = [Cheese(t) for t in kinds]
    for cheese in catalog:
        stock[cheese.kind] = cheese

    stock_from_catalog()
    print('catalog', catalog)
    print('stock', sorted(stock.items()))

    del catalog
    print('stock', sorted(stock.items()))

    del cheese
    print('stock', sorted(stock.items()))

