#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.30

class MySeq:
    def __getitem__(self, index):
        return index

if __name__ == '__main__':
    print('this demonstrates how slicing works without actually slicing anything...')
    seq = MySeq()
    print('seq[1]=', seq[1])
    print('seq[1:4]=', seq[1:4])
    print('seq[1:4:2]=', seq[1:4:2])
    print('seq[1:4:2, 9]=', seq[1:4:2, 9])
    print('seq[1:4:2, 7:9]=', seq[1:4:2, 7:9])


