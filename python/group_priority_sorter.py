#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.03

def priority_sort(values, group):
    found = False
    def helper(x):
        nonlocal found
        if x in group:
            found = True
            return (0, x)
        return (1, x)

    sdata = sorted(values, key=helper)
    return (found, sdata)

if __name__ == '__main__':
    print('priority sort')
    input = [ int(x) for x in '8 3 1 2 5 4 7 6'.split(' ') ]
    group = [ int(x) for x in '2 3 5 7'.split(' ')]
    output = priority_sort(input, group)

    print('sort with group hits...')
    print(input, group)
    print(output)

    print('sort without group hits...')
    input = [ int(x) for x in '8 1 4 6 9 22 0'.split(' ') ]
    output = priority_sort(input, group)
    print(input, group)
    print(output)

