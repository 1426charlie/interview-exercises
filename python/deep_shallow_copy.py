#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

import copy

class Bus:
    def __init__(self, passengers=None):
        if passengers is None:
            self.passengers = []
        else:
            self.passengers = list(passengers) # make a copy...

    def pickup(self, name):
        print('pick up', name)
        self.passengers.append(name)

    def dropoff(self, name):
        print('drop off', name)
        self.passengers.remove(name)

    def __repr__(self):
        return str(self.passengers)

def show(busses):
    for name, b in sorted(busses.items()):
        print(id(b), name, ':', b)

if __name__ == '__main__':
    names = ['Bill', 'Claire', 'David', 'Tom','Dick','Harry','Sally','Patty']
    bus = Bus(names)
    shallow = copy.copy(bus)
    deep = copy.deepcopy(bus)

    busses = { '1 orig':bus, '2 shal':shallow, '3 deep':deep }
    show(busses);

    bus.dropoff('Bill')
    print('Notice that the original and shallow copies have dropped off a passenger but the deep copy stays intact...')
    show(busses);

    deep.dropoff('Patty')
    print('Notice that the original and shallow copies remain the same while the deep copy has a drop off...')
    show(busses);
