#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.24

from random import randint

def shell_sort(seq):
    inc = len(seq) // 2
    while inc:
        print(inc)
        for i, el in enumerate(seq):
            while i >= inc and seq[i - inc] > el:
                print(i, (i - inc))
                seq[i] = seq[i - inc]
                i -= el

            print(i, el)
            seq[i] = el

        inc = 1 if inc == 2 else int(inc * 5.0 / 11)

if __name__ == "__main__":
    print('warning, this is broken...')
    list = [ 22, 7, 2, -5, 8, 4 ] # [ randint(0, 10000) for x in range(100) ]
    sorted = shell_sort( list )

    # print(sorted)

