#!/usr/bin/env python

"""Uses heapq as a priority queue"""

# @see https://docs.python.org/3/library/heapq.html

# dpw@seattle.local
# 2016.10.31

import itertools
import time
from heapq import heappush, heappop

pq = []
entry_finder = {}
REMOVED = '<removed-task>'
counter = itertools.count()

def add_task(task, priority=0):
    '''add a new task or update the priority of and existing task'''
    if task in entry_finder:
        remove_task(task)

    count = next(counter)
    entry = [priority, count, task]
    entry_finder[task] = entry
    heappush(pq, entry)

def remove_task(task):
    '''mark an existing task as removed'''
    entry = entry_finder.pop(task)
    entry[-1] = REMOVED

def pop_task():
    '''pop the lowest priority task. raise if empty'''
    while pq:
        priority, count, task = heappop(pq)
        if task is not REMOVED:
            del entry_finder[task]
            return task
    raise KeyError('pop from an empty priority queue')   

if __name__ == '__main__':
    for t in [ 'task0', 'task1', 'task2', 'task3', 'task4' ]:
        add_task(t)

    # change a priority
    add_task('task2', 3)

    print('tasks:', pq, ', length', len(pq))

    while pq:
        task = pop_task()
        print('poped: ', task)
        time.sleep(1.0)


