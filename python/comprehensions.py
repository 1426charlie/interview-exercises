#!/usr/bin/env python

# comprehension
list = [ 1, 3, 5, 10, 60 ]

pow = [x**2 for x in list]

print(list, '**2', pow)

# sequences + comprehension
seq1 = ( 'a', 'b', 'c' )
seq2 = ( 1, 2, 3 )
result = [(x, y) for x in seq1 for y in seq2]

print(result)

num_types = [ type(0), type(0.) ]
list = [1, '4', 9, 'a', True, False, 0, 4.4]
[ print(v, type(v), type(v) in num_types) for v in list ]

squared = [ v**2 for v in list if type(v) in num_types ]
print('squared: ', squared)

print('more squared:', [ x**2 for x in range(10) ])
print('powers of 2:', [ 2**i for i in range(16) ])

noprimes = [j for i in range(2,8) for j in range(i*2, 100, i)]
# print('noprimes: ', noprimes)
primes = [x for x in range(2, 100) if x not in noprimes]
print('primes: ', primes)

# lambdas

words = 'The quick brown fox jumps over the lazy dog'.split()
stuff = map(lambda w: [ w.lower(), len(w)], words)
print('stuff: ', [ w for w in stuff ])

# identity
print('identity: ', [ [ 1 if item_idx == row_idx else 0 for item_idx in range(0, 3) ] for row_idx in range(0, 3) ])
