#!/usr/bin/env python

def generate_ints(n):
    for i in range(n):
        yield i

gen = generate_ints(5)

loop = True

while True:
    try:
        n = next(gen)

        print(n)

    except Exception as ex:
        print('done...')
        break

