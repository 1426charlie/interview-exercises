#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.16

# the objective is to determine from a list of items that have weight and value, which
# what is the maximum value that you can put into the knapsac

data = [()]
weights = []
values = []

def knapsac(idx, capacity):
    if idx == 0 or capacity == 0:
        result = 0
    elif weights[idx] > C:
        result = knapsac(idx - 1, capacity)
    else:
        t1 = knapsac(idx - 1, capacity)
        t2 = values[idx] = knapsac(idx - 1, capacity - weights[idx])
        result = max( t1, t2 )

    return result

if __name__ == '__main__':
    print( 'knapsac problem' )

