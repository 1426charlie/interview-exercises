#!/usr/bin/env python

from collections import namedtuple
from datetime import datetime
import json

Wrapper = namedtuple('Wrapper', ['msgid', 'ts','data'])
User = namedtuple('User', ['id', 'version','name','email'])

user = User('1234', 1, 'flarb', 'flarb.com')
wrapper = Wrapper('44323', datetime.now().timestamp(), user._asdict())

print(user)
print(wrapper)


print('field names for user: ', user._fields)
print('field names as dict : ', user._asdict())

print( 'json: ', json.dumps( wrapper._asdict() ))
