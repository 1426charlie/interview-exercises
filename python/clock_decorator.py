#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

import time

FMT = '[{elapsed:0.8f}s] {name} ({args}) -> {result}'

def clock(fmt=FMT):
    def decorate(fn):
        def clocked(*_args):
            t0 = time.time();
            _result = fn(*_args)
            elapsed = time.time() - t0
            name = fn.__name__
            args = ', '.join(repr(arg) for arg in _args)
            result = repr(_result)
            print(fmt.format(**locals()))
            return _result

        return clocked
    return decorate

if __name__ == '__main__':
    @clock('{name}({args}): {elapsed}s')
    def snooze(seconds):
        time.sleep(seconds)

    for i in range(6):
        snooze(.123)

