#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.29

from clock_decorator import clock

fmt = '[{elapsed:0.8f}s] {name} ({args})'

@clock(fmt)
def read_data_bad(filename):
    f = open(filename, 'r')
    uids = (line.strip() for line in f)
    uidcsv = ','.join(uids)
    f.close()
    return uidcsv

@clock(fmt)
def read_data_with(filename):
    with open(filename, 'r') as f:
        uids = ','.join(line.strip() for line in f)

    return uids

if __name__ == '__main__':
    read_data_bad('data.txt')
    read_data_with('data.txt')
