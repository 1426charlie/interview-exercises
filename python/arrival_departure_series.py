#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.15

debug = False

class Person:
    def __init__(self, name, arrive, depart):
        self.name = name
        self.arrive = arrive
        self.depart = depart

    def __repr__(self):
        return repr((self.name, self.arrive, self.depart))

class HourlyOccupancy:
    def __init__(self, t0, t1, count):
        self.t0 = t0
        self.t1 = t1
        self.count = count

    def __repr__(self):
        return repr((self.t0, self.t1, self.count))

people = [
    Person('Tom', 10, 18),
    Person('Bob', 8, 15),
    Person('Alice', 9, 17),
    Person('Sally', 13, 16),
    Person('Bill', 19, 21)
]

def getArrivals():
    return sorted(people, key=lambda person: person.arrive)

def trimDeparted(t, onsite):
    list = [ p for p in onsite if p.depart >= t ]
    if debug: print(list)
    return list

def calcOnSite():
    arrivals = getArrivals()
    onsite = [] # currently on site

    # the first segment has zero occupants...
    segments = [ HourlyOccupancy(0, arrivals[0].arrive, 0) ]

    for i in range(len(arrivals)):
        person = arrivals[i]

        if segments[-1].t1 > person.arrive:
            segments[-1].t1 = person.arrive

        onsite.append( person )
        onsite = trimDeparted(person.arrive, onsite)

        if debug: print(person.arrive, onsite)

        segments.append(HourlyOccupancy(person.arrive, person.depart, len(onsite)))

    onsite = trimDeparted(24, onsite)
    segments.append(HourlyOccupancy(segments[-1].t1, 24, len(onsite)))

    return segments

if __name__ == '__main__':
    print(people)
    print(getArrivals())
    print('calc segments of who is on site...')
    print(calcOnSite())

