#!/usr/bin/env python

def div1(x,y):
    print("{}/{} = {}".format(x, y, x/y))

def div2(x,y):
    print("{}//{} = {}".format(x, y, x//y))

div1(5,2)
div1(5.0,2)

div2(5,2)
div2(5.,2.)

