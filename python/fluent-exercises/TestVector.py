#!/usr/bin/env python
"""Vector Tests"""

# dpw@seattle.local
# 2016.10.25

import unittest

from Vector import Vector

class VectorTests(unittest.TestCase):
    """Vector Tests"""

    # test fixtures

    # setUp and tearDown

    def test_instance(self):
        """vector instance test"""
        vector = Vector([0, 0, 0])
        self.assertEqual(vector.x, 0.0, 'x should be zero')
        self.assertEqual(vector.y, 0.0, 'y should be zero')
        self.assertEqual(vector.z, 0.0, 'z should be zero')
        # self.assertEqual(vector, False, 'should be false with all zeros')

        vector = Vector([3, 4])
        # self.assertTrue(vector, 'has non-zero numbers so should be true')
        self.assertEqual(abs(vector), 5.0, '3 4 should have 5 magnitude')

        vector = Vector(range(1, 100))
        self.assertEqual(len(vector), 99, 'length of range')
        self.assertEqual(vector.x, 1.0, 'first item should be 1.0')
        svec = vector[3:8]
        self.assertEqual(len(svec), 5, 'should have 5 elements')

    def test_equals(self):
        """test the equals function"""
        vector = Vector(range(100))
        other = Vector(range(100))
        self.assertEqual(vector, other, 'the two should match')
        other = Vector(range(1, 101))
        self.assertNotEqual(vector, other, 'should not match')

        vector = Vector([1, 2, 3])
        other = Vector([1, 2])
        self.assertNotEqual(vector, other, 'should not match')

        seq = (1, 2, 3)
        self.assertNotEqual(vector, seq, "should not match (type)")

    def test_format(self):
        """test the basic formatting"""
        vector = Vector(range(0, 5))
        self.assertEqual(str(vector), '(0.0, 1.0, 2.0, 3.0, 4.0)', 'formatted string')

    def test_add(self):
        """Add two vectors of equal length"""
        vector = Vector(range(10, 15))
        other = Vector(range(100, 105))
        result = vector + other
        self.assertEqual(len(result), 5, 'should have 5 elements')
        self.assertEqual(result, Vector([110.0, 112.0, 114.0, 116.0, 118.0]))

        large = Vector(range(20))
        with self.assertRaises(ValueError):
            result = vector + large

    def test_dot_product(self):
        """test the dot product between two vectors"""
        vector = Vector([1, 2, 3])
        other = Vector([5, 6, 7])
        product = vector @ other
        self.assertEqual(product, 38, "should equal 38")

    def test_add_and_fill(self):
        """test the adding of two vectors with different lengths"""
        vector = Vector(range(10, 15))
        other = Vector(range(100, 103))
        result = vector.add_and_fill(other)
        self.assertEqual(len(result), 5, 'should have 5 elements')
        self.assertEqual(result, Vector([110, 112, 114, 13, 14]), 'should equal')

    def test_sub(self):
        """Subtract two vectors"""
        vector = Vector(range(10, 15))
        other = Vector(range(0, 5))
        result = vector - other
        self.assertEqual(result, Vector([10.0, 10.0, 10.0, 10.0, 10.0]))

        large = Vector(range(20))
        with self.assertRaises(ValueError):
            result = vector + large

    def test_mul(self):
        """Multiply a scalar against the Vector components"""
        vector = Vector(range(10, 15))
        scalar = 10
        result = vector * scalar
        self.assertEqual(result, Vector([100, 110, 120, 130, 140]))

    def test_from_polar(self):
        """test Vector from polar"""
        vector = Vector.from_polar(800, 20)
        self.assertEqual(round(vector.x), 752, 'x should equal 752')
        self.assertEqual(round(vector.y), 274, 'y should equal 274')
