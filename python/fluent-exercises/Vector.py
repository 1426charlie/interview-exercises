#!/usr/bin/env python
"""VectorClass"""

# dpw@seattle.local
# 2016.10.25

from array import array
import reprlib
import math
import numbers
import functools
import operator
import itertools

class Vector:
    """Vector class to support multiple components"""
    typecode = 'd'

    def __init__(self, components):
        self._components = array(self.typecode, components)

    def __iter__(self):
        return iter(self._components)

    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find('['):-1]
        return 'Vector({})'.format(components)
 
    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        return bytes([ord(self.typecode)]) + bytes(self._components)

    def __eq__(self, other):
        if isinstance(other, Vector):
            return len(self) == len(other) and all(a == b for a, b in zip(self, other))
        else:
            return NotImplemented

    def __hash__(self):
        hashes = (hash(x) for x in self)
        return functools.reduce(operator.xor, hashes, 0)

    def __abs__(self):
        return math.sqrt(sum(x * x for x in self))

    def __bool__(self):
        return bool(abs(self))

    def __len__(self):
        return len(self._components)

    def __getitem__(self, index):
        cls = type(self)
        if isinstance(index, slice):
            return cls(self._components[index])
        elif isinstance(index, numbers.Integral):
            return self._components[index]
        else:
            msg = '{.__name__} indices must be integers'
            raise TypeError(msg.format(cls))

    def check_equal_sizes(self, other):
        """verify that both vectors have the same number of elements"""
        if len(self) != len(other):
            msg = "Error! vector len({}) does not match len({})".format(len(self), len(other))
            raise ValueError(msg)

    def __add__(self, other):
        """add the two vectors and return the result in a new Vector"""
        self.check_equal_sizes(other)
        return Vector([a + b for a, b in zip(self, other)])

    def add_and_fill(self, other):
        """add the two vectors and fill in one if it's not the correct length"""
        pairs = itertools.zip_longest(self, other, fillvalue=0.0)
        return Vector(a + b for a, b in pairs)

    def __sub__(self, other):
        """subtract other from self and return result vector"""
        self.check_equal_sizes(other)
        return Vector([a - b for a, b in zip(self, other)])

    def __mul__(self, scalar):
        """multiply the vector elements by the scalar and return a new vector"""
        return Vector([x * scalar for x in self._components])

    def __matmul__(self, other):
        """implement the dot product with @"""
        self.check_equal_sizes(other)
        return sum(a * b for a, b in zip(self, other))

    shortcut_names = 'xyzt'

    def __getattr__(self, name):
        cls = type(self)
        if len(name) == 1:
            pos = cls.shortcut_names.find(name)
            if 0 <= pos < len(self._components):
                return self._components[pos]

        msg = '{.__name__!r} object has no attribute {!r}'
        raise AttributeError(msg.format(cls, name))

    @classmethod
    def frombytes(cls, octets):
        """create a Vector from bytes"""
        print(cls)
        typecode = chr(octets[0])
        memv = memoryview(octets[1:]).cast(typecode)
        return cls(*memv)

    def angle(self, n, indegrees=True):
        """return the angle of the indexed item in degrees [default] or radians"""
        r = math.sqrt(sum(x * x for x in self[n:]))
        a = math.atan2(r, self[n-1])

        if (n == len(self) - 1) and (self[-1] < 0):
            a = math.pi * 2 - a

        if indegrees:
            return math.degrees(a)
        else:
            return a

    def angles(self):
        """return angels for all vector components"""
        return (self.angle(n) for n in range(1, len(self)))

    def equals(self, other):
        """try this"""
        return len(self) == len(other) \
            and tuple(self) == tuple(other) \
            and isinstance(other, Vector)

    def __format__(self, fmt_spec=''):
        """format the vector"""
        if fmt_spec.endswith('h'):
            fmt_spec = fmt_spec[:-1]
            coords = itertools.chain([abs(self)], self.angles())
            outer_fmt = '<{}>'
            components = (format(c, fmt_spec) for c in coords)
        else:
            outer_fmt = '({})'
            components = (format(c, fmt_spec) for c in self)

        return outer_fmt.format(', '.join(components))

    @classmethod
    def from_polar(cls, radial, theta):
        """return the vector from radial magnitud and theta in degrees"""
        rad = math.radians(theta)
        x, y = radial * math.cos(rad), radial * math.sin(rad)
        return Vector([x, y])

if __name__ == '__main__':
    print(Vector([3, 4, 5]), 'mag =', abs(Vector([3, 4, 5])))
