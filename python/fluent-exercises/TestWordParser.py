#!/usr/bin/env python

"""Test the WordParser class"""

# dpw@seattle.local
# 2016.10.31

import unittest

from faker import Factory

from WordParser import Sentence

class WorkParserTest(unittest.TestCase):
    """Test the WordParser class"""

    # test fixtures
    # setUp / tearDown

    def test_instance(self):
        """instance test"""
        faker = Factory.create()
        s = 'This is a test'
        parser = Sentence(s)
        self.assertEqual(len(parser), 4, "four words")

        parser = Sentence(faker.sentence())
        self.assertTrue(len(parser) > 0, "should have words")

    def test_iteration(self):
        """test the parser iteration"""
        s = 'This is a long sentence with lots of words'
        parser = Sentence(s)
        count = 0
        for _ in parser:
            count += 1

        self.assertEqual(len(parser), count, "should match")

