#!/usr/bin/env python
"""CardDeckTests"""

# dpw@seattle.local
# 2016.10.25

import unittest
from random import choice

from CardDeck import FrenchDeck, Card


class CardDeckTests(unittest.TestCase):
    """CardDeckTests"""

    # test fixtures

    # setUp / tearDown

    def test_instance(self):
        """instance test"""
        deck = FrenchDeck()
        self.assertEqual(len(deck), 52, 'deck should have 52 cards')
        self.assertEqual(len(deck[:3]), 3, 'should pick three')
        aces = deck[12::13]
        self.assertEqual(len(aces), 4, 'should be 4 aces in the deck')
        for card in aces:
            self.assertEqual(card.rank, 'A', 'should be an ace')

    def test_suffle(self):
        """test shuffle"""
        cards = FrenchDeck()
        two_of_spades = Card('2', 'spades')
        card = cards[0:1].pop()
        self.assertEqual(card, two_of_spades, 'first card')
        cards.shuffle()
        self.assertEqual(len(cards), 52, 'deck size should remain intact')
        card1 = cards[0:1].pop()
        self.assertFalse(card.rank == card1.rank and card.suit == card1.suite)
        self.assertTrue(card1 in cards, 'the poped card should still exist in deck')

    def deal_from_top(self):
        """spades hight"""
        cards = FrenchDeck()
        card = cards.deal_from_top()
        self.assertEqual(len(cards), 51, 'deck less one card')
        self.assertEqual(card, Card('A', 'spades'), 'delt card should be ace of spaces')
        count = len(cards)
        for i in range(count):
            card = cards.deal_from_top()
            self.assertTrue(card != None, 'should be a valid card')
            self.assertEqual(len(cards), count - i, 'decrement by one')

        self.assertEqual(len(cards), 0, 'should have a zero deck')
        card = cards.deal_from_top()
        self.assertEqual(card, None, 'should be none')

    def test_choice(self):
        """test choice"""
        deck = FrenchDeck()
        card = choice(deck)
        self.assertTrue(card != None, 'card not none')
        self.assertTrue(card.suit >= '2')

