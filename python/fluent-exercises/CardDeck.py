#!/usr/bin/env python
"""FrenchDeck"""

# dpw@seattle.local
# 2016.10.25

from random import choice, shuffle
import collections

Card = collections.namedtuple('Card', ['rank', 'suit'])

class FrenchDeck(collections.MutableSequence):
    """FrenchDeck"""
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    suites = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suites for rank in self.ranks]

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]

    def __setitem__(self, position, value):
        self._cards[position] = value

    def __delitem__(self, position):
        del self._cards[position]

    def insert(self, position, value):
        self._cards.insert(position, value)

    def shuffle(self):
        """shuffled the deck in place"""
        shuffle(self._cards)

    def deal_from_top(self):
        """deal a card from the top of the deck.  remove the card from the set"""
        if len(self._cards) > 0:
            return self._cards.pop()
        else:
            return None

if __name__ == "__main__":
    for card in FrenchDeck():
        print(card)
    print('-')
    print(choice(FrenchDeck()))
