#!/usr/bin/env python

"""Parse words from a block of text"""

# dpw@seattle.local
# 2016.10.31

import re
import reprlib

RE_WORD = re.compile('\\w+')

class Sentence:
    """Parse the sentence into words"""

    def __init__(self, text):
        self.text = text
        self.count = None

    def __iter__(self):
        n = 0
        for match in RE_WORD.finditer(self.text):
            n += 1
            yield match.group()

        self.count = n

    def __len__(self):
        if self.count is None:
            for _ in self:
                pass
        return self.count

    def __repr__(self):
        return 'Sentence(%s)' % reprlib.repr(self.text)

    def unique_words(self):
        """return the unique words"""
        pass

    def word_count(self):
        """return the dict of words with their counts"""
        pass

if __name__ == '__main__':
    print('parse words from a block of text')
    s = Sentence('The time has come for all good men to come to the aid of their maker')
    print(s)
    print('word count:', len(s))
    for w in s:
        print(w)

    print(list(s))

