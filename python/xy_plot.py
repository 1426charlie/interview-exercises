#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.19

import pylab
import random

filename = 'images/xy-plot.png'

x = [x for x in range(20)]
y = [(y+1) ** (random.random() + 1.2) + 2.3 for y in range(20)]

# insure outputs ascend
y.sort()

print(x)
print(y)

pylab.plot(x, y, 'bo')
pylab.savefig(filename)

