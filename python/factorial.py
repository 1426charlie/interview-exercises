#!/usr/bin/env python

# dpw@seattle.local
# 2016.10.28

from functools import reduce
from operator import mul

def old_factorial(n):
    """returns n!"""
    return 1 if n < 2 else n * factorial(n-1)

def factorial(n):
    return reduce(mul, range(1, n+1))

if __name__ == '__main__':
    for n in range(2,42):
        x = factorial(n)
        print('factorial of {} is {}'.format(n, x))

