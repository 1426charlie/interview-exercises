/**
 * Task: find the bugs... 
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class BugFileWriter {

    private String name;

    public boolean isEqual(String s) {
        return name.equals(s);
    }

    public void writeText(String text) throws Exception {
        File file = new File("file.txt");
        FileOutputStream fos = new FileOutputStream(file);

        fos.write(text.getBytes());
        fos.close();
    }

    public static void main(String[] args) throws Exception {
        BugFileWriter bug = new BugFileWriter();
        if (bug.isEqual("Fred")) {
            bug.writeText("my test string");
        }
    }
}
