package testsuite;

/**
 *
 * @created 2018-03-04 10:31:23
 * @author darryl.west
 */

import java.util.*;
import java.io.*;

public class TestSuite implements Testable {
    List<Testable> testList = new ArrayList<>();
    List<TestResults> resultList = new ArrayList<>();

    public void addTest(Testable test) {
        testList.add(test);
    }

    public TestResults run() {
        TestResults results = new TestResults("Suite");

        for (Testable test : testList) {
            TestResults r = test.run();
            resultList.add(r);

            results.tests += r.tests;
            results.passed += r.passed;
            results.failed += r.failed;
        }

        results.totalMillis = results.startTime - System.currentTimeMillis();

        return results;
    }

}
