package testsuite;

public interface Testable {
    TestResults run();
}

