package testsuite;

public class TestResults {
    String name;
    int tests;
    int passed;
    int failed;
    long totalMillis;
    long startTime;

    public TestResults(String name) {
        this.name = name;
        startTime = System.currentTimeMillis();
    }

    public String toString() {
        return new StringBuilder()
            .append("Tests Results for ").append(name)
            .append(": Tests: ").append(tests)
            .append(", Passed: ").append(passed)
            .append(", Failed: ").append(failed)
            .append(", Duration: ").append(totalMillis)
            .toString();
    }
}
