/**
 * Design and implement a Calculator class that supports 4 integer functions: add, sub, mul, and div.  Ideally 
 * you would implement this class with JDK8 using lambdas.  The implementation should include basic tests.
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Calc {
    int testCount = 0;
    int testFailedCount = 0;

    public Calc() {
    }

    public int calculate(String op, int p1, int p2) throws Exception {
        int result = 0;
        printf("%d %s %d = %d", p1, op, p2, result);

        return result;
    }

    public void printf(String format, Object... args) {
        System.out.println(String.format(format, args));
    }

    public void test(boolean ok) {
        testCount++;
        if (!ok) testFailedCount++;
    }

    public static void main(String[] args) throws Exception {
        Calc calc = new Calc();

        int a = 7;
        int b = 3;

        calc.test(calc.calculate("add", a, b) == 10);
        calc.test(calc.calculate("sub", a, b) == 4);
        calc.test(calc.calculate("mul", a, b) == 21);
        calc.test(calc.calculate("div", a, b) == 2);

        calc.printf("%d Tests run, %d failed", calc.testCount, calc.testFailedCount);
        if (calc.testFailedCount == 0) calc.printf("Yippee! :)");
    }
}
