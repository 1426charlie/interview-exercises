//
// Task: given a set of integers, design and implement a class to identify sums within the integers that equal a specified 
// target.  For example, given the set [ 5, 3, 6, 2 ]  and a target amount of 8, the there are two sets, [ 5, 3 ], and [ 6, 2 ]. 
//
// Example:
//      List<Integer> inputs = Arrays.asList(3, 5, 2, -4, 8, 11);
//      int target = 7;
//

import java.util.*;
import java.io.*;

class TwoSum {
    public List find(final List<Integer> list, final int target) {
        return null;
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public static void main(String[] args) {
        TwoSum sum = new TwoSum();
        List<Integer> inputs = Arrays.asList(3, 5, 2, -4, 8, 11);
        int target = 7;
        List sums = sum.find(inputs, target);
        sum.printf("\nSums: %s\n", sums);
    }
}
