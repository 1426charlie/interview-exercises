/**
 * Task: Given a string of characters in the set [,{,(,),},] verify that the open characters [,{,(  match their respective close characters in a 
 * balanced way.  For example {,{,(,),},} is balanced whereas {,{,(,},},) is not.  The ideal implementation would use JDK8 with lambdas, predicates, etc.
 *
 * The implementation below replaces a classic switch/case with a hashmap to map open/close operations.
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Balanced {
    int totalTests = 0;
    int failed = 0;

    public Balanced() {
    }

    public boolean isBalanced(String input) {
        return false;
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public void test(boolean ok) {
        totalTests++;
        if (!ok) failed++;
    }

    public static void main(String[] args) {
        Balanced.runTests();
    }

    public static void runTests() {
        Balanced bal = new Balanced();
        String input;

        String[] inputs = { "{{{{{}}}}}", "((){{[]}})", "{(}", "}{" };
        boolean[] expect = { true, true, false, false };

        for (int i = 0; i < inputs.length; i++) {
            boolean balanced = bal.isBalanced(inputs[i]);
            bal.printf("Test %s = %b\n", inputs[i], balanced);
            bal.test(balanced == expect[i]);
        }

        bal.printf("%d Total tests, %d failed...\n", bal.totalTests, bal.failed);
    }

}
