function Node (key, left, right) {
  this.key = key
  this.left = left
  this.right = right
}

/** 

    a
  b   c
d  e f g
        h
        **/

var h = new Node('h')
var g = new Node('g',undefined, h)
var f = new Node('f')
var e = new Node('e')
var d = new Node('d')
var c = new Node('c',f,g)
var b = new Node('b',d,e)
var a = new Node('a',b,c)


function printNodesByLevel(level) {
  //console.dir("db", level)
  const len = level.length;
  // console.log('level: ', len)
  let out = []
  let children = []
  for (let i = 0 ; i < len; i++) {
    let node = level[i];
    out.push(node.key)
    if (node.left && node.left.key) {
      children.push(node.left)
    }
    if (node.right && node.right.key) {
      children.push(node.right)
    }  
  }
  
  if (out.length > 0) {
    
    console.log(out.join(''));
  }
  
  if (children.length > 0) {
    printNodesByLevel(children)
  }
  
}

printNodesByLevel([a])

/**
a
bc
defg
h
**/
