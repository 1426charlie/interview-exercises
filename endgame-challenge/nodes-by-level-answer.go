package main

import (
    "fmt"
)

type Node struct {
    key string
    left *Node
    right *Node
}

// todo : replace with a join?
func showLine(keys []string) {
    var s string
    for i := 0; i < len(keys); i++ {
        s += keys[i]
    }

    fmt.Println(s)
}

func printNodesByLevel(nodes []Node) {
    buf := []string{}
    children := []Node{};

    for i := 0; i < len(nodes); i++ {
        root := nodes[i]
        buf = append(buf, root.key)  


        if root.left != nil {
            children = append(children, *root.left)
        }

        if root.right != nil {
            children = append(children, *root.right)
        }
    }

    // figure out how to join
    // fmt.Println(buf)
    showLine(buf)

    if len(children) > 0 {
        printNodesByLevel(children)
    }

}

func main() {
    nodes := append([]Node{}, build())

    printNodesByLevel(nodes)
}

func build() Node {
    h := Node{ key:"h", left:nil, right:nil, }
    g := Node{ key:"g", left:nil, right:&h, }
    f := Node{ key:"f", left:nil, right:nil, }
    e := Node{ key:"e", left:nil, right:nil, }
    d := Node{ key:"d", left:nil, right:nil, }
    c := Node{ key:"c", left:&f, right:&g, }
    b := Node{ key:"b", left:&d, right:&e, }
    a := Node{ key:"a", left:&b, right:&c, }

    return a
}

