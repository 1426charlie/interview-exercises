#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.11
'use strict';

const assert = require('assert');
const dash = require('lodash');
const TestSuiteRunner = require('./TestSuiteRunner');

const UDPService = require('../app/udp_service');
const MockDGram = require('./MockDGram');
const config = require('../test-config.json');
const dgram = require('dgram');

const log = (function() {
    const SimpleLogger = require('simple-node-logger');
    const man = new SimpleLogger()
    man.createFileAppender({logFilePath:'logs/test_udp_service.log'});
    // man.createConsoleAppender();
    return man.createLogger('UDServiceTests', 'info');
})();

const createMessageDataService = function() {
    const opts = dash.clone( config );
    opts.log = log;
    return new MessageDataService( opts );
}

const createOptions = function() {
    const opts = dash.clone( config );
    opts.log = log;
    opts.dgram = new MockDGram();

    return opts;
}

const testInstance = function(done) {
    log.info('run the service tests...');
    const service = new UDPService( createOptions() );

    assert(service instanceof UDPService, 'should be the correct service');

    done();
};

const testStart = function(done) {
    log.info('test the start with mock dgram');
    const service = new UDPService( createOptions() );
    const socket = service.createClient();
    socket.on('message', console.log);
    service.start();

    setTimeout(() => {
        done();
    }, 10);
};

const testCreateClient = function(done) {
    let opts = createOptions();
    // don't use the mock for this
    delete opts.dgram;
    const service = new UDPService( opts );

    const socket = service.createClient();
    assert(socket, 'should have a socket');
    assert.equal(socket.type, 'udp4', 'should be a udp4 type');

    done();
};

const stats = {
    name: 'UDPServiceTests',
    tests: [ testInstance, testStart, testCreateClient ],
    testCount: 0,
    errorCount: 0
};

new TestSuiteRunner( stats );
