#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.11
'use strict';

const assert = require('assert');
const dash = require('lodash');
const TestSuiteRunner = require('./TestSuiteRunner');
const ServiceFactory = require('../app/service_factory');
const UDPService = require('../app/udp_service');
const MessageDataService = require('../app/message_data_service');
const config = require('../test-config.json');
const MockDGram = require('./MockDGram');
const [ log, createLogger ] = (function() {
    const SimpleLogger = require('simple-node-logger');
    const man = new SimpleLogger()
    man.createFileAppender({logFilePath:'logs/test_service_factory.log'});
    // man.createConsoleAppender();
    return [ man.createLogger('UDServiceTests', 'info'), man.createLogger ];
})();

const createOptions = function() {
    const opts = dash.clone(config);
    opts.log = log;
    opts.createLogger = createLogger;
    opts.dgram = new MockDGram();

    return opts;
};

const testInstance = function(done) {
    log.info('test the service manager instance');
    
    const factory = new ServiceFactory( createOptions() );

    assert(factory instanceof ServiceFactory);
    assert.equal(dash.functions(factory).length, 4);

    done();
};

const testCreateUDPService = function(done) {
    const factory = new ServiceFactory( createOptions() );

    const service = factory.createUDPService();
    assert(service instanceof UDPService, 'should be a udp service object');

    done();
};

const testCreateDatabase = function(done) {
    const factory = new ServiceFactory( createOptions() );

    const db = factory.createMessageDataService();
    assert(db instanceof MessageDataService, 'should be a udp database object');

    done();
}

const stats = {
    name: 'ServiceFactory',
    tests: [ testInstance, testCreateUDPService, testCreateDatabase ],
    testCount: 0,
    errorCount: 0
};

new TestSuiteRunner( stats );
