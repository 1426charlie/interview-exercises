#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.12
'use strict';

const dash = require('lodash');
const MessageModel = require('../app/message_model');

/**
 * to simulate packets
 */
const Dataset = function() {
    const dataset = this;
    const headerSize = 12;

    this.createMessageModel = function(id='444:1') {
        const model = new MessageModel({id:id});

        // TODO insert random message data from dataset...
        
        return model;
    };

    this.createMessageData = function(sz=256) {
        const data = Buffer.from(Array.from({length:sz}, () => Math.floor(Math.random() * 256)));
        return data;
    }

    /**
     * returns a tuple of packet and info
     */
    this.createPacket = function(txid=1) {
        const data = dataset.createMessageData(dash.random(25, 90));
        const size = headerSize + data.length;
        const buf = Buffer.alloc(size);

        const flags = 0;
        const offset = dash.random(4000, 25000);

        buf.writeUInt16BE(flags, 0);
        buf.writeUInt16BE(data.length, 2);
        buf.writeUInt32BE(offset, 4);
        buf.writeUInt32BE(txid, 8);

        // Append the payload
        data.copy(buf, headerSize);

        const info = {
            size: buf.length,
            port: dash.random(5000, 29999)
        };

        return [ buf, info ];
    }
};

module.exports = Dataset;
