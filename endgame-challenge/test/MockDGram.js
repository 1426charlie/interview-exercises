#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.11
'use strict';


const dash = require('lodash');
const dgram = require('dgram');

const MockDGram = function() {
    const mock = this;

    this.createSocket = function() {
        let socket = dgram.createSocket('udp4');

        socket.bind = function() {
            // console.log('ignore bind...');
        };

        return socket;
    };

    this.on = function(event, fn) {
        console.log('event:', event, 'fn:', fn);
    };
};

module.exports = MockDGram;
