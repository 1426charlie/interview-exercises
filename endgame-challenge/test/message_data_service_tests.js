#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.11
'use strict';

const assert = require('assert');
const dash = require('lodash');
const TestSuiteRunner = require('./TestSuiteRunner');
const Dataset = require('./Dataset');
const MessageDataService = require('../app/message_data_service');
const MessageModel = require('../app/message_model');
const ActivityMonitor = require('../app/activity_monitor');
const config = require('../test-config.json');

const log = (function() {
    const SimpleLogger = require('simple-node-logger');
    const man = new SimpleLogger()
    man.createFileAppender({logFilePath:'logs/test_message_data_service.log'});
    // man.createConsoleAppender();
    return man.createLogger('MessageDataServiceTests', 'info');
})();

let dataset = new Dataset();

const createMockActivityMonitor = function(opts) {
    opts.threadWaitTime = 10;
    const monitor = new ActivityMonitor(opts);

    monitor.monitorTickHandler = function() {
        monitor.stopMonitor();
    }

    return monitor;
};
const createOptions = function() {
    const opts = dash.clone( config );
    opts.log = log;
    opts.monitor = createMockActivityMonitor(opts);

    return opts;
};

const testInstance = function(done) {
    log.info('run the database tests...');

    const service = new MessageDataService( createOptions() );
    assert(service instanceof MessageDataService, 'should be a message data service');

    service.shutdown();
    done();
};

const testStartSession = function(done) {
    log.info('start a new session...');

    const service = new MessageDataService( createOptions() );
    let session = service.startSession();

    // assert.equal(session.length, 36, 'should be a random string');

    // now put somethin in cache and test attempt to create a new session
    const model = dataset.createMessageModel();
    const key = `${session}:1`;
    service.set(key, model);

    try {
        session = service.startSession();
    } catch (ex) {
        log.info('session start exception caught');
        assert(ex, 'should throw');
    }
    
    service.shutdown();

    done();
};

const testIsDup = function(done) {
    log.info('should detect a know duplicate');

    const service = new MessageDataService( createOptions() );
    const id = 1;
    let offset = 100;

    let dup = service.isDup( id, offset );
    assert.equal(dup, false, 'should not be a dup');
    
    dup = service.isDup( id, offset );
    assert.equal(dup, true, 'now a dup should be detected');

    service.shutdown();

    done();
};

const testProcessMessage = function(done) {
    const service = new MessageDataService( createOptions() );
    const session = service.startSession();
    const txid = 1;
    const packetCount = 10;

    service.createBuffers(2);

    for (let i = 0; i < packetCount; i++) {
        const [ packet, info ] = dataset.createPacket(txid);
    
        service.processMessage(packet, info);
    }

    assert.equal(service.getCacheCount(), 1);
    const key = service.createKey(txid);

    const model = service.get(key);
    // console.log(model);
    assert(model instanceof MessageModel, 'should be an instance of MessageModel');

    service.shutdown();

    done();
};

const testSetGet = function(done) {
    log.info('set a message model to cache');

    const service = new MessageDataService( createOptions() );
    const id = 'abc:4';
    const ref = dataset.createMessageModel(id);

    assert.equal(service.getCacheCount(), 0, 'cache is empty');
    service.set(id, ref);
    assert.equal(service.getCacheCount(), 1, 'cache has one model');
    const model = service.get(id);

    assert.equal(ref.id, model.id, 'ids should match');
    assert.equal(ref.status, model.status, 'status should match');

    service.shutdown();

    done();
};

const testSaveSession = function(done) {
    log.info('test save');

    const service = new MessageDataService( createOptions() );

    // TODO : save the cache to disk; 

    service.shutdown();

    done();
};

const stats = {
    name: 'MessageDataServiceTests',
    tests: [ testInstance, testStartSession, testIsDup, testProcessMessage, testSetGet, testSaveSession ],
    testCount: 0,
    errorCount: 0
};

new TestSuiteRunner( stats );
