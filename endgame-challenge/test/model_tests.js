#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.14
'use strict';

const assert = require('assert');
const dash = require('lodash');
const TestSuiteRunner = require('./TestSuiteRunner');

const ActivityMonitor = require('../app/activity_monitor');
const MessageModel = require('../app/message_model');

const [ log, createLogger ] = (function() {
    const SimpleLogger = require('simple-node-logger');
    const man = new SimpleLogger()
    man.createFileAppender({logFilePath:'logs/test_models.log'});
    // man.createConsoleAppender();
    return [ man.createLogger('ModelTests', 'info'), man.createLogger ];
})();

const testActivityMonitor = function(done) {

    log.info('test the activity monitor');

    const monitor = new ActivityMonitor( { log:log, threadWaitTime:2 } );
    assert(monitor instanceof ActivityMonitor, 'should be a monitor');
    assert.equal(monitor.isBusy(), false, 'should be idle');
    assert.equal(monitor.checkBusySeconds(10), false, 'should not be busy');

    let stateChanged = false;
    let gotTick = false;
    monitor.stateChangeEvent = function() {
        stateChanged = true;
    };

    monitor.monitorTickHandler = function() {
        gotTick = true;
        monitor.stopMonitor();

        assert.equal(gotTick, true);

        done();
    };

    monitor.working();

    assert.equal(stateChanged, true, 'state change handler ok');
    assert.equal(monitor.checkBusy(), true, 'now busy...');
    assert.equal(monitor.isBusy(), true, 'now busy...');

    assert.equal(monitor.checkBusySeconds(20), true, 'should be busy');

    monitor.startMonitor();
};

const testMessageModel = function(done) {
    const model = new MessageModel();

    assert(model instanceof MessageModel, 'should be a message model');
    assert.equal(model.checkBusy(), false, 'should not be busy');

    model.id = 1;
    model.session = Date.now().toString(20);

    model.lastUpdated = Date.now();
    assert(model.checkBusy(), true, 'should be busy');

    done();
};

const stats = {
    name: 'ModelTests',
    tests: [ testActivityMonitor, testMessageModel ],
    testCount: 0,
    errorCount: 0
};

new TestSuiteRunner( stats );
