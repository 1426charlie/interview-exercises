# Endgame : UDP Consumer Service


## Overview

Here are the highlights...

* implemented in node, version 6.9.1
* dependencies: (see node package file)
* services are under the app/ folder
* tests are all under test/ folder
* log files are stored under the logs/ folder
* data files are stored in database/
* sandbox tests in python, go, and node in tools/

### Architecture & Implementation

The listener service is separated into three main modules: 1) ServiceFactory, 2) UDPService socket listener, and 3) MessageDataService operations including message parsing and dup checking.  There is also a message data model used to hold and persist received messages as they arrive.  A utility module ActivityMonitor is used to coordinate operations during busy and idle times.

To enable receiving multiple sets of messages 1..10, a session id was created that uniquely identifies each group of 10 messages.  Message data is persisted to disk as soon as it's data model detects idle state.  

## Service Installation

Follow these steps:

* `tar xvzf endgame-challenge.tar.gz`
* `cd endgame-challenge`
* `npm install`
* `make test`

## Running the Service

A single service should be started with this command:

`make start`

This starts a single service identified by pid.  There is a corresponding log file in logs identified by the process pid.  Data is stored in the database folder under the current session id in separate model and data files.  The output report is saved to the database/session folder.

Individual services can be started and stopped by killing the process `kill -2 pid`.

## Running the Unit Test Suite

`make test`

or to start the watcher...

`make watch`

## Deficiencies & Limitations

_Note: Multicast messages can be delivered to multiple clients listening for packets on that UDP address/port combination. With SO\_REUSEADDR set, the OS will deliver to all of them. With SO\_REUSEADDR set on unicast UDP sockets, the OS will deliver to only one of the listening sockets (and in my experience, it will deliver to the last socket bound to that address/port, though I don't know how this is across OS's or if it has changed)._

Here is a short list of items I wish I had time to fix...

* limited unit tests
* test fixture data is not as complete as it should be
* the simulated session is a work-around for not having unique identifiers for messages
* error handling is minimal; e.g., no re-start on socket failure

## Implementation Notes

### Tools Folder

A sandbox to help deconstruct the messages as they come in.  Discoveries from the sandbox were used in the implementation of services and tests.  Partial implementations were written in go, python and node.  If I had more time the scripts would have included a way to send a message set to disk to enable replaying in a predictable way for unit and integration testing.

### Portability

The service was developed on mac (Darwin V 15.6) but tested on centos 7.  Because the service was written in node without any compiled modules, it should run fine in any environment that supports the latest 6.x version of node and 3.10.x version of npm.

### Language Choice

I decided that the easiest implementation would be using node.  But, after having problems with message re-assembly, I turned to go, then python to verify the packet count and total sizes.  I was able to confirm delivery of all packets in all three languages.

### Data Persistance

The data is persisted to files under the database folder.  A separate data service was created to support concurrent service writers. Data is first cached, then saved to disk in the background.  For production this could be replaced with redis then pushed to S3, MySQL or other long term storage scheme.

### Logging

I used an open source logger (written by me) to log to separate files for production and test and separated by service and database.  I didn't add logging to udp_emitter.

### Test Harness

For simplicity, I used a small framework with as few dependencies as possible. with a watcher.  For production node.js projects, I usually use the full suite of jshint, mocha, chai/should.

### Test Fixtures

The Dataset.js file in test is used to generate sample data, packets, etc.


###### darryl.west@raincitysoftware.com | 14-Nov-2016 | version 1.0.8
