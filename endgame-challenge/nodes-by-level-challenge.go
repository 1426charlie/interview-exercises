package main

import (
    "fmt"
)

type Node struct {
    root string
    left *Node
    right *Node
}

func printNodesByLevel(nodes []Node) {
    mockOutput()
}

func main() {
    nodes := append([]Node{}, build())

    printNodesByLevel(nodes)
    
}

func mockOutput() {
    fmt.Println("TODO: implement this to print the nodes by level, e.g.,\na\nbc\ndefg\nh")
}

func build() Node {
    h := Node{ root:"h", left:nil, right:nil, }
    g := Node{ root:"g", left:nil, right:&h, }
    f := Node{ root:"f", left:nil, right:nil, }
    e := Node{ root:"e", left:nil, right:nil, }
    d := Node{ root:"d", left:nil, right:nil, }
    c := Node{ root:"c", left:&f, right:&g, }
    b := Node{ root:"b", left:&d, right:&e, }
    a := Node{ root:"a", left:&b, right:&c, }

    return a
}

