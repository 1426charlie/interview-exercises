#!/usr/bin/env node

// darryl.west@raincitysoftware.com
// 2016.11.11
'use strict';

const dash = require('lodash');
const UDPService = require('./udp_service');
const MessageDataService = require('./message_data_service');

/**
 * this utility is a factory generation to create the udp database and listener services.
 * it also starts a one or more services.
 */
const ServiceFactory = function(options) {
    const factory = this,
        createLogger = options.createLogger;
        
    let log = options.log || createLogger('ServiceFactory');
    let udpService = options.udpService;
    let dataService = options.dataService;

    this.startServices = function(callback) {
        if (!dataService) {
            // TODO : kill any existing socket file?
            factory.createMessageDataService();
        }

        if (!udpService) {
            factory.createUDPService();
        }

        log.info('create the UDP client socket and add a message listener');
        const udpsocket = udpService.createClient();
        udpsocket.on('message', dataService.processMessage );

        dataService.createBuffers();

        log.info('start the first data capture session');
        dataService.startSession();

        log.info('start listening on UDP socket');
        udpService.start();

        if (typeof callback === 'function') {
            callback();
        }
    };

    this.stopServices = function() {
        log.info('stop the services...');
        console.log('preparing for shutdown...');
        
        udpService.shutdown();
        dataService.shutdown();

        setTimeout(() => {
            console.log('shutdown...');
            process.exit(0);
        }, 1000);
    };

    this.createUDPService = function() {
        if (!udpService) {
            log.info('create the UDPService...');
            const opts = dash.clone( options );
            opts.log = createLogger('UDPService');

            udpService = new UDPService(opts);
        }

        return udpService;
    };

    this.createMessageDataService = function() {
        if (!dataService) {
            log.info('create the message data service...');
            const opts = dash.clone( options );
            opts.log = createLogger('MessageDataService');
            
            dataService = new MessageDataService(opts);
        }

        return dataService;
    };
};

if (process.mainModule && process.mainModule.filename.indexOf('/app/service_factory.js') > 0) {
    // TODO add command line options for start/stop?

    const config = require('../package.json');
    const opts = dash.clone( config );
    opts.logFilePath = `logs/data-service-${process.pid}.log`;
    const [ log, createLogger ] = (() => {
        const SimpleLogger = require('simple-node-logger');
        const man = new SimpleLogger()
        man.createFileAppender( opts );
        // man.createConsoleAppender();
        return [ man.createLogger('ServiceFactory', 'info'), man.createLogger ];
    })();

    opts.log = log;
    opts.createLogger = createLogger;

    const factory = new ServiceFactory(opts);

    console.log('Starting Service Factory, Version: ', opts.version);
    console.log('Log file is:', opts.logFilePath, ', database file is:', opts.dbfile, ', pid: ', process.pid); 

    // TODO : separate this out to start data service then fork the udp services
    factory.startServices((err) => {
        if (err) {
            throw err;
        }

        process.on('SIGINT', factory.stopServices)
    });
}

module.exports = ServiceFactory;
