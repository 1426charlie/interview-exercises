/**
 * MessageModel is a value object container for messages shared across containers.  It stores
 * stats for messages that are persisted in json format to the database (dbhome); 
 * 
 * @author darryl.west@raincitysoftware.com
 * @created 14-nov-2016
 */
'use strict';

const dash = require('lodash');

const MessageModel = function(params = {}) {
    const model = this;

    this.id = params.id; // txid
    this.session = params.session;

    this.dateCreated = dash.isInteger(params.dateCreated) ? params.dateCreated : Date.now();
    this.lastUpdated = dash.isInteger(params.lastUpdated) ? params.lastUpdated : 0;
    
    this.dups = params.dups || 0;
    this.bytes = params.bytes || 0;
    this.offsets = params.offsets || []; // tuple of offset, length
    
    this.sha = params.sha;
    this.eofReceived = dash.isBoolean( params.eofReceived ) || false;
    this.minOffset = params.minOffset || Number.MAX_SAFE_INTEGER;
    this.holes = params.holes || [];
    this.errors = params.errors || [];

    this.checkBusy = function() {
        return (Date.now() - model.lastUpdated) < MessageModel.DFLT_IDLE_MILLIS;
    };

    this.status = params.status || MessageModel.RAW;
};

MessageModel.DFLT_IDLE_MILLIS = 1000;

MessageModel.RAW = 'raw';
MessageModel.PROCESSED = 'processed';
MessageModel.TIMETOUT = 'timedout';

module.exports = MessageModel;

