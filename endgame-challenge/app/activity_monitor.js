#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.14
'use strict';

const dash = require('lodash');

/**
 * monitor activity to signify busy or idle (with events?)
 */
const ActivityMonitor = function(options) {
    const monitor = this,
        log = options.log,
        threadWaitTime = options.threadWaitTime || 1000;

    let idleMillis = Math.max(dash.isInteger(options.idleMillis) ? options.idleMillis : 1000, 1000);
    let lastWorkTime = 0;
    let busy = false;
    let thread = null;

    this.working = function() {
        lastWorkTime = Date.now();
        if (!busy) {
            // fire a state change event
            log.info('busy working now...');
            
            busy = true;
            monitor.stateChangeEvent(busy);
        }
    };

    /**
     * return true if work has occurred within supplied seconds
     */
    this.checkBusySeconds = function(seconds=1) {
        const idle = Math.floor(seconds * 1000);
        return (Date.now() - idle < lastWorkTime );
    };

    this.checkIdleSeconds = function(seconds=1) {
        const idle = Math.floor(seconds * 1000);
        return (Date.now() - lastWorkTime) > idle;
    }

    /**
     * return true if last work was recent
     */
    this.checkBusy = function() {
        return (Date.now() - lastWorkTime) < idleMillis ;
    };

    this.checkIdle = function() {
        return monitor.checkBusy() === false;
    }

    /**
     * check busy and return the current state
     */
    this.isBusy = function() {
        const check = monitor.checkBusy();
        if (!busy === check) {
            // state change, so fire an event?
            log.info(`activity change, now ${busy ? 'busy' : 'idle'}...`);

            busy = check;
        }

        return busy;
    };

    this.monitorTickHandler = function() {
        // no-op; should be overridden by parent...
    };

    this.stateChangeEvent = function() {
        // no-op; should be overridden
    };

    this.startMonitor = function() {
        if (!thread) {
            log.info('start the monitor thread...');
            thread = setInterval(() => {
                monitor.monitorTickHandler();
            }, threadWaitTime);
        }
    };

    this.stopMonitor = function() {
        if (thread) {
            log.info('stop the monitor thread...');
            clearInterval( thread );
            thread = null;
        }
    }

    if (!log) {
        throw new Error('ActivityMonitor must be constructed with a log');
    }
};

module.exports = ActivityMonitor;
