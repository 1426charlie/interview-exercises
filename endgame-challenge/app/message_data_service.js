#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.11
'use strict';

const pathutil = require('path');
const assert = require('assert');
const crypto = require("crypto");
const MessageModel = require('./message_model');
const ActivityMonitor = require('./activity_monitor');

/**
 * methods to set/get data, write to data file, and parse incoming message buffers
 */
const MessageDataService = function(options) {
    const service = this,
        log = options.log,
        dbhome = options.dbhome,
        fs = options.fs || require('fs'),
        monitor = options.monitor || new ActivityMonitor(options),
        packetMaxSize = options.packetMaxSize || 512,
        packetHeaderSize = options.packetHeaderSize || 12,
        maxDataIncompleteTime = options.maxDataIncompleteTime || 30000,
        bufferSize = 1.2e6;

    let buffers = [];
    let session = null;
    let cache = new Map();
    let dups = new Set();
    let messages = 0;

    /**
     * start a new session, with cleared cache and dups
     */
    this.startSession = function() {
        // check cache and save first...
        if (cache.size > 0) {
            throw new Error('new session start attempted but cache is non-empty; save and empty the cache prior to staring new sessions');
        }

        session = service.createSessionId();
        cache.clear();
        dups.clear();
        messages = 0;

        monitor.startMonitor();

        return session;
    };

    this.createBuffers = function(count=10) {
        log.info(`create ${count} message buffers with size ${bufferSize} each...`);
        
        for (let i = 0; i < 10; i++) {
            buffers.push( new Buffer(bufferSize) );
        }
    };

    /**
     * return the current cache size
     */
    this.getCacheCount = function() {
        return cache.size;
    };

    /**
     * create a key by combining the message session and message txid
     */
    this.createKey = function(id) {
        return `${session}:${id}`;
    };

    /**
     * store model in cache
     */
    this.set = function(key, model) {
        log.debug('set model from key: ', key);

        cache.set(key, model);
    };

    /**
     * get the model from cache
     */
    this.get = function(key) {
        log.debug('get from key: ', key);

        return cache.get( key );
    };

    /**
     * check the txid and offset to see if this is a dup
     */
    this.isDup = function(txid, offset) {
        const key = Number(`${txid}.${offset}`);
        if (dups.has( key )) {
            log.debug('duplicate packet: ', key);
            return true;
        } else {
            dups.add( key );
            return false;
        }
    };

    /**
     * primary message receiver called from socket service. it parses the UDP packet,
     * finds the key, locates the cached model, and updates with new message
     */
    this.processMessage = function(buf, info) {
        if (!session) {
            service.startSession();
        }

        // set the busy flags
        monitor.working();

        // parse the big endians
        const flags = buf.readUInt16BE(0);
        const len = buf.readUInt16BE(2);
        const offset = buf.readUInt32BE(4);
        const txid = buf.readUInt32BE(8);

        const key = service.createKey( txid );
        const buffer = buffers[ txid - 1 ];

        log.debug(`model key ${key}, offset ${offset} flags: ${flags.toString(16)} message length ${len}`);

        let model = service.get(key);
        if (!model) {
            model = new MessageModel();
            model.id = txid;
            model.session = session;
            log.info('new model created: ', model.id, ', ', model.session);
        }

        if (service.isDup( txid, offset )) {
            model.dups++;
        }

        // save the offset and data not the entire buffer
        // src   target, targetStart, srcStart
        buf.copy(buffers[txid - 1], offset, packetHeaderSize);
        const tuple = [ offset, len ];
        model.offsets.push(tuple);  

        model.bytes += len;
        model.lastUpdated = Date.now();
        model.minOffset = Math.min(offset, model.minOffset);

        if ((flags & 0x8000) > 0) {
            log.info('messages complete for txid ', txid);
            model.eofReceived = true;
            // queue the re-assembly process and save process
        }

        // now update the model
        service.set(key, model);

        // return a tuple of what was just processed
        return [ flags, txid, offset, len ];
    };

    // don't want a random string but it needs to be unique-ish...
    this.createSessionId = function() {
        return Date.now().toString(20);
    };

    this.shutdown = function() {
        log.info('shutting down the data service...');
        monitor.stopMonitor();
    };

    this.monitorTickHandler = function() {
        if (monitor.checkIdle()) {
            // see if there is work to do...
            if (cache.size > 0) {
                log.info('process the cache...');
                service.reassembleMessages();

                if (cache.size === 0) {
                    log.info('clear the session')
                    session = null;
                }
            }
        }
    };

    this.stateChangeEventHandler = function(busy) {
        log.info('new state: ', busy ? 'busy' : 'idle');
    };

    this.createSessionPath = function(ss, callback) {
        const path = pathutil.join(dbhome, ss);
        fs.stat(path, (err, stat) => {
            if (err) {
                log.info('create the session folder: ', path);
                fs.mkdir(path, callback);
            } else {
                callback();
            }
        });
    };

    this.saveReport = function(ss, text, callback) {
        const filename = pathutil.join(dbhome, ss, 'report.txt');
        service.createSessionPath(ss, (err) => {
            if (err) {
                callback(err);
            } else {
                log.info('save the report to file: ', filename);
                fs.writeFile(filename, text, callback);
            }
        });
    };

    /**
     * save the current cache; clear cache and dups; return the new session in callback
     */
    this.saveModel = function(model, callback) {
        const filename = pathutil.join(dbhome, model.session, `model-${model.id}.json`);
        const json = JSON.stringify(new MessageModel( model ));
        
        service.createSessionPath(model.session, (err) => {
            if (err) {
                callback(err);
            }

            log.info('save model to file: ', filename);
            fs.writeFile(filename, json, callback);
        });
    };

    this.saveMessage = function(ss, txid, len, callback) {
        const filename = pathutil.join(dbhome, ss, `data-${txid}.bin`);
        const src = buffers[ txid - 1 ];
        const dst = Buffer.alloc(len);
        src.copy(dst, 0, 0, len);
        service.createSessionPath(ss, (err) => {
            if (err) {
                callback(err);
            }

            log.info('save message buffer to file: ', filename);
            fs.writeFile(filename, dst, callback);
        });
    };

    /**
     * reassemble all messages currently in cache with complete status; write report
     * and data to database/session/
     */
    this.reassembleMessages = function() {
        let report = []; // line buffer for report
        let keys = Array.from( cache.keys() ).reverse();
        log.info('key count: ', keys.length);

        const saveModel = function(key, model, callback) {
            service.saveModel(model, (err) => {
                if (err) {
                    log.error('error saving model: ', model.id, err);
                } else {
                    log.info('remove the model from cache: ', key);
                    cache.delete( key );
                }

                service.saveMessage(model.session, model.id, model.bytes, callback);
            });
        };

        let ss = null;
        const loop = function(err) {
            if (err) {
                log.error( err );
            }

            const key = keys.pop();
            if (key) {
                const model = cache.get(key);

                // set the session for the report
                if (!ss) {
                    ss = model.session;
                    report.push(`Reassembly report for session ${ss}`);
                }
                
                if (model.eofReceived) {
                    service.reassembleMessage(model, report, (err) => {
                        if (err) {
                            log.error( err );
                            loop();
                        } else {
                            saveModel( key, model, loop );
                        }
                    });
                } else {
                    log.warn('eof not recieved for model: ', model.id);
                    if ((Date.now() - model.lastUpdated) > maxDataIncompleteTime) {
                        log.error('data incomplete timeout');
                        model.status = MessageModel.TIMEDOUT;
                        saveModel( key, model, loop );
                    } else {
                        loop();
                    }
                }
            } else {
                report.push('\n\n');
                service.saveReport(session, report.join('\n'), (err) => {
                    if (err) {
                        log.error('report save failed');
                        log.error(err);
                    }

                    // clear the session
                    cache.clear();
                    dups.clear();
                    session = null;

                    log.info('reassembly complete...');

                    return;
                });
            }
        };

        loop();
    };

    // TODO move this to a separate reporting module...
    this.reassembleMessage = function(model, report, callback) {
        log.info(`re-assemble txid:${model.id} size:${model.bytes} nPkts: ${model.offsets.length} dups:${model.dups}`);

        let name = `Message #${model.id} `;
        let missing = [];

        // sort by offset
        model.offsets.sort((a,b) => a[0] - b[0])
        
        let pos = model.minOffset;
        if (pos !== 0) {
            log.warn(name, 'min offset', pos);
        }

        model.offsets.forEach(tuple => {
            const [ offset, len, buf ] = tuple;
            if (offset !== pos) {
                missing.push(`${name} Hole at: ${pos} offset: ${offset} len: ${len}`);
                pos = offset;
            }

            pos += len;
        });

        if (model.eofReceived === false) {
            log.warn(`${name} did not receive EOF flag`);
        }

        if (missing.length === 0) {
            // compute the sha
            model.sha = service.calcSHA256(model.id, model.bytes);
            const msg = `${name} length:${model.bytes} sha256:${model.sha}`;
            report.push( msg );
            log.info(msg);
        } else {
            missing.forEach(msg => {
                report.push(msg);
                log.warn(msg);
            });
            
        }

        model.status = MessageModel.PROCESSED;

        callback();
    };

    this.calcSHA256 = function(txid, len) {
        const src = buffers[txid - 1];
        const dst = Buffer.alloc(len);
        log.info('copy src buffer to dest, bytes: ', len);
        src.copy(dst, 0, 0, len);
        const hash = crypto.createHash('sha256').update(dst);
        const hex = hash.digest('hex');
        log.info(`txid ${txid} hash: ${hex}`);
        return hex;
    }

    // override the monitor methods
    monitor.monitorTickHandler = service.monitorTickHandler;
    monitor.stateChangeEvent = service.stateChangeEventHandler;

    (function() {
        if (!log) {
            throw new Error('database must be constructed with a logger');
        }
        if (!dbhome) {
            throw new Error('database must be constructed with a db home folder');
        }
    })();
};

module.exports = MessageDataService;
