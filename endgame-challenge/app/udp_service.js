#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.14
'use strict';

const assert = require("assert");
const MessageModel = require('./message_model');

/**
 * the primary UDP listening/receiver service.  It simply reads UDP messages and forwards them to the 
 * dataservice through a simple message write command (implementation configured in factory);
 */
const UDPService = function(options) {
    const service = this,
        log = options.log,
        port = options.port,
        dgram = options.dgram || require('dgram');

    let client = options.client;

    /**
     * create the UDP client socket
     */
    this.createClient = function() {
        const socket = dgram.createSocket({type:'udp4', reuseAddr:true});

        socket.on('error', service.onError);
        socket.on('listening', () => {
            const address = client.address();
            log.info('client now listening on ', address.address, ':', address.port);
        });

        log.info('dgram socket listener created, type: ', socket.type);

        // return socket to enable added a message listener
        client = socket;

        return socket;
    };

    /**
     * start listening for UDP messages
     */
    this.start = function() {
        log.info('start the udp service on shared port: ', port);
        if (!client) {
            throw new Error('must create the client and add a message handler prior to invoking start');
        }

        // check for a message listener

        client.bind( {
            address:'localhost',
            port:port,
            exclusive:false
        });
    };

    this.onError = function(err) {
        log.error( err );
        if (client) {
            client.close();
            client = null;

            // restart?
        }
    };

    this.shutdown = function() {
        log.info('stop the service');
        if (client) {
            client.close();
        }
    };

    (function() {
        if (!log) {
            throw new Error('service must be constructed with a logger');
        }
    })();
};

module.exports = UDPService;
