#!/usr/bin/env node

// dpw@alameda.local
// 2015.03.04
'use strict';

const fs = require('fs'),
    spawn = require('child_process').spawn,
    clearScreen = '[H[2J';

let files = new Set();
let tid = null;

// it will just run the first one for now...
const tests = [ 
    'test/model_tests.js', 
    'test/service_factory_tests.js', 
    'test/udp_service_tests.js', 
    'test/message_data_service_tests.js' 
];

const run = function() {
    process.stdout.write( clearScreen ); 
    console.log('Changed files: ', files);

    let list = tests.map(t => t);

    const loop = function() {
        const test = list.shift();
        if (test) {
            console.log(test);
            let runner = spawn( 'node', [ test ] );

            runner.stdout.on('data', function( data ) {
                process.stdout.write( data );
            });

            runner.stderr.on('data', function( data ) {
                process.stdout.write( data );
            });

            runner.on('close', function(code) {
                tid = null;
                files.clear();
                loop();
            });
        } else {
            console.log('tests complete...')
        }
    }

    loop();
};

const changeHandler = function(event, filename) {
    if ( filename.endsWith('.js') ) {
        files.add( filename );

        if (!tid) {
            tid = setTimeout(function() {
                run();
            }, 250);
        }
    }
};

run();
fs.watch( './', { recursive:true }, changeHandler );

