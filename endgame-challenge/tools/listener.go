package main

import (
	"encoding/binary"
	"fmt"
	"net"
	"os"
)

func main() {

	port := ":6789"
	ServerAddr, err := net.ResolveUDPAddr("udp4", port)
	if err != nil {
		fmt.Println("Address Error: ", err)
		os.Exit(-1)
	}

	ServerConn, err := net.ListenUDP("udp4", ServerAddr)
	if err != nil {
		fmt.Println("Listen Error: ", err)
		os.Exit(-2)
	}

	fmt.Println("listening on port: ", port)

	buf := make([]byte, 1024)
	msgcount := 0

	for {
		n, _, err := ServerConn.ReadFromUDP(buf)
		if err != nil {
			fmt.Println("Error: ", err)
		} else {
			msgcount++

			flags := binary.BigEndian.Uint16(buf[0:2])
			len := binary.BigEndian.Uint16(buf[2:4])
			offset := binary.BigEndian.Uint32(buf[4:8])
			txid := binary.BigEndian.Uint32(buf[8:12])

			fmt.Println("txid:", txid, "bytes:", n, " len:", len, " offset:", offset, " count: ", msgcount)
			if flags != 0 {
				fmt.Println("end of file for txid: ", txid)
			}

			if len > 500 {
				fmt.Println("warning: len > 500: ", len)
			}
		}
	}
}
