#!/usr/bin/env python

# dpw@seattle.local
# 2016.11.12

import socket
import struct

port = 6789
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
s.bind(("", port))
print("waiting on port:", port)

while True:
    data, addr = s.recvfrom(1024)
    flags, size, offset, txid  = struct.unpack(">HHLL", data[0:12])

    if flags != 0:
        print(txid, 'sz:', size, 'off:', offset, 'eof flag:', flags)
    else:
        print(txid, 'sz:', size, 'off:', offset)

