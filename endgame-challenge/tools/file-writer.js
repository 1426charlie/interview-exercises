#!/usr/bin/env node

e/ dpw@seattle.local
// 2016.11.13
'use strict';

const dash = require('lodash');
const dgram = require('dgram');
const server = dgram.createSocket( { type:'udp4', reuseAddr:true } );
const config = require('../package.json');
const port = config.port;

let dups = new Set();
let messageCount = 0;
let lastMessage = 0;
let errors = [];
let done = false;
let minOffset = Number.MAX_SAFE_INTEGER;
let maxOffset = 0;
let busy = false;
let cache = new Map();
let queue = [];

const bufsize = 1.2e6;
let buffers = []; 
let metadata = [];

const createNewBuffer = function(txid) {
    const model = {
        txid:txid,
        packets:0,
        offsetLengths:[],
        holes:[],
        bytes:0,
        tsfirst:0.0,
        tslast:0.0,
        busy:false,
        buf:Buffer.alloc(bufsize, 0)
    }

    return model;
};

server.on('error', (err) => {
    console.log('server error:', err);
    server.close();
});

server.on('message', (buf, info) => {

    messageCount++;
    lastMessage = Date.now();

    const flags = buf.readUInt16BE(0);
    const len = buf.readUInt16BE(2);
    const offset = buf.readUInt32BE(4);
    const txid = buf.readUInt32BE(8);

    let model = cache.get(txid);
    if (!model) {
        model = createNewBuffer(txid);

        model = { id:txid };
        model.len = 0;
        model.eof = false;
        model.packets = 0;
        model.dups = 0;
    }

    model.len += len;
    model.packets++;
    if (flags === 0x8000) {
        model.eof = true;
    }

    const key = `${txid}/${offset}`;

    if (len > 500) {
        console.log('len > 500', len);
        errors.push(`message ${key} over 500 chars: ${len}`); 
    }

    if (len + 12 !== buf.length) {
        errors.push(`ERROR! ${key} has wrong buffer size:${buf.length - 12} should be ${len}`);
    }

    if (dups.has(key)) {
        // verify that the lengths are the same
        console.log('dup:', key, 'len', len);
        model.dups++;
    } else {
        dups.add(key);

        // create a filename using txid + offset
        // queue to write
        // console.log('file:', key, 'len', len);

    }

    cache.set(txid, model);
});

server.on('listening', () => {
    const address = server.address();
    console.log('listening on', address.address, ':', address.port);
});

server.bind({
    address:"localhost",
    port: port,
    exclusive: false
});

const showStats = function() {
    console.log('packets received:', messageCount);
    console.log('errors:', errors.length);
    console.log('cache:', cache.size);

    let keys = Array.from(cache.keys()).sort((a,b) => a-b);
    keys.forEach(key => {
        const model = cache.get(key);
        console.log( model );
    });

    clearStats();
};

const clearStats = function() {
    console.log('clear the stats');

    errors.splice(0);
    messageCount = 0;
    dups.clear();
    cache.clear();
};

const drainQueue = function() {
    const obj = queue.pop();
    if (obj) {
        // write the buffer to file...
        setImmediate(() => drainQueue);
    }
};

setInterval(() => {
    drainQueue();

    if (Date.now() - lastMessage > 2000) {
        if (busy) {
            showStats();
            console.log('not busy now...');
            // show the stats
        }
        busy = false;
    } else {
        if (!busy) {
            console.log('busy now...');
        }
        busy = true;
    }
}, 1000);
