#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.13
'use strict';

const net = require('net');
const dgram = require('dgram');

const config = require('../package.json');
const sockfile = config.sockfile;
const port = config.port;

let connected = false;

const startDataService = function() {
    console.log('connect to ', sockfile);

    const client = net.connect( sockfile );
    // console.log('set no socket delay...');

    client.on('connect', () => {
        connected = true;
        console.log('connection made...');
    });

    // now set up a loop to write buffers of data...
    // let buf = new Buffer('ping');
    // client.write( buf );

    return client;
};

const startUDPService = function(client) {
    let bytes = 0;
    let messagesReceived = 0;
    let messagesSent = 0;
    let acks = 0;
    let queue = [];
    let cache = new Map();
    let busy = false;
    let lastMessage = 0;
    let errors = 0;
    let draining = false;

    const server = dgram.createSocket( { type:'udp4', reuseAddr:true } );
    server.on('listening', () => {
        const address = server.address();
        console.log('listening on', address.address, ':', address.port);
    });

    server.on('message', (buf, info) => {
        lastMessage = Date.now();
        messagesReceived++;

        const flags = buf.readUInt16BE(0);
        const len = buf.readUInt16BE(2);
        const offset = buf.readUInt32BE(4);
        const txid = buf.readUInt32BE(8);

        if (buf.length - 12 === len && info.size === buf.length) {

            if (cache.has(txid)) {
                let [ size, packets ] = cache.get(txid);

                size += len;
                packets++;

                cache.set(txid, [ size, packets ]);
            } else {
                cache.set(txid, [ len, 1 ]);
            }


            // console.log(txid, len, offset, flags, info);
            queue.push( buf );
            if (!draining) {
                drainQueue();
            }
        } else {
            console.log('error: buffer size bad', buf.length, info.size);
        }

        // verify the inputs flags, len offset, txid 1..10
        // if good then queue to send...
        
    });

    server.bind({
        address:'localhost',
        port: port,
        exclusive: false
    });

    const reportStats = function() {
        let keys = Array.from( cache.keys() ).sort((a,b) => a - b);
        keys.forEach(key => {
            const [ size, packets ] = cache.get(key);
            console.log(`Message #${key} size:${size} pck:${packets}`);
        });

        console.log('received', messagesReceived, 'sent', messagesSent, 'acks', acks);
        if (messagesSent !== acks) {
            console.log(`Error: messages sent ${messagesSent} doesnt match acks ${acks} (${messagesSent - acks})`);
        }

        console.log('errors count: ', errors);

        clearCounters();
    };

    const clearCounters = function() {
        console.log('clearing counters...');
        cache.clear();
        errors = 0;
        messagesSent = 0;
        messagesReceived = 0;
        acks = 0;
    };

    const sendMessage = function(buf) {
        messagesSent++;
        lastMessage = Date.now();
        client.write(buf);
    };

    const drainQueue = function() {
        if (queue.length > 0) {
            draining = true;
            sendMessage(queue[0]);
        } else {
            draining = false;
        }
    };

    client.on('data', (buf) => {
        const msg = buf.toString();
        if (msg === 'ack') {
            // remove what was just sent...
            queue.shift();
            acks++;
        } else {
            errors++;
            console.log('received data: ', msg);
        }

        drainQueue();
    });

    client.on('error', (err) => {
        errors++;
        console.log('client socket error: ', err);
    });

    // create a one second timer to flush the queue, report data, etc
    setInterval(() => {
        // aways check
        drainQueue();

        if (Date.now() - lastMessage > 2500) {
            if (busy) {
                console.log('quiet now...');
                if (queue.length === 0) {
                    reportStats();
                    busy = false;
                } else {
                    console.log('queue not empty: ', queue.length);
                }
            }

        } else {
            if (!busy) {
                console.log('busy now...');
            }

            busy = true;
        }
    }, 1000);

};

const client = startDataService();
startUDPService(client);
