#!/usr/bin/env node

// dpw@seattle.local
// 2016.11.11
'use strict';

/**
 * provided for initial sandbox tests to decompose the message
 */

const dash = require('lodash');
const dgram = require('dgram');
const server = dgram.createSocket( { type:'udp4', reuseAddr:true } );
const port = 6789;
const MessageModel = require('../app/message_model');

let dups = new Set();
let models = [];
let messageCount = 0;
let done = false;
let minOffset = Number.MAX_SAFE_INTEGER;
let maxOffset = 0;

server.on('error', (err) => {
    console.log('server error:', err);
    server.close();
});

server.on('message', (buf, info) => {
    messageCount++;
    const flags = buf.readUInt16BE(0);
    const len = buf.readUInt16BE(2);
    const offset = buf.readUInt32BE(4);
    const txid = buf.readUInt32BE(8);

    minOffset = Math.min(offset, minOffset);
    maxOffset = Math.max(offset, maxOffset);

    if (len > 500) {
        console.log('len > 500', len);
    }

    let model = dash.find(models, { id:txid });
    if (!model) {
        model = new MessageModel();
        model.id = txid;
        models.push( model );
        // console.log('created new model: ', model);
        model.minOffset = offset;
        model.length = len;
    } else {
        model.length += len;
    }

    if (offset === 0) {
        console.log(buf);
    }

    model.minOffset = Math.min(offset, model.minOffset);

    if (txid === 1) {
        let data = new Buffer(len);
        buf.copy(data, 0, 12);
        model.packets.push([ offset, len, data ]);
    } else {
        model.packets.push([ offset, len ]);
    }

    if (info.size !== buf.length) {
        console.log('id:', txid, 'len:', len, 'sz:', info.size, 'off:', offset);
        console.log(`sz is bad, got ${info.size} should be ${buf.length}`);
        throw new Error('bad size');
    }

    let key = Number(`${txid}.${offset}`);
    if (dups.has(key)) {
        // console.log('dup:', key);
        // verify that they are the same 
        model.dups++;
    } else {
        dups.add(key);

        // model.length += len;
    }

    if (flags === 0x8000) {
        model.eofReceived = true;
        console.log('id:', txid, 'eof flag:', flags.toString(16), 'length:', model.length );

        if (txid === 10) {
            console.log('that should be the last of it...');
            done = true;

            if (done) {
                // wait for about 2 seconds then dump
                setTimeout(() => {
                    console.log('message count: ', messageCount);

                    if (models.length !== 10) {
                        console.log('missing models: ', models.length);
                    } else {
                        console.log('all models had data');
                    }

                    const list = models.filter(model => model.eofReceived === false);
                    if (list.length === 0) {
                        console.log('all models received eof');
                    } else {
                        list.forEach(model => {
                            console.log('ERROR: no eof for model: ', model);
                        });
                    }
                    console.log('min/max offsets:', minOffset, maxOffset);
                    models.forEach(model => {
                        console.log('model', model.id,'min offset:', model.minOffset, 'len:', model.length);
                    });
                }, 2000);
            }
        }
    }

});

server.on('listening', () => {
    const address = server.address();
    console.log('listening on', address.address, ':', address.port);
});

server.bind({
    address:"localhost",
    port: port,
    exclusive: false
});
