#!/usr/bin/env node

// dpw@seattle.local
// 2016.12.19
'use strict';

/*
book | |1| |2|3| |4|5| |6|7|

    maxpage 7   1   2   3   4   5   6   7
    front2back  0   1   1   2   2   3   3
    back2front  3   2   2   1   1   0   0

page count:     1 <= n <= 1e5
target turns:   1 <= p <= n
*/

function front2back(maxpage, target) {
    return target >> 1;
}

function back2front(maxpage, target) {
    return (maxpage >> 1) - (target >> 1);
}

function minTurns(maxpage, target) {
    return Math.min(front2back(maxpage, target), back2front(maxpage, target))
}

if (process.argv[1].endsWith('minPageTurn.js')) {
    const testit = function(options) {
        let maxpage = options.maxpage;
        let targets = options.targets;
        let front2bk = options.front2bk;
        let back2fnt = options.back2fnt;
        let expect = options.expect;

        console.log(`\nmaxpage: ${maxpage}, targets: ${targets}`);
        targets.forEach((target, i) => {
            let f2b = front2back(maxpage, target);
            let b2f = back2front(maxpage, target);
            let ans = minTurns(maxpage, target);

            console.log(`target: ${target} -> f2b: ${f2b}, b2f: ${b2f}, answer: ${ans}`);

            let exp = expect[i];

            if (ans === exp) {
                console.log('ok...\n');
            } else {
                console.log(`ERROR! expected: ${exp}, got: ${ans}\n`);
            }
        });
    }

    const op1 = {
        maxpage:7,
        targets: Array.from({length:7}, (x,i) => i+1),
        front2bk: [ 0, 1, 1, 2, 2, 3, 3 ],
        back2fnt: [ 3, 2, 2, 1, 1, 0, 0 ],
        expect:   [ 0, 1, 1, 1, 1, 0, 0 ]
    };

    const op2 = {
        maxpage:8,
        targets: Array.from({length:8}, (x,i) => i+1),
        front2bk: [ 0, 1, 1, 2, 2, 3, 3, 4 ],
        back2fnt: [ 4, 3, 3, 2, 2, 1, 1, 0 ],
        expect:   [ 0, 1, 1, 2, 2, 1, 1, 0 ]
    };

    testit(op1);
    testit(op2);
}
