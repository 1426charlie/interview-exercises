# Balanced Open/Close Brackets

Create a function that takes a stream of characters containing open/close characters and returns true if the streamed characters are balanced, else false.

## Character Stream Examples

* balanced: `{}, [{}{{}({})}], {[]}`
* not balanced: `{[(}]), (){{}, (`

## Test Driven Design

* it("should return true if the stream of open/closed bracket characters is balanced")
* it("should return false if the stream of open/closed bracket characters is not balanced")
* it("should return false if the count of the open/closed character stream is odd")
* it("should return false if the open/closed character stream includes an unrecognized character")

## Data Fixtures

Pairs:

```
bracketPairs = new HashMap<Character,Character>();

bracketPairs.put('(', ')');
bracketPairs.put('[', ']');
bracketPairs.put('{', '}');
```

Sample Data:

```
balanced = [
    "{}", "{{}}", "{[()]}"
];

unbalanced = [
    "{", "[}", "{[(])}"
];

badChars = [
	"{!", "{{{{}}}#"
];
```

## Separating Concerns

### Stream/Character Iteration

* check character count for even (not odd)
* call character evaluation
* is stack empty?

### Character Evaluation

* open / close
* push to stack
* pop from stack
* verify valid character

###### darryl.west | 2018.01.28
