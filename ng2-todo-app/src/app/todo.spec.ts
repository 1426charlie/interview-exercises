/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import {Todo} from './todo';

describe('Todo', () => {
  it('should create an instance', () => {
    
    const todo = new Todo();
    expect(todo).toBeTruthy();
    expect(todo instanceof Todo).toEqual( true );

    expect(todo.id).toBeUndefined();
    expect(todo.title).toEqual( '' );
    expect(todo.complete).toEqual( false );
  });

  it('should accept and assign constructor values', () => {
    const obj = {
      id: 12345,
      title: 'My Test Todo',
      complete: false,
      flerb: 'bleck'
    };

    const todo = new Todo( obj );
    expect(todo).toBeTruthy();
    expect(todo instanceof Todo).toEqual( true );

    expect(todo.id).toEqual( obj.id );
    expect(todo.title).toEqual( obj.title );
    expect(todo.complete).toEqual( obj.complete );

    console.log( JSON.stringify( todo ));
  });
});
