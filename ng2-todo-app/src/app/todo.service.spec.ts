/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { TodoService } from './todo.service';
import { Todo } from './todo';

describe('Todo Service', () => {
  beforeEachProviders(() => [TodoService]);

  function createTodoList(): Todo[] {
    let list = [];
    list.push( new Todo({title: 'Test 1', complete: false }) );
    list.push( new Todo({title: 'Test 2', complete: true }) );

    return list;
  }

  describe('#instance', () => {
    it('should create the service', inject([TodoService], (service: TodoService) => {
      expect(service).toBeTruthy();
    }));
  });
  
  describe('getAllTodos', () => {
    it('should return an empty array by default', inject([TodoService], (service: TodoService) => {
      expect(service.getAllTodos().length).toEqual( 0 );
    }));

    it('should return all todos from known list', inject([TodoService], (service: TodoService) => {
      const list = createTodoList();

      list.forEach(todo => {
        service.addTodo( todo );
      });

      expect(service.getAllTodos()).toEqual(list);
    }));
  });

  describe('save', () => {
    it('should assign an incrementing id', inject([TodoService], (service: TodoService) => {
      const list = createTodoList();

      const td1 = list[0];
      const td2 = list[1];

      list.forEach(todo => {
        service.addTodo( todo );
      });

      expect(service.getTodoById( td1.id )).toEqual( td1 );
      expect(service.getTodoById( td2.id )).toEqual( td2 );
    }));
  });

  
});
