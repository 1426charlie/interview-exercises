import { Injectable } from '@angular/core';
import { Todo } from './todo';

@Injectable()
export class TodoService {

  lastId: number = 0;

  todos: Todo[] = [];

  constructor() {}

  addTodo(todo: Todo): TodoService {
    if (!todo.id) {
      todo.id = ++this.lastId
      console.log(`created todo id: ${todo.id}`)
    }

    this.todos.push( todo );

    console.log(`todo list count: ${this.todos.length}`);

    return this;
  }

  updateTodoById(id: number, values: Object = {}): Todo {
    let todo = this.getTodoById( id );
    if (!todo) {
      return null;
    }

    Object.assign(todo, values);
    console.log(`updated todo: ${JSON.stringify( todo )}`);

    return todo;
  }

  deleteTodoById(id: number): TodoService {
    console.log(`todo length: ${this.todos.length} before delete by id: ${id}`);
    this.todos = this.todos.filter(todo => todo.id !== id);
    console.log(`todo length: ${this.todos.length} after delete by id: ${id}`);

    return this;
  }

  getAllTodos(): Todo[] {
    return this.todos;
  }

  getTodoById(id: number): Todo {
    return this.todos.filter(todo => todo.id === id).pop();
  }

  toggleTodoComplete(todo: Todo) {
    let updatedTodo = this.updateTodoById(todo.id, { 
      complete: !todo.complete
    });

    return updatedTodo;
  }
}
