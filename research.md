# Research Links

### Low level Port Control Protocol (PCP)

Secure Pub/Sub Framework for building robust TCP transport layers in node.js applications:
[nodecraft/ricochet](https://github.com/nodecraft/ricochet.js).

### es6 & Bable

[standalone bable](https://github.com/Daniel15/babel-standalone).

### JS Date Utils

Should clone/re-implement in es6 and add formatting: 
[instadate](https://github.com/Teamweek/instadate/blob/master/instadate.js)

_needs a good name..._

