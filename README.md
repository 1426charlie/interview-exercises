# Programming Interviews


## Pair Programming Interviews

Let the candidate drive. Watch and listen more than talk.  A good platform for this is [code bunny](https://codebunny.io/) where you can create an interview environment and use javascript, java, c++, etc (no go yet).

### Exercises




### Javascript Questions

* Name two programming paradigms important for javascript. (prototypal inheritance and FP).
* Difference between classical and prototypal inheritance? (prototypal is usually more flexible than class; classes sometimes create tight coupling and hierarchies; prototypal creates delegation;)
* Pros and Cons of FP vs OOP? (FP strives to eliminate side effects and shared state; more coders are familiar with OOP; FP has a longer learning curve;)
* When is it better to use classical inheritance rather than prototypal?
* When is prototypal inheritance an appropriate choice? (delegation, concatenative, functional, in most cases; Object.assign())
* What is asynchronous programming and why is it important in JS? (non-blocking; event-loop; better performance;)

### General Questions

* What can you tell me about test driven design?
* What is functional programming? (produce programs using mathematical functions and avoids state; Lisp, Haskell, Erlang, Closure, Elm, F# OCaml; it's a central concept in JS; Pure functions, avoid side-effects, function composition; map/filter/reduce;)
* Pros and cons of monolithic vs microservice architectures? (monolithic support cross-cutting concerns that may have to be duplicated in micros; monos tend to be tightly coupled and more difficult to refactor; micros are deployable through VMs or containers;)
* What does "favor object composition over inheritance" mean? (can-do, has-a or uses-a instead of is-a; creates brittle and tight coupling; gorilla/banana; code is more flexible)
* Big O

### Testing Questions

* Why bother creating unit tests? (documents programmer's intent; QA, continuous delivery; developer understanding; reinforces good functional design (functions do one thing))
* How would you test a flashlight?
* How would you test a RESTful interface? (integration tests)
* How would you you test a function? (unit)
* Give the following code, how would you refactor to make testable?  Why not test it the way its written?

```
function calculateTotalAmount(orderDetails) {
	let totalAmount = 0;
	for (let i = 0; i < orderDetails.length; i++) {
		const detail = orderDetails[i];
		
		totalAmount += detail.quantity * detail.price;
		totalAmount += detail.quantity * detail.price * detail.taxRate;
		
		switch (detail.customerType) {
		case 'loyal':
			totalAmount -= detail.quantity * detail.price * detail.LoyaltyDiscount;
			break;
		case 'firstTime':
			totalAmount -= detail.quantity * detail.price * detail.firstTimeDiscount;
			break;
		case 'returnCustomer':
			totalAmount -= detail.quantity * detail.price * detail.returnCustomerDiscount;
			break;
		}
	}
	
	return totalAmount;
}
```


###### darryl.west@raincitysoftware.com | Version 2017.01.03


