package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	// TODO : get from flags
	port := 8081
	host := fmt.Sprintf("127.0.0.1:%d", port)
	ss, err := net.Listen("tcp", host)
	if err != nil {
		fmt.Println("error opening host: ", host, err.Error())
		os.Exit(1)
	}

	defer ss.Close()
	fmt.Println("listening on: ", host)
	for {
		conn, err := ss.Accept()
		if err != nil {
			fmt.Println("error on accept: ", err.Error())
		}

		go handleRequest(conn)
	}
}

func handleRequest(conn net.Conn) {
	buf := make([]byte, 2048)
	defer conn.Close()

	for {
		n, err := conn.Read(buf)
		if err != nil {
			fmt.Println("connection lost...")
			return
		}

		fmt.Printf("%s", buf[:n])
	}
}
