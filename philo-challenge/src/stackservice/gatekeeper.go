package stackservice

import (
	"container/list"
	"sync"
	"time"
)

// GateConn interface for gatekeeper connections
type GateConn interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
	Close() error
}

// Client with connection and ttl
type Client struct {
	id   int64
	conn GateConn
	ttl  int64
}

// Gatekeeper object with client list, etc
type Gatekeeper struct {
	sync.RWMutex
	clients    *list.List
	maxclients int
	timeout    int64
	log        *Logger
}

// LeaveFunc defined for request handlers
type LeaveFunc func(client *Client)

var (
	join  = make(chan *Client, 1)
	leave = make(chan *Client, 1)
)

// NewClient create a new client object with connection and set ttl
func (g *Gatekeeper) NewClient(conn GateConn) *Client {
	client := Client{}
	now := time.Now()
	client.id = now.UnixNano()
	client.conn = conn
	client.ttl = now.Unix() + g.timeout // ttl in seconds

	return &client
}

// NewGatekeeper create a new gatekeeper object with the supplied context
func NewGatekeeper(cfg *Config) *Gatekeeper {
	gatekeeper := new(Gatekeeper)

	// or, create a specific gatekeeper logger...
	gatekeeper.log = NewLogger("Gatekeeper", LogTargetTCP)

	gatekeeper.clients = list.New()
	gatekeeper.maxclients = cfg.maxconns
	gatekeeper.timeout = cfg.timeout

	gatekeeper.log.Info("create Gatekeeper, max clients: %d, timeout: %d", gatekeeper.maxclients, gatekeeper.timeout)

	return gatekeeper
}

// Start the gatekeeper service
func (g *Gatekeeper) Start(handlers *StackRequestHandlers) {
	info := g.log.Info
	info("starting the gatekeeper...")

	for {
		select {
		case client := <-join:
			info("new client %v, count: %d", client, g.clients.Len())
			if g.clients.Len() < g.maxclients {
				g.Lock()
				g.clients.PushFront(client)
				g.Unlock()
				info("new client %v, new count: %d", client, g.clients.Len())

				// invoke the normal handler for push/pop
				go handlers.HandleRequest(client, g.Leave)
			} else {
				// the oldest client is at the back of the list
				e := g.clients.Back()
				if cli, ok := e.Value.(*Client); ok {
					// if client.ttl has expired, then remove and insert the new client
					if cli.ttl < time.Now().Unix() {
						g.Lock()
						// TODO you need to clean up any push/pop requests from the queue
						// prior to removing
						g.clients.Remove(e)
						g.clients.PushFront(client)
						g.Unlock()

						g.log.Info("remove this client: %v, size: %d", client, g.clients.Len())
						go handlers.HandleRequest(client, g.Leave)
					} else {
						g.log.Info("ignore this client, size: %d", g.clients.Len())
						go handlers.HandleBusyResponse(client, g.Leave)
					}
				}
			}
		case client := <-leave:
			e, ok := g.FindClient(client)
			if ok {
				g.Lock()
				g.clients.Remove(e)
				g.Unlock()
				info("client done & removed,  size: %d", g.clients.Len())
			}
			client.conn.Close()
		}
	}
}

// Join wrap the connection in a client object and fire the join event/channel
func (g *Gatekeeper) Join(conn GateConn) {
	join <- g.NewClient(conn)
}

// Leave fire the leave channel event
func (g *Gatekeeper) Leave(client *Client) {
	leave <- client
}

// FindClient locate the client from the list and return the element or false if not found
func (g *Gatekeeper) FindClient(client *Client) (*list.Element, bool) {
	defer g.RUnlock()
	g.RLock()
	for e := g.clients.Front(); e != nil; e = e.Next() {
		c, ok := e.Value.(*Client)
		if ok && c.conn == client.conn {
			return e, true
		}
	}

	return nil, false
}

// GetClientCount returns the total number of clients
func (g *Gatekeeper) GetClientCount() int {
	g.RLock()
	size := g.clients.Len()
	g.RUnlock()

	return size
}
