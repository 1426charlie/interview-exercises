package stackservice

import (
	"fmt"
	"net"
)

var running = true

// Server the tcp server
type Server struct {
	cfg  *Config
	quit chan bool
	log  *Logger
}

// NewServer : create a new tcp socket server with context
func NewServer(cfg *Config) (*Server, error) {
	server := new(Server)
	server.cfg = cfg
	server.quit = make(chan bool)

	server.log = NewLogger("Server", LogTargetTCP)

	server.log.Info("Create the TCP Stack Service, Version: %s", cfg.Version())

	return server, nil
}

// Start creates the handlers and gatekeeper then starts the tcp server
func (s Server) Start() {
	info := s.log.Info
	host := fmt.Sprintf("%s:%d", s.cfg.host, s.cfg.port)
	info("start tcp server, host=%s", host)

	listener, err := net.Listen("tcp", host)
	s.cfg.checkError(err)

	defer listener.Close()

	handlers := NewStackRequestHandlers(s.cfg)
	gatekeeper := NewGatekeeper(s.cfg)
	go gatekeeper.Start(handlers)

	for running == true {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Printf("CONNECTION ERROR %v\n", err)
			continue
		}

		go gatekeeper.Join(conn)
	}
}

// Shutdown stop the server
func (s Server) Shutdown() {
	s.log.Info("shutting down by request...")
	running = false
	s.quit <- true
}
