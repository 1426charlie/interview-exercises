package stackservice

import (
	"flag"
	"fmt"
	"os"
)

const (
	version = "0.3.24"
)

// Config : configure context from defaults and command line; configure the logging system
type Config struct {
	env        string // defaults to production
	host       string
	port       int
	timeout    int64
	maxconns   int
	maxstack   int
	maxpayload int
	headersize int
	pid        int
	logger     *Logger
}

// ToMap : return a map of the current context
func (cfg *Config) ToMap() map[string]interface{} {
	hash := make(map[string]interface{})

	hash["env"] = cfg.env

	hash["host"] = cfg.host
	hash["port"] = cfg.port
	hash["timeout"] = cfg.timeout
	hash["maxconns"] = cfg.maxconns
	hash["maxstack"] = cfg.maxstack
	hash["maxpayload"] = cfg.maxpayload
	hash["headersize"] = cfg.headersize
	hash["pid"] = cfg.pid

	return hash
}

// NewDefaultConfig : create and return the a default context
func NewDefaultConfig() *Config {
	cfg := new(Config)

	cfg.env = "production"

	cfg.host = "127.0.0.1"
	cfg.port = 8080
	cfg.timeout = int64(10) // seconds in unix time
	cfg.maxconns = 100
	cfg.maxstack = 100
	cfg.maxpayload = 127
	cfg.headersize = 1

	cfg.pid = os.Getpid()

	return cfg
}

// NewConfig create a new context object and copy the supplied values from name/value map
func NewConfig(hash map[string]interface{}) *Config {
	cfg := NewDefaultConfig()

	// re-assign selected values cfg...
	if env, ok := hash["env"]; ok {
		cfg.env = env.(string)
	}

	return cfg
}

// ParseArgs : parse the command line args; --help to dump the flags
func ParseArgs() *Config {
	// use the defaults to set flag defaults
	dflt := NewDefaultConfig()

	// setup the command line switches
	vers := flag.Bool("version", false, "show the version and exit")
	env := flag.String("env", dflt.env, "set the environment, defaults to "+dflt.env)
	port := flag.Int("port", dflt.port, "set the server's port number (e.g., 8080)...")
	timeout := flag.Int64("timeout", dflt.timeout, "set the timeout in seconds")

	flag.Parse()

	if *vers == true {
		fmt.Printf("StackService Version: %s\n", version)
		os.Exit(0)
	}

	cfg := new(Config)
	// copy defaults to new context
	*cfg = *dflt

	// now copy the command line options
	cfg.env = *env
	cfg.port = *port
	cfg.timeout = *timeout

	return cfg
}

func (cfg Config) GetPort() int {
	return cfg.port
}

// Version : show the current app version
func (cfg Config) Version() string {
	return version
}

func (cfg Config) checkError(err error) {
	if err != nil {
		panic(err)
	}
}
