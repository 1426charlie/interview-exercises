package stackservice

import (
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

type LogTarget int

const (
	LogTargetTCP LogTarget = 1 << iota
	LogTargetUDP
	LogTargetFILE
	LogTargetSTDOUT
)

// Logger accepts messages from targets to log specific categories/levels
type Logger struct {
	writer   io.Writer
	category string
	target   LogTarget
}

// shared log connection...
var logconn net.Conn

// NewLogger : create a new manager
func NewLogger(category string, target LogTarget) *Logger {
	logger := Logger{}

	logger.writer = os.Stdout
	logger.category = category

	switch target {
	case LogTargetTCP:
		if logconn == nil {
			host := "127.0.0.1:8081"
			conn, err := net.Dial("tcp", host)
			if err != nil {
				fmt.Printf("error connecting to %s\n", host)
				fmt.Println(err.Error())
			} else {
				logger.writer = conn
				logconn = conn
			}
		} else {
			logger.writer = logconn
		}
	}

	return &logger
}

func (lm Logger) send(level string, format string, v ...interface{}) {
	now := time.Now().Format("15:04:05.000")
	msg := fmt.Sprintf(format, v...)
	str := fmt.Sprintf("%v %s %s: %v\n", now, level, lm.category, msg)

	fmt.Fprintf(lm.writer, str)
}

// Info send an info message to the log
func (lm Logger) Info(format string, v ...interface{}) {
	lm.send("INFO", format, v...)
}

func (lm Logger) Error(format string, v ...interface{}) {
	lm.send("ERROR", format, v...)
}
