package stackservice

import (
	"encoding/binary"
	"net"
)

const (
	busyResponse uint8 = 0xFF
	pushResponse uint8 = 0x00
	pushMask     uint8 = 0x80
)

// StackRequestHandlers container for log and stack
type StackRequestHandlers struct {
	stack *Stack
	log   *Logger
}

// NewStackRequestHandlers create with context
func NewStackRequestHandlers(cfg *Config) *StackRequestHandlers {
	h := StackRequestHandlers{}

	h.stack = NewStack(cfg.maxstack)
	h.log = NewLogger("Handlers", LogTargetTCP)

	h.log.Info("create the stack request handlers...")

	return &h
}

// HandleRequest handle the normal push/pop request
func (h StackRequestHandlers) HandleRequest(client *Client, closer LeaveFunc) {
	defer closer(client)

	// if this is a real net connection, then operate on it, else it's a mock
	if conn, ok := client.conn.(net.Conn); ok {
		var header uint8

		h.log.Info("read the header from conn: %v", conn)

		err := binary.Read(conn, binary.LittleEndian, &header)
		if err != nil {
			h.log.Error("header %x", header)
			h.log.Error("binary read error %v", err)
			h.log.Info("writing busy indicator...")
			binary.Write(conn, binary.LittleEndian, busyResponse)
		} else {
			complete := make(chan StackType, 1)
			h.log.Info("header from conn: %v = %x", conn, header)
			// determine push or pop and count from header
			push, size := h.IsPush(header)

			if push {
				// read the bytes(size) and push to stack
				payload := make([]byte, size)
				err = binary.Read(conn, binary.LittleEndian, &payload)

				h.log.Info("push payload: %s, chan: %v", payload, complete)
				h.stack.Push(client.id, payload, complete)
				// wait for complete

				// TODO multiplex between complete and quit
				<-complete

				h.log.Info("return the push response: %x, chan: %v", pushResponse, complete)
				err := binary.Write(conn, binary.LittleEndian, pushResponse)
				if err != nil {
					h.log.Error("error sending response: %v", err)
				}
			} else {
				// pop the value and write the pop response
				h.log.Info("pop request: %x, chan: %v", header, complete)
				payload := <-complete

				h.log.Info("pop'd payload %s, chan: %v", payload, complete)

				size := uint8(len(payload) & 0xFF)
				response := make([]byte, size+1)
				response[0] = size
				copy(response[1:], payload)

				// return count + payload
				binary.Write(conn, binary.LittleEndian, response)
			}

			// make sure this channel is closed and garbage collected
			close(complete)
		}

		conn.Close()
	}
}

// IsPush reads the header byte and decodes for push with count or pop
func (h StackRequestHandlers) IsPush(header byte) (bool, uint8) {
	var size uint8
	push := (header & pushMask) == 0

	if push {
		size = header & ^pushMask
	}

	// h.log.Info("push=%t header: %x size %d", push, header, size)

	return push, size
}

// HandleBusyResponse return the busy indicator
func (h StackRequestHandlers) HandleBusyResponse(client *Client, closer LeaveFunc) {
	defer closer(client)

	if conn, ok := client.conn.(net.Conn); ok {
		h.log.Info("respond with the busy flag...")
		binary.Write(conn, binary.LittleEndian, busyResponse)
		conn.Close()
	}
}
