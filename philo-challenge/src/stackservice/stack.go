package stackservice

import (
	"container/list"
	"sync"
)

// StackType defines the stack's datatype
type StackType []byte

type pushContainer struct {
	cid   int64
	done  chan<- StackType
	value StackType
}

type popContainer struct {
	cid  int64
	done chan<- StackType
}

// Stack : the stack structure with sync/mutex and maxsize; maxsize is used to determine the "Full" state.
type Stack struct {
	sync.RWMutex
	lifo      []StackType
	popQueue  *list.List // complete chan
	pushQueue *list.List // complete chan + value
	maxsize   int
	log       *Logger
}

// StackStats container for stack, pop and push queue sizes
type StackStats struct {
	StackSize int
	PopQSize  int
	PushQSize int
}

// NewStack create a new stack with the max size set to size
func NewStack(size int) *Stack {
	stack := Stack{}

	stack.log = NewLogger("Stack", LogTargetTCP)
	stack.maxsize = size
	stack.lifo = make([]StackType, 0, size)
	stack.pushQueue = list.New()
	stack.popQueue = list.New()

	stack.log.Info("create the lifo stack, max size %d", stack.maxsize)

	return &stack
}

// Push push a value to the stack; block if the stack is full until there is an open spot
func (s *Stack) Push(clientID int64, v StackType, done chan<- StackType) {
	defer s.Unlock()
	s.Lock()
	if len(s.lifo) == 0 && s.popQueue.Len() > 0 {
		e := s.popQueue.Front()

		// now remove it...
		s.popQueue.Remove(e)

		if container, ok := e.Value.(*popContainer); ok {
			container.done <- v
			s.log.Info("value %s pop'd from the popQueue, cid: %v, popqsize: %d", v, container.cid, s.popQueue.Len())
		} else {
			s.log.Error("pop queue failed to return container, memory leak!!!")
		}

		done <- v
		s.log.Info("returned %s the psuedo pop'd value to pusher: %v", v, done)
		return
	}

	if len(s.lifo) >= s.maxsize {
		s.pushQueue.PushBack(s.createPushContainer(clientID, v, done))

		s.log.Info("queue the push id: %d, stack size: %d, push q size: %d", clientID, len(s.lifo), s.pushQueue.Len())
		return
	}

	s.lifo = append(s.lifo, v)
	s.log.Info("return from push: %s, stack size: %d", v, len(s.lifo))
	done <- v
}

// Pop an item off the top of the stak; return nil if the stack is empty
func (s *Stack) Pop(clientID int64, done chan<- StackType) {
	defer s.Unlock()
	s.Lock()
	if len(s.lifo) == 0 {
		s.popQueue.PushBack(s.createPopContainer(clientID, done))
		s.log.Info("queue the pop for cid: %d, pop q size: %d", clientID, s.popQueue.Len())
		return
	}

	if len(s.lifo) == s.maxsize && s.pushQueue.Len() > 0 {
		e := s.pushQueue.Front()
		s.pushQueue.Remove(e)

		c := e.Value.(*pushContainer)
		c.done <- c.value
		done <- c.value

		s.log.Info("return push queue value %s, id: %d stack size: %d, queue size: %d", c.value, c.cid, len(s.lifo), s.pushQueue.Len())

		return
	}

	top := len(s.lifo) - 1

	item := s.lifo[top]
	s.lifo = s.lifo[:top]

	s.log.Info("return from pop: %s", item)
	done <- item
}

func (s *Stack) createPushContainer(clientID int64, v StackType, done chan<- StackType) *pushContainer {
	c := new(pushContainer)
	c.cid = clientID
	c.done = done
	c.value = v

	return c
}

func (s *Stack) createPopContainer(clientID int64, done chan<- StackType) *popContainer {
	c := new(popContainer)
	c.cid = clientID
	c.done = done

	return c
}

// Size return the current stack size
func (s *Stack) Size() int {
	s.RLock()
	size := len(s.lifo)
	s.RUnlock()
	return size
}

// Full return true if the stack has reached it's capacity
func (s *Stack) Full() bool {
	size := s.Size()
	return size >= s.maxsize
}

// Empty return true if the stack is empty
func (s *Stack) Empty() bool {
	s.RLock()
	f := len(s.lifo) == 0
	s.RUnlock()
	return f
}

// GetStats return the stack, pop and push sizes
func (s *Stack) GetStats() StackStats {
	defer s.RUnlock()
	s.RLock()
	stats := StackStats{
		StackSize: len(s.lifo),
		PopQSize:  s.popQueue.Len(),
		PushQSize: s.pushQueue.Len(),
	}

	s.log.Info("stack: %d, popq: %d, pushq: %d", stats.StackSize, stats.PopQSize, stats.PushQSize)

	return stats
}

// Peek show what is on the top of the stack
func (s *Stack) Peek() StackType {
	defer s.RUnlock()
	s.RLock()
	top := len(s.lifo) - 1
	if top < 0 {
		return nil
	}

	return s.lifo[top]
}
