package main

import (
	"fmt"
	"log"

	"./stackservice"
)

func main() {
	// create the context and main TCP service
	cfg := stackservice.ParseArgs()
	server, err := stackservice.NewServer(cfg)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("\nStarting Stack Server version %s port %d\n", cfg.Version(), cfg.GetPort())
	fmt.Printf("Stack Server context: %v\n\n", cfg.ToMap())

	server.Start()
}
