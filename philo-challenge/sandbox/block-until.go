package main

// just to demonstrate waiting for ready

import (
	"fmt"
	"time"
)

func main() {
	ready := make(chan bool, 1)

	go block(ready)

	time.Sleep(time.Second)
	fmt.Println("release...")
	ready <- true

	fmt.Println("ready for you now...")
}

func block(ok chan bool) {
	fmt.Print("waiting...")
	<-ok
	fmt.Print("ok, I'm unblocked...")
}
