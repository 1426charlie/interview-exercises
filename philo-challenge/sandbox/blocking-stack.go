package main

import (
	"fmt"
	"time"

	"../src/stackservice"
)

// in implementation that uses shared stack and blocking channel
func producer1(stack *stackservice.Stack, pushReady <-chan bool, done chan bool) {
	fmt.Println("start the stack prodcer 1 thread")
	for {
		select {
		case <-time.After(time.Millisecond * 200):
			for i := 0; i < 14; i++ {
				msg := fmt.Sprintf("msg (1) %d", time.Now())
				for stack.Full() {
					fmt.Println("full so wait for block, size: ", stack.Size())
					<-pushReady
				}
				fmt.Printf("push %v, size: %d\n", msg, stack.Size())
				stack.Push(msg)
			}
		case <-done:
			break
		}

	}

	fmt.Println("stack producer completed...")
}

func producer2(stack *stackservice.Stack, pushReady <-chan bool, done chan bool) {
	fmt.Println("start the stack prodcer 2 thread")
	for {
		select {
		case <-time.After(time.Millisecond * 250):
			for i := 0; i < 12; i++ {
				msg := fmt.Sprintf("msg (2) %d", time.Now())
				for stack.Full() {
					fmt.Println("full so wait for block, size: ", stack.Size())
					<-pushReady
				}
				fmt.Printf("push %v, size: %d\n", msg, stack.Size())
				stack.Push(msg)
			}
		case <-done:
			break
		}

	}

	fmt.Println("stack producer completed...")
}

func consumer(stack *stackservice.Stack, pushReady chan<- bool, done chan bool) {
	fmt.Println("start the stack consumer thread")
	for {
		select {
		case <-time.After(300 * time.Millisecond):
			// if the stack is currently full, then send a ready signal after the pop
			for i := 0; i < 5; i++ {
				if stack.Empty() {
					time.Sleep(10 * time.Millisecond)
				} else {
					signalReady := stack.Full()
					v := stack.Pop()
					fmt.Printf("popped: %v\n", v)
					if signalReady {
						pushReady <- true
					}
				}
			}

		case <-done:
			break
		}
	}

	fmt.Println("stack consumer completed...")
}

func main() {
	complete := make(chan bool)
	stack := stackservice.NewStack(20)
	pushReady := make(chan bool)

	fmt.Println("initial stack size: ", stack.Size())

	go producer1(stack, pushReady, complete)
	go consumer(stack, pushReady, complete)
	go producer2(stack, pushReady, complete)

	for count := 1; count <= 10; count++ {
		time.Sleep(time.Second)
		// print something useful...
		fmt.Println("---------------------------- seconds: ", count)
	}

	fmt.Println("set complete to true...")
	complete <- true
}
