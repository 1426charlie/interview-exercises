package main

import (
	"fmt"

	"../src/stackservice"
)

func main() {
	max := 10
	stack := stackservice.NewStack(max)

	fmt.Println("create the stack with max size: ", max)

	stack.Push("my value")
	v := stack.Pop()

	fmt.Println(v)
}
