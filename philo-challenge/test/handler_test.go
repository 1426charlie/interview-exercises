package stackservice_test

import (
	"testing"

	"../mocks"
	"../src/stackservice"

	. "github.com/franela/goblin"
)

func TestHandler(t *testing.T) {
	g := Goblin(t)
	cfg := createTestConfig()
	gatekeeper := stackservice.NewGatekeeper(cfg)
	handlers := stackservice.NewStackRequestHandlers(cfg)

	g.Describe("Handler", func() {
		g.It("should execute the standard handler", func(done Done) {
			// will timeout if complete isn't called by handler
			var complete = func(client *stackservice.Client) {
				done()
			}

			conn := mocks.NewMockConn()
			client := gatekeeper.NewClient(conn)

			g.Assert(client != nil).Equal(true)
			handlers.HandleRequest(client, complete)
		})

		g.It("should execute the busy, quick-exit handler", func(done Done) {
			// will timeout if complete isn't called by handler
			var complete = func(client *stackservice.Client) {
				done()
			}

			conn := mocks.NewMockConn()
			client := gatekeeper.NewClient(conn)

			g.Assert(client != nil).Equal(true)
			handlers.HandleBusyResponse(client, complete)
		})

		g.It("should decode a series of push headers", func() {
			values := make(map[uint8]bool)
			values[0x01] = true
			values[0x8F] = false
			values[0x81] = false
			values[0x08] = true
			values[0x78] = true

			for k, v := range values {
				// fmt.Println(k, v)
				push, size := handlers.IsPush(k)

				g.Assert(push).Equal(v)
				if push {
					var mask uint8 = 0x7F
					g.Assert(size).Equal(k & mask)
					g.Assert(size < 128).IsTrue()
				} else {
					g.Assert(size).Equal(uint8(0))
				}
			}
		})
	})
}
