package stackservice_test

import (
	"testing"

	"../src/stackservice"

	. "github.com/franela/goblin"
)

func TestLogger(t *testing.T) {
	g := Goblin(t)

	target := stackservice.LogTargetTCP

	g.Describe("Logger", func() {
		g.It("should create a logger struct", func() {
			log := stackservice.NewLogger("What", target)

			g.Assert(log != nil)
		})

		g.It("should accept log messages", func() {
			log := stackservice.NewLogger("TestLogger", target)

			log.Info("this is my test log message")
		})
	})
}
