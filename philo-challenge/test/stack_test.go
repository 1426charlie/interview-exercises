package stackservice_test

import (
	"fmt"
	"testing"
	"time"

	"../src/stackservice"

	. "github.com/franela/goblin"
)

func createPayload(n uint8) stackservice.StackType {
	// TODO create a random byte array 1..127
	s := fmt.Sprintf("%d test", n)
	return []byte(s)
}

func TestStack(t *testing.T) {
	g := Goblin(t)

	g.Describe("Stack", func() {
		g.It("should create a stack struct", func() {
			stack := stackservice.NewStack(10)

			g.Assert(stack != nil)
			g.Assert(stack.Empty()).IsTrue()
			g.Assert(stack.Size()).Equal(0)
		})
	})

	g.Describe("Async", func() {
		g.It("should push/pop async", func(done Done) {
			max := 25
			// don't let a block occur
			stack := stackservice.NewStack(max + 10)

			for i := 0; i < max; i++ {
				payload := createPayload(uint8(i + 100))

				// push them all in the background
				go func(pay stackservice.StackType) {
					id := time.Now().UnixNano()
					complete := make(chan stackservice.StackType, 1)
					stack.Push(id, pay, complete)
					v := <-complete
					g.Assert(v).Equal(pay)
				}(payload)
			}

			for i := 0; i < max; i++ {
				go func() {
					id := time.Now().UnixNano()
					complete := make(chan stackservice.StackType, 1)
					stack.Pop(id, complete)
					pay := <-complete
					g.Assert(pay != nil)
				}()
			}

			for {
				time.Sleep(50 * time.Millisecond)
				if stack.Empty() {
					done()
					break
				}
			}
		})
	})

	g.Describe("PushPopNoBlock", func() {
		g.It("should store elements on the stack", func() {
			pushComplete := make(chan stackservice.StackType, 1)
			popComplete := make(chan stackservice.StackType, 1)

			max := 10
			// make the stack max size larger than max
			stack := stackservice.NewStack(max + 1)

			for i := 0; i < max; i++ {
				id := time.Now().UnixNano()
				ref := createPayload(uint8(i + 1))
				stack.Push(id, ref, pushComplete)
				v := <-pushComplete

				g.Assert(v).Equal(ref)
				g.Assert(stack.Size()).Equal(i + 1)
				g.Assert(stack.Peek()).Equal(ref)
				g.Assert(stack.Full()).IsFalse()
			}

			for i := 0; i < max; i++ {
				id := time.Now().UnixNano()
				stack.Pop(id, popComplete)
				v := <-popComplete

				g.Assert(v != nil).IsTrue()
				g.Assert(stack.Size()).Equal(max - 1 - i)
			}

			g.Assert(stack.Size()).Equal(0)
			g.Assert(stack.Empty()).IsTrue()

		})
	})

	g.Describe("PushPopBlock", func() {
		g.It("should queue a pop requests when stack is empty", func(done Done) {
			inputs := []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
			expects := []uint8{1, 2, 3, 4, 5, 10, 9, 8, 7, 6}

			max := 10
			stack := stackservice.NewStack(max)

			g.Assert(stack.Size()).Equal(0)
			g.Assert(stack.Empty()).Equal(true)

			for i := 0; i < max/2; i++ {
				// push them all in the background
				go func() {
					id := time.Now().UnixNano()
					complete := make(chan stackservice.StackType, 1)
					stack.Pop(id, complete)
					v := <-complete
					g.Assert(v != nil).IsTrue()
				}()
			}

			// give time for the pops' to queue
			time.Sleep(time.Millisecond * 10)

			// verify that the stack is zero, popqueue = 5
			g.Assert(stack.Empty()).Equal(true)

			stats := stack.GetStats()
			g.Assert(stats.StackSize).Equal(0)
			g.Assert(stats.PopQSize).Equal(5)
			g.Assert(stats.PushQSize).Equal(0)

			popQSize := 5
			for i := 0; i < 5; i++ {
				id := time.Now().UnixNano()
				complete := make(chan stackservice.StackType, 1)
				ref := createPayload(inputs[i])
				stack.Push(id, ref, complete)
				v := <-complete

				popQSize--

				g.Assert(v).Equal(ref)
				g.Assert(v).Equal(createPayload(expects[i]))
				stats := stack.GetStats()
				g.Assert(stats.StackSize).Equal(0)
				g.Assert(stats.PopQSize).Equal(popQSize)
				g.Assert(stats.PushQSize).Equal(0)
			}

			// now push the remaining set and verify pop/push queues are empty
			stackSize := 0
			for i := 5; i < 10; i++ {
				id := time.Now().UnixNano()
				complete := make(chan stackservice.StackType, 1)
				ref := createPayload(inputs[i])
				stack.Push(id, ref, complete)
				v := <-complete
				stackSize++

				g.Assert(v).Equal(ref)
				stats := stack.GetStats()
				g.Assert(stats.StackSize).Equal(stackSize)
				g.Assert(stats.PopQSize).Equal(0)
				g.Assert(stats.PushQSize).Equal(0)
			}

			// verify normal pop stack order
			for i := 5; i < 10; i++ {
				id := time.Now().UnixNano()
				complete := make(chan stackservice.StackType, 1)
				stack.Pop(id, complete)
				v := <-complete

				stackSize--
				expect := createPayload(expects[i])
				g.Assert(v).Equal(expect)
				stats := stack.GetStats()
				g.Assert(stats.StackSize).Equal(stackSize)
				g.Assert(stats.PopQSize).Equal(0)
				g.Assert(stats.PushQSize).Equal(0)
			}

			done()
		})

		g.It("should queue push requests when stack is full", func(done Done) {
			inputs := []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
			expects := []uint8{7, 8, 9, 10, 6, 5, 4, 3, 2, 1}

			max := 6
			stack := stackservice.NewStack(max)

			stackSize := 0
			for i := 0; i < max; i++ {
				id := time.Now().UnixNano()
				complete := make(chan stackservice.StackType, 1)
				payload := createPayload(inputs[i])
				stack.Push(id, payload, complete)
				v := <-complete
				stackSize++

				g.Assert(v).Equal(payload)
				stats := stack.GetStats()
				g.Assert(stats.StackSize).Equal(stackSize)
				g.Assert(stats.PopQSize).Equal(0)
				g.Assert(stats.PushQSize).Equal(0)
			}

			// now push to the full stack, don't wait...
			for i := max; i < len(inputs); i++ {

				payload := createPayload(inputs[i])

				go func(pay stackservice.StackType) {
					id := time.Now().UnixNano()
					complete := make(chan stackservice.StackType, 1)

					stack.Push(id, pay, complete)
					v := <-complete
					g.Assert(v).Equal(pay)
				}(payload)
			}

			// some time for the push queue to settle
			time.Sleep(10 * time.Millisecond)

			stats := stack.GetStats()
			g.Assert(stats.StackSize).Equal(max)
			g.Assert(stats.PopQSize).Equal(0)
			g.Assert(stats.PushQSize).Equal(len(inputs) - max)

			// now pop the values and verify the returns
			for i := 0; i < len(expects); i++ {
				id := time.Now().UnixNano()
				complete := make(chan stackservice.StackType, 1)
				stack.Pop(id, complete)
				v := <-complete

				g.Assert(v).Equal(createPayload(expects[i]))
			}

			done()
		})
	})
}
