package stackservice_test

import (
	"testing"

	"../src/stackservice"

	. "github.com/franela/goblin"
)

func TestService(t *testing.T) {
	g := Goblin(t)
	cfg := createTestConfig()

	g.Describe("Server", func() {
		g.It("should create a stack server", func() {
			server := new(stackservice.Server)

			g.Assert(server != nil)
		})
	})

	g.Describe("NewServer", func() {
		g.It("should create a new tcp server", func() {

			server, err := stackservice.NewServer(cfg)

			g.Assert(err == nil)
			g.Assert(server != nil)
		})
	})
}
