package stackservice_test

import (
	"testing"

	"../mocks"

	"../src/stackservice"
	. "github.com/franela/goblin"
)

func createConn() mocks.MockConn {
	return mocks.NewMockConn()
}

func TestGatekeeper(t *testing.T) {
	g := Goblin(t)
	cfg := createTestConfig()
	handlers := stackservice.NewStackRequestHandlers(cfg)

	g.Describe("Gatekeeper", func() {
		g.It("should create a gatekeeper struct", func() {
			gatekeeper := stackservice.NewGatekeeper(cfg)
			g.Assert(gatekeeper != nil)
		})
	})

	g.Describe("Join", func() {
		g.It("should accept a new client from mock connection", func() {

			gatekeeper := stackservice.NewGatekeeper(createTestConfig())
			go gatekeeper.Start(handlers)

			count := gatekeeper.GetClientCount()
			g.Assert(count).Equal(0)

			conn := createConn()
			gatekeeper.Join(conn)

			count = gatekeeper.GetClientCount()
			// can't be sure if it was added then removed (all happens quick)
			g.Assert(count < 2).Equal(true)

		})
	})

}
