package stackservice_test

import (
	"fmt"
	"testing"

	"../src/stackservice"

	"os"

	. "github.com/franela/goblin"
)

func createTestConfig() *stackservice.Config {
	hash := make(map[string]interface{})
	hash["env"] = "test"
	hash["logname"] = fmt.Sprintf("test-%d.log", os.Getpid())

	cfg := stackservice.NewConfig(hash)

	return cfg
}

func TestConfig(t *testing.T) {
	g := Goblin(t)

	g.Describe("Config", func() {
		g.It("should create a context struct", func() {
			cfg := new(stackservice.Config)

			g.Assert(cfg != nil).IsTrue()
		})
	})

	g.Describe("NewDefaultConfig", func() {
		g.It("should create a default context", func() {
			cfg := stackservice.NewDefaultConfig()

			g.Assert(cfg != nil).IsTrue()

			hash := cfg.ToMap()

			g.Assert(hash["host"]).Equal("127.0.0.1")
			g.Assert(hash["port"]).Equal(8080)
			g.Assert(hash["timeout"]).Equal(int64(10))
			g.Assert(hash["maxconns"]).Equal(100)
			g.Assert(hash["maxstack"]).Equal(100)
			g.Assert(hash["maxpayload"]).Equal(127)
			g.Assert(hash["headersize"]).Equal(1)
		})
	})

	g.Describe("Parser", func() {
		g.It("should create a resonable context even without command line params", func() {
			dflt := stackservice.NewDefaultConfig()
			cfg := stackservice.ParseArgs()

			g.Assert(cfg != nil).IsTrue()
			g.Assert(cfg).Equal(dflt)
		})
	})
}
