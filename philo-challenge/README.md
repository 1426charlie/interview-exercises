# Philo Challenge : LIFO Stack Server

## Overview

The *LIFO Stack Server* is implemented in golang version 1.7 but should run fine in version 1.6.  There is a docker container if you want to run the tests from there (container has golang 1.7 and ruby 2.3.3) but it's not necessary.

## Operation

### Installing the Service

Extract the tar distribution.

`tar xvzf philo-challenge-darrylwest.tar.gz`

Now `cd philo-challenge` and you should have bin, src, test, tools and a few other files/folders.  There is a Makefile with multiple targets to run, test, start, etc.  It's probably a good idea to run the unit tests first.  Just do this...

`make test`

This should run a handful of tests that all pass.

### Starting the Service

The service can be run from the Makefile `start` target or from the bin folder with `./bin/start`.

`make start`

Then to run the tests...

`ruby stack-test.rb`

## Architecture & Implementation

Key Points:

### Blocking LIFO Stack

A blocking stack synchronized was created to... 

* block push's if the stack is full (max size = 100)
* block pop's if the stack is empty
* lock/unlock the lifo array/slice for reads, push and pop

Blocking is implemented with two channels, popReady and pushReady.  Timeouts were added to insure minimum waits for concurrent operations.

### Connection Gatekeeper

The connection gatekeeper is responsible for controlling the number of connections as they are accepted from the socket server.  It does the following...

* caps at maximum connections and returns try-later flag when full
* closes oldest connection when max connections is reached and additional requests are made and the 10 second timeout has been exceeded

The list of clients is held in a collections/list to make finding the oldest one a quick search.  I considered using a map, but decided the sorted linked-list would be faster, especially since it's capped to 100 connections.

### Stack Request Handlers

Requests for push and pop are handled by the 


### Logging

There are two forms of logging: 1) stdout and 2) TCP socket listener on port 8081.  There is a socket server/listener in the tools folder.  You can run it by doing this:

`go run tools/tcp-log-server.go`

It simply spits it's output to stdout that can be redirected to a file or observed from another terminal.

### Language Options

* ruby
* node.js / javascript (this wasn't listed in the approved languages, so...)
* golang
* python

### Containerized Ruby Test Driver

One way to keep up to date with language distributions is to use docker containers.  So, I created a small docker image to run the ruby test script.  I also created a go container to support 1.7 for the Stack Server.

## Checklist

### Specification

Picking out the highlights from the spec to verify that all is done...

* Write a server that manages a _LIFO stack_, supporting push and pop
 operations. The server listens for requests from clients connecting over _TCP_
 on port _8080_. The server should _respond to the request and then close the
 connection_.
 
* A push request pushes the given payload onto the stack. However, the stack
 can _have no more than 100 items_ on it. _Push requests_ for a full stack _should
 block_ until stack space becomes available.
 
* A pop request returns the top item from the stack to the client. If the stack is empty, the _pop request should block until an item becomes available_ on the stack.

* Both push and pop requests should be served (and their associated stack operations performed) in the order in which they arrive _fully_. _Fast clients should get their response before the slow clients_.

* The server should not have to juggle more than _100 clients simultaneously_. Additional client connections should be _rejected by sending a single byte response_ indicating busy-state and then immediately disconnected.

* If the server is already handling 100 connections and a new client tries to connect, _it must disconnect the oldest client_, provided their connection is _older than 10 seconds_. The server should only disconnect an old client when _provoked by a new incoming client connection that would otherwise have to be rejected_.

* A _push request_ format is as follows. The _first byte is the header_. The rest of the request is the payload. The most significant bit in the header byte is 0; the _7 remaining bits are the length of the payload_, in bytes. The _minimum size_ for a push request is _2 bytes_: 1 header byte and 1 payload byte. The _maximum size_ for a push request is _128 bytes_: 1 header byte and _127 payload bytes_.)

* The format of a pop request is a single byte with the _most significant bit set to 1_. The rest of the byte is ignored.

* The format of a _push response is 1 byte, all zeros_.

* The format of a pop response is _1 header byte_, with the _most significant bit set to 0_ and the _rest of the payload indicating the payload size_. The rest of the response is the payload indicated number of bytes of payload.

* The format of a _busy-state response_ is _1 byte, value 0xFF_.

* You have to assume _little-endian byte ordering_, the default on x86 architectures. Bytes are sent in network order.

* You are welcome to write this in the language of your choice, ideally in Ruby, Python, _Go_, or C. _You should choose the language you feel strongest in_.

### Known Tests

```
  def test_single_request
  def test_serialized_requests
  def test_full_stack_push_and_pop
  def test_interleaved_requests
  def test_long_polling_get
  def test_long_polling_push
  def test_pops_to_empty_stack
  def test_full_stack_ignore
  def test_server_resource_limit
  def test_slow_client_gets_killed_for_fast_client
  def test_one_slow_client_gets_killed_for_fast_clients
  def test_slowest_client_gets_killed
  def test_server_survives_half_message
  def test_server_queues_slow_message_correctly
  def test_slow_clients_are_not_disconnected_for_no_reason
```

###### darryl.west | 27-Nov-2016