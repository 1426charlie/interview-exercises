#!/bin/sh
# dpw@seattle.local
# 2016.11.22
#

# start the container but stay in shell
docker info > /dev/null || {
    echo "docker is not running, or your env is not configured correctly..."
}

name=ruby-stack-tests
docker run --name $name -it --rm -v "$PWD":/usr/src/app -w /usr/src/app ruby-go

