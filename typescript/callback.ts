
interface Callback {
    (err: Error, data: any): void;
}

function callServer(callback: Callback) {
    callback(null, 'hello!');
}

callServer((err, data) => console.log( data ));

