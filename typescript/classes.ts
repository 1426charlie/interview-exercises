
class Fun {
    game: string;
    play() {
        console.log('playing ', this.game);
    }

    paint() {
        console.log('painting');
    }
}

class Study {
    subject: string;
    study() {
        console.log('study ', this.subject);
    }
}

class Student implements Fun, Study {

    constructor(game, subject) {
        this.game = game;
        this.subject = subject;
    }

    game: string;
    subject: string;
    play:()=>void;
    paint:()=>void;
    study:()=>void;
}

function applyMixins(dctor: any, bctors: any[]) {
    bctors.forEach(bctor => {
        Object.getOwnPropertyNames(bctor.prototype).forEach(name => {
            dctor.prototype[name] = bctor.prototype[name];
        });
    });
}


