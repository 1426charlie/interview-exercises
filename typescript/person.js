var Person = (function () {
    function Person(nm, nick) {
        this.name = nm;
        this.nickName = nick || "";
    }
    return Person;
}());
var tom = new Person('Tom', 'Tommy');
console.log(tom);
var sam = new Person('Sam');
console.log(sam);
