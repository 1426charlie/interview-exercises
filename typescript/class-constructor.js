var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
function assign(target, source) {
    for (var id in source) {
        target[id] = source[id];
    }
    return target;
}
var DocumentIdentifier = (function () {
    function DocumentIdentifier(params) {
        if (typeof params === 'string' && params.indexOf('{') === 0) {
            try {
                params = JSON.parse(params);
            }
            catch (err) {
                console.log(err);
            }
        }
        if (typeof params === 'object') {
            assign(this, params);
        }
    }
    DocumentIdentifier.prototype.json = function () {
        return JSON.stringify(this);
    };
    return DocumentIdentifier;
}());
var Disclosure = (function (_super) {
    __extends(Disclosure, _super);
    function Disclosure(params) {
        if (params === void 0) { params = {}; }
        _super.call(this, params);
    }
    return Disclosure;
}(DocumentIdentifier));
var model = new Disclosure();
console.dir(model);
model = new Disclosure({
    id: '47e30316541b11e6869b22000bb0e74d',
    dateCreated: new Date(),
    lastUpdated: new Date(),
    version: 1,
    userId: 'ae984d1e541b11e6bae022000bb0e74d',
    type: 'a1',
    institution: 'My Company',
    status: 'active'
});
console.log(model.json());
model.startDate = new Date();
model.endDate = new Date();
model.version++;
var copy = new Disclosure(model);
console.log(copy.json());
copy.version++;
copy = new Disclosure(copy.json());
console.log(copy.json());
copy = new Disclosure('{idff');
console.log(copy.json());
var obj = {
    id: '47e30316541b11e6869b22000bb0e74d',
    dateCreated: 'flarb',
    status: 'active',
    extra: 'thing that doesnt belong'
};
copy = new Disclosure(obj);
console.log(copy.json());
console.log(Disclosure.prototype.type);
