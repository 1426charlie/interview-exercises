var Fun = (function () {
    function Fun() {
    }
    Fun.prototype.play = function () {
        console.log('playing ', this.game);
    };
    Fun.prototype.paint = function () {
        console.log('painting');
    };
    return Fun;
}());
var Study = (function () {
    function Study() {
    }
    Study.prototype.study = function () {
        console.log('study ', this.subject);
    };
    return Study;
}());
var Student = (function () {
    function Student(game, subject) {
        this.game = game;
        this.subject = subject;
    }
    return Student;
}());
function applyMixins(dctor, bctors) {
    bctors.forEach(function (bctor) {
        Object.getOwnPropertyNames(bctor.prototype).forEach(function (name) {
            dctor.prototype[name] = bctor.prototype[name];
        });
    });
}
