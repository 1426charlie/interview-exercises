function assign<T extends U, U>(target: T, source: U): T {
    for (let id in source) {
        target[ id ] = source[ id ];
    }

    return target;
}

class DocumentIdentifier {
    id:string;
    dateCreated:Date;
    lastUpdated:Date;
    version:number;

    constructor(params:any) {
        if (typeof params === 'string' && params.indexOf('{') === 0) {
            try {
                params = JSON.parse( params );
            } catch (err) {
                console.log( err );
            }
        }

        if (typeof params === 'object') {
            assign(this, params);
        }
    }

    json(): string {
        return JSON.stringify( this );
    }
}

class Disclosure extends DocumentIdentifier {

  userId:string;
  type:string;

  institution:string;
  grantProvided:boolean;
  paidEmployee:boolean;
  personalFee:boolean;
  otherFee:boolean;
  comment:string;
  description:string;
  startDate:Date;
  endDate:Date;
  yearDisclosed:string;

  status:string;

  constructor(params:any = {}) {
    super( params );
  }
}

let model = new Disclosure();

console.dir( model );

model = new Disclosure({
    id:'47e30316541b11e6869b22000bb0e74d',
    dateCreated: new Date(),
    lastUpdated: new Date(),
    version: 1,
    userId: 'ae984d1e541b11e6bae022000bb0e74d',
    type: 'a1',
    institution:'My Company',
    status:'active'
});

console.log( model.json() );
model.startDate = new Date();
model.endDate = new Date();
model.version++;

let copy = new Disclosure( model );
console.log( copy.json() );
copy.version++;

copy = new Disclosure( copy.json() );

console.log( copy.json() );

copy = new Disclosure( '{idff');

console.log( copy.json() );

let obj = {
    id:'47e30316541b11e6869b22000bb0e74d',
    dateCreated: 'flarb',
    status:'active',
    extra:'thing that doesnt belong'
};

copy = new Disclosure( obj );
console.log( copy.json() );

console.log(Disclosure.prototype.type );