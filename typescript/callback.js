function callServer(callback) {
    callback(null, 'hello!');
}
callServer(function (err, data) { return console.log(data); });
