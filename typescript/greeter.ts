/**
 * a simple greeter class to output hello world with inside tags
 */
"use strict";

class Greeter<T> {
    greeting: T;
    tag: string;

    constructor(greeting: T, tag: string = "h1") {
        this.greeting = greeting;
        this.tag = tag;
    }

    greet() {
        return `<${this.tag}>${this.greeting}</${this.tag}>`;
    }
};

var greeter = new Greeter<string>("Hello, world!", "p");
console.log( greeter.greet() );

var ngreeter = new Greeter<number>(55, "p");
console.log( ngreeter.greet() );
