
class Person {
    name: string;
    nickName: string;

    constructor(nm: string, nick?: string) {
        this.name = nm;
        this.nickName = nick || "";
    }
}

let tom = new Person('Tom', 'Tommy');

console.log( tom );

let sam = new Person('Sam');
console.log( sam );

