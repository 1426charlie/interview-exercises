var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
function Override(label) {
    return function (target, key) {
        Object.defineProperty(target, key, {
            configurable: false,
            get: function () { return label; }
        });
    };
}
var Tester = (function () {
    function Tester() {
        this.name = 'pat';
    }
    __decorate([
        Override('tester'), 
        __metadata('design:type', String)
    ], Tester.prototype, "name");
    return Tester;
}());
var t1 = new Tester();
console.log(t1.name);
function ReadOnly(target, key) {
    Object.defineProperty(target, key, {
        writable: false
    });
}
var Test = (function () {
    function Test(name) {
        // @ReadOnly
        this.name = 'farley';
        this.name = name;
    }
    return Test;
}());
var t2 = new Test("flarb");
console.log(t2.name);
t2.name = 'mica';
console.log(t2.name);
