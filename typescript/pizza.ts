
class Pizza {
    toppings: string[];

    constructor(toppings: string[]) {
        this.toppings = toppings;
        console.log('pizza created with toppings: ', toppings);
    }

}

let pizza = new Pizza(['olives', 'meat', 'fish'])

console.log( pizza );
