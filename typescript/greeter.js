/**
 * a simple greeter class to output hello world with inside tags
 */
"use strict";
var Greeter = (function () {
    function Greeter(greeting, tag) {
        if (tag === void 0) { tag = "h1"; }
        this.greeting = greeting;
        this.tag = tag;
    }
    Greeter.prototype.greet = function () {
        return "<" + this.tag + ">" + this.greeting + "</" + this.tag + ">";
    };
    return Greeter;
}());
;
var greeter = new Greeter("Hello, world!", "p");
console.log(greeter.greet());
var ngreeter = new Greeter(55, "p");
console.log(ngreeter.greet());
