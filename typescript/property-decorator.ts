function Override(label: string) {
    return function(target: any, key: string) {
        Object.defineProperty(target, key, {
            configurable:false,
            get: () => label
        });
    }
}

class Tester {
    @Override('tester')
    name: string = 'pat';
}

let t1 = new Tester();
console.log( t1.name );

function ReadOnly(target: any, key: string) {
    Object.defineProperty( target, key, {
        writable: false
    });
}

class Test {
    @ReadOnly
    name: string = 'farley';

    constructor(name: string) {
        this.name = name;
    }

    
}

let t2 = new Test("flarb");

console.log( t2.name );

t2.name = 'mica';

console.log( t2.name );
