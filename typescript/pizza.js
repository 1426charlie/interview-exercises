var Pizza = (function () {
    function Pizza(toppings) {
        this.toppings = toppings;
        console.log('pizza created with toppings: ', toppings);
    }
    return Pizza;
}());
var pizza = new Pizza(['olives', 'meat', 'fish']);
console.log(pizza);
