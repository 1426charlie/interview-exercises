# Sea Condition Prediction System

## Overview

To predict sea condtions for marine vessels traveling through or near the West Coast of the US, NOAA has installed and maintains about 1,000 Buoys spread from the 48th to 29th parallel and extending 3 to 10 miles off the coast.  Each buoy transmits current sea and weather conditions each 10 to 15 seconds to a central hub where the data is stored and analysed. 

## Raw Data Set

Raw data transmitted from each buoy includes the following:

* buoy-id
* latitude
* longitude
* water-temp
* air temp
* wind direction
* wind speed
* wave/current direction
* current speed
* wave height
* status/meta data

## Buoy Position Grid

## Raw Data API

Raw data is pushed to a specific URL where it is validated and stored for predictions.  It must be capable of handling all data for the 1,000 active buoys each 10 seconds without any data loss (excluding filtered/bad data).

_Design a system that can reasonably handle this set of data and maintain a very high availablilty._

_Design an API to recieve the data including request and response._

## Data Accuracy

From time to time, data received from a buoy source may be in error, or the transmission may stop intermittantly.   The system must accomodate and validate data as it is recieved to determine if the buoy is still active or should be removed from predictions.

_Explain how you would accomplish this..._

## Consumer API

The consumers of this application will request sea condition predictions for n number of hours (4 to 12) in the future for a specified lat/long.  The prediction engine is expected to take between 60 and 120 seconds to complete.  When the prediction is complete it is returned to the requestor.

_Given the delay in response, design a system that would return and immediate response (without predictions) to the requester, then transmit the predictions when they are complete._

###### darryl.west | 2018.01.26
