<small>December 8th, 2016</small>
<hr/>

Dear Talent Connection,

I am very interested your recent offering for a Senior Software Engineer  in San Francisco (Ref: 56250/164562792).  My background includes over 25 years of software design, implementation and mentoring including many years with the following technologies:

* Golang: many backend, REST & web services, TCP/UDP/UNIX socket projects; github contributions
* Node.js: backend REST services, WebSockets, etc. Many github contributions
* RoR: small projects
* Javascript:mostly node but some client side
* Java projects - going back to the late 1990's and 2000's for Sun Microsystems
* Docker: container developer, maintainer, etc
* AWS: S3/EC2/RDS/SQS/ELB as a developer and admin for centos7, fedora, etc
* Other: Solr, Redis, Mongo, MySQL, SQL Server, DB2, Oracle

I look forward to talking with you soon about this opportunity.

Regards,

Darryl West
<br>775.250.8168</br>


###### darryl.west@raincitysoftware.com | 2016-12-08 | Ref: 56250/164562792
