
-module(echo).
-export([send/1,loop/0]).

send(Greeting) ->
    register(echo, spawn(echo, loop, [])),
    echo ! {self(), Greeting},
    receive
        {_Pid, Msg} -> io:format("~s~n", [Msg])
    end.

loop() ->
    receive
        {From, Msg} ->
            From ! {self(), Msg},
            loop();
        stop ->
            true
    end.

