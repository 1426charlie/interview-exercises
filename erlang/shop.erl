%% created by dpw on 2018-03-19 13:59:53

-module(shop).
-export([get_list/0,double/1]).

get_list() -> [{oranges,4},{newspaper,1},{apples,4},{pears,5},{milk,2}].

double(List) -> [{Name, X*2} || {Name, X} <- List].
