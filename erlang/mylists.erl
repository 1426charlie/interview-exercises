%% created by dpw on 2018-03-15 12:27:14

-module(mylists).
-export([sublist/2,for/3,sum/1,map/2,pythag/1]).

sublist(_,0) -> [];
sublist([],_) -> [];
sublist([H|T], N) when n > 0 -> [H|sublist(T,N-1)].

for(Max, Max, F) -> [F(Max)];
for(Idx, Max, F) -> [F(Idx) | for(Idx + 1, Max, F)].

sum([H|T]) -> H + sum(T);
sum([]) -> 0.

map(F, L) -> [F(X) || X <- L].

pythag(N) ->
    Min = 3,
    [ {A,B,C} ||
        A <- lists:seq(Min, N),
        B <- lists:seq(Min, N),
        C <- lists:seq(Min, N),
        A + B + C =< N,
        A*A + B*B =:= C*C
    ].
