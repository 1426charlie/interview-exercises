%% created by dpw on 2018-03-15 12:27:14
%% exported the function double with an arity of 1 (one)...

-module(demo).
-export([double/1,grown/1,equal/2,beach/1,create_list/1,sum/1,reverse/1,tail_reverse/1,sublist/2,for/3]).

double(Value) ->
  times(Value, 2).
times(X, Y) ->
  X * Y .

grown(N) when N >= 21, N < 105 -> true;
grown(_) -> false.

equal(X, Y) when X == Y -> thesame;
equal(X, Y) when X > Y -> xbigger;
equal(X, Y) when X < Y -> xsmaller.

beach(Temp) ->
  case Temp of
    {celsius, N} when N >=20, N =< 45 ->
        'favorable';
    {kelvin, N} when N >= 293, N =< 318 ->
        'scientifacally fav';
    {fahrenheit, N} when N >= 68, N =< 113 ->
        'fav in the US';
    _ ->
        'avoid the beach'
  end.

sum(Top) -> sum_acc(1, Top, 0).
sum_acc(Idx, Top, Sum) when Idx =< Top -> sum_acc(Idx + 1, Top, Sum + Idx);
sum_acc(_I, _B, Sum) -> Sum.

create_list(Size) -> create_acc(Size, []).
create_acc(Idx, List) when Idx > 0 -> create_acc(Idx - 1, [Idx | List]);
create_acc(_I, List) -> List.

reverse([]) -> [];
reverse([H|T]) -> reverse(T)++[H].

tail_reverse(List) -> reverse_acc(List, []).
reverse_acc([], Acc) -> Acc;
reverse_acc([ H | T ], Acc) -> reverse_acc(T, [H | Acc]).

sublist(_,0) -> [];
sublist([],_) -> [];
sublist([H|T], N) when n > 0 -> [H|sublist(T,N-1)].

for(Max, Max, F) -> [F(Max)];
for(Idx, Max, F) -> [F(Idx) | for(Idx + 1, Max, F)].

