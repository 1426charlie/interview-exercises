
-module(rec).
-export([sum/2,even/1,odd/1,qsort/1]).

%% created by dpw on 2018-03-16 07:19:44
%% note that erlang optimized direct recursion so tail-recursion may not be more efficient...

sum([], Sum) -> Sum;
sum([Head|Tail], Sum) ->
  sum(Tail, Head + Sum).

even([]) -> [];
even([Head|Tail]) when Head rem 2 == 0 ->
  [Head | even(Tail)];
  even([_ | Tail]) -> even(Tail).

odd([]) -> [];
odd([Head|Tail]) when Head rem 2 == 1 ->
  [Head | odd(Tail)];
  odd([_ | Tail]) -> odd(Tail).

qsort([]) -> [];
qsort([Pivot|Rest]) ->
  qsort([Smaller || Smaller <- Rest, Smaller =< Pivot])
  ++ [Pivot] ++
  qsort([Larger  || Larger  <- Rest, Larger > Pivot]).

