%% created by dpw on 2018-03-15 12:27:14

-module(greetings).
-export([greet/2,talk/1]).

greet(male, Name) ->
  io:format("Hello, Mr. ~s!~n", [Name]);
greet(female, Name) ->
  io:format("Hello, Ms. ~s!~n", [Name]);
greet(_, Name) ->
  io:format("Hello, ~s!~n", [Name]) .

talk(Animal) ->
    Talk = if Animal == cat -> "meow";
              Animal == dog -> "woof";
              Animal == goat -> "baaa";
              Animal == cow -> "mooo";
              Animal == lion -> "ROAR";
              true -> "what?"
            end,
  
    {Animal, "says " ++ Talk ++ "!"} .
