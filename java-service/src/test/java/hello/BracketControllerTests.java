/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hello;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;

import hello.Bracket;
import hello.BracketController;

import java.util.List;
import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BracketControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void balancedBracketPairsShouldReturnOk() throws Exception {
        this.mockMvc.perform(get("/brackets").param("pairs", "{{}}"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("ok"));
    }

    @Test
    public void nonBalancedBracketPairsShouldReturnFailed() throws Exception {
        this.mockMvc.perform(get("/brackets").param("pairs", "{{}}]"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("failed"));
    }

    @Test
    public void oddBracketStreamShouldReturnFailed() throws Exception {
        this.mockMvc.perform(get("/brackets").param("pairs", "{"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("failed"));
    }

    @Test
    public void openCharShouldBeAddedToStack() throws Exception {
        List<Character> stack = new ArrayList<Character>();
        BracketController controller = new BracketController();

        boolean result = controller.evaluateChar('{', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(1);

        result = controller.evaluateChar('[', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(2);

        result = controller.evaluateChar('(', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(3);
    }

    @Test
    public void closeCharShouldBeRemovedFromStack() throws Exception {
        List<Character> stack = new ArrayList<Character>();
        BracketController controller = new BracketController();

        boolean result = controller.evaluateChar('(', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(1);

        result = controller.evaluateChar(')', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(0);

        result = controller.evaluateChar('{', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(1);

        result = controller.evaluateChar('}', stack);
        assertThat(result).isEqualTo(true);
        assertThat(stack.size()).isEqualTo(0);
    }

    @Test
    public void closeCharShouldFailIfStackEmpty() throws Exception {
        List<Character> stack = new ArrayList<Character>();
        BracketController controller = new BracketController();

        boolean result = controller.evaluateChar(')', stack);
        assertThat(result).isEqualTo(false);
        assertThat(stack.size()).isEqualTo(0);
    }

    @Test
    public void badCharShouldFailIfInvalidChar() throws Exception {
        List<Character> stack = new ArrayList<Character>();
        BracketController controller = new BracketController();

        boolean result = controller.evaluateChar('!', stack);
        assertThat(result).isEqualTo(false);
        assertThat(stack.size()).isEqualTo(0);
    }

    @Test
    public void streamOfBalancedCharsShouldReturnTrue() throws Exception {
        String stream = "{{}[[](())]}";
        BracketController controller = new BracketController();

        boolean result = controller.isBalanced(stream);
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void streamOfUnbalancedCharsShouldReturnFalse() throws Exception {
        String stream = "{{}[[](())]}{";
        BracketController controller = new BracketController();

        boolean result = controller.isBalanced(stream);
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void emptyStreamShouldReturnTrue() throws Exception {
        BracketController controller = new BracketController();
        boolean result = controller.isBalanced("");
        assertThat(result).isEqualTo(true);
    }
}
