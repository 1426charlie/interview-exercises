package hello;

public class Bracket {

    public static String OK = "ok";
    public static String FAILED = "failed";

    private final long id;
    private final String content;
    private final String result;

    public Bracket(long id, String content, String result) {
        this.id = id;
        this.content = content;
        this.result = result;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getResult() {
        return result;
    }
}

