package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.ArrayList;

import java.util.Map;
import java.util.HashMap;

@RestController
public class BracketController {
    private final AtomicLong counter = new AtomicLong();

    private static final Logger log = LoggerFactory.getLogger(BracketController.class);

    private final static Map<Character,Character> bracketPairs = new HashMap<Character,Character>();

    static {
        bracketPairs.put('(', ')');
        bracketPairs.put('{', '}');
        bracketPairs.put('[', ']');

        log.info("bracket characters: " + bracketPairs.toString());
    }

    @RequestMapping("/brackets")
    public Bracket balanced(@RequestParam(value="pairs") String bracketStream) {
        log.info("stream: " + bracketStream);
        if (isBalanced(bracketStream)) {
            return new Bracket(counter.incrementAndGet(), bracketStream, Bracket.OK);
        }
        return new Bracket(counter.incrementAndGet(), bracketStream, Bracket.FAILED);
    }

    public boolean isBalanced(String stream) {
        if (stream.length() % 2 != 0) {
            log.info("odd character count: " + stream.length());
            return false;
        }

        List<Character> stack = new ArrayList<Character>();

        for (Character ch : stream.toCharArray()) {
            if (!evaluateChar(ch, stack)) {
                return false;
            }
        }

        log.info("stack size: " + stack.size());
        return stack.size() == 0;
    }

    public boolean evaluateChar(final Character ch, List<Character> stack) {
        // check for openner and push if it is
        if (bracketPairs.containsKey(ch)) {
            stack.add(0, ch);
            return true;
        }

        if (bracketPairs.containsValue(ch)) {
            if (stack.isEmpty()) {
                log.info("stack empty...");
                return false;
            }

            return (ch == bracketPairs.get(stack.remove(0)));
        } else {
            log.info("invalid character: " + ch);
        }

        return false;
    }

}

