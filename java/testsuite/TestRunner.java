package testsuite;

public class TestRunner {
    TestResults results;
    public TestRunner(String name) {
        results = new TestResults(name);
    }

    public void test(boolean b) {
        results.tests++;
        if (b) {
            results.passed++ ;
        } else {
            results.failed++;
        }
    }

    public String toString() {
        return results.toString();
    }
}

