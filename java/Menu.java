/**
 * menu a list of menu items is presented to the user.  the user is given a specific amount of cash.  
 * The objective is for the user to order all combinations of items from the menu that exactly match
 * the amount of cash allotted.
 *
 *
 */

import java.util.*;
import java.io.*;

public class Menu {
    class MenuItem {
        String id;
        String name;
        Double price;
    }

    class Order {
        String id;
        List<OrderItem> items;
        Double cost;
    }

    class OrderItem {
        MenuItem item;
        Integer quantity;
        Double cosst;
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.printf("hello menu...\n");
    }
}
