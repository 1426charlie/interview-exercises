/**
 * Task: Given a string containing a random list of comma separated integers, 
 * implement a set of methods to search the list for one or more target values.  
 *
 * For example, if the list is [ 34, 55, 2, 29, 44 ] the numbers 2 and 29 would
 * return true where the numbers 3 and 55 would return false.  Implement assert
 * tests to prove your implementation.
 *
 * Q&A
 *  *) what is the average Big O time for your implementation?  How could you make it O(log n)
 *  *)
 */

import java.util.*;
import java.io.*;
import java.util.function.*;
import java.util.stream.*;

public class BSortSearcher {

    private Random rand = new Random();

    public Collection<Integer> parse(String input) {
        return Arrays.stream(input.split(","))
            .mapToInt(Integer::valueOf)
            .boxed()
            .collect(Collectors.toList()
        );
    }

    public Collection<Integer> sort(Collection<Integer> numbers) {
        return numbers.stream().sorted().collect(Collectors.toList());
    }

    public boolean exists(Collection<Integer> list, int target) {
        return Arrays.binarySearch(list.toArray(), target) >= 0;
    }

    public String createRandomList(int count) {
        return asString(rand.ints(count, 0, 1000)
            .distinct()
            .collect(ArrayList::new, ArrayList::add, ArrayList::addAll)
        );
    }

    public String asString(Collection<Integer> list) {
        StringJoiner sj = new StringJoiner(",");
        list.stream().forEach(c -> sj.add(c.toString()));
        return sj.toString();
    }

    Function<Collection<Integer>, String> p = (list) -> Arrays.toString(list.toArray());

    public Collection<Integer> reverse(Collection<Integer> list) {
        return list.stream().collect(ArrayDeque::new, ArrayDeque::addFirst, ArrayDeque::addAll);
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public static void main(String[] args) {
        BSortSearcher.runTests();
    }

    public static void runTests() {
        BSortSearcher bss = new BSortSearcher();

        String input = bss.createRandomList(35);

        Collection<Integer> list = bss.parse(input);
        bss.printf("%s -> %s\n", input, bss.asString(list));

        Collection<Integer> sorted = bss.sort(list);
        Collection<Integer> reversed = bss.reverse(sorted);

        bss.printf("input   : %s\n", bss.p.apply(list));
        bss.printf("sorted  : %s\n", bss.p.apply(sorted));
        bss.printf("reversed: %s\n", bss.p.apply(reversed));

        Collection<Integer> tests = Arrays.asList( 32, 23, 77, 87, 88, 90 );
        bss.printf("\nTest this list: %s\n", bss.asString(tests));
        tests.stream().forEach(target -> {
            boolean exists = bss.exists(sorted, target);
            bss.printf("target %d exists=%b\n", target, exists);
        });

        bss.printf("\nShould all be true...\n");
        // all of these should exist in the list
        list.stream().forEach(target -> {
            boolean exists = bss.exists(sorted, target);
            // bss.printf("target %d exists=%b\n", target, exists);
            assert(exists);
        });
    }
}
