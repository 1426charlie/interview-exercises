#!/bin/sh
# darwest@ebay.com <darryl.west>
# 2018-02-25 14:13:58
#

[ $# -eq 0 ] && {
    echo "USE: $0 file"
    exit 1
}

file=$1
target=`echo $file | sed -e 's/.java$//'`

[ -d bin ] || mkdir bin

javac -cp "./" -d bin/ $file && {
    if fgrep 'public static void main' $file > /dev/null
    then
        ( cd bin && java "$target" )
    fi
}

