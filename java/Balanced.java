/**
 * Task: Given a string of characters in the set [,{,(,),},] verify that the open characters [,{,(  match their respective close characters in a 
 * balanced way.  For example {,{,(,),},} is balanced whereas {,{,(,},},) is not.  The ideal implementation would use JDK8 with lambdas, predicates, etc.
 *
 * The implementation below replaces a classic switch/case with a hashmap to map open/close operations.
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Balanced {
    final Map<String,String> charMap = new HashMap<>();

    // fill the open/close character map
    public Balanced() {
        charMap.put("(",")");
        charMap.put("[","]");
        charMap.put("{","}");

        printf("%s\n", charMap.toString());
    }

    Predicate<String> isOpener = ch -> charMap.containsKey(ch);
    Predicate<String> isCloser = ch -> charMap.containsValue(ch);
    BiPredicate<String, String> isBalanced = (closer, opener) -> closer.equals(charMap.get(opener));

    public boolean test(String input) {
        printf("test %s\n", input);
        // must be an even set of chars to be balanced...
        if (input.length() % 2 != 0) {
            printf("ERROR! %s is not an even number of characters...", input);
            return false;
        }

        Stack<String> stack = new Stack<>();

        String[] chars = input.split("");

        for (String str : chars) {
            printf("%s -> ", str);
            if (isOpener.test(str)) {
                stack.push(str);
                printf("pushed, stack: %s\n", stack.toString());
                continue;
            } 
            
            if (isCloser.test(str)) {
                if (!isBalanced.test(str, stack.pop())) {
                    printf("popped, stack: %s\n", stack.toString());
                    return false;
                }
                printf("popped, stack: %s\n", stack.toString());
                continue;
            }

            printf("ERROR! the character %s is not a recognized...", str);
            return false;
        }

        printf("stack %s", stack.toString());
        return stack.size() == 0;
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public static void main(String[] args) {
        Balanced bal = new Balanced();
        String input;

        if (args.length == 0) {
            input = "((){{[]}})";
        } else {
            input = args[0];
        }

        bal.printf("Test %s = %b\n", input, bal.test(input));
    }
}
