/**
 * This is a good example of how to use a hashmap and lambdas to replace a classic switch/case statement. The hashmap
 * is easier to test and expand with new operations.
 *
 * Now, create a cache to memoize all processed operations...
 *
 * Task: Design and implement a Calculator class that supports 4 integer functions: add, sub, mul, and div.  Ideally 
 * you would implement this class with JDK8 using lambdas.  The implementation should include basic tests.
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Calc {
    int testCount = 0;
    int testFailedCount = 0;

    Map<String, IntBinaryOperator> ops = new HashMap<>();

    public Calc() {
        ops.put("add", Integer::sum);
        ops.put("sub", (a, b) -> a - b);
        ops.put("mul", (a, b) -> a * b);
        ops.put("div", (a, b) -> a / b);
    }

    public int calculate(String name, int p1, int p2) throws Exception {
        IntBinaryOperator op = ops.get(name);
        if (op == null) {
            throw new Exception("not a valid operator: " + name);
        }

        return op.applyAsInt(p1, p2);
    }

    public void printf(String format, Object... args) {
        System.out.println(String.format(format, args));
    }

    public void test(boolean ok) {
        testCount++;
        if (!ok) testFailedCount++;
    }

    public static void main(String[] args) throws Exception {
        Calc calc = new Calc();

        int a = 7;
        int b = 3;

        calc.test(calc.calculate("add", a, b) == 10);
        calc.test(calc.calculate("sub", a, b) == 4);
        calc.test(calc.calculate("mul", a, b) == 21);
        calc.test(calc.calculate("div", a, b) == 2);

        calc.printf("%d Tests run, %d failed", calc.testCount, calc.testFailedCount);
        if (calc.testFailedCount == 0) calc.printf("Yippee! :)");
    }
}
