/**
 * Implement a PrintWriter using string writer to enable using as print writer mock
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class MockWriter {

    private PrintWriter printWriter;
    private StringWriter stringWriter;

    public MockWriter() {
        stringWriter = new StringWriter();
        this.printWriter = new PrintWriter(stringWriter);
    }

    public MockWriter(StringWriter stringWriter) {
        this.stringWriter = stringWriter;
        this.printWriter = new PrintWriter(stringWriter);
    }

    public StringBuffer getBuffer() {
        return stringWriter.getBuffer();
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public void close() {
        printWriter.close();
    }

    public static void main(String[] args) {
        MockWriter writer = new MockWriter();
        PrintWriter out = writer.getPrintWriter();

        out.println("this is a mock string writer");

        StringBuffer buf = writer.getBuffer();

        writer.printf("buffer output: %s", buf);
        writer.close();

    }
}
