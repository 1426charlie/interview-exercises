/**
 * Task: implement a custom hashmap by implementing the CustomMap class below.  Include unit
 * tests for each method.  Implement without any additional imports.
 *
 * @created 2018-03-04 10:31:23
 * @author darryl.west
 *
 * Q&A:
 *  *) did you test all methods?  what other testing would you add if you had more time?
 *  *) what would happend if the map grew to 100K elements?  would this affect the put/get times?  What is the Big O for this?  how could it be improved
 *  *) is this implementation thread safe? if not, whot could you do to make it safe? What type of performance hit would you expect
 *  *) what would you modify to make force the access time to worst case O(log(n))
 *  *) if you maintained a sorted set of keys, what would the average Big O time for accessing a value be? What would the worst case Big O time be?
 *
 */

import java.util.Arrays;
import testsuite.*;
import logger.*;

class CustomMap<K, V> {
    Logger log = new Logger("CustomMap", 2);

    class KVPair<K, V> {
        final K key;
        V value;

        KVPair(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private KVPair<K, V>[] map;
    int sz = 0;

    public CustomMap() {
        this(10);
    }

    public CustomMap(int capability) {
        log.debug("create a map with capability set to %d", capability);
        map = new KVPair[capability];
    }

    // return index 0..sz
    private int findKeyIndex(K key) {
        // TODO replace with binary search...
        for (int i = 0; i < sz; i++) {
            if (map[i] != null && map[i].key.equals(key)) {
                log.info("key: %s found at idx: %d", key, i);
                return i;
            }
        }

        log.debug("key: %s not found", key);

        return -1;
    }

    public V get(K key) {
        int idx = findKeyIndex(key);
        if (idx < 0) {
            return null;
        }

        return map[idx].value;
    }

    public void put(K key, V value) {
        int idx = findKeyIndex(key);
        if (idx < 0) {
            if (sz == map.length) {
                int cap = sz * 2;
                map = Arrays.copyOf(map, cap);
                log.info("new capability: %d", cap);
            }

            map[sz] = new KVPair(key, value);
            sz++;
        } else {
            if (map[idx].key.equals(key)) {
                map[idx].value = value;
            }
        }
    }

    public V remove(K key) {
        int idx = findKeyIndex(key);
        if (idx < 0) {
            return null;
        }

        log.info("removed key/value %s from index: %d", key, idx);

        V value = map[idx].value;
        map[idx] = null;

        sz--;
        if (sz != idx) {
            map[idx] = map[sz];
        }

        return value;
    }

    public int size() {
        return sz;
    }

    public Object[] keys() {
        Object[] keys = new Object[sz];

        for (int i = 0; i < sz; i++) {
            keys[i] = map[i].key;
        }

        Arrays.sort(keys);
        return keys;
    }

    public boolean containsKey(K key) {
        return findKeyIndex(key) >= 0;
    }

    public boolean containsValue(V value) {
        for (int i = 0; i < sz; i++) {
            if (map[i] != null && map[i].value.equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * the main runner; instantiates the custom map and runs tests...
     */
    public static void main(String[] args) {
        TestRunner tr = new TestRunner("CustomMap tests");

        CustomMap<String, String> map = new CustomMap<String, String>();

        tr.test(map != null);
        tr.test(map.size() == 0);

        map = new CustomMap<String, String>(5);
        tr.test(map.size() == 0);

        // test put and size
        String dfltKey = "mykey";
        String value = "my value";
        map.put(dfltKey, value);
        int sz = 1;
        tr.test(map.size() == sz);
        String val = map.get("mykey");
        tr.test(val != null);
        if (val != null) {
            tr.test(val.equals("my value"));
        }

        // test null on unkown key...
        tr.test(map.get("some-unknown-key") == null);
        tr.test(map.size() == sz);

        // test adding the same key multiple times with different values
        sz = map.size();
        for (int i = 0; i < 10; i++) {
            value = String.format("my value: %d", i);
            map.put(dfltKey, value);
            tr.test(map.size() == sz);
            val = map.get(dfltKey);
            tr.test(val != null);
            tr.test(val != null && val.equals(value));
        }

        sz = map.size();
        for (int i = 0; i < 25; i++) {
            String key = String.format("test-%d", i+1);
            map.put(key, key);
            sz++;
            tr.test(map.size() == sz);
        }

        // test remove
        sz = map.size();
        value = map.get(dfltKey);
        tr.test(value != null);
        tr.test(map.remove(dfltKey).equals(value));
        tr.test(map.size() == sz - 1);
        tr.test(map.remove(dfltKey) == null);
        tr.test(map.size() == sz - 1);

        // test contains key
        tr.test(map.containsKey(dfltKey) == false);
        value = "my value";
        map.put(dfltKey, value);
        tr.test(map.containsKey(dfltKey) == true);

        // test contains value
        tr.test(map.containsValue("an unkown value thing") == false);
        tr.test(map.containsValue(value) == true);

        // test key set
        Object[] keys = map.keys();
        tr.test(keys.length == map.size());
        System.out.println(Arrays.toString(keys));

        // now test removing all the keys
        sz = map.size();
        for (int i = 0; i < keys.length; i++) {
            String key = (String)keys[i];
            tr.test(map.remove(key) != null);
            sz--;
            tr.test(map.size() == sz);
            tr.test(map.remove(key) == null);
        }

        tr.test(map.size() == 0);

        System.out.println(tr);
    }
}
