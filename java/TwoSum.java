//
// @see https://en.wikipedia.org/wiki/Subset_sum_problem
//
// Task: given a set of integers, design and implement a class to identify sums within the integers that equal a specified 
// target.  For example, given the set [ 5, 3, 6, 2 ]  and a target amount of 8, the there are two sets, [ 5, 3 ], and [ 6, 2 ]. 
//
// Example:
//      List<Integer> inputs = Arrays.asList(3, 5, 2, -4, 8, 11);
//      int target = 7;
//

import java.util.*;
import java.io.*;

class TwoSum {
    boolean debug = true;
    int logid = 100;
    void log(Object obj) {
        if (debug) {
            StringBuilder sb = new StringBuilder();
            sb.append(logid++).append(": ").append(obj);
            printf("%s\n", sb);
        }
    }

    class Pair {
        int n1;
        int n2;

        public Pair(int n1, int n2) {
            this.n1 = n1;
            this.n2 = n2;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(n1).append("+").append(n2).append("=").append(n1+n2);

            return sb.toString();
        }

    }

    public List<Pair> find(final List<Integer> list, final int target) {
        log(new StringBuilder().append("inputs: ").append(list).append(", target: ").append(target));

        List<Pair> sums = new ArrayList<Pair>();
        Set<Integer> set = new HashSet<Integer>();

        for (int value : list) {
            log(value);
            int remainder = target - value;
            log(remainder);

            if (set.contains(remainder)) {
                sums.add(new Pair(value, remainder));
                log(sums);
            }

            set.add(value);
            log(set);
        }

        return sums;
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public static void main(String[] args) {
        TwoSum sum = new TwoSum();
        List<Integer> inputs = Arrays.asList(3, 5, 2, -4, 8, 11);
        int target = 7;
        List sums = sum.find(inputs, target);
        sum.printf("\nSums: %s\n", sums);
    }
}
