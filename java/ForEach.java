/**
 * Design and implement an iterator to operate on a list of objects to do the following:
 *  1) print the entire list, line by line to the stdout
 *  2) print a filtered list based on one or more rules
 *  3) map a list of objects from one type to another
 *  4) apply a reduction method to calculate averages or other aggregates
 *
 * The implementation may not use any for() loops or goto's
 *
 * @created 2018-03-03 05:29:32
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class ForEach {

    interface Rule {
        boolean verify(Object item);
    }

    interface Filterable {
        boolean test(Rule rule);
    }

    interface Mapper {
        Object map(Object obj);
    }

    interface Reducable {
        void reduce(Object item);
        Object getResult();
    }

    interface RunnableFilterItem extends Runnable, Filterable {
    }

    interface IndexedItemList {
        RunnableFilterItem getItem(int idx);
        int getLength();
    }

    private IndexedItemList list;

    ForEach(final IndexedItemList list) {
        this.list = list;
    }

    public RunnableFilterItem[] filter(Rule rule) {
        List<RunnableFilterItem> filtered = new ArrayList<>();
        filterit(0, rule, filtered);
        RunnableFilterItem[] items = new RunnableFilterItem[filtered.size()];
        return filtered.toArray(items);
    }

    protected void filterit(int idx, Rule rule, List<RunnableFilterItem> filtered) {
        RunnableFilterItem item = list.getItem(idx);
        if (rule.verify(item)) {
            filtered.add(item);
        }
        
        if (++idx < list.getLength()) filterit(idx, rule, filtered);
    }

    public Object[] map(Mapper mapper) {
        Object[] mapped = new Object[list.getLength()];
        mapit(0, mapper, mapped);
        return mapped;
    }

    protected void mapit(int idx, Mapper mapper, Object[] mapped) {
        mapped[idx] = mapper.map(list.getItem(idx));
        
        if (++idx < list.getLength()) mapit(idx, mapper, mapped);
    }

    public void iterate() {
        it(0);
    }

    protected void it(int idx) {
        RunnableFilterItem item = list.getItem(idx);
        item.run();

        if (++idx < list.getLength()) it(idx);
    }

    public Object reduce(Reducable reducer) {
        reduceit(0, reducer);
        return reducer.getResult();
    }

    protected void reduceit(int idx, Reducable reducer) {
        Object item = list.getItem(idx);

        reducer.reduce(item);

        if (++idx < list.getLength()) reduceit(idx, reducer);
    }

    public static void main(String[] args) throws Exception {

        class PrintRunner implements Runnable {
            private Object item;
            PrintRunner(Object item) {
                this.item = item;
            }

            public void run() {
                String str = String.format("%s", item);
                System.out.println(str);
            }
        }

        class ListIterator implements IndexedItemList {
            private RunnableFilterItem[] list;
            private Filterable rule;
            private int idx;

            ListIterator(RunnableFilterItem[] list) {
                this.list = list;
            }

            public RunnableFilterItem getItem(int index) {
                return list[index];
            }

            public int getLength() {
                return list.length;
            }
        }

        class Person implements RunnableFilterItem {
            protected String id;
            protected String name;
            protected int age;
            protected boolean female;
            protected Runnable runner;
            protected Reducable reducer;

            private Person() {
            }

            Person(String name, int age, boolean female) {
                this.id = UUID.randomUUID().toString().replace("-", "");
                this.name = name;
                this.age = age;
                this.female = female;
                this.runner = new PrintRunner(this);
                // this.reducer
            }

            public Person copy() {
                Person copy = new Person();

                copy.id = id;
                copy.name = name;
                copy.age = age;
                copy.female = female;
                copy.runner = runner;

                return copy;
            }

            public String toString() {
                String sex = female ? "female" : "male";
                return new StringBuilder()
                    .append(id)
                    .append(" ").append(name)
                    .append(", ").append(sex)
                    .append(", age:").append(age)
                    .toString();
            }

            public void setRunner(Runnable runner) {
                this.runner = runner;
            }

            public void run() {
                runner.run();
            }

            public boolean test(Rule rule) {
                if (rule == null) {
                    return true;
                }

                return rule.verify(this);
            }
        }

        class MinAgeRule implements Rule {
            Integer min;
            public MinAgeRule(int min) {
                this.min = min;
            }

            public boolean verify(Object obj) {
                try {
                    int age = obj.getClass().getDeclaredField("age").getInt(obj);

                    return age  >= min;
                } catch (Exception e) {
                    return false;
                }
            }
        }

        class MaleRule implements Rule {
            public boolean verify(Object obj) {
                try {
                    return obj.getClass().getDeclaredField("female").getBoolean(obj) == false;
                } catch (Exception e) {
                    return false;
                }
            }
        }

        class NameMapper implements Mapper {
            public Object map(Object obj) {
                try {
                    return obj.getClass().getDeclaredField("name").get(obj);
                } catch (Exception e) {
                    return null;
                }
            }
        }

        class AgeMapper implements Mapper {
            public Object map(Object obj) {
                try {
                    return obj.getClass().getDeclaredField("age").getInt(obj);
                } catch (Exception e) {
                    return null;
                }
            }
        }

        class CopyMapper implements Mapper {
            public Object map(Object obj) {
                Person p = (Person)obj;
                return p.copy();
            }
        }

        class AverageAgeReducer implements Reducable {
            int count = 0;
            int total = 0;

            public void reduce(Object item) {
                count++;
                Person p = (Person)item;
                total += p.age;
            }

            public Object getResult() {
                return new Integer(total / count);
            }
        }


        Person[] list = {
            new Person("Tom", 43, false),
            new Person("Sara", 22, true),
            new Person("John", 17, false),
            new Person("Adam", 27, false),
            new Person("Sally", 38, true),
            new Person("Thad", 20, false),
            new Person("Jane", 28, true),
            new Person("Lidia", 23, true)
        };

        ListIterator iterator = new ListIterator(list);
        ForEach forEach = new ForEach(iterator);

        System.out.println("The entire list...");
        forEach.iterate();

        System.out.println("Adult list...");
        ListIterator adults = new ListIterator(forEach.filter(new MinAgeRule(21)));
        new ForEach(adults).iterate();

        System.out.println("Male list...");
        ListIterator males = new ListIterator(forEach.filter(new MaleRule()));
        new ForEach(males).iterate();

        Object[] names = forEach.map(new NameMapper());
        System.out.println(Arrays.toString(names));

        Object[] copies = forEach.map(new CopyMapper());
        System.out.println(Arrays.toString(copies));

        Object[] ages = forEach.map(new AgeMapper());
        System.out.println(Arrays.toString(ages));

        Reducable reducer = new AverageAgeReducer();
        Object result = forEach.reduce(reducer);
        System.out.println("Average Age: " + result);

        result = new ForEach(adults).reduce(new AverageAgeReducer());
        System.out.println("Average Adult Age: " + result);
    }
}
