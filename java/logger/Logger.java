package logger;

import java.io.*;

public class Logger {
    static String[] levels = {"ALL", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"};
    int msgid = 100;
    String category;
    int level;
    PrintStream out;
    
    public Logger(String category) {
        this(category, 2);
    }

    public Logger(String category, int level) {
        this.category = category;
        this.level = level;

        this.out = System.out;
    }

    protected void log(String lvl, String fmt, Object... args) {
        out.print(msgid++);
        out.print(" ");
        out.print(lvl);
        for (int i = lvl.length(); i <= 5; i++) {
            out.print(" ");
        }
        
        out.print(category);
        out.print(": ");
        out.println(String.format(fmt, args));
    }

    public void debug(String fmt, Object... args) {
        if (level <= 1) {
            log(levels[1], fmt, args);
        }
    }

    public void info(String fmt, Object... args) {
        if (level <= 2) {
            log(levels[2], fmt, args);
        }
    }

    public void warn(String fmt, Object... args) {
        if (level <= 3) {
            log(levels[3], fmt, args);
        }
    }

    public void error(String fmt, Object... args) {
        if (level <= 4) {
            log(levels[4], fmt, args);
        }
    }

    public void fatal(String fmt, Object... args) {
        if (level <= 5) {
            log(levels[4], fmt, args);
        }
    }
}
