/**
 * Task: You are given a collection of ABC blocks.  There are twenty blocks each with two letters. A complete alphabet is gauranteed 
 * amongst all sides of the blocks.  Write a function that takes a string (word) and determines whether the word can be spelled with 
 * the given collection of blocks. Once a letter on a block is used, that block cannot be used again.
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class ABCBlocks {

    String findBlock(String ch, List<String> blocks) {
        for (String block : blocks) {
            if (block.contains(ch)) {
                return block;
            }
        }

        return null;
    }

    boolean canMakeWord(String word, List<String> blocks) {
        if (word == null || word == "") {
            return true;
        }

        List<String> list = new ArrayList<String>(blocks);

        String[] ss = word.toUpperCase().split("");

        for (int i = 0; i < word.length(); i++) {
            String block = findBlock(ss[i], list);
            if (block == null) {
                return false;
            }
            list.remove(block);
        }

        return true;
    }

    public static void main(String[] args) {
        final List<String> blocks = Arrays.asList("BO", "XK", "DQ", "CP", "NA", "GT", "RE", "TG", "QD", "FS","JW", "HU", "VI", "AN", "OB", "ER", "FS", "LY", "PC", "ZM");
        ABCBlocks abc = new ABCBlocks();

        final Map<String,Boolean> words = new HashMap<String, Boolean>() {{
            put("A", true);
            put("kix", false);
            put("kiss", true);
            put("BARK", true);
            put("babble", false);
            put("BOOK", false);
            put("TREAT", true);
            put("COMMON", false);
            put("CANCEL", true);
            put("SQUAD", true);
            put("CONFUSE", true);
        }};

        // System.out.println(blocks);
        // System.out.println(words);

        int errors = 0;
        for (Map.Entry<String, Boolean> entry : words.entrySet()) {
            String word = entry.getKey();
            Boolean expect = entry.getValue();
            Boolean actual = abc.canMakeWord(word, blocks);

            System.out.printf("%s : %s %s\n", word, actual, expect == actual ? "" : "-> fail...");

            if (actual != expect) {
                errors++;
            }
        }

        System.out.printf("%s: errors: %d\n", errors == 0 ? "Passed" : "Failed", errors);
    }
}
