/**
 * Task: implement the tree functions to add, find and iterator.  Begin by implementing add verify that
 * all unit tests pass.
 *
 * @created 2018-03-12 08:45:10
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;
import logger.*;
import testsuite.*;

class TreeFunc {
    Logger log = new Logger("TreeFunc", 2);

    class Node {
        Node left, right;
        int value;
        Node(int value) {
            this.value = value;
        }
    }

    interface Func {
        void run(int value);
    }

    int sz;
    Node root;

    void traverse(Node node, Func fn) {
        if (node != null) {
            traverse(node.left, fn);
            fn.run(node.value);
            traverse(node.right, fn);
        }
    }

    void reverse(Node node, Func fn) {
        if (node != null) {
            reverse(node.right, fn);
            fn.run(node.value);
            reverse(node.left, fn);
        }
    }

    Node add(Node node, int value) {
        if (node == null) {
            log.info("add new node: %d", value);
            node = new Node(value);
            sz++;
            if (root == null) {
                log.info("set root: %d", value);
                root = node;
            }
        } else {
            if (value < node.value) {
                node.left = add(node.left, value);
            } else {
                node.right = add(node.right, value);
            }
        }

        return node;
    }

    Node find(Node node, int target) {
        if (node == null) {
            log.info("target %d not found...", target);
            return null;
        }

        if (node.value == target) {
            log.info("target %d found", target);
            return node;
        }

        if (target < node.value) {
            return find(node.left, target);
        } else {
            return find(node.right, target);
        }
    }

    void balance() {
    }

    Node remove(Node node, int target) {
        return null;
    }

    int size() {
        return sz;
    }

    public static void main(String[] args) {
        TestRunner tst = new TestRunner("CustomMap tests");
        TreeFunc tree = new TreeFunc();
        tst.test(tree.size() == 0);
        tst.test(tree.root == null);

        Node root = tree.add(null, 53);

        tst.test(tree.size() == 1);
        tst.test(tree.root != null);

        int[] list = {58, 23, 40, 66, 10, 90, 32, 23, 77, 87, 88, 9};
        System.out.println("traverse the list: " + Arrays.toString(list));

        int sz = tree.size();
        for (int i = 0; i < list.length; i++) {
            tree.add(root, list[i]);
            sz++;
            tst.test(tree.size() == sz);
        }

        List<Integer> values = new ArrayList<>();
        Func collector = new Func() {
            public void run(int value) {
                values.add(value);
            }
        };

        tree.traverse(tree.root, collector);
        tree.log.info("%s", values);
        // check in order
        for (int i = 0; i < values.size()- 1; i++) {
            tst.test(values.get(i) <= values.get(i+1));
        }

        values.clear();
        tree.reverse(tree.root, collector);
        tree.log.info("%s", values);
        // check reverse order
        for (int i = 0; i < values.size() - 1; i++) {
            tst.test(values.get(i) >= values.get(i+1));
        }

        for (int i = 0; i < list.length; i++) {
            int target = list[i];
            Node node = tree.find(tree.root, target);
            tst.test(node != null && node.value == target);
        }

        Node node = tree.find(tree.root, 543);
        tst.test(node == null);

        System.out.println(tst);
    }
}
