/**
 * Design and implement a Calculator class that supports 4 integer functions: add, sub, mul, and div.  
 * Ideally you would implement this class with JDK8 using lambdas.  The implementation should include basic tests.
 */

import java.util.*;

public class OOCalc {
    public Map<String, Operator> ops = new HashMap<>();

    public abstract class Operator {
        abstract int calculate(int left, int right);
    }

    class AddOp extends Operator {
        int calculate(int left, int right){
            return left + right;
        }
    }

    class SubOp extends Operator {
        int calculate(int left, int right){
            return left - right;
        }
    }

    class MulOp extends Operator {
        int calculate(int left, int right){
            return left * right;
        }
    }

    class DivOp extends Operator {
        int calculate(int left, int right){
            return left / right;
        }
    }

    public OOCalc() {
        ops.put("add", new AddOp());
        ops.put("sub", new SubOp());
        ops.put("mul", new MulOp());
        ops.put("div", new DivOp());
    }

    public int calculate(String opname, int left, int right) {
        Operator op = ops.get(opname);

        if (op == null) {
            throw new RuntimeException(opname + " is not a valid operator...");
        }

        return op.calculate(left, right);
    }

    public void printf(String format, Object... args) {
        System.out.print(String.format(format, args));
    }

    public final static void main(String[] args) {
        OOCalc calc = new OOCalc();

        assert(calc.calculate("add", 4, 2) == 6);
        assert(calc.calculate("sub", 4, 2) == 2);
        assert(calc.calculate("mul", 4, 2) == 8);
        assert(calc.calculate("div", 4, 2) == 2);
    }
}
