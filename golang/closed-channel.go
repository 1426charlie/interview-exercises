package main

import "fmt"

// ok, it's open for now, but what about right after the call?  this isn't too useful
func IsClosed(ch <- chan int) bool {
    select {
    case <-ch:
        return true
    default:
    }

    return false
}

func SafeSend(ch chan int, value int) (closed bool) {
    defer func() {
        if recover() != nil {
            closed = true
        }
    }()

    ch <- value
    return false
}

func main() {
    c1 := make(chan int)
    fmt.Println(IsClosed(c1))
    close(c1)
    fmt.Println(IsClosed(c1))


    /*
    c2 := make(chan int)
    fmt.Println(SafeSend(c2, 45))
    close(c2)
    fmt.Println(SafeSend(c2, 40))
    */
}

