package main

import (
    "fmt"
    "math"
)

// TODO: implement me...

type Point struct {
    x, y float64
    length float64
}

func smallestCircle(input [][]int) []int {
    points := []Point{}
    smallest := []int{}

    for i := 0; i < len(input); i++ {
        x, y := float64(input[i][0]), float64(input[i][1])
        p := Point{ x, y, math.Hypot(x, y) };

        points = append(points, p);
    }

    fmt.Println(points)

    return smallest
}

func main() {
    for i := 0; i < len(testdata); i++ {
        smallestCircle(testdata[i].points)
    }
}

// tests...
type TestData struct {
    points [][]int
    result []int
}

var testdata []TestData = []TestData{
    TestData{
        [][]int{
            []int{ -2,  0 },
            []int{  5, -2 },
            []int{  0, -5 },
            []int{ -4,  2 },
            []int{ -5, -1 },
        },
        []int{ 1, 3, 4 },
    },
    TestData{
        [][]int{
            []int{  5, -5 },
            []int{ -1, -2 },
            []int{ -3,  0 },
            []int{  1,  2 },
            []int{  0, -4 },
            []int{  0, -9 },
            []int{  0, -4 },
            []int{  0,  5 },
            []int{  0, -1 },
            []int{  1,  2 },
        },
        []int{ 5, 7 },
    },
    TestData{
        [][]int{
            []int{ -4, -3 },
            []int{ -1,  2 },
            []int{  1,  5 },
            []int{ -3,  0 },
            []int{ -8, -4 },
            []int{  0,  5 },
            []int{  0, -6 },
            []int{  2, -3 },
            []int{  6,  2 },
            []int{ -3, -2 },
        },
        []int{ 4, 8 },
    },
    TestData{
        [][]int{
            []int{  4,  6 },
            []int{  2,  7 },
            []int{  6, -6 },
            []int{ -2, -3 },
            []int{  0, -1 },
            []int{  4, -3 },
            []int{  0, -4 },
            []int{ -4, -4 },
            []int{ -2, -1 },
            []int{  4,  4 },
            []int{  2,  2 },
            []int{  2, -1 },
            []int{  0, -2 },
            []int{  0, -6 },
            []int{  2, -9 },
            []int{  3, -5 },
            []int{  5,  2 },
            []int{ -6, -3 },
            []int{  0, -5 },
        },
        []int{ 1, 14, 17 },
    },
}

