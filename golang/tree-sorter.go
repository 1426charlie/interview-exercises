package main

// TODO turn this into a package/struct
// TODO ability to balance
// TODO ability to remove a node

import (
	"fmt"
)

type Tree struct {
	value       int
	left, right *Tree
}

func Create(values []int) *Tree {
	var root *Tree
	for _, v := range values {
		root = add(root, v)
	}

	return root
}

func Traverse(t *Tree) []int {
	return traverse([]int{}, t)
}

func traverse(values []int, t *Tree) []int {
	if t != nil {
		values = traverse(values, t.left)
		values = append(values, t.value)
		values = traverse(values, t.right)
	}

	return values
}

func Reverse(t *Tree) []int {
	return reverse([]int{}, t)
}

func reverse(values []int, t *Tree) []int {
	if t != nil {
		values = reverse(values, t.right)
		values = append(values, t.value)
		values = reverse(values, t.left)
	}

	return values
}

func add(t *Tree, value int) *Tree {
	if t == nil {
		t = new(Tree)
		t.value = value
		return t
	}
	if value < t.value {
		t.left = add(t.left, value)
	} else {
		t.right = add(t.right, value)
	}

	return t
}

func main() {
	list := []int{5, 23, 4, 66, 1, 9}
	root := Create(list)

	fmt.Println(list)
	add(root, 60)

	sorted := Traverse(root)
	fmt.Println(sorted)

	reversed := Reverse(root)
	fmt.Println(reversed)
}
